

import biseps as b
import numpy as np
import matplotlib.pyplot as plt
import numpy.random as ra
import scipy as sci


x=b.readFile()
y=b.readFile()

x.appMag5('mag.all.targAll.1')
y.readKicMag('kic.targAll.mag.2',sep=",")

yInd=(y.appMag.jj>0.0)&(y.appMag.jh>0.0)&(y.appMag.jk>0.0)&(y.appMag.g>0.0)&(y.appMag.r>0.0)&(y.appMag.i>0.0)&(y.appMag.z>0.0)&(y.appMag.d51>0.0)


g=np.copy(x.appMag.g)
r=np.copy(x.appMag.r)
i=np.copy(x.appMag.i)
z=np.copy(x.appMag.z)
d51=np.copy(x.appMag.d51)

#ik1=((i+0.0583)-0.0696*(r+0.0383))/(0.9304+(0.0696*0.0548))

#rk1=((r+0.0383)+0.0548*ik1)/1.0548

#gk1=((g+0.0985)+(0.0921*rk1))/1.0921

#zk1=((z+0.0597)-(0.1587*ik1))/0.84132
#d51k1=((d51+0.0597)-(0.1587*ik1))/0.84132

#gk1=gk1+0.04
#rk1=rk1+0.028
#ik1=ik1+0.045
#zk1=zk1+0.042
#d51k1=d51k1+0.042

gk1=g
rk1=r
ik1=i


ind1=(g-r < 0.3)
ind2=(g-r >= 0.3)&(g-r<1.2)
ind3=(g-r>1.2)

yInd1=yInd&(y.appMag.g-y.appMag.r < 0.3)
yInd2=yInd&(y.appMag.g-y.appMag.r >= 0.3)&(y.appMag.g-y.appMag.r <1.2)
yInd3=yInd&(y.appMag.g-y.appMag.r >=1.2)

bins=300
rng=[[-2.0,2.0],[-2.0,2.0]]

histRef1,t,t=np.histogram2d(y.appMag.g[yInd1]-y.appMag.r[yInd1],y.appMag.r[yInd1]-y.appMag.i[yInd1],bins=bins,range=rng,normed=True)
histRef2,t,t=np.histogram2d(y.appMag.g[yInd2]-y.appMag.r[yInd2],y.appMag.r[yInd2]-y.appMag.i[yInd2],bins=bins,range=rng,normed=True)
histRef3,t,t=np.histogram2d(y.appMag.g[yInd3]-y.appMag.r[yInd3],y.appMag.r[yInd3]-y.appMag.i[yInd3],bins=bins,range=rng,normed=True)

def pinnCorrect(g,r,i):
	gg=0.0921
	gi=0.0985
	rg=0.0548
	ri=0.0383
	ig=0.0696
	ii=0.0583
	
	ik=((i+ii)*(1.0+rg)-ig*(r+ri))/(1.0-ig+rg)
	rk=(r+ri+ik*rg)/(1.0+rg)
	gk=(g+gi+rk*gg)/(1.0+gg)

	return gk,rk,ik

#def alter1(ind,XX,histRef):

	#gk2=np.copy(gk1)
	#rk2=np.copy(rk1)
	#ik2=np.copy(ik1)
	##zk2=np.copy(zk1)
	#shap=np.count_nonzero(ind)


	#gk2[ind]=-2.5*np.log10(ra.lognormal(gk1[ind],XX,shap))
	#rk2[ind]=-2.5*np.log10(ra.lognormal(rk1[ind],XX,shap))
	#ik2[ind]=-2.5*np.log10(ra.lognormal(ik1[ind],XX,shap))
	
	##gk2[ind]=-2.5*np.log10(((ranG)/(XX*10**(gk1[ind]/(-2.5))))+10**(gk1[ind]/(-2.5)))
	##rk2[ind]=-2.5*np.log10(((ranR)/(XX*10**(rk1[ind]/(-2.5))))+10**(rk1[ind]/(-2.5)))
	##ik2[ind]=-2.5*np.log10(((ranI)/(XX*10**(ik1[ind]/(-2.5))))+10**(ik1[ind]/(-2.5)))

	#gk2[ind],rk2[ind],ik2[ind]=pinnCorrect(gk2[ind],rk2[ind],ik2[ind])

	#hist1,xedges,yedges = np.histogram2d(gk2[ind]-rk2[ind],rk2[ind]-ik2[ind],bins=bins,range=rng,normed=True)


	#res11=np.sum(((hist1[histRef>0]-histRef[histRef>0])**2)/np.sqrt(histRef[histRef>0]))

	#return res11


#minI=1.0
#maxI=3.0
#num=250
#step=(maxI-minI*1.0)/num*1.0
#r1=np.zeros(num)
#r2=np.zeros(num)
#r3=np.zeros(num)
#grMult=np.zeros(num)

#for i in range(num):
	##grMult[i]=10**(minI+(i*step))
	#grMult[i]=minI+(i*step)
	##r1[i]=alter1(ind1,grMult[i],histRef1)
	##r2[i]=alter1(ind2,grMult[i],histRef2)
	#r3[i]=alter1(ind3,grMult[i],histRef3)


##plt.figure(1)
##plt.plot(grMult,r1)
##p1=np.poly1d(np.polyfit(grMult,r1,5))
##plt.plot(grMult,p1(grMult))
##plt.title('1')

##plt.figure(2)
##plt.plot(grMult,r2)
##p2=np.poly1d(np.polyfit(grMult,r2,5))
##plt.plot(grMult,p2(grMult))
###plt.legend(loc=0)
##plt.title('2')

#plt.figure(3)
#plt.plot(grMult,r3)
#p3=np.poly1d(np.polyfit(grMult,r3,5))
#plt.plot(grMult,p3(grMult))
#plt.legend(loc=0)
#plt.title('3')


#plt.show()


##print grMult[np.argmin(p1(grMult))]
##print grMult[np.argmin(p2(grMult))]
#print grMult[np.argmin(p3(grMult))]

