#USes the data in the target.*.c files to plot ssystem properties

import biseps as b
import numpy as np
import scipy as sci
import matplotlib.pyplot as plt
import matplotlib as m
from matplotlib import rc
import getopt
import sys as sys


opts, extra = getopt.getopt(sys.argv[1:],'') 
rc('text', usetex=True)
rc('font', family='serif')

#Assume we have catted all the extracts and target files together
x=b.readFile()
e=b.extras()
#.kep

masslabel=r'Mass $[M_{\odot}]$'
radlabel=r'$\log_{10} R [R_{\odot}]$'
tefflabel=r'$T_{eff} [k]$'
logglabel=r'$\log g$'
appMaglabel=r'$m_{kp}$'
absmaglabel=r'$M_{kp}$'
latlabel=r'Galactic Latitude, b [degrees]'
longlabel=r'Galactic Longitude, l [degrees]'
dislabel=r'$\log_{10}$ Distance [pc]'
periodlabel=r'$\log_{10}$ Period [days]'
semilabel=r'$\log_{10}$ a [$R_{\odot}$]'

massRlabel=r'$\frac{M_{2}}{M_{1}}$'
teffRlabel=r'$\frac{T_{2}}{T_{1}}$'
radRlabel=r'$\frac{R_{2}}{R_{1}}$'
loggRlabel=r'$\frac{\log g_{2}}{\log g_{1}}$'


def twoHist(y1,y2,l1,l2,title,xtitle,savefig,xmin=None,xmax=None,numBins=50):
	plt.figure()
	if xmin == None:
		xmin = sci.floor(sci.minimum(y1.min(), y2.min()))
	if xmax == None:
		xmax = sci.ceil(sci.maximum(y1.max(), y2.max()))
	plt.hist(y1,bins=numBins,range=[xmin,xmax],alpha=0.5,label=l1,normed=1)
	plt.hist(y2,bins=numBins,range=[xmin,xmax],alpha=0.5,label=l2,normed=1)
	plt.legend(loc=0)
	plt.xlabel(xtitle)
	plt.title(title)
	plt.savefig(savefig)
	
def multiHist(y,labels,title,xtitle,savefig,xmin=None,xmax=None,numBins=50):
	plt.figure()
	if xmin == None:
		xmin=10**10
		for i in y:
			xmin=sci.floor(sci.minimum(xmin, y[i].min()))
	if xmax == None:
		xmax=0.0
		for i in y:
			xmax=sci.ceil(sci.maximum(xmax, y[i].max()))

	for j in y:
		plt.hist(y[j],label=labels[j],bins=numBins,range=[xmin,xmax],alpha=1.0/i,normed=1)
	
	plt.legend(loc=0)
	plt.xlabel(xtitle)
	plt.title(title)
	plt.savefig(savefig)
	
#x.ranLoc('ranLoc.all')
#fig=plt.figure(4)
#fig.suptitle('Kepler magnitude distribution')
#plt.hist(x.ranLoc.appMag,bins=36,range=[8.0,17.0])
#plt.xlabel(appMaglabel)
#plt.savefig('magAll.png')

#x.kepler3('extract.bin')
#x.ranLoc('ranLoc.bin')
#x.kicProp('kic.bin')
#label1=r'Star 1'
#label2=r'Star 2'
#label3=r'KIC'
#multiHist([x.kep.t1,x.kep.t2,x.kicProp.teff],[label1,label2,label3],'Binary star temperature distribution',tefflabel,'teff3.'+sysType+'.png',xmax=10000.0)

#Plot mass,radius,teff,mag,lat/long,dis,logg
#Binaries plot m1/m2,t1/t2,r1/r2,mag1/mag2,logg1/logg2,lat/long,dis,period,semi major axis
sysType=extra[0]
if sysType=='sing':
	x.kepler3('extract.sing.targ.1')
	#.ranLoc
	x.ranLoc('ranLoc.sing.targ.1')
	plt.figure(0)
	plt.hist(x.kep.m1,bins=50,normed=1,range=[0.0,8.0])
	plt.xlabel(masslabel)
	plt.title('Single star mass distribution (truncated)')
	plt.savefig('mass.'+sysType+'.png')
	
	fig=plt.figure(1)
	plt.hist(x.kep.t1,bins=50,normed=1,range=[np.min(x.kep.t1),20000.0])
	plt.xlabel(tefflabel)
	plt.title('Single star temperature distribution (truncated)')
	plt.savefig('teff.'+sysType+'.png')
	
	
	fig=plt.figure(2)
	plt.hist(np.log10(x.kep.r1),bins=50,normed=1)
	plt.xlabel(radlabel)
	plt.title('Single star radius distribution')
	plt.savefig('radius.'+sysType+'.png')
	
	fig=plt.figure(3)
	logg=e.massrad2logg(x.kep.r1,x.kep.m1)
	plt.hist(logg,bins=50,normed=1)
	plt.xlabel(logglabel)
	plt.title(r'Single star $\log g$ distribution')
	plt.savefig('logg.'+sysType+'.png')
	
	fig=plt.figure(4)
	fig.suptitle('Single star Kepler magnitude distribution')
	plt.subplot(1,2,1)
	plt.hist(x.ranLoc.appMag,bins=50,normed=1)
	plt.xlabel(appMaglabel)
	plt.subplot(1,2,2)
	plt.hist(x.ranLoc.mag,bins=50,normed=1)
	plt.xlabel(absmaglabel)
	plt.savefig('mag.'+sysType+'.png')
	
	#fig=plt.figure(5)
	#fig.suptitle('Single star spatial distribution')
	#plt.subplot(1,2,1)
	#plt.hist(x.ranLoc.long,bins=10,normed=1)
	#plt.xlabel(longlabel)
	#plt.subplot(1,2,2)
	#plt.hist(x.ranLoc.lat,bins=10,normed=1)
	#plt.xlabel(latlabel)
	#plt.savefig('loc.'+sysType+'.png')
	
	#fig=plt.figure(6)
	#plt.hist(np.log10(x.ranLoc.dist),bins=50,normed=1)
	#plt.xlabel(dislabel)
	#plt.title('Single star distance distribution')
	#plt.savefig('dist.'+sysType+'.png')
	
if sysType=='bin':
	x.kepler3('extract.bin')
	#.ranLoc
	x.ranLoc('ranLoc.bin')
	label1=r'Star 1'
	label2=r'Star 2'

	#twoHist(x.kep.m1,x.kep.m2,label1,label2,'Binary star mass distribution',masslabel,'mass.'+sysType+'.png')
	
	#twoHist(x.kep.m1,x.kep.m2,label1,label2,'Binary star mass distribution (truncated)',masslabel,'mass2.'+sysType+'.png',xmax=6.0)

	#twoHist(x.kep.t1,x.kep.t2,label1,label2,'Binary star temperature distribution',tefflabel,'teff.'+sysType+'.png')

	#twoHist(x.kep.t1,x.kep.t2,label1,label2,'Binary star temperature distribution (truncated)',tefflabel,'teff2.'+sysType+'.png',xmax=10000.0)

	#twoHist(np.log10(x.kep.r1),np.log10(x.kep.r2),label1,label2,'Binary star radius distribution',radlabel,'radius.'+sysType+'.png')

	#logg1=e.massrad2logg(x.kep.r1,x.kep.m1)
	#logg2=e.massrad2logg(x.kep.r2,x.kep.m2)
	#twoHist(logg1,logg2,label1,label2,r'Binary star $\log g$ distribution',logglabel,'logg.'+sysType+'.png')

	#fig=plt.figure(4)
	#fig.suptitle('Binary star Kepler magnitude distribution')
	#plt.subplot(1,2,1)
	#plt.hist(x.ranLoc.appMag,bins=50,normed=1)
	#plt.xlabel(appMaglabel)
	#plt.subplot(1,2,2)
	#plt.hist(x.ranLoc.mag,bins=50,normed=1)
	#plt.xlabel(absmaglabel)
	#plt.savefig('mag.'+sysType+'.png')
	
	#fig=plt.figure(5)
	#fig.suptitle('Binary star spatial distribution')
	#plt.subplot(1,2,1)
	#plt.hist(x.ranLoc.long,bins=10,normed=1)
	#plt.xlabel(longlabel)
	#plt.subplot(1,2,2)
	#plt.hist(x.ranLoc.lat,bins=10,normed=1)
	#plt.xlabel(latlabel)
	#plt.savefig('loc.'+sysType+'.png')
	
	#fig=plt.figure(6)
	#plt.hist(np.log10(x.ranLoc.dist),bins=100,normed=1)
	#plt.xlabel(dislabel)
	#plt.title('Binary star disatnce distribution')
	#plt.savefig('dist.'+sysType+'.png')
	
	fig=plt.figure(7)
	plt.hist(np.log10(x.kep.p),bins=50)
	plt.xlabel(periodlabel)
	plt.title('Binary star orbital period distribution')
	#plt.savefig('period.'+sysType+'.png')
	plt.show()

	fig=plt.figure(8)
	semi=e.porb2a(x.kep.m1,x.kep.m2,x.kep.p)
	plt.hist(np.log10(semi),bins=100)
	plt.xlabel(semilabel)
	plt.title('Binary star semi-major axis distribution')
	#plt.savefig('semimajor.'+sysType+'.png')
	plt.show()


	#fig=plt.figure(14)
	#plt.hist(np.minimum(x.kep.m2,x.kep.m1)/np.maximum(x.kep.m2,x.kep.m1),bins=50,normed=1)
	#plt.xlabel(massRlabel)
	#plt.title('Binary star mass ratio distribution ')
	#plt.savefig('massRatio.'+sysType+'.png')

	
	#fig=plt.figure(16)
	#plt.hist(np.minimum(x.kep.r2,x.kep.r1)/np.maximum(x.kep.r2,x.kep.r1),bins=50,normed=1)
	#plt.xlabel(radRlabel)
	#plt.title('Binary star radius ratio distribution ')
	#plt.savefig('radiusRatio.'+sysType+'.png')
	
	#fig=plt.figure(17)
	#logg1=e.massrad2logg(x.kep.r1,x.kep.m1)
	#logg2=e.massrad2logg(x.kep.r2,x.kep.m2)
	#plt.hist(np.minimum(logg1,logg2)/np.maximum(logg1,logg2),bins=50,normed=1)
	#plt.xlabel(loggRlabel)
	#plt.title(r'Binary star $\log g$ ratio distribution ')
	#plt.savefig('loggRatio.'+sysType+'.png')

	#fig=plt.figure(20)
	#plt.hist(np.minimum(x.kep.t2,x.kep.t1)/np.maximum(x.kep.t2,x.kep.t1),bins=50,normed=1)
	#plt.xlabel(teffRlabel)
	#plt.title('Binary star temperature ratio distribution ')
	#plt.savefig('teffRatio.	two'+sysType+'.png')
