import numpy as np
import biseps as b
import matplotlib.pyplot as plt
import matplotlib as m

def plotHist(x1,x2,xlabel,figNum):
	plt.figure(figNum)

	bins=50
	minX=np.minimum(x1.min(),x2.min())
	maxX=np.maximum(x1.max(),x2.max())
	
	plt.hist(x1,bins=50,range=[minX,maxX],label='Us',alpha=0.5,normed=True)
	plt.hist(x2,bins=50,range=[minX,maxX],label='Quart 2',alpha=0.5,normed=True)
	plt.legend(loc=0)
	plt.xlabel(xlabel)
	#plt.show()


def plotScatt(x1,y1,x2,y2,figNum,xlabel,ylabel):
	plt.figure(figNum)
	minX=np.minimum(x1.min(),x2.min())
	maxX=np.maximum(x1.max(),x2.max())
	minY=np.minimum(y1.min(),y2.min())
	maxY=np.maximum(y1.max(),y2.max())

	H1, xedges, yedges = np.histogram2d(x1, y1, bins=(250,250),range=[[minX,maxX],[minY,maxY]])
	H2, xedges, yedges = np.histogram2d(x2, y2, bins=(200,200),range=[[minX,maxX],[minY,maxY]])
	#norm=m.colors.Normalize(vmin=1,vmax=np.maximum(H1.max(),H2.max()))
	extent = [yedges[0], yedges[-1], xedges[-1], xedges[0]]

	ax1=plt.subplot(121)
	plt.imshow(H1, extent=extent, interpolation='none',origin='lower',vmin=1,vmax=np.maximum(H1.max(),H2.max()),cmap='jet_r')
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.colorbar()
	#cb=m.colorbar.ColorbarBase(ax1,cmap='jet_r',norm=norm,orientation='vertical',format='%5.2E')


	ax2=plt.subplot(122,sharex=ax1,sharey=ax1)
	plt.imshow(H2, extent=extent, interpolation='none',origin='lower',vmin=1,vmax=np.maximum(H1.max(),H2.max()),cmap='jet_r')
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.colorbar()




x=b.readFile()
y=b.readFile()

x.appMag5('mag.all.dat.1')

y.appMag3('../quart.mag.2')

indY=(y.appMag.g>0)&(y.appMag.r>0)&(y.appMag.i>0)&(y.appMag.z>0)&(y.appMag.d51>0)&(y.appMag.jj>0)&(y.appMag.jk>0)&(y.appMag.jh>0)&(y.appMag.kp>0)

magX=[x.appMag.g,x.appMag.r,x.appMag.i,x.appMag.z,x.appMag.d51,x.appMag.jj,x.appMag.jk,x.appMag.jh,x.appMag.kp]
magY=[y.appMag[indY].g,y.appMag[indY].r,y.appMag[indY].i,y.appMag[indY].z,y.appMag[indY].d51,y.appMag[indY].jj,y.appMag[indY].jk,y.appMag[indY].jh,y.appMag[indY].kp]
label=['g','r','i','z','d51','jj','jk','jh','kp']

#for i in range(len(magX)):
	#plotHist(magX[i],magY[i],label[i],i)
	
#i=0
#plotScatt(magX[i]-magX[i+1],magX[i+1]-magX[i+2],magY[i]-magY[i+1],magY[i+1]-magY[i+2],i,label[i]+'-'+label[i+1],label[i+1]+'-'+label[i+2])
#plt.show()

for i in range(7):
	plotScatt(magX[i]-magX[i+1],magX[i+1]-magX[i+2],magY[i]-magY[i+1],magY[i+1]-magY[i+2],i,label[i]+'-'+label[i+1],label[i+1]+'-'+label[i+2])
plt.show()
