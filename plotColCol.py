import biseps as b
import numpy as np
import matplotlib.pyplot as plt


x=b.readFile()
y=b.readFile()
z=b.readFile()

x.appMag5('mag.dat.idl.all.5')
y.readKicMag('kic.targAll.mag.2',sep=",")
#y.appMag5('mag.all.idl.1')

yInd=(y.appMag.jj>0.0)&(y.appMag.jh>0.0)&(y.appMag.jk>0.0)&(y.appMag.g>0.0)&(y.appMag.r>0.0)&(y.appMag.i>0.0)&(y.appMag.z>0.0)&(y.appMag.d51>0.0)


def plotIm(x1,y1,x2,y2,xlabel,ylabel,minX,minY,maxX,maxY):
	gr=b.plot()
	plt.figure()

	#minX=np.minimum(x1.min(),x2.min())
	#maxX=np.maximum(x1.max(),x2.max())
	#minY=np.minimum(y1.min(),y2.min())
	#maxY=np.maximum(y1.max(),y2.max())

	ax1=plt.subplot(121)
	hist,xedges,yedges = np.histogram2d(x1,y1,bins=250,range=[[minX,maxX],[minY,maxY]],normed=True)
	extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
	plt.imshow(np.log10(hist.T),extent=extent,interpolation='nearest',origin='lower',cmap='binary')
	plt.title(r'Us pre target')
	plt.xlim(minX,maxX)
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	#gr.plotGRContours()
	plt.colorbar()

	
	plt.subplot(122,sharex=ax1,sharey=ax1)
	hist,xedges,yedges = np.histogram2d(x2,y2,bins=250,range=[[minX,maxX],[minY,maxY]],normed=True)
	extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
	plt.imshow(np.log10(hist.T),extent=extent,interpolation='nearest',origin='lower',cmap='binary')
	plt.colorbar()
	plt.xlim(minX,maxX)
	#plt.xlim(maxX,minX)
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	#gr.plotGRContours()
	plt.title(r'$KIC$')
	


label=['g','r','i','z']

fig1=[x.appMag.g,x.appMag.r,x.appMag.i,x.appMag.z]
fig2=[y.appMag[yInd].g,y.appMag[yInd].r,y.appMag[yInd].i,y.appMag[yInd].z]
#fig3=[z.appMag.g,z.appMag.r,z.appMag.i,z.appMag.z]

i=0
plotIm(fig1[i]-fig1[i+1],fig1[i+1]-fig1[i+2],fig2[i]-fig2[i+1],fig2[i+1]-fig2[i+2],
label[i]+' - '+label[i+1],label[i+1]+' - '+label[i+2],
np.min(fig2[i]-fig2[i+1]),np.min(fig2[i+1]-fig2[i+2]),np.max(fig2[i]-fig2[i+1]),np.max(fig2[i+1]-fig2[i+2]))

plt.show()

#plotIm(fig1[i]-fig1[i+1],fig1[i+1]-fig1[i+2],fig2[i]-fig2[i+1],fig2[i+1]-fig2[i+2],
#label[i]+' - '+label[i+1],label[i+1]+' - '+label[i+2],
#np.min(fig2[i]-fig2[i+1]),np.min(fig2[i+1]-fig2[i+2]),np.max(fig2[i]-fig2[i+1]),np.max(fig2[i+1]-fig2[i+2]))

#plt.show()


hist1,xedges1,yedges1 = np.histogram2d(x.appMag.g-x.appMag.r,x.appMag.r-x.appMag.i,bins=200,range=[[-2.0,2.0],[-2.0,2.0]],normed=True)


hist2,xedges1,yedges1 =np.histogram2d(y.appMag[yInd].g-y.appMag[yInd].r,y.appMag[yInd].r-y.appMag[yInd].i,bins=200,range=[[-2.0,2.0],[-2.0,2.0]],normed=True)

extent = [xedges1[0], xedges1[-1], yedges1[0], yedges1[-1] ]
plt.imshow(hist1.T-hist2.T,extent=extent,interpolation='nearest',origin='lower',cmap='binary')

plt.colorbar()
plt.show()


##############################################################################

import biseps as b
import numpy as np
import matplotlib.pyplot as plt


x=b.readFile()
y=b.readFile()

x.appMag5('mag.all.targ.1')
y.readKicMag('kic.targAll.mag.2',sep=",")
#y.appMag5('kepler2/merge/mag.all.targAll.1.kic')

yInd=(y.appMag.jj>0.0)&(y.appMag.jh>0.0)&(y.appMag.jk>0.0)&(y.appMag.g>0.0)&(y.appMag.r>0.0)&(y.appMag.i>0.0)&(y.appMag.z>0.0)&(y.appMag.d51>0.0)

def plotIm(x1,y1,xlabel,ylabel):
	plt.figure()

	minX=x1.min()
	maxX=x1.max()
	minY=y1.min()
	maxY=y1.max()

	hist,xedges,yedges = np.histogram2d(x1,y1,bins=250,range=[[minX,maxX],[minY,maxY]])
	extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
	plt.imshow(np.log10(hist.T),extent=extent,interpolation='nearest',origin='lower',vmin=0.0,vmax=4.0)
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.colorbar()
	

label=['g','r','i','z']

fig2=[y.appMag[yInd].g,y.appMag[yInd].r,y.appMag[yInd].i,y.appMag[yInd].z]

i=0
plotIm(fig2[i]-fig2[i+1],fig2[i+1]-fig2[i+2],label[i]+' - '+label[i+1],label[i+1]+' - '+label[i+2])


gr0=[1.2513,1.2498,1.1961,1.0716,0.916,0.7827,0.6775,0.5908,0.5136,0.4419,0.3763,0.3153,0.2578,0.2033,0.1511,0.1007,0.0268,-0.0201,-0.0668,-0.1132,-0.1521,-0.1827,
-0.2078,-0.2289,-0.2465,-0.2612,-0.2735,-0.2923,-0.3058,-0.3161,-0.3248,-0.3325,-0.34,-0.3539,-0.3672,-0.3799,-0.3917,-0.4029,-0.4133,-0.4231,-0.4319,-0.4398,
-0.4467,-0.4528,-0.4587,-0.4649,-0.4715,-0.4784,-0.4847,-0.4898,-0.4933,-0.4954,-0.4962,-0.4962,-0.4960]
ri0=[0.7444,0.5781,0.4550,0.3744,0.3117,0.2573,0.2112,0.1734,0.1407,0.1113,0.0832,0.0555,0.0286,0.0015,-0.0257,-0.0532,-0.0946,-0.1219,-0.1474,-0.1692,-0.1884,-0.2051,
-0.2187,-0.23,-0.2395,-0.2476,-0.2546,-0.2657,-0.2744,-0.2812,-0.2868,-0.2918,-0.2964,-0.3051,-0.3133,-0.321,-0.3285,-0.3356,-0.3424,-0.3487,-0.3549,-0.3607,-0.3657,
-0.3701,-0.3742,-0.3783,-0.3828,-0.3874,-0.3919,-0.3956,-0.3983,-0.4,-0.4011,-0.4012,-0.4007]

grp1=[0.9667,0.9691,1.0762,1.1136,1.0577,0.9498,0.8390,0.7416,0.6555,0.5739,0.4953,0.4203,0.3501,0.2811,0.2179,0.1424,0.084,0.0277,-0.0303,-0.0822,-0.1231,
-0.1594,-0.1913,-0.2184,-0.2411,-0.2596,-0.2749,-0.2978,-0.3139,-0.3258,-0.3357,-0.3446,-0.3530,-0.3686,-0.3823,-0.3947,-0.4060,-0.4167,-0.4264,-0.4348,-0.4416,
-0.4470,-0.4517,-0.4566,-0.4623,-0.4687,-0.4750,-0.4806,-0.4850,-0.4880,-0.4902]
rip1=[1.0994,0.7684,0.5329,0.4001,0.323,0.2636,0.2112,0.1673,0.1320,0.1016,0.0732,0.0446,0.0159,-0.0123,-0.0405,-0.0807,-0.1088,-0.1356,-0.1602,-0.1823,-0.2006,
-0.217,-0.2306,-0.2421,-0.2519,-0.2603,-0.2674,-0.2791,-0.2877,-0.2941,-0.2993,-0.3038,-0.3083,-0.3169,-0.3250,-0.3326,-0.3399,-0.3471,-0.3540,-0.3606,-0.3664,-0.3708
,-0.3744,-0.3777,-0.3816,-0.3863,-0.313,-0.3960,-0.3997,-0.4023,-0.4024]

grm1=[1.4712,1.3093,1.1409,0.9579,0.8030,0.6882,0.6007,0.5261,0.4580,0.3928,0.3329,0.2776,0.2264,0.181,0.1310,0.085,0.0384,-0.0328,-0.0757,-0.1180,-0.1560,-0.1851,
-.2086,-0.2274,-0.2434,-0.2565,-0.2675,-0.2844,-0.2975,-0.3082,-0.3174,-0.3259,-0.3337,-0.3483,-0.3621,-0.3751,-0.3870,-0.3981,-0.4082,-0.4178,-0.4265,-0.4349,-0.4427
,-0.4499,-0.4568,-0.4635,-0.4703,-0.4771,-0.4835,-0.4892,-0.4935,-0.4966,-0.4983,-0.4986,-0.4981]
rim1=[0.6636,0.5621,0.4729,0.3829,0.3171,0.2666,0.2253,0.1896,0.1579,0.1275,0.0985,0.0701,0.0424,0.0147,-0.0128,-0.0404,-0.0683,-0.1084,-0.1351,-0.1583,-0.1779,
-0.1956,-0.2104,-0.2223,-0.2321,-0.24,-0.2467,-0.2574,-0.2659,-0.2731,-0.2795,-0.2851,-0.2904,-0.3,-0.3089,-0.317,-0.3248,-0.3320,-0.3385,-0.3445,-0.3502,-0.3557,-0.3609,
-0.3657,-0.3702,-0.3747,-0.3791,-0.3836,-0.388,-0.3914,-0.3939,-0.3955,-0.3962,-0.3960,-0.3949]

grm2=[1.1435,1.2592,1.0626,0.8976,0.7636,0.6648,0.5804,0.5046,0.4373,0.3746,0.3181,0.2666,0.2183,0.1719,0.1264,0.0814,0.0350,-0.0342,-0.0761,-0.1175,-0.1550,
-0.1839,-0.2069,-0.2256,-0.2410,-0.2537,-0.2642,-0.2810,-0.2941,-0.3051,-0.3147,-0.3234,-0.3316,-0.3470,-0.3611,-0.3742,-0.3861,-0.3971,-0.4072,-0.4164,-0.4251,
-0.4334,-0.4415,-0.4492,-0.4566,-0.4638,-0.4708,-0.4775,-0.4837,-0.4891,-0.4931,-0.4960]
rim2=[0.439,0.5604,0.4744,0.3960,0.3284,0.2824,0.241,0.2042,0.1701,0.1374,0.1064,0.0775,0.0492,0.0215,-0.0059,-0.0333,-0.0624,-0.1015,-0.1289,-0.1534,-0.1734,-0.1913,
-0.2066,-0.2189,-0.2288,-0.2368,-0.2436,-0.2544,-0.2630,-0.2704,-0.2769,-0.2829,-0.2885,-0.2985,-0.3077,-0.3162,-0.3241,-0.3313,-0.3378,-0.3437,-0.3492,-0.3544,
-0.3596,-0.3646,-0.3695,-0.3742,-0.3787,-0.3832,-0.3873,-0.3907,-0.3931,-0.3946]

plt.plot(grp1,rip1, linewidth=2,label='+1.0')
plt.plot(gr0,ri0, linewidth=2,label='0.0')
plt.plot(grm1,rim1, linewidth=2,label='-1.0')
plt.plot(grm2,rim2, linewidth=2,label='-2.0')
plt.legend(loc=0)

plt.show()






#####################
plt.hist(x.appMag.r,bins=48,label='us',alpha=0.25,range=[6.0,18.0])
plt.hist(y.appMag[yInd].r,bins=48,label='kic',alpha=0.25,range=[6.0,18.0])
plt.legend(loc=0)
plt.show()


##################################

import biseps as b
import numpy as np
import matplotlib.pyplot as plt
import numpy.random as rn


x=b.readFile()
y=b.readFile()
z=b.readFile()

x.appMag5('mag.all.idl.4')
y.readKicMag('kic.targAll.mag.2',sep=",")
#y.appMag5('mag.all.idl.1')

yInd=(y.appMag.jj>0.0)&(y.appMag.jh>0.0)&(y.appMag.jk>0.0)&(y.appMag.g>0.0)&(y.appMag.r>0.0)&(y.appMag.i>0.0)&(y.appMag.z>0.0)&(y.appMag.d51>0.0)


def plotIm(x1,y1,x2,y2,xlabel,ylabel,minX,minY,maxX,maxY):
	gr=b.plot()
	plt.figure()

	ax1=plt.subplot(121)
	hist,xedges,yedges = np.histogram2d(x1,y1,bins=250,range=[[minX,maxX],[minY,maxY]])
	extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
	plt.imshow(np.log10(hist.T),extent=extent,interpolation='nearest',origin='lower',vmin=0.0,vmax=4.0,cmap='binary')
	plt.title(r'Us pre target')
	plt.xlim(minX,maxX)
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	#gr.plotGRContours()
	plt.colorbar()

	
	plt.subplot(122,sharex=ax1,sharey=ax1)
	hist,xedges,yedges = np.histogram2d(x2,y2,bins=250,range=[[minX,maxX],[minY,maxY]])
	extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
	plt.imshow(np.log10(hist.T),extent=extent,interpolation='nearest',origin='lower',vmin=0.0,vmax=4.0,cmap='binary')
	plt.colorbar()
	plt.xlim(minX,maxX)
	#plt.xlim(maxX,minX)
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	#gr.plotGRContours()
	plt.title(r'$KIC$')
	
#label=['g','r','i','z']

#fig1=[x.appMag.g,x.appMag.r,x.appMag.i,x.appMag.z]
#fig2=[y.appMag[yInd].g,y.appMag[yInd].r,y.appMag[yInd].i,y.appMag[yInd].z]

g=x.appMag.g
r=x.appMag.r
i=x.appMag.i
z=x.appMag.z

g=g+(rn.randn(np.size(g))*((g-r)-np.min(g-r))*0.02)
r=r+(rn.randn(np.size(g))*((r-i)-np.min(r-i))*0.02)
i=i+(rn.randn(np.size(g))*((r-i)-np.min(r-i))*0.02)
z=z+(rn.randn(np.size(g))*((i-z)-np.min(i-z))*0.02)


#ik=((i+0.0583)-0.0696*(r+0.0383))/(0.9304+(0.0696*0.0548))

#rk=((r+0.0383)+0.0548*ik)/1.0548

#gk=((g+0.0985)+(0.0921*rk))/1.0921

#zk=((z+0.0597)-(0.1587*ik))/0.84132

plotIm(g-r,r-i,y.appMag[yInd].g-y.appMag[yInd].r,y.appMag[yInd].r-y.appMag[yInd].i,'','',-1.0,-1.0,3.0,2.0)

plt.show()


hist1,xedges1,yedges1 = np.histogram2d(g-r,r-i,bins=200,range=[[-2.0,2.0],[-2.0,2.0]],normed=True)


hist2,xedges1,yedges1 = np.histogram2d(y.appMag[yInd].g-y.appMag[yInd].r,y.appMag[yInd].r-y.appMag[yInd].i,bins=200,range=[[-2.0,2.0],[-2.0,2.0]],normed=True)

extent = [xedges1[0], xedges1[-1], yedges1[0], yedges1[-1] ]
plt.imshow(hist1.T-hist2.T,extent=extent,interpolation='nearest',origin='lower',cmap='binary')

plt.colorbar()
plt.show()

################################

import biseps as b
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt


y=b.readFile()

y.readKicMag('kic.targAll.mag.2',sep=",")

yInd=(y.appMag.jj>0.0)&(y.appMag.jh>0.0)&(y.appMag.jk>0.0)&(y.appMag.g>0.0)&(y.appMag.r>0.0)&(y.appMag.i>0.0)&(y.appMag.z>0.0)&(y.appMag.d51>0.0)


hist2,xedges1,yedges1 = np.histogram2d(y.appMag[yInd].g-y.appMag[yInd].r,y.appMag[yInd].r-y.appMag[yInd].i,bins=200,range=[[-2.0,2.0],[-2.0,2.0]],normed=True)

fig = plt.figure()
ax = fig.gca(projection='3d')
xx, yy = np.meshgrid(xedges1,xedges1)
surf = ax.plot_surface(xx[0:200,0:200],yy[0:200,0:200],hist2.T,rstride=3,cstride=3, cmap=cm.jet, antialiased=False)

plt.show()


hist2,xedges1,yedges1 = np.histogram2d(y.appMag[yInd].g-y.appMag[yInd].r,y.appMag[yInd].r-y.appMag[yInd].i,bins=200,range=[[-2.0,2.0],[-2.0,2.0]],normed=True)

Ax=np.vander(xedges1,3)
A=np.hstack((xedges1,yedges1))
xx, yy = np.meshgrid(xedges1,xedges1)

(coeffs, residuals, rank, sing_vals) = np.linalg.lstsq(xx[0:200,0:200],hist2.T)

extent = [xedges1[0], xedges1[-1], yedges1[0], yedges1[-1] ]
plt.plot(coeffs*xx[0:200,0:200])
plt.colorbar()
plt.show()

##########



from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np

fig = plt.figure()
ax = fig.gca(projection='3d')
X = np.arange(-5, 5, 0.25)
Y = np.arange(-5, 5, 0.25)
X, Y = np.meshgrid(X, Y)
R = np.sqrt(X**2 + Y**2)
Z = np.sin(R)
surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.jet,
        linewidth=0, antialiased=False)
ax.set_zlim(-1.01, 1.01)

ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

fig.colorbar(surf, shrink=0.5, aspect=5)

plt.show()


#################################################


import biseps as b
import numpy as np
import matplotlib.pyplot as plt
import numpy.random as ra

x=b.readFile()
y=b.readFile()
#z=b.readFile()

x.appMag5('mag.all.targAll.1')
y.readKicMag('kic.targAll.mag.2',sep=",")
#y.appMag5('mag.all.idl.1')

yInd=(y.appMag.jj>0.0)&(y.appMag.jh>0.0)&(y.appMag.jk>0.0)&(y.appMag.g>0.0)&(y.appMag.r>0.0)&(y.appMag.i>0.0)&(y.appMag.z>0.0)&(y.appMag.d51>0.0)


def plotIM2(x1,y1,x2,y2,xlabel,ylabel):
	gr=b.plot()
	plt.figure()

	minX=np.minimum(x1.min(),x2.min())
	maxX=np.maximum(x1.max(),x2.max())
	minY=np.minimum(y1.min(),y2.min())
	maxY=np.maximum(y1.max(),y2.max())

	locRI=np.arange(minY,maxY,(maxY-minY)/1000.0)
	locGR=locRI_lam(locRI)


	ax1=plt.subplot(121)
	hist,xedges,yedges = np.histogram2d(x1,y1,bins=250,range=[[minX,maxX],[minY,maxY]])
	extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
	plt.imshow(np.log10(hist.T),extent=extent,interpolation='nearest',origin='lower',vmin=0.0,vmax=4.0,cmap='binary')
	plt.title(r'Us pre target')
	plt.xlim(minX,maxX)
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.plot(locGR,locRI)
	#gr.plotGRContours()
	plt.colorbar()

	
	plt.subplot(122,sharex=ax1,sharey=ax1)
	hist,xedges,yedges = np.histogram2d(x2,y2,bins=250,range=[[minX,maxX],[minY,maxY]])
	extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
	plt.imshow(np.log10(hist.T),extent=extent,interpolation='nearest',origin='lower',vmin=0.0,vmax=4.0,cmap='binary')
	plt.colorbar()
	plt.xlim(minX,maxX)
	#plt.xlim(maxX,minX)
	plt.xlabel(xlabel)
	plt.plot(locGR,locRI)
	plt.ylabel(ylabel)
	#gr.plotGRContours()
	plt.title(r'$KIC$')
	


g=np.copy(x.appMag.g)
r=np.copy(x.appMag.r)
i=np.copy(x.appMag.i)
z=np.copy(x.appMag.z)
d51=np.copy(x.appMag.d51)



#ik1=((i+0.0583)-0.0696*(r+0.0383))/(0.9304+(0.0696*0.0548))

#rk1=((r+0.0383)+0.0548*ik1)/1.0548

#gk1=((g+0.0985)+(0.0921*rk1))/1.0921

#zk1=((z+0.0597)-(0.1587*ik1))/0.84132
#d51k1=((d51+0.0597)-(0.1587*ik1))/0.84132

#gk1=gk1+0.04
#rk1=rk1+0.028
#ik1=ik1+0.045
#zk1=zk1+0.042
#d51k1=d51k1+0.042

ik1=i
gk1=g
rk1=r
zk1=z
d51k1=d51

ind1=(g-r < 0.3)
ind2=(g-r >= 0.3)&(g-r<1.2)
ind3=(g-r>1.2)

gk2=np.copy(gk1)
rk2=np.copy(rk1)
ik2=np.copy(ik1)


cdict3 = {'red':  ((0.0, 0.0, 0.0),
                   (0.25,0.0, 0.0),
                   (0.495,1.0, 1.0),
                   (0.505, 1.0, 1.0),
                   (0.75,1.0, 1.0),
                   (1.0, 0.4, 1.0)),

         'green': ((0.0, 0.0, 0.0),
                   (0.25,0.0, 0.0),
                   (0.495, 1.0, 1.0),
                   (0.505, 1.0, 0.9),
                   (0.75,0.0, 0.0),
                   (1.0, 0.0, 0.0)),

         'blue':  ((0.0, 0.0, 0.4),
                   (0.25,1.0, 1.0),
				   (0.495,1.0, 1.0),
                   (0.505, 1.0, 0.8),
                   (0.75,0.0, 0.0),
                   (1.0, 0.0, 0.0))
        }

plt.register_cmap(name='BlueRed3', data=cdict3)


def locRI_lam(x):
	return 1.39*(1.0-np.exp(-4.9*x**3-2.45*x**2-1.68*x-0.05))

def pinnCorrect(g,r,i):
	gg=0.0921
	gi=0.0985
	rg=0.0548
	ri=0.0383
	ig=0.0696
	ii=0.0583
	
	ik=((i+ii)*(1.0+rg)-ig*(r+ri))/(1.0-ig+rg)
	rk=(r+ri+ik*rg)/(1.0+rg)
	gk=(g+gi+rk*gg)/(1.0+gg)

	return gk,rk,ik

shap=np.count_nonzero(ind1)
XX=0.01
gk2[ind1]=2.5*np.log10(ra.lognormal(gk1[ind1],XX,shap))
rk2[ind1]=2.5*np.log10(ra.lognormal(rk1[ind1],XX,shap))
ik2[ind1]=2.5*np.log10(ra.lognormal(ik1[ind1],XX,shap))


shap=np.count_nonzero(ind2)
XX=0.01
gk2[ind2]=2.5*np.log10(ra.lognormal(gk1[ind2],XX,shap))
rk2[ind2]=2.5*np.log10(ra.lognormal(rk1[ind2],XX,shap))
ik2[ind2]=2.5*np.log10(ra.lognormal(ik1[ind2],XX,shap))

shap=np.count_nonzero(ind3)
XX=0.01
gk2[ind3]=2.5*np.log10(ra.lognormal(gk1[ind3],XX,shap))
rk2[ind3]=2.5*np.log10(ra.lognormal(rk1[ind3],XX,shap))
ik2[ind3]=2.5*np.log10(ra.lognormal(ik1[ind3],XX,shap))

gk2,rk2,ik2=pinnCorrect(gk2,rk2,ik2)

plotIM2(gk2-rk2,rk2-ik2+0.045,y.appMag[yInd].g-y.appMag[yInd].r,y.appMag[yInd].r-y.appMag[yInd].i,'g-r','r-i')
plt.show()

hist1,xedges1,yedges1 = np.histogram2d(gk2-rk2,rk2-ik2+0.035,bins=200,range=[[-2.0,2.0],[-2.0,2.0]],normed=True)


hist2,xedges1,yedges1 = np.histogram2d(y.appMag[yInd].g-y.appMag[yInd].r,y.appMag[yInd].r-y.appMag[yInd].i,bins=200,range=[[-2.0,2.0],[-2.0,2.0]],normed=True)

extent = [xedges1[0], xedges1[-1], yedges1[0], yedges1[-1] ]
plt.imshow((hist1.T-hist2.T),extent=extent,interpolation='nearest',origin='lower',cmap='BlueRed3',vmin=-np.max(np.abs(hist1.T-hist2.T)),vmax=np.max(np.abs(hist1.T-hist2.T)))
print np.max(np.abs(hist1.T-hist2.T))

plt.colorbar()
plt.show()


arr=np.zeros(300)
for i in range(np.size(arr)):
	hist1,xedges1,yedges1 = np.histogram2d(gk2-rk2,rk2-ik2+0.02+(i*((0.05-0.02)/300.0)),bins=200,range=[[-2.0,2.0],[-2.0,2.0]],normed=True)
	arr[i]=np.max(np.abs(hist1-hist2))

print np.min(arr),0.02+(np.argmin(arr)*(0.05-0.02)/0.0001)

##########################################

import biseps as b
import numpy as np
import matplotlib.pyplot as plt
import numpy.random as ra
import scipy as sci

x=b.readFile()
y=b.readFile()

x.appMag5('mag.all.targAll.1')
y.readKicMag('kic.targAll.mag.2',sep=",")

yInd=(y.appMag.jj>0.0)&(y.appMag.jh>0.0)&(y.appMag.jk>0.0)&(y.appMag.g>0.0)&(y.appMag.r>0.0)&(y.appMag.i>0.0)&(y.appMag.z>0.0)&(y.appMag.d51>0.0)


g=np.copy(x.appMag.g)
r=np.copy(x.appMag.r)
i=np.copy(x.appMag.i)
z=np.copy(x.appMag.z)
d51=np.copy(x.appMag.d51)

ik1=((i+0.0583)-0.0696*(r+0.0383))/(0.9304+(0.0696*0.0548))

rk1=((r+0.0383)+0.0548*ik1)/1.0548

gk1=((g+0.0985)+(0.0921*rk1))/1.0921

zk1=((z+0.0597)-(0.1587*ik1))/0.84132
d51k1=((d51+0.0597)-(0.1587*ik1))/0.84132

gk1=gk1+0.04
rk1=rk1+0.028
ik1=ik1+0.045
zk1=zk1+0.042
d51k1=d51k1+0.042

ind1=(g-r < 0.3)
ind2=(g-r >= 0.3)&(g-r<1.2)
ind3=(g-r>1.2)

yInd1=yInd&(y.appMag.g-y.appMag.r < 0.3)
yInd2=yInd&(y.appMag.g-y.appMag.r >= 0.3)&(y.appMag.g-y.appMag.r <1.2)
yInd3=yInd&(y.appMag.g-y.appMag.r >=1.2)

bins=300
rng=[[-2.0,2.0],[-2.0,2.0]]

histRef1,t,t=np.histogram2d(y.appMag.g[yInd1]-y.appMag.r[yInd1],y.appMag.r[yInd1]-y.appMag.i[yInd1],bins=bins,range=rng,normed=True)
histRef2,t,t=np.histogram2d(y.appMag.g[yInd2]-y.appMag.r[yInd2],y.appMag.r[yInd2]-y.appMag.i[yInd2],bins=bins,range=rng,normed=True)
histRef3,t,t=np.histogram2d(y.appMag.g[yInd3]-y.appMag.r[yInd3],y.appMag.r[yInd3]-y.appMag.i[yInd3],bins=bins,range=rng,normed=True)


#histRef11,t,t=np.histogram2d(y.appMag.r[yInd1]-y.appMag.i[yInd1],y.appMag.i[yInd1]-y.appMag.z[yInd1],bins=bins,range=[[-2.0,2.0],[-2.0,2.0]],normed=True)
#histRef22,t,t=np.histogram2d(y.appMag.r[yInd2]-y.appMag.i[yInd2],y.appMag.i[yInd2]-y.appMag.z[yInd2],bins=bins,range=[[-2.0,2.0],[-2.0,2.0]],normed=True)
#histRef33,t,t=np.histogram2d(y.appMag.r[yInd3]-y.appMag.i[yInd3],y.appMag.i[yInd3]-y.appMag.z[yInd3],bins=bins,range=[[-2.0,2.0],[-2.0,2.0]],normed=True)

def alter1(ind,XX,histRef):

	gk2=np.copy(gk1)
	rk2=np.copy(rk1)
	ik2=np.copy(ik1)
	#zk2=np.copy(zk1)
	shap=np.count_nonzero(ind)
	gk2[ind]=-2.5*np.log10(((ra.rand(shap)-0.5)/(XX))+10**(gk1[ind]/(-2.5)))
	rk2[ind]=-2.5*np.log10(((ra.rand(shap)-0.5)/(XX))+10**(rk1[ind]/(-2.5)))
	ik2[ind]=-2.5*np.log10(((ra.rand(shap)-0.5)/(XX))+10**(ik1[ind]/(-2.5)))

	#gk2[ind]=(((ra.randn(shap))/(XX))*(gk1[ind]))
	#rk2[ind]=(((ra.randn(shap))/(XX))*(rk1[ind]))
	#ik2[ind]=(((ra.randn(shap))/(XX))*(ik1[ind]))
	
	#zk2[ind]=-2.5*np.log10(((ra.randn(shap))/(XX))*10**(zk1[ind]/(-2.5)))

	hist1,xedges,yedges = np.histogram2d(rk2[ind]-ik2[ind],rk2[ind],bins=bins,range=rng,normed=True)

	#hist2,xedges,yedges = np.histogram2d(rk2[ind]-ik2[ind],ik2[ind]-zk2[ind],bins=bins,range=[[-2.0,2.0],[-2.0,2.0]],normed=True)

	#res1=np.abs(hist1-histRef)
	#res2=np.abs(hist2-histRef2)

	#res11=np.sum((res1-np.mean(res1))**2)
	#res22=np.sum((res2-np.mean(res2))**2)

	res11=np.sum(((hist1[hist1>0]-histRef[hist1>0])**2)/np.sqrt(hist1[hist1>0]))
	#res22=np.sum((hist2-histRef2)**2)

	#print res11,
	#print res22
	return res11


minI=5.0
maxI=10.0
num=1000
step=(maxI-minI*1.0)/num*1.0
r1=np.zeros(num)
r2=np.zeros(num)
r3=np.zeros(num)
grMult=np.zeros(num)

for i in range(num):
	grMult[i]=10**(minI+(i*step))
	r1[i]=alter1(ind1,grMult[i],histRef1)
	r2[i]=alter1(ind2,grMult[i],histRef2)
	r3[i]=alter1(ind3,grMult[i],histRef3)
	

pl.figure(1)
plt.plot(np.log10(grMult),r1,label='g-r')
p1=np.poly1d(np.polyfit(grMult,r1,5))
plt.plot(np.log10(grMult),p1(grMult),label='fit')
plt.legend(loc=0)

pl.figure(2)
plt.plot(np.log10(grMult),r2,label='g-r')
p2=np.poly1d(np.polyfit(grMult,r2,5))
plt.plot(np.log10(grMult),p2(grMult),label='fit')
plt.legend(loc=0)

pl.figure(3)
plt.plot(np.log10(grMult),r3,label='g-r')
p3=np.poly1d(np.polyfit(grMult,r3,5))
plt.plot(np.log10(grMult),p3(grMult),label='fit')
plt.legend(loc=0)


plt.show()




print grMult[np.argmin(p1(grMult))]
print grMult[np.argmin(p2(grMult))]
print grMult[np.argmin(p3(grMult))]


##################################

import biseps as b
import numpy as np
import matplotlib.pyplot as plt


x=b.readFile()
y=b.readFile()

x.appMag5('mag.all.targAll.1')
y.readKicMag('kic.targAll.mag.2',sep=",")

yInd=(y.appMag.jj>0.0)&(y.appMag.jh>0.0)&(y.appMag.jk>0.0)&(y.appMag.g>0.0)&(y.appMag.r>0.0)&(y.appMag.i>0.0)&(y.appMag.z>0.0)&(y.appMag.d51>0.0)


def plotIM(x,y,xlabel,ylabel,bins=20):
	plt.figure()
	hist,xedges,yedges = np.histogram2d(x,y,bins=bins)
	extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
	plt.imshow(np.log10(hist.T),extent=extent,interpolation='nearest',origin='lower',vmin=0.0,vmax=4.0,cmap='binary',aspect=0.25)
	#plt.title(r'Us pre target')
	plt.ylim(np.max(y),np.min(y))
	#plt.xlim(minX,maxX)
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	#gr.plotGRContours()
	plt.colorbar()
	plt.show()

def pinnCorrect(g,r,i):
	gg=0.0921
	gi=0.0985
	rg=0.0548
	ri=0.0383
	ig=0.0696
	ii=0.0583
	
	gk=g+gg*(g-r)-0.0985
	rk=r+rg*(r-i)-0.0383
	ik=i+ig*(r-i)-0.0583

	return gk,rk,ik


lambda locRI_lam(x) x:1.39*(1.0-np.exp(-4.9*x**3-2.45*x**2-1.68*x-0.05))

def plotIM2(x1,y1,kg,kr,ki,xlabel='',ylabel='',bins=250):
	plt.figure()

	minX=-0.5
	maxX=2.5
	minY=-0.5
	maxY=1.5
	
	locRI=np.arange(minY,maxY,(maxY-minY)/1000.0)
	locGR=locRI_lam(locRi)

	ax1=plt.subplot(121)
	hist,xedges,yedges = np.histogram2d(x1,y1,bins=bins,range=[[minX,maxX],[minY,maxY]])
	extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
	plt.imshow(np.log10(hist.T),extent=extent,interpolation='nearest',origin='lower',vmin=0.0,vmax=4.0,cmap='binary')
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.plot(locGR,locRI)
	plt.xlim(minX,maxX)
	plt.ylim(minY,maxY)
	plt.colorbar()

	
	plt.subplot(122,sharex=ax1,sharey=ax1)


	kg,kr,ki=pinnCorrect(kg,kr,ki)

	
	hist,xedges,yedges = np.histogram2d(kg-kr,kr-ki,bins=bins,range=[[minX,maxX],[minY,maxY]])
	extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
	plt.imshow(np.log10(hist.T),extent=extent,interpolation='nearest',origin='lower',vmin=0.0,vmax=4.0,cmap='binary')
	plt.colorbar()
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.plot(locGR,locRI)
	plt.xlim(minX,maxX)
	plt.ylim(minY,maxY)
	plt.show()



plotIM2(x.appMag.g-x.appMag.r,x.appMag.r-x.appMag.i,y.appMag[yInd].g,y.appMag[yInd].r,y.appMag[yInd].i,'g','r',bins=250)
#plotIM2(10**(x.appMag.g/-2.5)-10**(x.appMag.r/-2.5),10**(x.appMag.r/-2.5)-10**(x.appMag.i/-2.5),
#10**(y.appMag[yInd].g/-2.5)-10**(y.appMag[yInd].r/-2.5),10**(y.appMag[yInd].r/-2.5)-10**(y.appMag[yInd].i/-2.5),'g-z','r-i')
