import biseps as b
import numpy as np
import matplotlib.pyplot as plt

x=b.readFile()
x.kicProp('popsy1/kic.dat.2')
x.kepler3('popsy1/extract.dat.1')


G=6.67384*10**(-11)
Msun=1.98892*10**(30)
Rsun=6.955*10**(8)
nbins=100

plt.figure(1)
plt.title('Plots system properties using the KIC property code')

plt.subplot(3,2,1)
plt.hist(x.kicProp.teff[(x.kicProp.tefferr < 10**9.99)],alpha=0.25,normed=1,bins=nbins,label='Kic',range=[0.0,20000.0])
plt.hist(x.kep.t1[(x.kep.t1 < 999999.9)],alpha=0.25,normed=1,bins=nbins,label='Extract',range=[0.0,20000.0])
plt.xlabel(r'$T_{eff}$')
plt.legend(loc=0)

#plt.subplot(3,2,2)
#plt.hist(x.kicProp.logg[(x.kicProp.loggerr < 9.99)],alpha=0.25,normed=1,bins=nbins,label='Kic',range=[0.0,5.5])
#plt.hist(x.kep.logg[(x.kep.logg < 999999.9)],alpha=0.25,normed=1,bins=nbins,label='Extract',range=[0.0,5.5])
#plt.xlabel(r'$\log g$')

plt.subplot(3,2,3)
plt.hist(np.log10(x.kicProp.rad[(x.kicProp.loggerr < 9.99)]),alpha=0.25,normed=1,bins=nbins,label='Kic',range=[-1.0,2.0])
plt.hist(np.log10(x.kep.r1[(x.kep.r1 < 999999.9)]),alpha=0.25,normed=1,bins=nbins,label='Extract',range=[-1.0,2.0])
plt.xlabel(r'$\log_{10} R_{\odot}$')

plt.subplot(3,2,4)
mass=np.zeros(np.size(x.kicProp.rad[(x.kicProp.loggerr < 9.99)]))
mass=(10**(x.kicProp.logg[(x.kicProp.loggerr < 9.99)])*(x.kicProp.rad[(x.kicProp.loggerr < 9.99)]*Rsun)**2)/(100.0*G*Msun)
plt.hist(mass,alpha=0.25,normed=1,bins=nbins,label='Kic',range=[0.0,5.0])

mass=np.zeros(np.size(x.kicProp.rad[(x.kicProp.loggerr < 9.99)]))
mass=(10**(x.kicProp.logg[(x.kicProp.loggerr < 9.99)])*(x.kicProp.rad[(x.kicProp.loggerr < 9.99)]*Rsun)**2)/(100.0*G*Msun)
plt.hist(mass,alpha=0.25,normed=1,bins=nbins,label='Kic',range=[0.0,5.0])
plt.hist(x.kep.m1[(x.kep.m1 < 999999.9)],alpha=0.25,normed=1,bins=nbins,label='Extract',range=[0.0,5.0])
plt.xlabel(r'$M_{\odot}$')

plt.subplot(3,2,5)
plt.hist(x.kicProp.logz[(x.kicProp.logzerr < 9.99)],alpha=0.25,normed=1,bins=nbins,label='Kic',range=[-3.0,1.0])
#plt.hist(x.kep.feh[(x.kep.feh < 999999.9)],alpha=0.25,normed=1,bins=nbins,label='Extract',range=[-3.0,1.0])
plt.xlabel(r'$\log z$')

plt.show()
		