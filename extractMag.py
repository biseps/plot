#Extract from extract.dat.1 the stars we want based on target.dat.*c file
import biseps as b
import numpy as np
import getopt
import sys as sys

opts, extra = getopt.getopt(sys.argv[1:],'')
version=str(extra[0])

try:
	v2=str(extra[1])
except:
	v2=version
	
sysType='targAll'

x=b.readFile()
y=b.readFile()
x.appMag5('mag.dat.perturb.'+v2)

ind=[(x.appMag.kp < 16.0)][0]
#print ind

x.ranLoc('ranLoc.dat.'+v2)
x.write_array('ranLoc.'+sysType+'.'+version,x.ranLoc[ind],'w')

try:
	x.kepler5('extract.'+v2)
except:
	x.kepler3('extract.'+v2)

x.write_array('extract.'+sysType+'.'+version,x.kep[ind],'w')

x.kicProp('kic.dat.'+v2)
x.write_array('kic.'+sysType+'.'+version,x.kicProp,'w')

y.appMag5('mag.dat.perturb.'+v2)
y.write_array('mag.'+sysType+'.'+version,y.appMag[ind],'w')

x.readPop('pop.'+v2)
x.write_array('pop.'+sysType+'.'+version,x.pop[ind],'w')
