import biseps as b
import numpy as np

x=b.readFile()
ex=b.bfuncs()

version="1"

x.kepler3('extract.bin.targAll.'+version)
x.readTarget('target.bin.dat.'+version)

#Biseps output


a=ex.porb2a(x.kep.m1,x.kep.m2,x.kep.p)

lenObs=125.0

#Longest period to see three tranists
longPeriod=lenObs/2.0

res1=0
res2=0
def area(a,r1,r2,inclin):
	s=a*np.cos(np.radians(inclin))
	ep=s/r1
	p=r2/r1
	al1=np.arccos((p**2+(ep**2)-1.0)/(2*ep*p))
	al2=np.arccos((1.0+(ep**2)-p**2)/(2*ep))
	res=np.zeros(np.size(a))
	res[(1+p)<ep]=0.0
	ind=(((1.0-p)<ep)&(ep<=(1.0+p)))
	#Drop factors of r1**2 so we get the fractional area
	res[ind]=r1[ind]**2*(p[ind]**2*al1[ind]+al2[ind]-np.sqrt(4.0*ep[ind]**2-(1.0+ep[ind]**2-p[ind]**2)**2)/2.0)
	ind=((1.0-p)>=ep)
	res[ind]=np.pi*(p[ind]**2)*r1[ind]**2
	#We want to rturn the fractional area thats left after the transit 
	res=1.0-(res/(np.pi*r1**2))
	res[(r1+s<r2)]=0.0
	return res


for v2 in range(1,10):
	v2=str(v2)
	x.readTransCalc('trans.'+v2)
	ind=(x.trans.inclin>=x.trans.crit)
	area1=area(a[ind],x.kep.r1[ind],x.kep.r2[ind],x.trans.inclin[ind])
	area2=area(a[ind],x.kep.r2[ind],x.kep.r1[ind],x.trans.inclin[ind])
	#area1=area1*(x.kep.l1[ind]/(x.kep.l1[ind]+x.kep.l2[ind]))
	#area2=area2*(x.kep.l2[ind]/(x.kep.l1[ind]+x.kep.l2[ind]))

	sL=x.kep.l1[ind]+x.kep.l2[ind]
	t1=(sL-area1*x.kep.l1[ind]+x.kep.l2[ind])/sL
	t2=(sL-area2*x.kep.l2[ind]+x.kep.l1[ind])/sL

	trans=(((t1>10**-4)&(t2>10**-4))|(t1>10**-2)|(t2>10**-2))&(x.kep[ind].p<longPeriod)
	
	res1=res1+np.count_nonzero(trans&(((0<x.target[ind].bin)&(x.target[ind].bin<12))|(x.target[ind].bin>13)))
	res2=res2+np.count_nonzero(trans)

print res1/10.0,res2/10.0