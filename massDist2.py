import numpy as np
import matplotlib.pyplot as plt

def imfCalc(x):
	y=np.zeros(np.size(x))

	y[(x<0.1)]=0.0
	y[(x>0.1) & (x<=0.5)]=0.29056*x[(x>0.1) & (x<=0.5)]**(-1.3)
	y[(x>0.5) & (x<=1.0)]=0.1557*x[(x>0.5) & (x<=1.0)]**(-2.2)
	y[(x>1.0)]=0.1557*x[(x>1.0)]**(-2.7)

	return y


m1=np.linspace(0.1,20.0,10000)
m1imf=imfCalc(m1)

culSum=np.cumsum(m1imf,dtype='double')

norm=culSum/np.max(culSum)

ran=np.random.rand(100000)

loc=np.searchsorted(norm,ran)

ran2=np.random.random_sample(100000)

m2=0.1+((m1[loc]-0.1)*ran2)
plt.figure()
a,b,c=plt.hist(m1[loc],alpha=0.5,label='m1',normed=1,bins=100,range=[0.0,6.0])
a,b,c=plt.hist(m2,alpha=0.5,label='m2',normed=1,bins=100,range=[0.0,6.0])

plt.plot(m1[(m1>0.1)&(m1<6.0)],m1imf[(m1>0.1)&(m1<6.0)],'--')
plt.legend(loc=0)
plt.show()


plt.figure()
a,b,c=plt.hist(np.log10(m1[loc]),alpha=0.5,label='m1',normed=1,bins=50)
a,b,c=plt.hist(np.log10(m2),alpha=0.5,label='m2',normed=1,bins=50)

plt.plot(np.log10(m1[(m1>0.1)]),np.log10(m1imf[(m1>0.1)]),'--')

#plt.plot(np.log10(m1[(m1>0.1)&(m1<=0.5)]),-1.3*np.log10(0.29056*m1[(m1>0.1)&(m1<=0.5)]),'--')
#plt.plot(np.log10(m1[(m1>0.5)&(m1<=1.0)]),-2.2*np.log10(0.1557*m1[(m1>0.5)&(m1<=1.0)]),'--')
#plt.plot(np.log10(m1[(m1>1.0)]),-2.7*np.log10(0.1557*m1[(m1>1.0)]),'--')

plt.legend(loc=0)
plt.show()



