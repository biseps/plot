# -*- coding: utf-8 -*-


# In[]:
from matplotlib.patches import Rectangle
import getopt, os, sys, time, shutil
import math
from matplotlib.ticker import MultipleLocator, FormatStrFormatter


# from astropy.coordinates import ICRS, Galactic
# from astropy.coordinates import match_coordinates_sky
# from astropy import units as u
from astropy.io import ascii
from astropy.table import vstack
from astropy.table import hstack
from astropy.table import Table
from astropy.table import join
import astropy.constants as const


import numpy as np
from random import randint

import matplotlib
import matplotlib.pyplot as plt

import plotUtils as utils



# In[]:

version  = '1'
base_dir = '/padata/beta/users/efarrell/data/plato/pop_output/field1_5deg_26'

input_files =  []
input_files.append(os.path.join(base_dir, 'run_1/pop_s_o/40/mag.dat.1')) 
input_files.append(os.path.join(base_dir, 'run_1/pop_s_o/42/mag.dat.1')) 
input_files.append(os.path.join(base_dir, 'run_1/pop_s_o/46/mag.dat.1')) 
input_files.append(os.path.join(base_dir, 'run_1/pop_s_o/48/mag.dat.1')) 

# Run:
# Comment: run squares 95 and 96 
# Output Dir: /padata/beta/users/efarrell/data/plato/plato_cluster/output/field1/run_14
# Time: 2014-03-02, 14:33:36

# Run:
# Comment: run squares 75 and 76 
# Output Dir: /padata/beta/users/efarrell/data/plato/plato_cluster/output/field1/run_15
# Time: 2014-03-02, 14:36:33

# Run:
# Comment: run squares 55 and 56 
# Output Dir: /padata/beta/users/efarrell/data/plato/plato_cluster/output/field1/run_16
# Time: 2014-03-02, 14:37:28

# Run:
# Comment: run squares 15 and 16 
# Output Dir: /padata/beta/users/efarrell/data/plato/plato_cluster/output/field1/run_17
# Time: 2014-03-02, 14:38:21

task_id  = '1'
output_dir = 'temp_output'


# Get command line arguments
# opts, extra = getopt.getopt(sys.argv[1:],'')

# version     = str(extra[0])
# base_dir    = str(extra[1])
# task_id     = str(extra[2])
# output_dir  = str(extra[3])

print 'version: ' + version
print 'base_dir: ' + base_dir
print 'task_id: ' + task_id
print 'output_dir: ' + output_dir


# In[]:




def get_file_list(base_dir, task_id, filename, version):
    """return a list of files 
       which was the output from a cluster job

    """
    file_list=[]
    star_types = ['pop_s_o', 'pop_s_y', 'pop_b_o', 'pop_b_y']

    for star_type in star_types:
        file_list.append(base_dir + '/' + star_type + '/' + task_id + '/' + filename + '.' + version)

    return file_list



# In[]:

# Get magnitudes from "mag.dat" files

# input_files = get_file_list(base_dir, task_id, 'mag.dat', version)

headers = ['id', 'long', 'lat', 'u', 'g', 'r', 'i', 'z', 'd51', 'jj', 'jk', 'jh', 'kp']

magfiles=[]

for magfile in input_files:
    magfiles.append(ascii.read(magfile, delimiter=' ', names=headers))




# combine young and old info
# star_mags     = mag_pop_s_o
# star_mags     = vstack([mag_pop_s_o, mag_pop_s_y])
# binary_mags   = vstack([mag_pop_b_o, mag_pop_b_y])

# star_props    = vstack([extract_pop_s_o, extract_pop_s_y])
# binary_props  = vstack([extract_pop_b_o, extract_pop_b_y])


# Finally combine magnitudes and properties 
# into single tables for easier access
# stars    = hstack([star_mags, star_props])
# binaries = hstack([binary_mags, binary_props])






# In[]:


# create output folder to hold blend files
utils.make_folder(output_dir)
       

# In[]:

fig = plt.figure(figsize=(10, 8))
ax = plt.subplot(111)

# for magfile in magfiles:
    # x = magfile['long']
    # y = magfile['lat']
    # ax.scatter(x, y, s=15, lw=0, c=np.random.rand(3,1), alpha=0.7)

# ax.set_xlim([x.min(), x.max()])
# ax.set_ylim([y.min(), y.max()])

# plato field
# ax.set_xlim([40, 90])
# ax.set_ylim([15, 65])


# In[]:

# a bit around the field
ax.set_xlim([35, 95])
ax.set_ylim([10, 70])

x_corner, y_corner = 40, 15
ax.add_patch(Rectangle((x_corner, y_corner), 50, 50, facecolor="grey", alpha=0.6))

ax.set_title('5 x 5 degrees')

# ax.minorticks_on()
# ax.grid(True, which='minor')
ax.grid(True, which='both')

majorLocator   = MultipleLocator(20)
Formatter      = FormatStrFormatter('%d')
xminorLocator   = MultipleLocator(1)
yminorLocator   = MultipleLocator(1)

# ax.xaxis.set_major_formatter( majorLocator )
# ax.xaxis.set_minor_formatter( minorLocator )

ax.xaxis.set_minor_locator( xminorLocator )
ax.xaxis.set_minor_formatter( Formatter )

ax.yaxis.set_minor_locator( yminorLocator )
ax.yaxis.set_minor_formatter( Formatter )

ax.set_xlabel('lon', fontsize=13)
ax.set_ylabel('lat', fontsize=13)

# utils.save_plot(fig, output_dir, 'Plato_5degree')
# utils.save_plot(fig, output_dir, 'Plato_5degree_wideview')
# utils.save_plot(fig, output_dir, 'Plato_field_multisquares')
plt.show()



# In[]:

