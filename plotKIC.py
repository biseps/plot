import biseps as b
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc
from scipy import stats

rc('text', usetex=True)
rc('font', family='serif')

G=6.67384*10**(-11)
Msun=1.98892*10**(30)
Rsun=6.955*10**(8)
nbins=25

ex=b.extras()

x=b.readFile()
z=b.readFile()
#y=b.readFile()
#x.kicProp('kic.dat.all.1')
#z.kic5('kic.dat.all')
#z.kicProp('kic.dat.ll.6')

z.kicProp5('quart.dat.2')
#x.kicProp('kic.dat.all.6a')
x.kicProp('kic.sing.targ.17')
#z.kicProp5('quart.dat.2')
#y.kicProp('kic.all.targ.1')

#xInd=False
xInd=(x.kicProp.tefferr < 10**10.0)&(x.kicProp.loggerr < 9.0)&(x.kicProp.logzerr < 9.0)
#zInd=(z.kicProp.tefferr < 10**10.0)&(z.kicProp.loggerr < 9.0)&(z.kicProp.logzerr < 9.0)
zInd=(z.kicProp.teff >0.0)&(z.kicProp.kp>0.0)
#yInd=(((y.kicProp.tefferr < 10**10.0)&(y.kicProp.loggerr < 9.0)&(y.kicProp.logzerr < 9.0)))
#&np.logical_not((y.kicProp.tefferr < 1.0)&(y.kicProp.logzerr<0.001)&(y.kicProp.loggerr<0.001)&(y.kicProp.teff>6000.0))


#yInd=((y.kicProp.tefferr < 1.0)&(y.kicProp.logzerr<0.001)&(y.kicProp.loggerr<0.001)&(y.kicProp.teff>6000.0))

#xLabel='Err 0.01'
#yLabel='Err 0.02'


#xLabel='Target'
#yLabel='Pre Target'
#zLabel='Q2'

xLabel='Us 1'
yLabel='Us 2'
zLabel='Q2'

#zInd=np.in1d(z.kicProp.kepId,y.kicProp[yInd].id)

plt.figure(1)
#plt.title('Thin disc solar thick disc fe/h -1.7')

plt.subplot(3,2,1)


zz,zz,zz=plt.hist(x.kicProp.teff[xInd],alpha=0.25,normed=1,bins=nbins,label=xLabel,range=[2000.0,12000.0])
#zz,zz,zz=plt.hist(y.kicProp.teff[yInd],alpha=0.25,normed=1,bins=nbins,label=yLabel,range=[2000.0,10000.0])
zz,zz,zz=plt.hist(z.kicProp.teff[zInd],alpha=0.25,normed=1,bins=nbins,label=zLabel,range=[2000.0,12000.0])
plt.xlabel(r'$T_{eff}$')
plt.legend(loc=0)

plt.subplot(3,2,2)
zz,zz,zz=plt.hist(x.kicProp.logg[xInd],alpha=0.25,normed=1,bins=nbins,label=xLabel,range=[0.0,5.5])
#zz,zz,zz=plt.hist(y.kicProp.logg[yInd],alpha=0.25,normed=1,bins=nbins,label=yLabel,range=[0.0,5.5])
zz,zz,zz=plt.hist(z.kicProp.logg[zInd],alpha=0.25,normed=1,bins=nbins,label=zLabel,range=[0.0,5.5])
plt.xlabel(r'$\log g$')

plt.subplot(3,2,3)
zz,zz,zz=plt.hist(np.log10(x.kicProp.rad[xInd]),alpha=0.25,normed=1,bins=nbins,label=xLabel,range=[-1.0,3.0])
#zz,zz,zz=plt.hist(np.log10(y.kicProp.rad[yInd]),alpha=0.25,normed=1,bins=nbins,label=yLabel,range=[-1.0,3.0])
zz,zz,zz=plt.hist(np.log10(z.kicProp[zInd].rad),alpha=0.25,normed=1,bins=nbins,label=zLabel,range=[-1.0,3.0])
plt.xlabel(r'$\log_{10} R_{\odot}$')

plt.subplot(3,2,4)

massX=ex.radlogg2mass(x.kicProp[xInd].rad,x.kicProp[xInd].logg)
zz,zz,zz=plt.hist(massX,alpha=0.25,normed=1,bins=nbins,label=xLabel,range=[0.0,3.0])

#massY=ex.radlogg2mass(y.kicProp[yInd].rad,y.kicProp[yInd].logg)
#zz,zz,zz=plt.hist(massY,alpha=0.25,normed=1,bins=nbins,label=yLabel,range=[0.0,3.0])

try:
	massZ=z.kicProp.mass
except AttributeError:
	massZ=ex.radlogg2mass(z.kicProp[zInd].rad,z.kicProp[zInd].logg)

zz,zz,zz=plt.hist(massZ,alpha=0.25,normed=1,bins=nbins,label=zLabel,range=[0.0,3.0])
plt.xlabel(r'$M_{\odot}$')

plt.subplot(3,2,5)
zz,zz,zz=plt.hist(x.kicProp.logz[xInd],alpha=0.25,normed=1,bins=nbins,label=xLabel,range=[-3.0,1.0])
#zz,zz,zz=plt.hist(y.kicProp.logz[yInd],alpha=0.25,normed=1,bins=nbins,label=yLabel,range=[-3.0,1.0])
zz,zz,zz=plt.hist(z.kicProp.logz[zInd],alpha=0.25,normed=1,bins=nbins,label=zLabel,range=[-3.0,1.0])
plt.xlabel(r'$\log z$')

plt.subplot(3,2,6)
zz,zz,zz=plt.hist(x.kicProp.kp[xInd],alpha=0.25,normed=1,bins=nbins,label=xLabel,range=[7.0,17.0])
#zz,zz,zz=plt.hist(y.kicProp.kp[yInd],alpha=0.25,normed=1,bins=nbins,label=yLabel,range=[7.0,17.0])
zz,zz,zz=plt.hist(z.kicProp.kp[zInd],alpha=0.25,normed=1,bins=nbins,label=zLabel,range=[7.0,17.0])
plt.xlabel(r'$Kp$')

plt.tight_layout()
plt.show()
