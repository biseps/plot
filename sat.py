import constants as c
import numpy as np
import matplotlib.pyplot as plt 
import sys as sys
#Calcs the overflow from saturated pixels for the whole ccd
plt.figure()
plt.subplot(1,2,1)
#Convert ccd data which is in mags to flux in e- per sec to e- 
ccd=np.load('1.npy')
plt.imshow(ccd,cmap='jet_r',origin='lower',interpolation='nearest')
plt.colorbar()

ccd=(ccd/(10**(-0.4*12.0)))*c.f12*c.tint

#Now calc where we overflow charge and move half the overflow up/down the column
z=0
#while np.any(ccd[(ccd>c.wellDepth)]):
	#z=z+1
	#print z,np.count_nonzero(ccd[(ccd>c.wellDepth)])
	#for i in xrange(c.numCols):
		#for j in xrange(c.numRows):
			#if ccd[j,i] > c.wellDepth:
				##print "*",j,i
				#tmp=(ccd[j,i]-c.wellDepth)/2.0
				#ccd[j,i]=ccd[j,i]-2.0*tmp
				#for k in xrange(j+1,c.numRows):
					#if ccd[k,i] < c.wellDepth:
						#ccd[k,i]=ccd[k,i]+tmp
						#break
				#for k in xrange(j-1,0,-1):
					#if ccd[k,i] < c.wellDepth:
						#ccd[k,i]=ccd[k,i]+tmp
						#break
					
##convert back to flux
#ccd=ccd/(c.f12*c.tint)
#ccd=ccd*(10**(-0.4*12.0))
##plt.subplot(1,2,2)
##plt.imshow(ccd,cmap='jet_r',origin='lower',interpolation='nearest')
##plt.colorbar()
##plt.show()
#np.save('1.sat.npy',ccd)

z=0
while np.any(ccd[(ccd>c.wellDepth)]):
	z=z+1
	print z,np.count_nonzero(ccd[(ccd>c.wellDepth)])
	sortccd=np.sort(ccd,axis=None,kind='heapsort')
	x,y=np.nonzero(ccd==sortccd[-1])
	#print x,y,x[0],y[0],np.shape(ccd)
	diffUp=(ccd[x,y]-c.wellDepth)/2.0
	diffDown=(ccd[x,y]-c.wellDepth)/2.0
	ccd[x,y]=c.wellDepth
	#Go up
	for k in xrange(x[0]+1,np.shape(ccd)[0]):
		if diffUp<=0.0:
			break
		#Can take all overflow
		if ccd[k,y]+diffUp <= c.wellDepth:
			ccd[k,y]=ccd[k,y]+diffUp
			break
		#cant take any overflow
		elif ccd[k,y] > c.wellDepth:
			#Pick up extra flux 
			diffUp=diffUp+(ccd[k,y]-c.wellDepth)
			ccd[k,y]=c.wellDepth
			continue
		#can take some overflow
		else:
			diffUp=ccd[k,y]+diffUp-c.wellDepth			
			ccd[k,y]=c.wellDepth
	#Go down
	for k in xrange(x[0]-1,-1,-1):
		if diffDown<=0.0:
			break
		#Can take all overflow
		if ccd[k,y]+diffDown < c.wellDepth:
			ccd[k,y]=ccd[k,y]+diffDown
			break
		#cant take any overflow
		elif ccd[k,y] > c.wellDepth:
			#Pick up extra flux 
			diffDown=diffDown+(ccd[k,y]-c.wellDepth)
			ccd[k,y]=c.wellDepth
			continue
		#can take some overflow
		else:
			diffDown=ccd[k,y]+diffDown-c.wellDepth	
			ccd[k,y]=c.wellDepth
		
#convert back to flux
ccd=ccd/(c.f12*c.tint)
ccd=ccd*(10**(-0.4*12.0))
plt.subplot(1,2,2)
plt.imshow(ccd,cmap='jet_r',origin='lower',interpolation='nearest')
plt.colorbar()
plt.show()		
np.save('1.sat.npy',ccd)		
		
		
		
		
		
		


