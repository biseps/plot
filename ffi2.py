import fortUtil as f
import numpy as np
import biseps as b
import kepprf as kep
import matplotlib as mat
import constants as c
import getopt
import sys as sys


opts, extra = getopt.getopt(sys.argv[1:],'')
	
#f2py -c -m --opt="-Wno-all -march=native -msse3 -mtune=native -O3" fortUtil fortUtil.f90 > /dev/null && time python test.py

#extra=['1','2','../pop_s_y','../pop_s_o','../pop_b_y','../pop_b_o']

prfAll=np.zeros([c.numRows,c.numCols],dtype='float')
version=extra[1]

for fin in extra[2:]:
	#print extra[0],fin+'/'+str(extra[0])+'/mag.dat.'+version
	print fin
	x=b.readFile()
	isKic=0
	if isKic==0:
		#BIseps output
		x.appMag5('mag.'+fin)
		#x.kicProp('kic.'+fin)
		row,col=f.lonlat2pix(x.appMag.long,x.appMag.lat,np.size(x.appMag.long),extra[0],c.season,c.fovDir)
	elif isKic==1:
		#Kepelr data
		x.kicProp3('kic.dat.'+str(extra[0]))
		row,col=f.lonlat2pix(x.kicProp.long,x.kicProp.lat,np.size(x.kicProp.long),extra[0],c.season,c.fovDir)
	else:
		#KIC data
		x.kicProp5('kic.dat.1',sep=" ")
		row,col=f.lonlat2pix(x.kicProp.long,x.kicProp.lat,np.size(x.kicProp.long),extra[0],c.season,c.fovDir)

	prfAll=prfAll+kep.prfImage(row,col,x.appMag.kp,extra[0])
			
np.save(extra[0]+'.2',prfAll)


#x=b.readFile()
#x.readFOV2('fov/3/1.a.mag')
#prfAll=kp.prfImage(x.fov.row,x.fov.col,x.fov.mag,1)
#np.save('kicIn/1.npy',prfAll)
