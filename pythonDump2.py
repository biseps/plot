import numpy as np
import matplotlib.pyplot as plt
b=(10.0/360.0)*np.pi
sinb=np.sin(np.abs(b))
kr=0.88*0.001
hd=150.0
x=np.arange(0.0,50000,1.0)
y=np.zeros(np.size(x),dtype='float64')
y=(hd*kr/(sinb*1.0))*(1.0-np.exp(-x*sinb/(1.0*hd)))
plt.plot(x,y)
plt.show()


import numpy as np
import matplotlib.pyplot as plt
import biseps as b

x=b.readFile()
mydescr = np.dtype([('dmin','float'),('dmin2','float'),('dmax','float'),('dmax2','float')])
y=x.read_array(filename='dis.txt', dtype=mydescr,size=4)

#plt.hist(np.log10(y.dmax)-np.log10(y.dmin),range=[0.0,3],alpha=0.5,label='Us',bins=100,normed=1)
#plt.hist(np.log10(y.dmax2)-np.log10(y.dmin2),range=[0.0,3],alpha=0.5,label='Kic',bins=100,normed=1)
#plt.legend(loc=0)
#plt.show()

plt.figure(1)
plt.subplot(2,1,1)
plt.hist(np.log10(y.dmin),range=[0.0,np.log10(50000)],alpha=0.5,label='Us',bins=100,normed=1)
plt.hist(np.log10(y.dmin2),range=[0.0,np.log10(50000)],alpha=0.5,label='Kic',bins=100,normed=1)
plt.legend(loc=0)

plt.subplot(2,1,2)
plt.hist(np.log10(y.dmax),range=[0.0,np.log10(50000)],alpha=0.5,label='Us',bins=100,normed=1)
plt.hist(np.log10(y.dmax2),range=[0.0,np.log10(50000)],alpha=0.5,label='Kic',bins=100,normed=1)
plt.legend(loc=0)
plt.show()

#####################
import numpy as np
import matplotlib.pyplot as plt
import biseps as b

#y=b.readFile()
#y.kepler('kep.dat.1')

x=b.readFile()
x.kic5(file='kic.r3.out')

fig=plt.figure(1)
plt.subplot(3,2,1)
plt.hist(x.kic5.mass[(x.kic5.mass < 999999.9)],bins=100,normed=1)
plt.xlabel('Mass')

plt.subplot(3,2,2)
plt.hist(np.log10(x.kic5.rad[(x.kic5.rad < 999999.9)]),bins=100,normed=1)
plt.xlabel('log10 Radius')

plt.subplot(3,2,3)
plt.hist(x.kic5.teff[(x.kic5.teff < 999999.9)],bins=100,normed=1)
plt.xlabel('Teff')


plt.subplot(3,2,5)
#app mag
plt.hist(x.kic5.logg[(x.kic5.logg < 999999.9)],bins=100,normed=1)
plt.xlabel('Log g')

plt.subplot(3,2,6)
#app mag
plt.hist(x.kic5.feh[(x.kic5.feh < 999999.9)],bins=100,normed=1)
plt.xlabel('feh')
plt.show()

#################################################
import numpy as np
import matplotlib.pyplot as plt
import biseps as b
from matplotlib import rc

#plt.ion()
rc('text', usetex=True)
rc('font', family='serif')

#y=b.readFile()
#y.kepler('kep.dat.1')

x=b.readFile()
x.appMag4(file='sing.makeAllMags.1')
y=b.readFile()
y.appMag4(file='bin.makeAllMags.1')
a=b.readFile()
a.appMag2(file='kic.mag.out')

alpha=0.33

fig=plt.figure(1)
fig.text(.5, 0.95,r'KIC extinction comparing KIC to us for $8 \leq m_{kp} \leq 16$',
horizontalalignment='center',verticalalignment='top')
plt.subplot(3,3,1)
(c,d,e)=plt.hist(a.appMag.u[(a.appMag.u<99.9)],bins=100,normed=1,label='KIC',range=[0.0,25.0],alpha=alpha)
(c,d,e)=plt.hist(x.appMag.u,bins=100,normed=1,label='Single',range=[0.0,25.0],alpha=alpha)
(c,d,e)=plt.hist(y.appMag.u,bins=100,normed=1,label='Binary',range=[0.0,25.0],alpha=alpha)

plt.xlabel('U')
plt.legend(loc=0)

plt.subplot(3,3,2)
(c,d,e)=plt.hist(a.appMag.g[(a.appMag.g<99.9)],bins=100,normed=1,label='KIC',range=[0.0,20.0],alpha=alpha)
(c,d,e)=plt.hist(x.appMag.g,bins=100,normed=1,range=[0.0,25.0],alpha=alpha)
(c,d,e)=plt.hist(y.appMag.g,bins=100,normed=1,range=[0.0,25.0],alpha=alpha)

plt.xlabel('g')

plt.subplot(3,3,3)
(c,d,e)=plt.hist(a.appMag.r[(a.appMag.r<99.9)],bins=100,normed=1,label='KIC',range=[0.0,25.0],alpha=alpha)
(c,d,e)=plt.hist(x.appMag.r,bins=100,normed=1,range=[0.0,25.0],alpha=alpha)
(c,d,e)=plt.hist(y.appMag.r,bins=100,normed=1,range=[0.0,25.0],alpha=alpha)

plt.xlabel('r')

plt.subplot(3,3,4)
(c,d,e)=plt.hist(a.appMag.i[(a.appMag.i<99.9)],bins=100,normed=1,label='KIC',range=[0.0,25.0],alpha=alpha)
(c,d,e)=plt.hist(x.appMag.i,bins=100,normed=1,range=[0.0,25.0],alpha=alpha)
(c,d,e)=plt.hist(y.appMag.i,bins=100,normed=1,range=[0.0,25.0],alpha=alpha)

plt.xlabel('i')

plt.subplot(3,3,5)
(c,d,e)=plt.hist(a.appMag.z[(a.appMag.z<99.9)],bins=100,normed=1,label='KIC',range=[0.0,25.0],alpha=alpha)
(c,d,e)=plt.hist(x.appMag.z,bins=100,normed=1,range=[0.0,25.0],alpha=alpha)
(c,d,e)=plt.hist(y.appMag.z,bins=100,normed=1,range=[0.0,25.0],alpha=alpha)

plt.xlabel('z')

plt.subplot(3,3,6)
(c,d,e)=plt.hist(a.appMag.d51[(a.appMag.d51<99.9)],bins=100,normed=1,label='KIC',range=[0.0,25.0],alpha=alpha)
(c,d,e)=plt.hist(x.appMag.d51,bins=100,normed=1,range=[0.0,25.0],alpha=alpha)
(c,d,e)=plt.hist(y.appMag.d51,bins=100,normed=1,range=[0.0,25.0],alpha=alpha)

plt.xlabel('d51')

plt.subplot(3,3,7)
(c,d,e)=plt.hist(a.appMag.jj[(a.appMag.jj<99.9)],bins=100,normed=1,label='KIC',range=[0.0,20.0],alpha=alpha)
(c,d,e)=plt.hist(x.appMag.jj,bins=100,normed=1,range=[0.0,20.0],alpha=alpha)
(c,d,e)=plt.hist(y.appMag.jj,bins=100,normed=1,range=[0.0,20.0],alpha=alpha)

plt.xlabel('jj')

plt.subplot(3,3,8)
(c,d,e)=plt.hist(a.appMag.jk[(a.appMag.jk<99.9)],bins=100,normed=1,label='KIC',range=[0.0,20.0],alpha=alpha)
(c,d,e)=plt.hist(x.appMag.jk,bins=100,normed=1,range=[0.0,20.0],alpha=alpha)
(c,d,e)=plt.hist(y.appMag.jk,bins=100,normed=1,range=[0.0,20.0],alpha=alpha)

plt.xlabel('jk')

plt.subplot(3,3,9)
(c,d,e)=plt.hist(a.appMag.jh[(a.appMag.jh<99.9)],bins=100,normed=1,label='KIC',range=[0.0,20.0],alpha=alpha)
(c,d,e)=plt.hist(x.appMag.jh,bins=100,normed=1,range=[0.0,20.0],alpha=alpha)
(c,d,e)=plt.hist(y.appMag.jh,bins=100,normed=1,range=[0.0,20.0],alpha=alpha)
plt.xlabel('jh')


plt.show()

#######################################################
import numpy as np
import matplotlib.pyplot as plt
import biseps as b
from matplotlib import rc

#plt.ion()
rc('text', usetex=True)
rc('font', family='serif')

#y=b.readFile()
#y.kepler('kep.dat.1')

x=b.readFile()
x.appMag4(file='all.makeAllMags.1')
a=b.readFile()
a.appMag3(file='kic.mag2.out')

alpha=0.33

fig=plt.figure(1)
fig.text(.5, 0.95,r'Different Kp calculations (hakkila ectinction)',
horizontalalignment='center',verticalalignment='top')

plt.subplot(2,2,1)
kep=np.zeros(np.size(x.appMag.u),dtype='float')
kep=0.1*x.appMag.g[(x.appMag.g-x.appMag.r <=0.8)]+0.9*x.appMag.r[(x.appMag.g-x.appMag.r <=0.8)]
kep=0.2*x.appMag.g[(x.appMag.g-x.appMag.r >0.8)]+0.8*x.appMag.r[(x.appMag.g-x.appMag.r >0.8)]

(c,d,e)=plt.hist(a.appMag.kp[a.appMag.kp<99.9],bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='KIC')
(c,d,e)=plt.hist(x.appMag.kp,bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='Us')
(c,d,e)=plt.hist(kep,bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='Us (g,r)')
plt.xlabel('Kp')
plt.legend(loc=0)

plt.subplot(2,2,2)
kep=np.zeros(np.size(x.appMag.u),dtype='float')
kep=0.55*x.appMag.g[(x.appMag.g-x.appMag.i <=-0.05)]+0.45*x.appMag.i[(x.appMag.g-x.appMag.i <=-0.05)]
kep=0.3*x.appMag.g[(x.appMag.g-x.appMag.i >-0.05)]+0.7*x.appMag.i[(x.appMag.g-x.appMag.i >-0.05)]

(c,d,e)=plt.hist(a.appMag.kp[a.appMag.kp<99.9],bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='KIC')
(c,d,e)=plt.hist(x.appMag.kp,bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='Us')
(c,d,e)=plt.hist(kep,bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='Us (g,i)')
plt.xlabel('Kp')
plt.legend(loc=0)

plt.subplot(2,2,3)
kep=np.zeros(np.size(x.appMag.u),dtype='float')
kep=0.65*x.appMag.r[(x.appMag.r-x.appMag.i <=0.673)]+0.35*x.appMag.i[(x.appMag.r-x.appMag.i <=0.673)]
kep=1.2*x.appMag.r[(x.appMag.r-x.appMag.i >0.673)]-0.2*x.appMag.i[(x.appMag.r-x.appMag.i >0.673)]

(c,d,e)=plt.hist(a.appMag.kp[a.appMag.kp<99.9],bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='KIC')
(c,d,e)=plt.hist(x.appMag.kp,bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='Us')
(c,d,e)=plt.hist(kep,bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='Us (r,i)')
plt.xlabel('Kp')
plt.legend(loc=0)

plt.subplot(2,2,4)
kep=np.zeros(np.size(x.appMag.u),dtype='float')
kep=0.25*x.appMag.g[(x.appMag.g-x.appMag.r <=0.3)]+0.75*x.appMag.r[(x.appMag.g-x.appMag.r <=0.3)]
kep=0.3*x.appMag.g[(x.appMag.g-x.appMag.r >0.3)]+0.7*x.appMag.i[(x.appMag.g-x.appMag.r >0.3)]

(c,d,e)=plt.hist(a.appMag.kp[a.appMag.kp<99.9],bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='KIC')
(c,d,e)=plt.hist(x.appMag.kp,bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='Us')
(c,d,e)=plt.hist(kep,bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='Us (g,r,i)')
plt.xlabel('Kp')
plt.legend(loc=0)
plt.show()

