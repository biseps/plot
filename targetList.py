import fortUtil as f
import numpy as np
import biseps as b
import kepprf as kp
import matplotlib as mat
import matplotlib.pyplot as plt
import constants as c
import getopt
import sys as sys


opts, extra = getopt.getopt(sys.argv[1:],'') 

#f2py -c -m --opt="-Wno-all -march=native -msse3 -mtune=native -O3" fortUtil fortUtil.f90 > /dev/null && time python test.py


x=b.readFile()
ex=b.bfuncs()

ccd=extra[0]
ffiDir=extra[1]
version=extra[2]

try: 
	isKic=int(extra[3])
except IndexError:
	isKic=0


#ccd='1'
#ffiDir='/padata/beta/users/rfarmer/data/keplerFaint4/ffi/'
#version='1'
#ccd=1
#ffiDir='../../ffi/'
#ptype='pop_s_y'

#x.ranLoc('/padata/beta/users/rfarmer/data/keplerFaint/'+ptype+'/'+ccd+'/ranLoc.dat.1')
#x.ranLoc('ranLoc.dat.1')


if isKic==0:
	#BIseps output
	x.appMag5('mag.dat.perturb.'+version+'.kic')
	x.kicProp('kic.dat.'+version)
	prfAll=np.load(ffiDir+str(ccd)+'.'+version+'.npy')
	row,col=f.lonlat2pix(x.appMag.long,x.appMag.lat,np.size(x.appMag.long),ccd,c.season,c.fovDir)
elif isKic==1:
	#Kepelr data
	x.kicProp3('kic.dat.'+str(ccd))
	prfAll=np.load(ffiDir+str(ccd)+'.2a.npy')
	row,col=f.lonlat2pix(x.kicProp.long,x.kicProp.lat,np.size(x.kicProp.long),ccd,c.season,c.fovDir)
elif isKic==2:
	#Kepelr data
	x.appMag5('mag.new.dat')
	x.kicProp('kic.dat.'+version)
	prfAll=np.load(ffiDir+str(ccd)+'.'+version+'.npy')
	row,col=f.lonlat2pix(x.appMag.long,x.appMag.lat,np.size(x.appMag.long),ccd,c.season,c.fovDir)
else:
	#KIC data
	x.kicProp5('kic.dat.1')
	prfAll=np.load(ffiDir+str(ccd)+'.4.npy')
	row,col=f.lonlat2pix(x.kicProp.long,x.kicProp.lat,np.size(x.kicProp.long),ccd,c.season,c.fovDir)

#Add zodical emission
prfAll+=c.zod


#Get prf data
prfn, crpix1p, crpix2p, crval1p, crval2p, cdelt1p, cdelt2p,naxis1,naxis2=kp.getPrfDat(ccd)

minEarth=np.zeros(3,dtype='float')
#noise=np.zeros(1,dtype='float')

r=0.0
pix=0
noise=0.0

#x.kicProp.rad=np.log10(x.kicProp.rad)

#prog = ProgressBar(0, np.size(row), 77, mode='fixed', char='#')
#with open('target.dat.'+str(version),'w') as f:
with open('target.dat.'+version,'w') as f:
	for i in xrange(0,np.size(row)):
#	for i in xrange(629,630):
		if x.kicProp[i].kp <= 16.0:
			#Filter out bad kepler Prop points
			if x.kicProp[i].teff <0.0 or x.kicProp[i].logz < -99.99 or x.kicProp[i].logg< -99.99:
				f.write(str(i)+' -1\n')
				continue
			if (col[i]<0)or(row[i]<0)or(col[i]>=c.numCols)or(row[i]>=c.numRows):
				f.write(str(i)+' -2\n')
				continue
			if (x.kicProp[i].tefferr >50000 or x.kicProp[i].logzerr >9.0 or x.kicProp[i].loggerr >9.0):
				f.write(str(i)+' -3\n')
				continue
			#if isKic==0 and ((x.kicProp[i].tefferr < 1.0)&(x.kicProp[i].logzerr<0.001)&(x.kicProp[i].loggerr<0.001)&(x.kicProp[i].teff>6000.0)):
				#f.write(str(i)+' -8\n')
				#continue				
			if (x.kicProp[i].teff <1.0 or x.kicProp[i].kp<0.1):
				f.write(str(i)+' -4\n')
				continue
			if row[i] <=19 or row[i] > 1044 or col[i] <=12 or col[i] >1111:
				f.write(str(i)+' -7\n')
				continue

			a=np.zeros([c.numA,1])
			a=kp.calcA(x.kicProp[i].rad,x.kicProp[i].teff)		
			mass=ex.radlogg2mass(x.kicProp[i].rad,x.kicProp[i].logg)
			samples=kp.numSample(mass,x.kicProp[i].rad,a)
			trans=kp.numTransits(mass,a)
			#print row[i],col[i]
			isSat=0

			r,pix,noise,signal,bgFlux,isSat,pixX,pixY=kp.calcMinR(row[i],col[i],x.kicProp[i].kp,ccd,prfAll,prfn, crpix1p, crpix2p, crval1p, crval2p, cdelt1p, cdelt2p,naxis1,naxis2,i)
			
			tar=0
			minEarth[:]=0.0
			minEarth[0]=(x.kicProp[i].rad*c.rsun*np.sqrt((7.1*(noise/(np.sqrt(samples[0]*trans[0]))))/r))/c.rearth
			minEarth[1]=(x.kicProp[i].rad*c.rsun*np.sqrt((7.1*(noise/(np.sqrt(samples[1]*trans[1]))))/r))/c.rearth
			minEarth[2]=(x.kicProp[i].rad*c.rsun*np.sqrt((7.1*(noise/(np.sqrt(samples[2]*trans[2]))))/r))/c.rearth
			
			if trans[0] >= 3.0 and minEarth[0] <= 2.0 and x.kicProp[i].kp < 16.0:
				tar=13
			if trans[0] >= 3.0 and minEarth[0] <= 2.0 and x.kicProp[i].kp < 15.0:
				tar=12
			if trans[2] >= 3.0 and minEarth[2] <= 2.4 and x.kicProp[i].kp < 16.0:
			#if trans[2] >= 3.0 and minEarth[2] <= 2.0 and x.kicProp[i].kp < 16.0:
				tar=11
			if trans[2] >= 3.0 and minEarth[2] <= 2.4 and x.kicProp[i].kp < 15.0:
			#if trans[2] >= 3.0 and minEarth[2] <= 2.0 and x.kicProp[i].kp < 15.0:
				tar=10
			if trans[0] >= 3.0 and minEarth[0] <= 2.0 and x.kicProp[i].kp < 14.0:
				tar=9
			if trans[1] >= 3.0 and minEarth[1] <= 2.0 and x.kicProp[i].kp < 14.0:
				tar=8
			if trans[1] >= 3.0 and minEarth[1] <= 1.0 and x.kicProp[i].kp < 14.0:
				tar=7
			if trans[2] >= 3.0 and minEarth[2] <= 1.0 and x.kicProp[i].kp < 16.0:
				tar=6
			if trans[2] >= 3.0 and minEarth[2] <= 1.0 and x.kicProp[i].kp < 15.0:
				tar=5
			if trans[2] >= 3.0 and minEarth[2] <= 2.0 and x.kicProp[i].kp < 14.0:
				tar=4
			if trans[2] >= 3.0 and minEarth[2] <= 1.0 and x.kicProp[i].kp < 14.0:
				tar=3
			if trans[2] >= 3.0 and minEarth[2] <= 2.0 and x.kicProp[i].kp < 13.0:
				tar=2
			if trans[2] >= 3.0 and minEarth[2] <= 1.0 and x.kicProp[i].kp < 13.0:
				tar=1

				
			#Force all objects that saturate to be classified
			if tar ==0 and isSat==1:
				tar=14
				
			if (tar==0) and (x.kicProp[i].rad>3.0 and x.kicProp[i].rad <10.0) and (x.kicProp[i].kp <=14.0):
				tar=15

			if (tar==0 or tar==11 or tar==10) and trans[2] >= 3.0 and minEarth[2] <= 2.0 and x.kicProp[i].kp < 15.0:
				tar=16			
			if (tar==0 or tar==11 or tar==10) and trans[2] >= 3.0 and minEarth[2] <= 2.0 and x.kicProp[i].kp < 16.0:
				tar=17
				
			f.write(str(i)+' '+str(tar)+' \n')

			#prog.update_amount(i)
   			#print prog, '\r',
		else:
			f.write(str(i)+' -6\n')

			
f.close()

