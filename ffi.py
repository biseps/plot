import fortUtil as f
import numpy as np
import biseps as b
import kepprf as kep
import matplotlib as mat
import constants as c
import getopt
import sys as sys


opts, extra = getopt.getopt(sys.argv[1:],'')
	
#f2py -c -m --opt="-Wno-all -march=native -msse3 -mtune=native -O3" fortUtil fortUtil.f90 > /dev/null && time python test.py

#extra=['1','2','../pop_s_y','../pop_s_o','../pop_b_y','../pop_b_o']

prfAll=np.zeros([c.numRows,c.numCols],dtype='float')
version=extra[1]

isKic=int(extra[2])
print isKic

for fin in extra[3:]:
	#print extra[0],fin+'/'+str(extra[0])+'/mag.dat.'+version

	x=b.readFile()
	if isKic==0:
		#BIseps output
		x.appMag5(fin+'/'+str(extra[0])+'/mag.dat.perturb.'+version)
		#x.kicProp(fin+'/'+str(extra[0])+'/kic.dat.'+version)
		row,col=f.lonlat2pix(x.appMag.long,x.appMag.lat,np.size(x.appMag.long),extra[0],c.season,c.fovDir)
	elif isKic==1:
		#Kepelr data
		x.kicProp3('kic.dat.'+str(extra[0]))
		row,col=f.lonlat2pix(x.kicProp.long,x.kicProp.lat,np.size(x.kicProp.long),extra[0],c.season,c.fovDir)
	elif isKic==2:
		#Kepelr data
		x.appMag5('mag.new.dat')
		#x.kicProp('kic.dat.'+version)
		row,col=f.lonlat2pix(x.appMag.long,x.appMag.lat,np.size(x.appMag.long),extra[0],c.season,c.fovDir)
	else:
		#KIC data
		x.kicProp5('kic.dat.1',sep=" ")
		ind=(x.kicProp.kp>0.0)
		row,col=f.lonlat2pix(x.kicProp[ind].long,x.kicProp[ind].lat,np.size(x.kicProp[ind].long),extra[0],c.season,c.fovDir)

	prfAll=prfAll+kep.prfImage(row,col,x.appMag.kp,extra[0])
	
np.save(extra[0]+'.'+version,prfAll)


#x=b.readFile()
#x.readFOV2('fov/3/1.a.mag')
#prfAll=kp.prfImage(x.fov.row,x.fov.col,x.fov.mag,1)
#np.save('kicIn/1.npy',prfAll)
