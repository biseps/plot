#!/usr/bin/env python

import kepmsg
import numpy, scipy, pylab
import scipy.stats
from numpy import *
from pylab import *
from matplotlib import *

# -----------------------------------------------------------
# clean up x-axis of plot

def cleanx(time,logfile,verbose):

    status = 0
    try:
        time0 = float(int(time[0] / 100) * 100.0)
        if time0 < 2.4e6: time0 += 2.4e6
        timeout = time - time0
        label = 'BJD $-$ %d' % time0
    except:
        txt = 'ERROR -- KEPPLOT.CLEANX: cannot calculate plot scaling in x dimension'
        status = kepmsg.err(logfile,txt,verbose)
        label = ''

    return timeout, label, status

# -----------------------------------------------------------
# clean up y-axis of plot

def cleany(signal,cadence,logfile,verbose):

    status = 0
    try:
        signal /= cadence
	nrm = len(str(int(signal.max())))-1
	signal = signal / 10**nrm
	label = '10$^%d$ e$^-$ s$^{-1}$' % nrm
    except:
        txt = 'ERROR -- KEPPLOT.CLEANY: cannot calculate plot scaling in y dimension'
        status = kepmsg.err(logfile,txt,verbose)
        label = ''

    return signal, label, status

# -----------------------------------------------------------
# plot limits

def limits(x,y,logfile,verbose):

    status = 0
    try:
        xmin = x.min()
        xmax = x.max()
        ymin = y.min()
        ymax = y.max()
        xr = xmax - xmin
        yr = ymax - ymin
        x = insert(x,[0],[x[0]]) 
        x = append(x,[x[-1]])
        y = insert(y,[0],[0.0]) 
        y = append(y,0.0)
    except:
        txt = 'ERROR -- KEPPLOT.LIMITS: cannot calculate plot limits'
        status = kepmsg.err(logfile,txt,verbose)

    return x, y, xmin,  xmax, xr, ymin, ymax, yr, status

# -----------------------------------------------------------
# plot look and feel

def define(labelsize,ticksize,logfile,verbose):

    status = 0
    try:
        rc('text', usetex=True)
#        rc('font',**{'family':'sans-serif','sans-serif':['sans-serif']})
        params = {'backend': 'png',
                  'axes.linewidth': 2.5,
                  'axes.labelsize': labelsize,
                  'axes.font': 'sans-serif',
                  'axes.fontweight' : 'bold',
                  'text.fontsize': 12,
                  'legend.fontsize': 12,
                  'xtick.labelsize': ticksize,
                  'ytick.labelsize': ticksize}
        rcParams.update(params)
    except:
        pass

    return status

# -----------------------------------------------------------
# intensity scale limits of 1d array

def intScale1D(image,imscale):

    seterr(all="ignore") 
    nstat = 2; work2 = []
    work1 = array(sort(image),dtype=float32)
    for i in range(len(work1)):
        if 'nan' not in str(work1[i]).lower():
            work2.append(work1[i])
    work2 = array(work2,dtype=float32)
    if int(float(len(work2)) / 10 + 0.5) > nstat:
        nstat = int(float(len(work2)) / 10 + 0.5)
    zmin = median(work2[:nstat])
    zmax = median(work2[-nstat:])
    if imscale == 'logarithmic':
        if zmin < 0.0: zmin = 100.0
        image = log10(image)
        zmin = log10(zmin)
        zmax = log10(zmax)
    if (imscale == 'squareroot'):
        if zmin < 0.0: zmin = 100.0
        image = sqrt(image)
        zmin = sqrt(zmin)
        zmax = sqrt(zmax)

    return image, zmin, zmax

# -----------------------------------------------------------
# intensity scale limits of 2d array

def intScale2D(image,imscale):

    seterr(all="ignore") 
    nstat = 2
    work1 = numpy.array([],dtype='float32')
    (ysiz,xsiz) = numpy.shape(image)
    for i in range(ysiz):
        for j in range(xsiz):
            if numpy.isfinite(image[i,j]) and image[i,j] > 0.0:
                work1 = numpy.append(work1,image[i,j])
    work2 = array(sort(work1))
    if int(float(len(work2)) / 1000 + 0.5) > nstat:
        nstat = int(float(len(work2)) / 1000 + 0.5)
    zmin = median(work2[:nstat])
    zmax = median(work2[-nstat:])
    if imscale == 'logarithmic':
        image = log10(image)
        zmin = log10(zmin)
        zmax = log10(zmax)
    if (imscale == 'squareroot'):
        image = sqrt(image)
        zmin = sqrt(zmin)
        zmax = sqrt(zmax)

    return image, zmin, zmax
