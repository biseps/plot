

# In[]:

import numpy as np
from astropy.io import ascii 
from astropy.table import vstack
from astropy.table import Table
from astropy.table import join
import astropy.constants as const

import matplotlib
import matplotlib.pyplot as plt
import plotUtils as utils
import os

import seaborn as sns




# In[]:


# jktebop output layout
headers=[]
headers.append('id')
headers.append('porb')
headers.append('inclin')
headers.append('critical_inclin')
headers.append('trans_depth1')
headers.append('ellip_depth1')
headers.append('trans_depth2')
headers.append('ellip_depth2')
headers.append('noise')


# load data

# data_dir='/padata/beta/users/efarrell/data/plato/eb2_cluster/code/eb2/output'
data_dir='/padata/beta/users/efarrell/repos/BiSEPS_2.0/eb2_old/misc'
                                                         
# input_file = os.path.join(data_dir, 'transit_depths.dat.1')
# input_file = os.path.join(data_dir, 'pop_b_o_depths.csv')
input_file = os.path.join(data_dir, 'pop_b_y_depths.csv')
# input_file = os.path.join(data_dir, 'binaries.csv')
# input_file = os.path.join(data_dir, 'binaries.dat')

# transits  = ascii.read(input_file, delimiter=',', names=headers)
transitsy  = ascii.read(input_file, delimiter=',', names=headers)
# transits = ascii.read(input_file, names=headers)

output_dir = 'temp_output'


# In[]:

# x  = transits['inclin'].data
# y  = transits['trans_depth1'].data



# In[]:

# probability from R1 + R2 / a
x  = np.log(transits['porb'].data)
y  = np.cos(transits['critical_inclin'].data*np.pi/180)




# In[]:

# inclination on x axis
x  = transits['inclin'].data
y  = np.cos(transits['inclin'].data*np.pi/180)


# In[]:

min_depth=0.001
max_depth=0.01
mask1 = (transits['trans_depth1'].data > min_depth) & (transits['trans_depth1'].data < max_depth)
mask2 = (transits['trans_depth2'].data > min_depth) & (transits['trans_depth2'].data < max_depth)
total_mask = mask1 | mask2
small_transits = transits[total_mask]
x  = np.log(small_transits['porb'].data)
y  = np.cos(small_transits['critical_inclin'].data*np.pi/180)





# In[]:

# bar chart

# b1 = ax.bar(x, y, width=0.2) 

# ax.set_xlim([0,90])
# ax.set_ylim([0,1.2])

# ax.set_xlim([x.min(), x.max()])
# ax.set_ylim([y.min(), y.max()])


# utils.save_plot(fig, output_dir, 'allbinaries_smalldepth_close_bar')


# In[]:

# histogram

fig = plt.figure(figsize=(8, 8))
ax = plt.subplot(111)
ax.set_title('transit depths - histogram - relative frequency')
ax.grid(True, which='minor')
ax.set_xlabel('transit depth', fontsize=13)
ax.set_ylabel('frequency', fontsize=13)


# transit_depth 
# and probability from R1 + R2 / a

# x  = np.log(transits['trans_depth1'].data)
# x = x[np.logical_not(np.isnan(x))]

x  = transits['trans_depth1'].data
# y  = np.cos(transits['critical_inclin'].data*np.pi/180)


binsize=40
# binsize=100

# bartype='step'
bartype='bar'
# bartype='stepfilled'

do_normed=False
# do_normed=True

# ax.set_xlim([x.min(), x.max()])
# ax.set_xlim([0, 1])
# ax.set_ylim([0, 1])

# ax.hist(x, weights=np.zeros_like(x) + 1. / x.size, bins=binsize, histtype=bartype, normed=do_normed, lw=3, color='blue', alpha=0.5, label='run 1')
# ax.hist(x, bins=binsize, histtype=bartype, normed=do_normed, lw=3, color='blue', alpha=0.5, label='run 1')
# ax.hist(x, bins=binsize, log=True, histtype=bartype, normed=do_normed, lw=3, color='blue', alpha=0.5, label='run 1')

sns.kdeplot(x, shade=True);

utils.save_plot(fig, output_dir, 'transit_depths_histogram_relative_frequency')
# utils.save_plot(fig, output_dir, 'transit_depths_histogram')
# utils.save_plot(fig, output_dir, 'transit_depths_histogram_log')


# In[]:

# scatter plot

fig = plt.figure(figsize=(8, 8))
ax = plt.subplot(111)
ax.set_title('transit depths - histogram - relative frequency')
ax.grid(True, which='minor')
ax.set_xlabel('transit depth', fontsize=13)
ax.set_ylabel('frequency', fontsize=13)


# scatter plot

# ax.set_title(filename)
ax.scatter(x, y, s=5, lw=0, alpha=0.7)

# ax.scatter(x1, y1, s=10, marker='o',lw=0, alpha=0.5, color='red', label="depth < 0.1")
# ax.scatter(x2, y2, s=10, marker='>',lw=0, alpha=0.4, color='green', label="0.1  < depth < 0.2")
# ax.scatter(x3, y3, s=10, marker='^',lw=0, alpha=0.4, color='blue', label="0.2 < depth < 1")
# legend = ax.legend(loc='upper right', shadow=False)



# ax.set_xlim([x.min(), 4])
# ax.set_xlim([x.min(), x.max()])
# ax.set_xlim([x.min(), 20.4])

# ax.set_ylim([0,1.2])
# ax.set_ylim([y.min(), y.max()])

utils.save_plot(fig, output_dir, 'scatter')

# In[]:


# hex bin or 2d hist


title = ''
fig = plt.figure(figsize=(8, 8))
ax = plt.subplot(111)
ax.set_title(title)
ax.grid(True, which='minor')



# probability from R1 + R2 / a

# x  = np.log(transits['porb'].data)
# y  = np.cos(transits['critical_inclin'].data*np.pi/180)

x  = np.arange(10)
x1 = np.repeat(x, x, axis=0)
x2 = x1*3
# y  = (x+1) * 2

ax.set_xlabel('Orbital period', fontsize=13)
ax.set_ylabel('Probability', fontsize=13)

# histogram

# bartype='step'
# bartype='bar'
# bartype='stepfilled'

# ax.hist(x1, bins=11, histtype=bartype, normed=False, lw=3, color='blue', alpha=0.5, label='run 1')


# hexbin
gridsize=50
# gridsize=10

# ax.hexbin(x, y, C=z, gridsize=gridsize, cmap=CM.jet, bins=None)
im = ax.hexbin(x1, x2,  gridsize=gridsize, cmap="BuGn", bins=None)
cbar = fig.colorbar(im, ax=ax)

# 2d histo
# im = ax.hist2d(x, y, bins=100, cmap="BuGn" )

# cbar = fig.colorbar(im[3], ax=ax)
# cbar.set_label('counts')

# ax.axis([x.min(), x.max(), y.min(), y.max()])


plt.show()

# utils.save_plot(fig, output_dir, 'hexbin')


# In[]:

# seaborn kde plot

fig = plt.figure(figsize=(8, 8))
ax = plt.subplot(111)

ax.set_title('Distribution of transit depths ')
ax.grid(True, which='minor')


# for histogram
x  = transits['trans_depth1'].data
y  = transitsy['trans_depth1'].data

ax.set_xlabel('transit depth', fontsize=13)
ax.set_ylabel('density', fontsize=13)

pal = sns.blend_palette([sns.desaturate("royalblue", 0), "royalblue"], 5)

# pal = sns.dark_palette("palegreen", as_cmap=True)
# plt.figure(figsize=(6, 6))
# sns.kdeplot(sample, cmap=pal);
# sns.kdeplot(x, cmap=pal);

# sns.kdeplot(x, cmap=pal)
sns.kdeplot(x, shade=True, c=pal[0], ax=ax, label='old binaries')
sns.kdeplot(y, shade=True, c=pal[3], ax=ax, label='young binaries')
# utils.save_plot(fig, output_dir, 'kdeplot_jkt_popbo_hist')
# utils.save_plot(fig, output_dir, 'kdeplot_jkt_popby_hist')
utils.save_plot(fig, output_dir, 'kdeplot_jkt_popboth_hist')
# plt.show()


# In[]:


