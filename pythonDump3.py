import biseps as b
import numpy as np
import matplotlib.pyplot as plt

x=b.readFile()
x.kic3(file='kic.r1.out')

plt.scatter(x.kic3.long,x.kic3.lat)

plt.figure(1)
plt.subplot(2,2,1)
plt.hist(x.kic3.lat,bins=20,range=[5,25],normed=1)
plt.xlabel('Galactic Latitude, b [degrees]')

plt.subplot(2,2,2)
plt.hist(x.kic3.long,bins=20,range=[65,85],normed=1)
plt.xlabel('Galactic Longitude, l [degrees]')

plt.subplot(2,2,3)
#app mag
plt.hist(x.kic3.kp,bins=32,range=[8,16],normed=1)
plt.xlabel('m_kp')

plt.show()

#plt.savefig('kic.png')



import biseps as b
import numpy as np
import matplotlib.pyplot as plt

x=b.readFile()
x.kic3(file='kic.r1.out')
y=b.readFile()
y.ranLoc('all.0.5')

plt.figure(1)
plt.subplot(2,2,1)
plt.hist(x.kic3.lat,bins=20,range=[5,25],normed=1,alpha=0.5,label='KIC')
plt.xlabel('Galactic Latitude, b [degrees]')

plt.subplot(2,2,2)
plt.hist(x.kic3.long,bins=20,range=[65,85],normed=1,alpha=0.5)
plt.xlabel('Galactic Longitude, l [degrees]')

plt.subplot(2,2,3)
#app mag
plt.hist(x.kic3.kp,bins=32,range=[8,16],normed=1,alpha=0.5)
plt.xlabel('m_kp')


plt.subplot(2,2,1)
plt.hist(y.ranLoc.lat,bins=20,range=[5,25],normed=1,alpha=0.5,label='Us')
plt.legend()

plt.subplot(2,2,2)
plt.hist(y.ranLoc.long,bins=20,range=[65,85],normed=1,alpha=0.5)

plt.subplot(2,2,3)
#app mag
plt.hist(y.ranLoc.appMag,bins=32,range=[8,16],normed=1,alpha=0.5)
plt.show()

######################################################
import biseps as b
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as m
from mpl_toolkits.mplot3d import Axes3D

a=b.readFile()
a.readExitTest('hakkila.txt')
fig=plt.figure()

cm = plt.get_cmap('jet_r')

ind=(a.extTest.long >0)

norm=m.colors.Normalize(vmin=0.0,vmax=np.max(a.extTest.ext[ind]))

x=np.arange(5.0,25.1,0.5)
y=np.arange(65.0,85.1,0.5)
z=np.arange(1.0,50001.0,100.0)


#z=a.extTest.dist[ind].reshape(np.size(y),np.size(x))


ax = Axes3D(fig)
ax.scatter(x, y, z, cmap=cm, norm=norm)

plt.show()


#plt.scatter(a.extTest.lat[(a.extTest.long < 65.1)],np.log10(a.extTest.dist[(a.extTest.long < 65.1)]),a.extTest.ext[(a.extTest.long < 65.1)],cmap=cm,norm=norm)

cb=m.colorbar.ColorbarBase(ax2,cmap=cm,norm=norm,orientation='vertical',format='%5.2E')
plt.show()

##################################
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np

fig = plt.figure()
ax = Axes3D(fig)
X = np.arange(-5, 5, 0.25)
Y = np.arange(-5, 5, 0.25)
X, Y = np.meshgrid(X, Y)
R = np.sqrt(X**2 + Y**2)
Z = np.sin(R)
surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.jet,linewidth=0, antialiased=False)
#ax.set_zlim(-1.01, 1.01)

#ax.zaxis.set_major_locator(LinearLocator(10))
#ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

fig.colorbar(surf, shrink=0.5, aspect=5)

plt.show()

#############################################

import biseps as b

x=b.plot()
files={'Orig':'test.2','New':'test.1'}
x.plotAllKpMags('kic.r1.out',files)


files={'Kroupa':'kroupa.ranLoc.1','Log':'lognorm.ranLoc.1','exp':'exp.ranLoc.1'}
import biseps as b

x=b.plot()
files={'Kroupa':'kroupa.ranLoc.1','Log':'lognorm.ranLoc.1','exp':'exp.ranLoc.1'}
x.plotAllKpMags2('kic.r1.out',files)

import biseps as b

x=b.plot()
files={'Hakkila':'keplerHak2/merge/keplerHak2.ranLoc.dat.1','Drimmel':'keplerDrimmel/merge/keplerDrimmel.ranLoc.dat.1','Kepler':'keplerExt/merge/keplerExt.ranLoc.dat.1'}
x.plotAllKpMags2('kic.r1.out',files)


################
import biseps as b

x=b.plot()
x.plotAllExt('extTest.txt')
#####################
import biseps as b
import numpy as np
import matplotlib.pyplot as plt

inDir=['y_r','y_z','o_r','o_z']
plt.figure(1)

for j in range(0,4):
	plt.subplot(2,2,j+1)
	for i in range(0,4):
		arr=b.readFile()
		arr.ranLoc(inDir[j]+'/'+str(i+1)+'/all.ranLoc.1')
		(x,y,z)=plt.hist(arr.ranLoc.appMag,bins=32,range=[8,16],normed=1,alpha=0.0)
		bins=np.zeros(np.size(y),dtype='float64')
		for k in range(0,np.size(x)-1):
			bins[k]=(x[k]+x[k+1])/2.0
		plt.plot(y,bins,label=str(i+1))
		
	plt.legend(loc=0)

plt.show()


#########################

import numpy as np
import matplotlib.pyplot as plt


y_r_x=[2500,2650,2800,2950,3100]
y_r_y=[2817,3028,3211,3365,3494]

y_z_x=[200,250,300,350,400]
y_z_y=[2288,2793,3211,3554,3835]

o_r_x=[3400,3550,3700,3850,4000]
o_r_y=[534,547,556,564,569]

o_z_x=[800,900,1000,1100,1200]
o_z_y=[557,558,556,552,547]

plt.figure(1)
plt.subplot(1,1,1)
plt.xlabel('Scale Length')
plt.ylabel('# of stars')

plt.subplot(2,2,1)
plt.plot(y_r_x,y_r_y)
plt.title('Young radial')

plt.subplot(2,2,2)
plt.plot(y_z_x,y_z_y)
plt.title('Young z')

plt.subplot(2,2,3)
plt.plot(o_r_x,o_r_y)
plt.title('Old radial')

plt.subplot(2,2,4)
plt.plot(o_z_x,o_z_y)
plt.title('Old z')

plt.show()

#############################
import biseps as b
import numpy as np
import matplotlib as m
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rc
rc('text', usetex=True)
rc('font', family='serif')


fig=plt.figure(1)
ax = Axes3D(fig)
a=b.readFile()
a.readExitTest('stellDen.txt')

longIn=75.708484
dmax=50000.0
alpha=0.5
rstride=1
cstride=1

cm = plt.get_cmap('jet_r')

ind=(a.extTest.long > 72.859939) & (a.extTest.long < 72.859941) & (a.extTest.dist < dmax)

xArr=a.extTest.lat[ind]
c=np.nonzero(np.ediff1d(xArr))
xDiff=xArr[c[0][1]]-xArr[c[0][0]]
x=np.arange(np.min(xArr),np.max(xArr)+0.1*xDiff,xDiff)

yArr=a.extTest.dist[ind]
c=np.nonzero(np.ediff1d(yArr))
yDiff=yArr[c[0][1]]-yArr[c[0][0]]
y=np.arange(np.min(yArr),np.max(yArr)+0.1*yDiff,yDiff)

zArr=a.extTest.ext[ind]
z=zArr.reshape(np.size(x),np.size(y))
y,x=np.meshgrid(y,x)

norm=m.colors.Normalize(vmin=0.0,vmax=np.max(yArr))
surf=ax.plot_surface(y,x,z,cmap=cm,linewidth=0, antialiased=False,alpha=alpha,rstride=rstride,cstride=cstride)

ax.set_ylabel('Latitude, (b [degrees])')
ax.set_xlabel('Distance, (d [pc])')
ax.set_zlabel(r'Extinction, ($A_{Kp}$ [mag])')

fig.colorbar(surf,shrink=0.5,aspect=5)
plt.show()

#################
import biseps as b
x=b.extras()
x.writeKepler('kroupa','k')
x.writeKepler('lognormal','l')
x.writeKepler('exp','e')

######################
import biseps as b

x=b.plot()
files={'Hakkila':'keplerHak2/merge/keplerHak2.ranLoc.dat.1','Drimmel':'keplerDrimmel/merge/keplerDrimmel.ranLoc.dat.1','Kepler':'keplerExt/merge/keplerExt.ranLoc.dat.1'}
x.plotAllKpMags2('kic.r1.out',files)

x.plotCompDist('keplerDrimmel/merge/keplerDrimmel.ranLoc.dat.1','kic.r1.out')




