#from pyraf import iraf
import pylab, numpy, pyfits
from pylab import *
from matplotlib import *
from numpy import *
from pyfits import *
import kepio, kepmsg, kepplot, keparray
import sys, time, re, math, glob


def kepprf(infile,rownum,prfdir,imscale,colmap,verbose,logfile,status): 
#imsclae = 'logarithmic' or 'squareroot'

# input arguments

    status = 0
    seterr(all="ignore") 

# log the call 

    hashline = '----------------------------------------------------------------------------'
    kepmsg.log(logfile,hashline,verbose)
    call = 'KEPPRF -- '
    call += 'infile='+infile+' '
    call += 'rownum='+str(rownum)+' '
    call += 'prfdir='+prfdir+' '
    call += 'imscale='+imscale+' '
    call += 'colmap='+colmap+' '
    chatter = 'n'
    if (verbose): chatter = 'y'
    call += 'verbose='+chatter+' '
    call += 'logfile='+logfile
    kepmsg.log(logfile,call+'\n',verbose)

# start time

    kepmsg.clock('KEPPRF started at',logfile,verbose)

# test log file

    logfile = kepmsg.test(logfile)

# reference color map

    if colmap == 'browse':
        status = cmap_plot()

# open TPF FITS file

    if status == 0:
        kepid, channel, skygroup, module, output, quarter, season, \
            ra, dec, column, row, kepmag, xdim, ydim, barytime, status = \
            kepio.readTPF(infile,'TIME',logfile,verbose)
    if status == 0:
        kepid, channel, skygroup, module, output, quarter, season, \
            ra, dec, column, row, kepmag, xdim, ydim, tcorr, status = \
            kepio.readTPF(infile,'TIMECORR',logfile,verbose)
    if status == 0:
        kepid, channel, skygroup, module, output, quarter, season, \
            ra, dec, column, row, kepmag, xdim, ydim, cadno, status = \
            kepio.readTPF(infile,'CADENCENO',logfile,verbose)
    if status == 0:
        kepid, channel, skygroup, module, output, quarter, season, \
            ra, dec, column, row, kepmag, xdim, ydim, fluxpixels, status = \
            kepio.readTPF(infile,'FLUX',logfile,verbose)
    if status == 0:
        kepid, channel, skygroup, module, output, quarter, season, \
            ra, dec, column, row, kepmag, xdim, ydim, errpixels, status = \
            kepio.readTPF(infile,'FLUX_ERR',logfile,verbose)
    if status == 0:
        kepid, channel, skygroup, module, output, quarter, season, \
            ra, dec, column, row, kepmag, xdim, ydim, qual, status = \
            kepio.readTPF(infile,'QUALITY',logfile,verbose)

# print target data

    if status == 0:
        print ''
        print '      KepID:  %s' % kepid
        print ' RA (J2000):  %s' % ra
        print 'Dec (J2000): %s' % dec
        print '     KepMag:  %s' % kepmag
        print '   SkyGroup:    %2s' % skygroup
        print '     Season:    %2s' % str(season)
        print '    Channel:    %2s' % channel
        print '     Module:    %2s' % module
        print '     Output:     %1s' % output
        print ''

# is this a good row with finite timestamp and pixels?

    if status == 0:
        if not numpy.isfinite(barytime[rownum]) or not numpy.isfinite(fluxpixels[rownum,ydim*xdim/2]):
            message = 'ERROR -- KEPPRF: Row ' + str(rownum) + ' is a bad quality timestamp'
            status = kepmsg.err(logfile,message,verbose)

# construct input pixel image

    if status == 0:
        flux = fluxpixels[rownum,:]
        errs = errpixels[rownum,:]

# image scale and intensity limits of pixel data

    if status == 0:
        flux_pl, zminfl, zmaxfl = kepplot.intScale1D(flux,imscale)
        
# construct output summed image

    if status == 0:
        imgflux = empty((ydim,xdim))
        sigma2 = empty((ydim,xdim))
        imgflux_pl = empty((ydim,xdim))
        n = 0
        for i in range(ydim):
            for j in range(xdim):
                imgflux[i,j] = flux[n]
                sigma2[i,j] = errs[n]**2
                imgflux_pl[i,j] = flux_pl[n]
                n += 1

# determine suitable PRF calibration file

    if status == 0:
        if int(module) < 10:
            prefix = 'kplr0'
        else:
            prefix = 'kplr'
        prfglob = prfdir + '/' + prefix + str(module) + '.' + str(output) + '*' + '_prf.fits'
        try:
            prffile = glob.glob(prfglob)[0]
        except:
            message = 'ERROR -- KEPPRF: No PRF file found in ' + prfdir
            status = kepmsg.err(logfile,message,verbose)

# read PRF images

    if status == 0:
        prfn = [0,0,0,0,0]
        crpix1p = numpy.zeros((5),dtype='float32')
        crpix2p = numpy.zeros((5),dtype='float32')
        crval1p = numpy.zeros((5),dtype='float32')
        crval2p = numpy.zeros((5),dtype='float32')
        cdelt1p = numpy.zeros((5),dtype='float32')
        cdelt2p = numpy.zeros((5),dtype='float32')
        for i in range(5):
            prfn[i], crpix1p[i], crpix2p[i], crval1p[i], crval2p[i], cdelt1p[i], cdelt2p[i], status \
                = kepio.readPRFimage(prffile,i+1,logfile,verbose)    

# interpolate the calibrated PRF shape to the target position

    if status == 0:
        prf = numpy.zeros(numpy.shape(prfn[0]),dtype='float32')
        prfWeight = numpy.zeros((5),dtype='float32')
        for i in range(5):
            prfWeight[i] = math.sqrt((column - crval1p[i])**2 + (row - crval2p[i])**2)
            if prfWeight[i] == 0.0:
                prfWeight[i] = 1.0e6
            prf = prf + prfn[i] / prfWeight[i]
            prf = prf / numpy.nansum(prf)

# interpolate PRF centroid to new pixel position

    if status == 0:
        prfModDim1 = ydim / cdelt1p[0]
        prfModDim2 = xdim / cdelt2p[0]
        prfY0 = (numpy.shape(prf)[0] - prfModDim1) / 2
        prfX0 = (numpy.shape(prf)[1] - prfModDim2) / 2
        prfMod = prf[prfY0:prfY0+prfModDim1,prfX0:prfX0+prfModDim2]

# rebin PRF model to detector pixels

    if status == 0:
        prfFit = keparray.rebin2D(prfMod, [ydim,xdim], 'linear', False, False)
        prfFit = prfFit / cdelt1p[0] / cdelt2p[0]

# prf fit derivative in y

    if status == 0:
        dy = numpy.zeros((ydim,xdim),dtype='float32')
        for m in range(ydim):
            for n in range(xdim):
                try:
                    dy[m,n] = prfFit[m+1,n] - prfFit[m,n]
                except:
                    dy[m,n] = prfFit[m,n] - prfFit[m-1,n]

# prf fit derivative in x

    if status == 0:
        dx = numpy.zeros((ydim,xdim),dtype='float32')
        for m in range(ydim):
            for n in range(xdim):
                try:
                    dx[m,n] = prfFit[m,n+1] - prfFit[m,n]
                except:
                    dx[m,n] = prfFit[m,n] - prfFit[m,n-1]

# initialize fit parameters (b1 = normalization, b2 = x, b3 = y)

    if status == 0:
        b1 = numpy.nansum(imgflux) / numpy.nansum(prfFit)
        b2 = 0.0
        b3 = 0.0
        f1 = b1 * prfFit
        f2 = b2 * dx
        f3 = b3 * dy
        f = f1 + f2 + f3
        chisum = numpy.nansum(numpy.square(imgflux - f) / sigma2)
        chi2 = chisum / (xdim * ydim - 3)
        chi2last = chi2 + 1.0e6
	
# calculate fit coefficients for new model
        
    if status == 0:
        cthresh = 1.0e-4
        safe = 0.3
        i = 0
        while (abs(chi2 - chi2last) > cthresh):
            chi2last = copy(chi2)
            i += 1
            if float(i) / 100 == float(int(i / 100)):
                safe = safe / 3
	    d1 = b1 * prfFit
	    d2 = b2 * dx
	    d3 = b3 * dy
	    b1sum = numpy.nansum((imgflux - d2 - d3) * prfFit / sigma2)
	    b2sum = numpy.nansum((imgflux - d1 - d3) * dx / sigma2)
	    b3sum = numpy.nansum((imgflux - d1 - d2) * dy / sigma2)
	    b1den = numpy.nansum(numpy.square(prfFit) / sigma2)
	    b2den = numpy.nansum(numpy.square(dx) / sigma2)
	    b3den = numpy.nansum(numpy.square(dy) / sigma2)
	     
# fit parameter convergence

            oldb1 = b1
            oldb2 = b2
            oldb3 = b3
            b1 = b1sum / b1den
            b2 = b2sum / b2den
            b3 = b3sum / b3den
            b1 = oldb1 + safe * (b1 - oldb1) 
            b2 = oldb2 + safe * (b2 - oldb2)
            b3 = oldb3 + safe * (b3 - oldb3)
            b1terr = 1.0 / sqrt(b1den)
            b2terr = 1.0 / sqrt(b2den)
            b3terr = 1.0 / sqrt(b3den)

# recalculate model and errors

	    f1 = b1 * prfFit
	    f2 = b2 * dx
	    f3 = b3 * dy
	    f = f1 + f2 + f3 
            chisum = numpy.nansum(numpy.square(imgflux - f) / sigma2)

# calculate reduced chi^2 and test for convergence

            chi2 = chisum / (xdim * ydim - 3)
            print i,b1,b2/b1,b3/b1,chi2

# resample PRF subimage with new x and y position

            prfY0 = int((numpy.shape(prf)[0] - prfModDim1) / 2 + b3 / b1 * 49.5)
            prfX0 = int((numpy.shape(prf)[1] - prfModDim2) / 2 + b2 / b1 * 49.5)
            prfMod = prf[prfY0:prfY0+prfModDim1,prfX0:prfX0+prfModDim2]

# rebin PRF model to detector pixels

            prfFit = keparray.rebin2D(prfMod, [ydim,xdim], 'linear', True, False)
            prfFit = prfFit / cdelt1p[0] / cdelt2p[0]

# prf fit derivative in y

            for m in range(ydim):
                for n in range(xdim):
                    try:
                        dy[m,n] = prfFit[m,n] - prfFit[m-1,n]
                    except:
                        dy[m,n] = prfFit[m+1,n] - prfFit[m,n]

# prf fit derivative in x

            for m in range(ydim):
                for n in range(xdim):
                    try:
                        dx[m,n] = prfFit[m,n] - prfFit[m,n-1]
                    except:
                        dx[m,n] = prfFit[m,n+1] - prfFit[m,n]

# image scale and intensity limits for PRF model image

    if status == 0:
        imgprf_pl, zminpr, zmaxpr = kepplot.intScale2D(prfMod,imscale)
        
# image scale and intensity limits for PRF fit image

    if status == 0:
        prfFit = prfFit * b1
        imgfit_pl, zminfi, zmaxfi = kepplot.intScale2D(prfFit,imscale)
        
# calculate residual between data and image

    if status == 0:
        prfRes = imgflux - prfFit

# image scale and intensity limits for data - fit residual

    if status == 0:
        imgres_pl, zminre, zmaxre = kepplot.intScale2D(prfRes,imscale)
        
# plot style

    if status == 0:
        try:
            params = {'backend': 'png',
                      'axes.linewidth': 2.5,
                      'axes.labelsize': 24,
                      'axes.font': 'sans-serif',
                      'axes.fontweight' : 'bold',
                      'text.fontsize': 12,
                      'legend.fontsize': 12,
                      'xtick.labelsize': 10,
                      'ytick.labelsize': 10}
            pylab.rcParams.update(params)
        except:
            pass
        pylab.figure(figsize=[10,10])
        pylab.clf()
        plotimage(imgflux_pl,zminfl,zmaxfl,1,row,column,xdim,ydim,0.06,0.52,'flux',colmap)
        plotimage(imgprf_pl,zminpr,zmaxpr*0.9,2,row,column,xdim,ydim,0.52,0.52,'model',colmap)
        plotimage(imgfit_pl,zminfl,zmaxfl,3,row,column,xdim,ydim,0.06,0.06,'fit',colmap)
        plotimage(imgres_pl,zminfl,zmaxfl,4,row,column,xdim,ydim,0.52,0.06,'residual',colmap)
            
# stop time

    kepmsg.clock('KEPPRF ended at',logfile,verbose)

    return

# -----------------------------------------------------------
# plot channel image

def plotimage(imgflux_pl,zminfl,zmaxfl,plmode,row,column,xdim,ydim,winx,winy,tlabel,colmap):

# pixel limits of the subimage

    ymin = row
    ymax = ymin + ydim
    xmin = column
    xmax = xmin + xdim

# plot limits for flux image

    ymin = float(ymin) - 0.5
    ymax = float(ymax) - 0.5
    xmin = float(xmin) - 0.5
    xmax = float(xmax) - 0.5

# plot the image window

    ax = pylab.axes([winx,winy,0.46,0.46])
    imshow(imgflux_pl,aspect='auto',interpolation='nearest',origin='lower',
           vmin=zminfl,vmax=zmaxfl,extent=(xmin,xmax,ymin,ymax),cmap=colmap)
    pylab.gca().set_autoscale_on(False)
    labels = ax.get_yticklabels()
    setp(labels, 'rotation', 90)
    pylab.gca().xaxis.set_major_formatter(pylab.ScalarFormatter(useOffset=False))
    pylab.gca().yaxis.set_major_formatter(pylab.ScalarFormatter(useOffset=False))
    if plmode == 1:
        pylab.setp(pylab.gca(),xticklabels=[])
    if plmode == 2:
        pylab.setp(pylab.gca(),xticklabels=[],yticklabels=[])
    if plmode == 4:
        pylab.setp(pylab.gca(),yticklabels=[])
    if plmode == 3 or plmode == 4:
        pylab.xlabel('Pixel Column Number', {'color' : 'k'})
    if plmode == 1 or plmode == 3:
        pylab.ylabel('Pixel Row Number', {'color' : 'k'})
    pylab.text(0.05, 0.93,tlabel,horizontalalignment='left',verticalalignment='center',
               fontsize=28,fontweight=500,transform=ax.transAxes)
    pylab.draw()

    return

# -----------------------------------------------------------
# these are the choices for the image colormap

def cmap_plot():

    pylab.figure(1,figsize=[5,10])
    ion()
    a=outer(ones(10),arange(0,1,0.01))
    subplots_adjust(top=0.99,bottom=0.00,left=0.01,right=0.8)
    maps=[m for m in cm.datad if not m.endswith("_r")]
    maps.sort()
    l=len(maps)+1
    for i, m in enumerate(maps):
        print m
        subplot(l,1,i+1)
        pylab.setp(pylab.gca(),xticklabels=[],xticks=[],yticklabels=[],yticks=[])
        imshow(a,aspect='auto',cmap=get_cmap(m),origin="lower")
        pylab.text(100.85,0.5,m,fontsize=10)
    ioff()
    status = 1
    return status