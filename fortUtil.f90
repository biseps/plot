SUBROUTINE lonLat2pix(lon,lat,n,fileNum,season,fovDir,row,column)
    IMPLICIT NONE

    INTEGER :: n,fileNum,lineCount,aerr,i,season
    DOUBLE PRECISION,DIMENSION(n) :: lon,lat,ra,dec,row,column,ras,decs

    !f2py INTENT(IN) :: lon,lat,n,fovDir
    !f2py INTENT(IN) :: fileNum,season
    !f2py INTENT(OUT) :: row,column
    !f2py depend(n) :: lon,lat,ra,dec,ras,decs

    DOUBLE PRECISION,DIMENSION(n) :: dra,ddec,drow,dcol
    INTEGER,DIMENSION(n) :: ind

    DOUBLE PRECISION,PARAMETER :: pi=3.141592654
    DOUBLE PRECISION,PARAMETER :: deg2rad=pi/180.0
    DOUBLE PRECISION,PARAMETER :: an=32.93192*deg2rad
    DOUBLE PRECISION,PARAMETER :: gpr = 192.8594*deg2rad
    DOUBLE PRECISION,PARAMETER :: gpd = 27.12825*deg2rad

    CHARACTER(len=2) :: num,seas
    CHARACTER(len=100) :: fin,fovDir
    CHARACTER(len=1000) :: BUFFER
    LOGICAL :: fileExists

    TYPE :: pixel
    DOUBLE PRECISION :: ra,dec,lon,lat,row,col
    END TYPE pixel

    TYPE(pixel),ALLOCATABLE,DIMENSION(:) :: pix

    ras=0.d0
    decs=0.d0
    ra=0.d0
    dec=0.d0

    DO i=1,n
        CALL nasa(lon(i),lat(i),ras(i),decs(i))
        CALL jprecess(ras(i),decs(i),ra(i),dec(i))
    END DO

    ! 	write(*,*) lon,lat,n
    ! 	write(*,*) ras,decs,ra,dec

    if (fileNum<10) write(num,'(i1)') fileNum
    if (fileNum<100)  write(num,'(i2)') fileNum

    write(seas,'(i1)') season

    !Convert ra dec to row column

    !Open fov file
    fin=fovDir(1:LEN_TRIM(fovDir))//trim(adjustl(seas))//"/"//trim(adjustl(num))//".ra"
    !write(*,*) fin,seas,num
    fileExists=.false.
    inquire(FILE=fin(1:LEN_TRIM(fin)),exist=fileExists)
    if(fileExists .eqv. .false.) then
        write(0,*) "No file ", fin
        stop
    end if
    OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')
        
    lineCount=0
    DO WHILE (.true.)
        read(1, '(A)', end=99) BUFFER
        lineCount=lineCount+1
    ENDDO
99   CONTINUE
    CLOSE(1)
    aerr=0

    ALLOCATE(pix(1:lineCount))
    IF (aerr > 0) THEN
        WRITE(0,*)
        WRITE(0,*) '1, ERROR ALLOCATING MEMORY!'
        WRITE(0,*)
        STOP 'Abnormal program termination!!!'
    END IF

    OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')
    DO i=1,lineCount
        READ(1,*) pix(i)%ra,pix(i)%dec,pix(i)%lon,pix(i)%lat,pix(i)%row,pix(i)%col
    END DO
    CLOSE(1)

    do i=1,n
    !     ind(i:i)=MINLOC(sqrt((abs(pix%ra-ra(i))/15.0/cos(pix%dec*pi/180.0))**2+abs(pix%dec-dec(i))**2))
    ind(i:i)=MINLOC(((abs(pix%ra-ra(i))/15.0/cos(pix%dec*pi/180.0))**2+(pix%dec-dec(i))**2))
    end do

    ! 	write(*,*) pix(ind-1)%lon,pix(ind-1)%lat,pix(ind-1)%row,pix(ind-1)%col
    ! 	write(*,*) pix(ind)%lon,pix(ind)%lat,pix(ind)%row,pix(ind)%col
    ! 	write(*,*) pix(ind+1)%lon,pix(ind+1)%lat,pix(ind+i)%row,pix(ind+i)%col

    !NUmbers from (in python)
    ! 	cd1_1 = 0.000702794927969
    ! 	cd1_2 = -0.000853190160515
    ! 	cd2_1 = -0.000853190160515
    ! 	cd2_2 = -0.000702794927969
    ! 	cd = np.array([[cd1_1,cd1_2],[cd2_1,cd2_2]])
    ! 	cd = np.linalg.inv(cd)

    dra = pix(ind)%ra - ra
    ddec =pix(ind)%dec - dec

    drow = 575.187244540854408115 * dra + (-698.274956130074201610) * ddec
    dcol = (-698.274956130074201610) * dra + (-575.187244540854408115) * ddec

    !pixel coordinates of the nearest KIC target

    row = pix(ind)%row
    column =pix(ind)%col

    !pixel coordinate of target
    row = nint(row + drow + 0.5)
    column = nint(column + dcol + 0.5)

    ! 	write(*,*) pix(ind)%row,row,dra,drow
    ! 	write(*,*) pix(ind)%col,column,ddec,dcol

!     WHERE((abs(drow) .gt. 2.5) .or. (abs(dcol) .gt. 2.5) )
!     row=-1
!     column=-1
!     END WHERE
    ! 	write(*,*) pix(ind)%row,row,dra,drow
    ! 	write(*,*) pix(ind)%col,column,ddec,dcol
    ! 	write(*,*) "***"

END SUBROUTINE lonLat2pix

SUBROUTINE nasa(gl,gb,ras,decs)
    IMPLICIT NONE
    DOUBLE PRECISION,INTENT(IN) :: gl,gb
    DOUBLE PRECISION,INTENT(OUT) :: ras,decs
    DOUBLE PRECISION,PARAMETER :: pi = 3.141592654d0,radeg=180.0/pi,rapol=12.0d0 + 49.0d0/60.0d0
    DOUBLE PRECISION,PARAMETER :: decpol=27.4d0
    DOUBLE PRECISION,PARAMETER :: dlon=123.0d0
    DOUBLE PRECISION,PARAMETER :: sdp=sin(decpol/radeg),cdp = sqrt(1.0d0-sdp*sdp),radhrs=radeg/15.0d0
    DOUBLE PRECISION :: sgb,cgb,sdec,cdec,sinf,cosf

    sgb = sin(gb/radeg)
    cgb = sqrt(1.0d0-sgb*sgb)
    sdec = sgb*sdp + cgb*cdp*cos((dlon-gl)/radeg)
    decs = radeg * asin(sdec)

    cdec = sqrt(1.0d0-sdec*sdec)
    sinf = cgb * sin((dlon-gl)/radeg) / cdec
    cosf = (sgb-sdp*sdec) / (cdp*cdec)
    ras = rapol + radhrs*atan2(sinf,cosf)
    ras = ras*15.0d0

    !     CALL jprecess(ras,decs,ra,dec)

END SUBROUTINE nasa

SUBROUTINE jprecess(ra,dec,ra_2000,dec_2000)
    IMPLICIT NONE
    DOUBLE PRECISION,INTENT(IN) :: ra,dec
    DOUBLE PRECISION,INTENT(OUT) :: ra_2000,dec_2000
    DOUBLE PRECISION,PARAMETER :: epoch = 1950.0d0,radeg=57.29577951,pi = 3.141592654d0
    DOUBLE PRECISION,PARAMETER :: sec_to_radian = 1.0/radeg/3600.0d0
    DOUBLE PRECISION,DIMENSION(1:6,1:6) :: M
    DOUBLE PRECISION,PARAMETER,DIMENSION(1:3) :: A=(/-1.62557d-6,-0.31919d-6,-0.13843d-6/)
    DOUBLE PRECISION,PARAMETER,DIMENSION(1:3) :: A_dot=(/1.244d-3,-1.579d-3,-0.660d-3/),r0_dot=(/0.0,0.0,0.0/)
    DOUBLE PRECISION,PARAMETER :: mu_a = 0.0d0,mu_d = 0.0d0

    DOUBLE PRECISION :: ra_rad,dec_rad,cosra,sinra,cosdec,sindec,t,x,y,z,r2,rmag
    DOUBLE PRECISION,DIMENSION(1:3) :: r0,rr,v,rr1,r1,r1_dot
    DOUBLE PRECISION,DIMENSION(1:6) :: R,R_1

    M=reshape((/0.9999256782,0.0111820610,0.0048579479,-0.000551,0.238514,-0.435623, &
    -0.0111820611,0.9999374784,-0.0000271474,-0.238565,-0.002667,0.012254,&
    -0.0048579477,-0.0000271765,0.9999881997,0.435739,-0.008541,0.002117,&
    0.00000242395018,0.00000002710663,0.00000001177656,0.99994704,0.01118251,0.00485767,&
    -0.00000002710663,0.00000242397878,-0.00000000006582,-0.01118251,0.99995883,-0.00002714,&
    -0.00000001177656,-0.00000000006587,0.00000242410173,-0.00485767,-0.00002718,1.00000956/),shape=(/6,6/))

    ra_rad = ra/radeg
    dec_rad = dec/radeg
    cosra =  cos( ra_rad )
    sinra = sin( ra_rad )
    cosdec = cos( dec_rad )
    sindec = sin( dec_rad )

    r0(1)=cosra*cosdec
    r0(2)=sinra*cosdec
    r0(3)=sindec

    !Remove the effects of the E-terms of aberration to form r1 and r1_dot.

    r1 = r0 - A + (sum(r0 * A))*r0
    r1_dot = r0_dot - A_dot + (sum( r0 * A_dot))*r0

    R_1(1:3) = r1
    R_1(4:6) = r1_dot

    R = matmul(M,R_1)
    rr = R(1:3)
    v =  R(4:6)
    t = ((epoch - 1950.0d0) - 50.00021d0)/100.0d0
    rr1 = rr + sec_to_radian*v*t
    x = rr1(1)
    y = rr1(2)
    Z = rr1(3)


    r2 = x**2 + y**2 + z**2
    rmag = sqrt( r2 )
    dec_2000=asin( z / rmag)
    ra_2000= atan2( y, x)

    IF(ra_2000<0.0) ra_2000=ra_2000+2.d0*pi

    ra_2000 = ra_2000*radeg
    dec_2000 = dec_2000*radeg

END SUBROUTINE jprecess



SUBROUTINE prfAll(row,column,mag,num,numRows,numCols,crval1p,crval2p,prfn,naxis1,naxis2,res)
    IMPLICIT NONE

    INTEGER,DIMENSION(0:num-1) :: row,column
    INTEGER :: naxis1,naxis2,xGap,yGap,num,numRows,numCols
    INTEGER :: i
    DOUBLE PRECISION,DIMENSION(0:4) :: crval1p,crval2p
    DOUBLE PRECISION,DIMENSION(0:num-1) :: mag
    DOUBLE PRECISION,DIMENSION(0:4,0:naxis2-1,0:naxis1-1) :: prfn
    DOUBLE PRECISION,DIMENSION(0:numRows-1,0:numCols-1) :: res
    DOUBLE PRECISION,DIMENSION(0:(naxis2/50)-1,0:(naxis1/50)-1) :: img

    INTEGER, DIMENSION(0:num-1) :: xLow,xHigh,yLow,yHigh,xresLow,xresHigh,yresLow,yresHigh

    !f2py INTENT(IN) :: row,column,naxis1,naxis2,mag
    !f2py INTENT(IN) :: crval1p,crval2p,cdelt1p,cdelt2p
    !f2py INTENT(OUT) :: res
    !f2py depend(naxis1,naxis2) :: prfn
    !f2py depend(numRows,numCols) ::res
    !f2py depend(num) :: row,column,mag
    res=0.d0

    xGap=((naxis1/50)-1)/2
    yGap=((naxis2/50)-1)/2

    xLow=column-xGap
    where(xlow<0) xlow=0

    xHigh=column+xGap
    where(xHigh>=numCols) xHigh=numCols-1

    yLow=row-yGap
    where(yLow<0) yLow=0

    yHigh=row+yGap
    where(yHigh>=numRows) yHigh=numRows-1




    xresLow=0
    xresHigh=(naxis1/50)-1

    yresLow=0
    yresHigh=(naxis2/50)-1

    where(column<xGap) xresLow=abs(column-xGap)

    where(column+xGap>=numCols) xresHigh=xGap+(xGap-((column+xGap)-(numCols-1)))

    where(row<yGap) yresLow=abs(row-yGap)

    where(row+yGap>=numRows) yresHigh=yGap+(yGap-((row+yGap)-(numRows-1)))


    DO i=0,num-1
        img=0.d0
        CALL prfCalc(row(i),column(i),mag(i),crval1p,crval2p,prfn,naxis1,naxis2,img)
    ! 		write(*,*) i,row(i),column(i),yLow(i),yHigh(i),xLow(i),xHigh(i),yresLow(i),yresHigh(i),xresLow(i),xresHigh(i)
        res(yLow(i):yHigh(i),xLow(i):xHigh(i))=res(yLow(i):yHigh(i),xLow(i):xHigh(i))+&
        img(yresLow(i):yresHigh(i),xresLow(i):xresHigh(i))
    END DO


    END SUBROUTINE prfAll


SUBROUTINE prfCalc(row,column,mag,crval1p,crval2p,prfn,naxis1,naxis2,res)
    IMPLICIT NONE

    INTEGER :: row,column,naxis1,naxis2
    INTEGER :: i,j,xSmall,ySmall
    DOUBLE PRECISION,DIMENSION(0:4) :: dis,crval1p,crval2p
    DOUBLE PRECISION :: mag
    DOUBLE PRECISION,DIMENSION(0:4,0:naxis2-1,0:naxis1-1) :: prfn
    DOUBLE PRECISION,DIMENSION(0:naxis2-1,0:naxis1-1) :: prf
    DOUBLE PRECISION,DIMENSION(0:(naxis2/50)-1,0:(naxis1/50)-1) :: res
    DOUBLE PRECISION,DIMENSION(0:2) :: prfWeight
    LOGICAL,DIMENSION(0:4) :: mask
    INTEGER,DIMENSION(0:4) :: smallPRF

    !f2py INTENT(IN) :: row,column,naxis1,naxis2,mag
    !f2py INTENT(IN) :: crval1p,crval2p,cdelt1p,cdelt2p
    !f2py INTENT(OUT) :: res
    !f2py depend(naxis1,naxis2) :: prfn,res

    !interpolate the calibrated PRF shape to the target position

    prf = 0.d0
    prfWeight = 0.d0

    smallPRF=0
    dis=0.d0

    mask=.true.
    !Find the three prfs that are closest
    dis=sqrt((column - crval1p)**2 + (row - crval2p)**2)


    smallPRF(0:0)=minloc(dis,1)-1
    mask(smallPRF(0))=.false.


    smallPRF(1:1)=minloc(dis,mask)-1
    mask(smallPRF(1))=.false.

    smallPRF(2:2)=minloc(dis,mask)-1

    ! 	write(*,*) smallPRF(0),smallPRF(1),smallPRF(2),smallPRF(3),smallPRF(4)
    DO j=0,2
        i=0
        i=smallPRF(j)
        ! 		write(*,*) i,j,smallPRF(j)
        prfWeight(j) = sqrt((column - crval1p(i))**2 + (row - crval2p(i))**2)
        !Correction from pyke kepprf.py they had weight=1.d6 buut surely we want the prf at the location of the star
        !to have the most weight not the least?
        IF(prfWeight(j) == 0.0) prfWeight(j) = 1.0d6
        prf = prf + prfn(i,:,:) / prfWeight(j)
        prf = prf / sum(prf)
        ! 		write(*,*) "*",i,j,smallPRF(j)
    END DO


    ySmall=naxis2/50
    xSmall=naxis1/50

    res=0.d0
    DO j=0,xSmall-1
        DO i=0,ySmall-1
            res(i,j)=sum(prf(i*50:((i+1)*50)-1,j*50:((j+1)*50)-1))
        end do
    end do

    res=res/sum(res)
    res=res*10**(-mag/2.5)

! 	write(*,*) "*"
END SUBROUTINE prfCalc

SUBROUTINE saturate(img,wellDepth,rows,cols)
    IMPLICIT NONE
    INTEGER,INTENT(IN) :: rows,cols
    DOUBLE PRECISION,INTENT(INOUT),DIMENSION(rows,cols) :: img
    DOUBLE PRECISION,INTENT(IN) :: wellDepth
    INTEGER,DIMENSION(1:2) :: loc
    INTEGER :: k
    DOUBLE PRECISION :: diffUp,diffDown

    !f2py INTENT(IN) :: rows,cols,wellDepth
    !f2py INTENT(INOUT) :: img
    !f2py depend(rows,cols) :: img

    ! 	write(*,*) UBOUND(img,1),lBOUND(img,1),UBOUND(img)

    DO WHILE (maxval(img)>wellDepth) 
        loc=maxloc(img)
        diffUp=(img(loc(1),loc(2))-wellDepth)/2.0
        diffDown=(img(loc(1),loc(2))-wellDepth)/2.0
        img(loc(1),loc(2))=wellDepth

        !Going up
        up: DO k=loc(1)+1,UBOUND(img,1)
            IF (diffUp<=0.0) EXIT up
            !Takes all overflow
            IF(img(k,loc(2))+diffUp <=wellDepth) THEN
                img(k,loc(2))=img(k,loc(2))+diffUp
                EXIT up
            END IF
            IF(img(k,loc(2))+diffUp>=wellDepth) THEN
                diffUp=diffUp+(img(k,loc(2))-wellDepth)
                img(k,loc(2))=wellDepth
                CYCLE up
            END IF
        END DO up

        !Going Down
        down: DO k=loc(1)-1,LBOUND(img,1),-1
            IF (diffDown<=0.0) EXIT down
            !Takes all overflow
            IF(img(k,loc(2))+diffDown <=wellDepth) THEN
                img(k,loc(2))=img(k,loc(2))+diffDown
                EXIT down
            END IF
            IF(img(k,loc(2))+diffDown>=wellDepth) THEN
                diffDown=diffDown+(img(k,loc(2))-wellDepth)
                img(k,loc(2))=wellDepth
                CYCLE down
            END IF
        END DO down

    END DO 

    END SUBROUTINE

    SUBROUTINE smear(img,rows,cols,readTime,numRows,f12,tint)
    IMPLICIT NONE
    INTEGER,INTENT(IN) :: rows,cols,numRows
    DOUBLE PRECISION,INTENT(INOUT),DIMENSION(rows,cols) :: img
    DOUBLE PRECISION :: smearVal
    DOUBLE PRECISION,INTENT(IN) :: readTime,f12,tint
    INTEGER :: i
    !f2py INTENT(IN) :: rows,cols,wellDepth,numRows
    !f2py INTENT(INOUT) :: img
    !f2py depend(rows,cols) :: img

    DO i=LBOUND(img,2),UBOUND(img,2)
    smearVal=sum(img(:,i))*(readTime/numRows)
    !Convert flux to e^- 
    img(:,i)=f12*10**(12.0/2.5)*(tint*img(:,i)+smearVal)
    END DO


END SUBROUTINE smear

SUBROUTINE snr(img,bg,readNoise,quantNoise,numInts,rows,cols,numPix,noise,signal,pixX,pixY,bgFlux)
    IMPLICIT NONE
    INTEGER,INTENT(IN) :: rows,cols
    DOUBLE PRECISION,INTENT(IN),DIMENSION(rows,cols) :: img,bg
    DOUBLE PRECISION,INTENT(IN) :: readNoise,quantNoise,numInts
    DOUBLE PRECISION,INTENT(OUT) :: noise,signal,bgFlux
    INTEGER,INTENT(OUT) :: numPix
    DOUBLE PRECISION,DIMENSION(rows,cols) :: noiseAll
    DOUBLE PRECISION,DIMENSION(0:rows*cols) :: sumSig,sumBG,sumNoise,snrArr
    LOGICAL,DIMENSION(rows,cols) :: mask
    INTEGER,DIMENSION(1:2) :: loc1
    INTEGER,DIMENSION(1) :: loc2
    INTEGER :: i
    INTEGER,INTENT(OUT),DIMENSION(rows*cols) :: pixX,pixY

    !f2py INTENT(IN) :: img,bg,readNoise,quantNoise,numInts,rows,cols
    !f2py INTENT(OUT) :: numPix,noise,pixX,pixY,signal,,bgFlux
    !f2py depend(rows,cols) :: img,bg,noiseAll
    !f2py depend(rows*cols) :: sumSig,sumBG,sumNoise,snrArr,pixX,pixY

    noiseAll=0.d0
    WHERE(img+bg+readNoise+quantNoise > 0.d0) 
        noiseAll=img/sqrt(img+bg+readNoise+quantNoise)
    END WHERE

    ! 	write(*,*) noiseAll

    mask=.TRUE.
    pixX=-1
    pixY=-1

    sumSig=0.d0
    sumBG=0.d0
    sumNoise=0.d0
    snrArr=0.d0
    
!     write(*,*) LBOUND(img),UBOUND(img)
!     write(*,*) shape(img)
!     stop

    DO i=1,UBOUND(sumSig,1)
        loc1=maxloc(noiseAll,mask)
        mask(loc1(1),loc1(2))=.false.
        pixX(i)=loc1(2)
        pixY(i)=loc1(1)
        sumSig(i)=sumSig(i-1)+img(loc1(1),loc1(2))
        sumBG(i)=sumBG(i-1)+bg(loc1(1),loc1(2))
        sumNoise(i)=sumNoise(i-1)+(img(loc1(1),loc1(2))+bg(loc1(1),loc1(2))+readNoise+quantNoise)

    END DO

    sumNoise=sqrt(sumNoise)

    snrArr=sumSig/sumNoise

    loc2=maxloc(snrArr,1)-1

    pixX(loc2(1)+1:UBOUND(pixX,1))=-1
    pixY(loc2(1)+1:UBOUND(pixY,1))=-1
    

!   r=sumSig(loc2(1))/(sumSig(loc2(1))+sumBG(loc2(1)))
    numPix=loc2(1)
    noise=sumNoise(loc2(1))/(sqrt(numInts)*sumSig(loc2(1)))
    signal=sumSig(loc2(1))
    bgFlux=sumBG(loc2(1))

END SUBROUTINE snr

SUBROUTINE cte(img,par,ser,rows,cols,offset)
    IMPLICIT NONE
    INTEGER,INTENT(IN) :: rows,cols,offset
    DOUBLE PRECISION,INTENT(INOUT),DIMENSION(rows,cols) :: img
    DOUBLE PRECISION,INTENT(IN) :: par,ser
    INTEGER :: i,k

    !f2py INTENT(INOUT) :: img
    !f2py INTENT(IN) :: rows,cols,par,ser
    !f2py depend(rows,cols) :: img

    DO i=LBOUND(img,1),UBOUND(img,1)
        img(i,:)=img(i,:)*par**i
        DO k=i+1,min(UBOUND(img,1),i+9)
            img(k,:)=img(k,:)+(img(i,:)*(1.0-par)**(k-i))*i
        END DO
    END DO

    DO i=LBOUND(img,2),UBOUND(img,2)
        img(:,i)=img(:,i)*ser**i
        DO k=i+1,min(UBOUND(img,2),i+9)
            img(:,k)=img(:,k)+(img(:,i)*(1.0-ser)**(k-i))*(i+offset)
        END DO
    END DO

END SUBROUTINE cte
