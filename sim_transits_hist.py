# -*- coding: utf-8 -*-


# In[]:

import numpy as np
from astropy.io import ascii
from astropy.table import vstack
from astropy.table import Table
from astropy.table import join
import astropy.constants as const

import matplotlib
import matplotlib.pyplot as plt
import plotUtils as utils
import os
# import seaborn as sns




# In[]:





# simulated run headers
headers=['num', 'Porb', 'a', 'transit_prob', 'transit_depth1', 'transit_depth2', 'inclin']

# In[]:

# data_dir='/padata/beta/users/efarrell/repos/transits/clusterout/run_5'
data_dir='/padata/beta/users/efarrell/repos/BiSEPS_2.0/sim_transits/clusterout/run_6'

input_file = os.path.join(data_dir, 'simulated.01')
input_file = os.path.join(data_dir, 'medium_sim.dat')
input_file = os.path.join(data_dir, 'allsim.dat')

# transits  = ascii.read(input_file, delimiter=',', names=headers)
transits = ascii.read(input_file, names=headers)

output_dir = 'temp_output'


# In[]:

x  = transits['inclin'].data
y  = transits['transit_depth1'].data


# In[]:

# for histogram
x  = transits['transit_depth1'].data


# In[]:

x  = np.log(transits['Porb'].data)
# x  = np.log(transits['Porb'].data / 365)
y  = np.cos(transits['inclin'].data*np.pi/180)


# In[]:

x_prob  = np.log(transits['Porb'].data)
y_prob  = transits['transit_prob'].data

# In[]:


# small depth
min_depth=0.001
max_depth=0.01
mask1 = (transits['transit_depth1'].data > min_depth) & (transits['transit_depth1'].data < max_depth)
mask2 = (transits['transit_depth2'].data > min_depth) & (transits['transit_depth2'].data < max_depth)
total_mask = mask1 | mask2
small_transits = transits[total_mask]
x1  = np.log(small_transits['Porb'].data)
y1  = np.cos(small_transits['inclin'].data*np.pi/180)


# medium depth
min_depth=0.01
max_depth=0.2
mask1 = (transits['transit_depth1'].data > min_depth) & (transits['transit_depth1'].data < max_depth)
mask2 = (transits['transit_depth2'].data > min_depth) & (transits['transit_depth2'].data < max_depth)
total_mask = mask1 | mask2
small_transits = transits[total_mask]
x2  = np.log(small_transits['Porb'].data)
y2  = np.cos(small_transits['inclin'].data*np.pi/180)


# large depth
min_depth=0.2
max_depth=1
mask1 = (transits['transit_depth1'].data > min_depth) & (transits['transit_depth1'].data < max_depth)
mask2 = (transits['transit_depth2'].data > min_depth) & (transits['transit_depth2'].data < max_depth)
total_mask = mask1 | mask2
small_transits = transits[total_mask]
x3  = np.log(small_transits['Porb'].data)
y3  = np.cos(small_transits['inclin'].data*np.pi/180)




# In[]: Create Figure


fig = plt.figure(figsize=(8, 8))
ax = plt.subplot(111)
# ax.set_title('Distribution of transit depths - 20 runs')
ax.set_title('Distribution of transit depths - 1 simulated run')
# ax.set_title('Distribution of transit depths - 50 simulated runs')
# ax.set_title('Probability of transit - 50 simulated runs')
# ax.set_title('simulated transists - random inclination \n combine 50 different runs')
# ax.set_title('Simulated transits: eclipse probabilities - transit depth < 1%')
# ax.set_title('Simulated transits: eclipse probabilities - any depth')
# ax.set_title('eclipse probabilities - multiple depths')
ax.grid(True, which='minor')

# ax.set_xlabel('log Period (AU)', fontsize=13)
# ax.set_ylabel(u'Probability [cos( i(∆f/f) ]', fontsize=13)
# ax.set_ylabel(u'Probability', fontsize=13)

# ax.set_xlabel('inclination', fontsize=13)
# ax.set_ylabel('transit depth', fontsize=13)

ax.set_xlabel('transit depth', fontsize=13)
# ax.set_ylabel('count', fontsize=13)
ax.set_ylabel('(Relative) Frequency', fontsize=13)

# In[]: Bar Chart


# b1 = ax.bar(x, y, lw=0, width=0.2) 

# ax.set_xlim([0,90])
# ax.set_ylim([0,1.2])

# ax.set_xlim([x.min(), x.max()])
# ax.set_ylim([y.min(), y.max()])


# utils.save_plot(fig, output_dir, 'allbinaries_smalldepth_close_bar')



# In[]: Scatter Plot


ax.scatter(x, y, s=4, lw=0, alpha=0.7, color='red', label='Probability = cos(i)')
ax.scatter(x_prob, y_prob, s=4, lw=0, alpha=0.3, color='blue', label='Probability = (r1 + r2) / a')
# ax.scatter(x1, y1, s=5, lw=0, alpha=0.7, color='red')

# depth_small = dict(marker='o', s=5, color='red', lw=1, alpha=0.6)
# depth_medium = dict(marker='o', s=3, color='green', lw=1, alpha=0.6)
# depth_large = dict(marker='o', s=1, color='blue', lw=1, alpha=0.6)

# ax.scatter(x1, y1, label='depth < 0.1',  **depth_small)
# ax.scatter(x1, y1, label='0.1 < depth < 0.2',  **depth_medium)
# ax.scatter(x1, y1, label='0.2 < depth < 1',  **depth_large)

ax.set_xlim([x.min(), x.max()])

# ax.set_ylim([y.min(), y.max()])
ax.set_ylim([0, 1.2])

legend = ax.legend(loc='upper right', shadow=False)

utils.save_plot(fig, output_dir, 'simulated_eclipse_probs_count')
# utils.save_plot(fig, output_dir, 'simulated_eclipse_probs_any_depth_compare_probs')
# utils.save_plot(fig, output_dir, 'simulated_eclipse_probs_any_depth')
# utils.save_plot(fig, output_dir, 'simulated_eclipse_probs_small_depth')



# In[]: Histogram


binsize=100
# binsize=40
# bartype='step'
bartype='bar'
do_normed=False
# do_normed=True

ax.set_xlim([0, 0.5])
# ax.set_xlim([x.min(), x.max()])
# ax.set_ylim([0, 1000000])

ax.hist(x, weights=np.zeros_like(x) + 1. / x.size, bins=binsize, histtype=bartype, normed=do_normed, lw=3, color='blue', alpha=0.5, label='run 1')
# ax.hist(x, bins=binsize, log=True, histtype=bartype, normed=do_normed, lw=3, color='blue', alpha=0.5, label='run 1')
# ax.hist(x, bins=binsize, histtype=bartype, normed=do_normed, lw=3, color='blue', alpha=0.5, label='run 1')

# utils.save_plot(fig, output_dir, 'simulated_runs_one_hist_counts')
utils.save_plot(fig, output_dir, 'simulated_runs_one_hist_relative_freq')
# utils.save_plot(fig, output_dir, 'simulated_runs_med_hist_counts')
# utils.save_plot(fig, output_dir, 'simulated_runs_med_hist_relative_freq')

# In[]:

sample = np.random.multivariate_normal([0, 0], [[1, -.5], [-.5, 1]], size=100)
pal = sns.dark_palette("palegreen", as_cmap=True)
plt.figure(figsize=(6, 6))
# sns.kdeplot(sample, cmap=pal);
# sns.kdeplot(x, cmap=pal);

sns.kdeplot(x)
plt.show()

# In[]:


