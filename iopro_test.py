   
# import iopro
import iopro.pyodbc as pyodbc
from time import time



# In[]:

# sql

connection_string = 'DSN=odbcsqlite;DATABASE=./well.sqlite'

connection = pyodbc.connect(connection_string)

print "after connection"
# connection.autocommit = True 
# connection.autocommit = False 

# commit the transaction
# connection.commit() 

# rollback the transaction
# connection.rollback() 

with connection:
    cursor = connection.cursor()
    cursor.execute('select * from extract;')
    data = cursor.fetchone()

    print "heres the data"
    print data[0]

# temp debug code
print 'ending now'
sys.exit()


with connection:
    cursor = connection.cursor()
    cursor.execute('create table market (symbol_ varchar(5), open_ float, low_ float, high_ float, close_ float, volume_ int)')

    t0 = time()
    N = 100*100

    for i in xrange(N):
        cursor.execute(
            "insert into market(symbol_, open_, low_, high_, close_, volume_)"
            " values (?, ?, ?, ?, ?, ?)",
            (str(i), float(i), float(2*i), None, float(4*i), i))

    # not supported by SQLite
    # cursor.execute("commit")             

    t1 = time() - t0
    print "Stored %d rows in %.3fs" % (N, t1)

    # cursor.execute('select * from extract;')
    # data = cursor.fetchone()

    # print "heres the data"
    # print data[0]

# temp debug code
print 'ending now'
sys.exit()

# In[]:

# text file

# mydescr = np.dtype([('num','float'),('m1','float'),('r1','float'),('t1','float'),('l1','float'),
# ('m2','float'),('r2','float'),('t2','float'),('l2','float'),('p','float'),
# ('birth','float'),('death','float'),('massTran','int'),('kw1','int'),('kw2','int'),('bw','int'),('met','float'),('evol','a100')])
                            
                            
headers = (                 
'num',                      
'mt1',                      
'reff1',                    
'teff1',                    
'lum1',                     
'mt2',                      
'reff2',                    
'teff2',                    
'lum2',                     
'Porb',                     
'birth',                    
'death',                    
'massTran',                 
'kw1',                      
'kw2',                      
'bkw',                      
'met',                      
'evol')                     


# mydescr = np.dtype([('id','int64'),('num','float'),('m1','float'),('r1','float'),('t1','float'),('l1','float'),
# ('m2','float'),('r2','float'),('t2','float'),('l2','float'),('p','float'),
# ('birth','float'),('death','float'),('massTran','int'),('kw1','int'),('kw2','int'),('bw','int'),('met','float'),('evol','a100')])

datatypes = (    
'float',         
'float',         
'float',         
'float',         
'float',         
'float',         
'float',         
'float',         
'float',         
'float',         
'float',         
'float',         
'int',           
'int',           
'int',           
'int',           
'float',         
'S100')        

filename='WEBwide_kepler.dat.1'
filename='WEB_kepler.dat.1'
# filename='test.dat'

# data = iopro.loadtxt(filename, dtype=mydescr)
# data = iopro.loadtxt(d, names=headers)

# data = iopro.loadtxt(filename, delimiter=',', dtype={'names': headers, 'formats': datatypes})
data = iopro.loadtxt(filename, dtype={'names': headers, 'formats': datatypes})
data2 = iopro.loadtxt(filename, dtype={'names': headers, 'formats': datatypes})
mask = (data['num'] == 5488)

