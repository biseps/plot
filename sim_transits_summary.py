

# In[]:

import numpy as np
from astropy.io import ascii 
from astropy.table import vstack
from astropy.table import Table
from astropy.table import join
import astropy.constants as const

import matplotlib
import matplotlib.pyplot as plt
import plotUtils as utils
import os
import seaborn as sns


# In[]:

headers=[]
headers.append('depth')
headers.append('count')


# In[]:

data_dir='./'                                                         

input_file = os.path.join(data_dir, 'depths.csv.sorted')

# transits  = ascii.read(input_file, delimiter=',', names=headers)
totals = ascii.read(input_file, names=headers)

output_dir = './temp_output'




# In[]:


# scatter plot
fig = plt.figure(figsize=(8, 8))
ax = plt.subplot(111)

x = totals['depth'].data
y = np.log(totals['count'].data)

ax.set_xlabel('transit depths', fontsize=13)
# ax.set_ylabel('counts', fontsize=13)
ax.set_ylabel('log (counts)', fontsize=13)

# ax.set_title(filename)
ax.scatter(x, y, s=5, lw=0, alpha=0.7)

# ax.set_xlim([0,90])
# ax.set_ylim([0,1.2])

ax.set_title('Transit Depth Error Bars - 50 runs')
ax.grid(True, which='minor')


# plt.show()
utils.save_plot(fig, output_dir, 'transit_depth_errorbars_50runs_log')

# In[]:




