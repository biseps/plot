import biseps as b
import numpy as np

x=b.readFile()
c=b.extras()

#get x.ranLoc.appMag, x.ranLoc.id whic is the array num for x.kep
x.ranLoc('binaries.ranLoc.1')

#Read kepler file returns x.kep
x.kepler('../channels/WEB_kepler.dat.1')

ind=x.ranLoc.id


a=c.porb2a(x.kep.m1[ind],x.kep.m2[ind],x.kep.p[ind])

with open('binaries.kpl4','w') as f:
	for i in xrange(np.size(x.ranLoc.appMag)):
		out=''
		if x.ranLoc.appMag[i] < 15.0 and (x.kep.t1[ind[i]] < 7500.0  or x.kep.t2[ind[i]] < 7500.0):
			out=str(x.kep.m1[ind[i]])+' '+str(x.kep.m2[ind[i]])+' '
			out+=str(x.kep.r1[ind[i]])+' '+str(x.kep.r2[ind[i]])+' '
			out+=str(x.kep.t1[ind[i]])+' '+str(x.kep.t2[ind[i]])+' '
			out+=str(a[i])+' '+str(x.ranLoc.appMag[i])+' '
			out+=str(x.kep.birth[ind[i]])+' '+str(x.kep.death[ind[i]])
			out+='\n'
			f.write(out)
f.close()

#x.ranLoc('single.ranLoc.dat.1')

#Read kepler file returns x.kep
#x.kepler('../channels/WEBwide_kepler.dat.1')

#ind=x.ranLoc.id

#a=c.porb2a(x.kep.m1[ind],x.kep.m2[ind],x.kep.p[ind])

#with open('single.kpl','w') as f:
	#for i in xrange(np.size(x.ranLoc.appMag)):
		#out=''
		#if x.ranLoc.appMag[i] < 15.0 and x.kep.t1[ind[i]] < 7500.0:
			#out=str(x.kep.m1[ind[i]])+' '
			#out+=str(x.kep.r1[ind[i]])+' '
			#out+=str(x.kep.t1[ind[i]])+' '
			#out+=str(x.ranLoc.appMag[i])+' '
			#out+=str(x.kep.birth[ind[i]])+' '+str(x.kep.death[ind[i]])
			#out+='\n'
			#f.write(out)
#f.close()
