import biseps as b
import numpy as np
import matplotlib.pyplot as plt

x=b.readFile()
y=b.readFile()

x.kicProp4('kic.dat.1')
y.readTarget('target.dat2.1')

indX=(x.kicProp.inFov==2)
indY=(y.target.bin>0)&(y.target.bin<12)


minX=5.0
maxX=20.0
bins=30

plt.figure()
aa,ba,ca=plt.hist(x.kicProp[indX].kp,range=[minX,maxX],bins=bins,alpha=0.4,label="KIC")
ab,bb,cb=plt.hist(x.kicProp[y.target[indY].id].kp,range=[minX,maxX],bins=bins,alpha=0.4,label="Us")

#plt.figure()

#aa,ba,ca=plt.hist(x.kicProp[indX].kp,range=[minX,maxX],bins=bins,alpha=0.4,label="KIC",normed=True)
#ab,bb,cb=plt.hist(x.kicProp[y.target[indY].id].kp,range=[minX,maxX],bins=bins,alpha=0.4,label="Us",normed=True)
#plt.legend(loc=0)
#gridX=np.zeros(bins)
#for i in range(np.size(bb)-1):
	#gridX[i]=(bb[i]+bb[i+1])/2.0

#plt.plot(gridX,(aa-ab)/aa*1.0)

#plt.figure()
plt.show()

