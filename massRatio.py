import biseps as b
import numpy as np
import scipy as sci
import matplotlib.pyplot as plt
import matplotlib as m
from matplotlib import rc

rc('text', usetex=True)
rc('font', family='serif')

#Assume we have catted all the extracts and target files together
e=b.extras()
p=b.plot()
#.kep

mass1label=r'Mass $[M_{1,\odot}]$'
loggmass1label=r'$\log_{10} $ Mass $[M_{1,\odot}]$'
mass2label=r'Mass $[M_{2,\odot}]$'
loggmass2label=r'$\log_{10} $ Mass $[M_{2,\odot}]$'
periodlabel=r'$\log_{10}$ Period [days]'
massRlabel=r'$\frac{M_{2}}{M_{1}}$'


q0=b.readFile()
q0.kepler3('q0/extract.bin.targ.1')
q0.ranLoc('q0/ranLoc.bin.targ.1')

q1=b.readFile()
q1.kepler3('q1/extract.bin.targ.q1')
q1.ranLoc('q1/ranLoc.bin.targ.q1')

qm1=b.readFile()
qm1.kepler3('qm1/extract.bin.targ.qm1')
qm1.ranLoc('qm1/ranLoc.bin.targ.qm1')

bin=25

#x1=np.minimum(q0.kep.m2,q0.kep.m1)/np.maximum(q0.kep.m2,q0.kep.m1)
#x2=np.minimum(q1.kep.m2,q1.kep.m1)/np.maximum(q1.kep.m2,q1.kep.m1)
#minX=0.0
#maxX=1.0

x1=np.minimum(q0.kep.m2,q0.kep.m1)/np.maximum(q0.kep.m2,q0.kep.m1)
x2=np.minimum(q1.kep.m2,q1.kep.m1)/np.maximum(q1.kep.m2,q1.kep.m1)
x3=np.minimum(qm1.kep.m2,qm1.kep.m1)/np.maximum(qm1.kep.m2,qm1.kep.m1)

#x1=np.log10(q0.kep.p)
#x2=np.log10(q1.kep.p)
#x3=np.log10(qm1.kep.p)

minX=np.minimum(np.minimum(np.min(x1),np.min(x2)),np.min(x3))
maxX=np.maximum(np.maximum(np.max(x1),np.max(x2)),np.max(x3))

#maxX=5.0
(xAxis,a1)=p.histOutline(x1,range=[minX,maxX],density=True,bins=bin)
(xAxis,b1)=p.histOutline(x2,range=[minX,maxX],density=True,bins=bin)
(xAxis,c1)=p.histOutline(x3,range=[minX,maxX],density=True,bins=bin)

plt.plot(xAxis,a1,'k-',label='s=0',c='r')
plt.plot(xAxis,b1,'k-',label='s=1',c='g')
plt.plot(xAxis,c1,'k-',label='s=-0.99',c='b')

plt.plot([0.0,1.0],[1.0,1.0],'--',c='r')
plt.plot([0.0,1.0],[0.0,2.0],'--',c='g')


norm=(len(xAxis)*((xAxis[-1]-xAxis[0])/bin))
plt.plot(xAxis,(xAxis**-0.99)/norm,'--',c='b')
plt.ylim(0.0,np.maximum(np.maximum(np.max(a1),np.max(b1)),np.max(c1))*1.15)

plt.xlabel(massRlabel)
plt.title(r'IMR $\propto q^s$')
plt.legend(loc=0)
plt.show()


x1=np.log10(q0.kep.r1)
x2=np.log10(q1.kep.r1)


a1,a2,a3=plt.hist(x1,bins=bins,normed=1,histtype='step',label='flat')
b1,b2,b3=plt.hist(x2,bins=bins,normed=1,histtype='step',label='q')

plt.legend(loc=0)
plt.show()