#USes the data in the target.*.c files to plot ssystem properties

import biseps as b
import numpy as np
import scipy as sci
import matplotlib.pyplot as plt
import matplotlib as m
from matplotlib import rc
import getopt
import sys as sys


opts, extra = getopt.getopt(sys.argv[1:],'') 
rc('text', usetex=True)
rc('font', family='serif')

#Assume we have catted all the extracts and target files together
x=b.readFile()
e=b.extras()
#.kep

masslabel=r'Mass $[M_{\odot}]$'
radlabel=r'$\log_{10} (R [R_{\odot}])$'
tefflabel=r'$T_{eff} [k]$'
teffLoglabel=r'$\log_{10} (T_{eff} [k])$'
logglabel=r'$\log g$'
appMaglabel=r'$m_{kp}$'
absmaglabel=r'$M_{kp}$'
latlabel=r'Galactic Latitude, b [degrees]'
longlabel=r'Galactic Longitude, l [degrees]'
dislabel=r'$\log_{10}$ Distance [pc]'
periodlabel=r'$\log_{10}$ Period [days]'
semilabel=r'$\log_{10}$ (a [$R_{\odot}$])'

massRlabel=r'$\frac{M_{2}}{M_{1}}$'
teffRlabel=r'$\frac{T_{2}}{T_{1}}$'
radRlabel=r'$\frac{R_{2}}{R_{1}}$'
loggRlabel=r'$\frac{\log g_{2}}{\log g_{1}}$'


def imfCalc(x,bins=50):

	y=np.zeros(np.size(x))

#	y[x<=0.1]=0.0
	y[(x>0.1) & (x<=0.5)]=0.29056*x[(x>0.1) & (x<=0.5)]**(-1.3)
	y[(x>0.5) & (x<=1.0)]=0.1557*x[(x>0.5) & (x<=1.0)]**(-2.2)
	y[(x>1.0)]=0.1557*x[(x>1.0)]**(-2.7)

	#y=y/(np.sum(y)*((np.max(x)-np.min(x))/bins))


	#y=y/((x[1]-x[0])*bins)

	return y

def semiCalc():
	x=np.array([np.log10(3.0),6.0])
	y=np.zeros(np.size(x))

	y[x<=np.log10(3.0)]=0.0
	y[(x>np.log10(3.0)) & (x<6.0)]=0.078636
	y[x>=6.0]=0.0

	return y

def twoHist(y1,y2,l1,l2,title,xtitle,savefig=None,xmin=None,xmax=None,numBins=50,prop=None):
	plt.figure()
	if xmin == None:
		xmin = sci.floor(sci.minimum(y1.min(), y2.min()))
	if xmax == None:
		xmax = sci.ceil(sci.maximum(y1.max(), y2.max()))
	a1,b1,c1=plt.hist(y1,bins=numBins,range=[xmin,xmax],alpha=0.5,label=l1,normed=1)
	a2,b2,c2=plt.hist(y2,bins=numBins,range=[xmin,xmax],alpha=0.5,label=l2,normed=1)

	if prop=='mass':
		y=imfCalc(b1,bins=numBins)
		y=y/(np.sum(y)*((20.0)/numBins))
		plt.plot(b1[b1>0.1],y[b1>0.1],'k--',label=r'$IMF_{1}$')

	#if prop=='mass2':
		#y=imfCalc(b1,bins=numBins)
		#y=b1*y
		#y=y/(np.sum(y)*((np.max(b1)-np.min(b1))/numBins))
		#plt.plot(b1,y,'k--',label=r'$IMF_{2}$')

	if prop=='massR':
		mr=np.zeros(np.size(b1))
		mr[:]=1.0
		plt.plot(b1,mr,'k--',label='IMR')

	if prop=='semi':
		#y=np.array([0.0,np.maximum(a1.max(),a2.max())])
		#x=np.array([np.log10(3.0),np.log10(3.0)])
		#plt.plot(x,y,'k--')
		#x=np.array([6.0,6.0])
		#plt.plot(x,y,'k--')

		x=np.array([np.log10(3.0),6.0])
		y=np.zeros(np.size(x))
		y[:]=0.078636
		y=y/(np.sum(y)*((np.max(x)-np.min(x))/2.0))
		plt.plot(x,y,'k--')
		plt.plot([x[0],x[0]],[0.0,y[0]],'k--')
		

	plt.legend(loc=0)
	plt.xlabel(xtitle)
	plt.title(title)
		
	if savefig==None:
		plt.show()
	else:
		plt.savefig(savefig)


def threeHist(y1,y2,y3,l1,l2,l3,title=None,xtitle=None,savefig=None,xmin=None,xmax=None,numBins=50,prop=None):
	plt.figure()

	if xmin == None:
		xmin=sci.floor(sci.minimum((sci.minimum(y1.min(),y2.min())).min(),y3.min()))
	if xmax == None:
		xmax=sci.ceil(sci.maximum((sci.maximum(y1.max(),y2.max())).max(),y3.max()))

	a,b,c=plt.hist(y1,label=l1,bins=numBins,range=[xmin,xmax],alpha=1.0/3.0,normed=1)
	a,b,c=plt.hist(y2,label=l2,bins=numBins,range=[xmin,xmax],alpha=1.0/3.0,normed=1)
	a,b,c=plt.hist(y3,label=l3,bins=numBins,range=[xmin,xmax],alpha=1.0/3.0,normed=1)
		
	if prop=='mass':
		imf=imfCalc(b,bins=numBins)
		imf=imf/(np.sum(imf)*((np.max(b)-np.min(b))/numBins))
		plt.plot(b,imf,'k--',label='IMF')

	plt.legend(loc=0)
	plt.xlabel(xtitle)
	plt.title(title)
		
	if savefig==None:
		plt.show()
	else:
		plt.savefig(savefig)


def fourHist(y1,y2,y3,y4,l1,l2,l3,l4,title=None,xtitle=None,savefig=None,xmin=None,xmax=None,numBins=50,prop=None):
	plt.figure()

	if xmin == None:
		xmin=sci.floor(sci.minimum(sci.minimum((sci.minimum(y1.min(),y2.min())).min(),y3.min())).min(),y4.min())
	if xmax == None:
		xmax=sci.ceil(sci.maximum(sci.maximum((sci.maximum(y1.max(),y2.max())).max(),y3.max())).max(),y4.max())

	a,b,c=plt.hist(y1,label=l1,bins=numBins,range=[xmin,xmax],normed=1,histtype='step')
	a,b,c=plt.hist(y2,label=l2,bins=numBins,range=[xmin,xmax],normed=1,histtype='step')
	a,b,c=plt.hist(y3,label=l3,bins=numBins,range=[xmin,xmax],normed=1,histtype='step')
	a,b,c=plt.hist(y4,label=l4,bins=numBins,range=[xmin,xmax],normed=1,histtype='step')
			
	if prop=='mass':
		imf=imfCalc(b,bins=numBins)
		imf=imf/(np.sum(imf)*((np.max(b)-np.min(b))/numBins))
		#plt.plot(b,imf,'k--',label='IMF')

	plt.legend(loc=0)
	plt.xlabel(xtitle)
	plt.title(title)
		
	if savefig==None:
		plt.show()
	else:
		plt.savefig(savefig)

	
sysType=extra[0]
if sysType=='sing':
	x.kepler3('extract.sing.targ.1')
	#.ranLoc
	x.ranLoc('ranLoc.sing.targ.1')

	#int dist
	y=b.readFile()
	y.kepler3('extract.sing.targAll.1')
	y.ranLoc('ranLoc.sing.targAll.1')

	twoHist(y.kep.m1,x.kep.m1,'PDMF','Kepler MF','Single star mass distribution (truncated)',masslabel,xmax=6.0,prop='mass',savefig='mass.sing.png')

	twoHist(np.log10(y.kep.t1),np.log10(x.kep.t1),r'PD $T_{eff}$',r'Kepler $T_{eff}$','Single star $T_{eff}$ distribution ',teffLoglabel,numBins=50,xmax=np.max(np.log10(x.kep.t1)),savefig='teff.sing.png')

	twoHist(np.log10(y.kep.r1),np.log10(x.kep.r1),r'PD R',r'Kepler R','Single star radius distribution ',radlabel,savefig='rad.sing.png')

	logg=e.massrad2logg(x.kep.r1,x.kep.m1)
	loggPD=e.massrad2logg(y.kep.r1,y.kep.m1)
	twoHist(loggPD,logg,r'PD $\log_{10} g$',r'Kepler $\log_{10} g$','Single star $\log_{10} g$ distribution ',logglabel,savefig='logg.sing.png')


	twoHist(y.ranLoc.appMag,x.ranLoc.appMag,r'PD $m_{Kp}$',r'Kepler $m_{Kp}$','Single star $m_{Kp}$ distribution ',appMaglabel,savefig='appMag.sing.png')

if sysType=='bin':
	x.kepler3('extract.bin.targ.1')
	#	.ranLoc
	x.ranLoc('ranLoc.bin.targ.1')
	
	#	int dist
	y=b.readFile()
	y.kepler3('extract.bin.targAll.1')
	y.ranLoc('ranLoc.bin.targAll.1')

	twoHist(y.kep.m1,x.kep.m1,'$PDMF_{1}$',r'Kepler $MF_{1}$','Binary star mass$_{1}$ distribution ',masslabel,xmax=6.0,xmin=0.0,prop='mass',savefig='mass1.bin.png')

	twoHist(y.kep.m2,x.kep.m2,'$PDMF_{2}$',r'Kepler $MF_{2}$','Binary star mass$_{2}$ distribution ',masslabel,xmax=10.0,xmin=0.0,prop='mass2',savefig='mass2.bin.png')

	#fourHist(y.kep[y.ranLoc.id].m1,x.kep.m1,y.kep[y.ranLoc.id].m2,x.kep.m2,'$PDMF_{1}$',r'Kepler $MF_{1}$','$PDMF_{2}$',r'Kepler $MF_{2}$','Binary star mass distribution ',masslabel,xmax=10.0,xmin=0.0,prop='mass')

	twoHist(np.minimum(y.kep.m2,y.kep.m1)/np.maximum(y.kep.m2,y.kep.m1),
			np.minimum(x.kep.m1,x.kep.m2)/np.maximum(x.kep.m1,x.kep.m2),'$PDMR$',r'Kepler MR','Binary star mass ratio distribution ',massRlabel,prop='massR',savefig='massR.bin.png')


	twoHist(np.log10(y.kep.t1),np.log10(x.kep.t1),r'$PD T_{eff,1}$',r'Kepler $T_{eff,1}$',r'Binary star $T_{eff,1}$ distribution ',teffLoglabel,savefig='teff1.bin.png')

	twoHist(np.log10(y.kep.t2),np.log10(x.kep.t2),r'$PD T_{eff,2}$',r'Kepler $T_{eff,2}$',r'Binary star $T_{eff,2}$ distribution ',teffLoglabel,savefig='teff2.bin.png')

	#fourHist(y.kep[y.ranLoc.id].m1,x.kep.m1,y.kep[y.ranLoc.id].m2,x.kep.m2,'$PDMF_{1}$',r'Kepler $MF_{1}$','$PDMF_{2}$',r'Kepler $MF_{2}$','Binary star mass distribution ',masslabel,xmax=10.0,xmin=0.0,prop='mass')

	twoHist(np.minimum(y.kep.t2,y.kep.t1)/np.maximum(y.kep.t2,y.kep.t1),
			np.minimum(x.kep.t1,x.kep.t2)/np.maximum(x.kep.t1,x.kep.t2),r'PD $T_{eff}R$',r'Kepler $T_{eff}R$',r'Binary star $T_{eff}$ ratio distribution ',teffRlabel,savefig='teff.bin.png')



	twoHist(np.log10(y.kep.r1),np.log10(x.kep.r1),r'PD Radius$_{1}$',r'Kepler Radius$_{1}$',r'Binary star Radius$_{1}$ distribution ',radlabel,savefig='r1.bin.png')

	twoHist(np.log10(y.kep.r2),np.log10(x.kep.r2),r'PD Radius$_{2}$',r'Kepler Radius$_{2}$',r'Binary star Radius$_{2}$ distribution ',radlabel,savefig='r2.bin.png')

	#fourHist(y.kep[y.ranLoc.id].m1,x.kep.m1,y.kep[y.ranLoc.id].m2,x.kep.m2,'$PDMF_{1}$',r'Kepler $MF_{1}$','$PDMF_{2}$',r'Kepler $MF_{2}$','Binary star mass distribution ',masslabel,xmax=10.0,xmin=0.0,prop='mass')

	twoHist(np.minimum(y.kep.r2,y.kep.r1)/np.maximum(y.kep.r2,y.kep.r1),
			np.minimum(x.kep.r1,x.kep.r2)/np.maximum(x.kep.r1,x.kep.r2),'PD Radius R',r'Kepler Radius R',r'Binary star Radius ratio distribution ',radRlabel,savefig='radR.bin.png')


	semiNew=e.porb2a(x.kep.m1,x.kep.m2,x.kep.p)
	semiOrig=e.porb2a(y.kep.m1,y.kep.m2,y.kep.p)
	twoHist(np.log10(semiOrig),np.log10(semiNew),'PD Semi-Major Axis','Kepler Semi-Major Axis','Binary star semi-major axis distribution',semilabel,prop='semi',savefig='semi.bin.png')


	twoHist(y.ranLoc.appMag,x.ranLoc.appMag,r'PD $m_{Kp}$',r'Kepler $m_{Kp}$','Binary star $m_{Kp}$ distribution ',appMaglabel,savefig='appMag.bin.png')

	logg=e.massrad2logg(x.kep.r1,x.kep.m1)
	loggPD=e.massrad2logg(y.kep.r1,y.kep.m1)
	twoHist(loggPD,logg,r'PD $\log_{10} g_{1}$',r'Kepler $\log_{10} g_{1}$','Binary star $\log_{10} g_{1}$ distribution ',logglabel,savefig='logg1.bin.png')

	logg2=e.massrad2logg(x.kep.r2,x.kep.m2)
	loggPD2=e.massrad2logg(y.kep.r2,y.kep.m2)
	twoHist(loggPD2,logg2,r'PD $\log_{10} g_{2}$',r'Kepler $\log_{10} g_{2}$','Binary star $\log_{10} g_{2}$ distribution ',logglabel,savefig='logg2.bin.png')

	g1=10**logg
	g2=10**logg2
	pg1=10**loggPD
	pg2=10**loggPD2
	twoHist(np.log10(np.minimum(pg1,pg2)/np.maximum(pg1,pg2)),np.log10(np.minimum(g1,g2)/np.maximum(g1,g2)),'PD g Ratio','Kepler g Ratio','Binary star surface gravity ratio distribution ',r'$\log_{10} \left(\frac{g_{2}}{g_{1}}\right)$',savefig='gR.bin.png')


	twoHist(np.log10(y.kep.p),np.log10(x.kep.p),r'PD Period',r'Kepler Period',r'Binary star period distribution ',periodlabel,savefig='period.bin.png')