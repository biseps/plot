import biseps as b
import matplotlib.pyplot as plt
import numpy as np


x=b.readFile()
x.ranLoc('ranLoc.all')


z=x.ranLoc.dist*(np.sin(x.ranLoc.lat*(np.pi/180.0)))+15.0
n,bins,patchs=plt.hist(np.log10(z),bins=100,normed=1,alpha=0.75)
plt.plot(bins[0:100],np.cumsum(n)/np.sum(n),color='black')
plt.xlabel('Log Z [pc]')
plt.title('Our synthetic 8-16th mag sub sampled but not kic\'ed')
plt.show()