import numpy as np
import biseps as b
import getopt
import sys as sys


opts, extra = getopt.getopt(sys.argv[1:],'') 	 
x=b.readFile()
x.quarter1(extra[0])
#x.kicProp5(extra[1])
x.appMag6(extra[1],sep=",")

ind=np.in1d(x.appMag.id,x.kic.id)
if np.count_nonzero(ind):
	x.write_array(extra[2],x.appMag[ind],'w')