import urllib

#fov
#baseUrl='http://archive.stsci.edu/kepler/kepler_fov/search.php?'
#kic
baseUrl='http://archive.stsci.edu/kepler/kic10/search.php?'
step=100000


for i in range(1,(13161029/step)+1):
	print i
	seas=3
	url  = baseUrl
	url += 'action=Search'
	#url += '&kct_channel_season_'+str(seas)+'='+str(i)
	url += '&verb=3'
	url += '&kic_galaxy=0'
	url += '&kic_kepler_id='+str(((i-1)*step)+1)+'..'+str((i)*step)
#	url += '&kic_kepler_id=%3C'+str((i)*step)
	url += '&kic_fov_flag=%3E0'
	url += '&max_records='+str(step+10)
	url += '&selectedColumnsCsv=kic_degree_ra,kic_dec,kic_teff,kic_feh,kic_logg,kic_radius,kic_kepmag,kic_glon,kic_glat,kic_fov_flag'
	url += '&outputformat=CSV'
	
#	print url
	lines = urllib.urlopen(url)
	with open('kic.target.'+str(i),'w') as f:
		for line in lines:
			#print line
			line = line.strip()
			if (len(line) > 0 and
				'Kepler' not in line and
				'integer' not in line and
				'no rows found' not in line and
				'Teff' not in line):
					out = line.split(',')
					r = (float(out[0].split(' ')[0]) + \
						float(out[0].split(' ')[1]) / 60.0 + \
						float(out[0].split(' ')[2]) / 3600.0) * 15.0
					d = float(out[1].split(' ')[0]) + \
						float(out[1].split(' ')[1]) / 60.0 + \
						float(out[1].split(' ')[2]) / 3600.0
					if out[6] != "":
						f.write(str(r)+','+str(d)+','+str(out[2])+','+
							str(out[3])+','+str(out[4])+','+str(out[5])+','+
							str(out[6])+','+str(out[7])+','+str(out[8])+','+str(out[9])+'\n')
	f.close()

##########################################
import urllib

url='http://archive.stsci.edu/kepler/kepler_fov/search.php?action=Search&verb=3&kct_channel_season_3=3&kic_galacy=0&selectedColumnsCsv=kic_degree_ra,kic_dec,kic_glon,kic_glat,kct_row_season_3,kct_column_season_3&outputformat=CSV&max_records=1000000'

lines = urllib.urlopen(url)
with open('test.3.ra','w') as f:
	for line in lines:
		#print line
		line = line.strip()
		if (len(line) > 0 and
			'Kepler' not in line and
			'integer' not in line and
			'no rows found' not in line and
			'RA' not in line):
				out = line.split(',')
				r = (float(out[0].split(' ')[0]) + \
					float(out[0].split(' ')[1]) / 60.0 + \
					float(out[0].split(' ')[2]) / 3600.0) * 15.0
				d = float(out[1].split(' ')[0]) + \
					float(out[1].split(' ')[1]) / 60.0 + \
					float(out[1].split(' ')[2]) / 3600.0
				f.write(str(r)+' '+str(d)+' '+str(out[2])+' '+
						str(out[3])+' '+str(out[4])+' '+str(out[5])+' '+'\n')
