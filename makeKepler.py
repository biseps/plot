#Generates the pop.in files for kepler population runs

import biseps as b
import os, errno
import numpy as np

x=b.readFile()
x.kepGrid('/padata/beta/users/rfarmer/code/plot/kepCCD2.txt')

binaryClass=3
disk='young'
sfr=3000.0
scaleR=2800.0
scaleZ=300.0
isSingle=1
ext='h'
imfType='k'
imr=0.0
colourCorrect=0.88

baseFile='pop_s_y'
num=84

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST:
            pass
        else: raise

def writeKeplerOut(baseFile,num,binaryClass,disk,sfr,scaleR,scaleZ,isSingle,kepCCD,ext,imfType,imr,colourCorrect):

	mkdir_p(baseFile)

	for i in range(0,84):
		folder=baseFile+'/'+str(i+1)
		mkdir_p(folder)
		with open(folder+"/pop.in", "w") as f:
			f.write('# Binary class\n')
			f.write(str(binaryClass)+'\n')
			f.write('# BiSEPS input model'+'\n')		
			f.write('1'+'\n')
			f.write('# Starburst (burst) or continuous (ctu) star formation rate'+'\n')
			f.write('ctu '+str(disk)+' '+str(sfr)+'\n')
			f.write('# Ranges and number of bins in time'+'\n')
			f.write('0.0d0 1.5d4 300'+'\n')
			f.write('# Age of the galaxy in Myr'+'\n')
			f.write('13000.0'+'\n')
			f.write('# (R,z) coordinates of the Sun in pc'+'\n')
			f.write('8500.0 30.0'+'\n')
			f.write('# Scale length and height of the Galactic disc in pc'+'\n')
			f.write(str(scaleR)+' '+str(scaleZ)+'\n')
			f.write('# Ranges (degrees) and number of fields in galactic longitude and latitude'+'\n')
			f.write(str(np.min(kepCCD[i,:,0]))+' '+str(np.max(kepCCD[i,:,0]))+'\n')
			f.write(str(np.min(kepCCD[i,:,1]))+' '+str(np.max(kepCCD[i,:,1]))+'\n')
			f.write('# Want transits yes=1, are we on single stars yes=1,numTransits'+'\n')
			f.write('0 '+str(isSingle)+' 30'+'\n')
			f.write('# Min and max mag to run over'+'\n')
			f.write('8.0 16.0'+'\n')
			f.write(str(kepCCD[i,0,0])+' '+str(kepCCD[i,1,0])+' '+str(kepCCD[i,2,0])+' '+str(kepCCD[i,3,0])+'\n')
			f.write(str(kepCCD[i,0,1])+' '+str(kepCCD[i,1,1])+' '+str(kepCCD[i,2,1])+' '+str(kepCCD[i,3,1])+'\n')
			f.write(str(ext)+' '+str(imfType)+' '+str(imr)+' '+str(colourCorrect)+'\n')
			f.close()

writeKeplerOut('pop_s_y',84,3,'young',3000.0,2800.0,300.0,1,x.kepGrid,'h','k',0.0,0.88)
writeKeplerOut('pop_s_o',84,3,'old',3000.0,2800.0,300.0,1,x.kepGrid,'h','k',0.0,0.88)
writeKeplerOut('pop_b_y',84,2,'young',3000.0,3700.0,1000.0,0,x.kepGrid,'h','k',0.0,0.88)
writeKeplerOut('pop_b_o',84,2,'old',3000.0,3700.0,1000.0,0,x.kepGrid,'h','k',0.0,0.88)