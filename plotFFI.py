import fortUtil as f
import numpy as np
import biseps as b
import kepprf as kp
import matplotlib as m
import matplotlib.pyplot as plt
import constants as c
import getopt
import sys as sys


opts, extra = getopt.getopt(sys.argv[1:],'')
x=np.load(extra[0])
x=x+c.zod
fileNum=str(extra[1])

x=np.asfortranarray(x)

plt.figure()

ax1 = plt.subplot(2,2,1)
plt.imshow(np.log10(x),interpolation='nearest',origin='lower',cmap='jet_r')
plt.xlabel('flux')
plt.colorbar()

ax2 = plt.subplot(2,2,2,sharex=ax1,sharey=ax1)
f.smear(x,np.shape(x)[0],np.shape(x)[1],c.readTime,c.numRows,c.f12,c.tint)
plt.imshow(np.log10(x),interpolation='nearest',origin='lower',cmap='jet_r')
plt.xlabel('smear')
plt.colorbar()

ax3 = plt.subplot(2,2,3,sharex=ax1,sharey=ax1)
x=x*c.f2e
f.saturate(x,c.wellDepth[fileNum-1],np.shape(x)[0],np.shape(x)[1])
plt.imshow(np.log10(x),interpolation='nearest',origin='lower',cmap='jet_r')
plt.xlabel('sat')
plt.colorbar()

ax4 = plt.subplot(2,2,4,sharex=ax1,sharey=ax1)
f.cte(x,c.ctePar,c.cteSer,np.shape(x)[0],np.shape(x)[1],0)
plt.imshow(np.log10(x),interpolation='nearest',origin='lower',cmap='jet_r')
plt.xlabel('cte')
plt.colorbar()

plt.show()

