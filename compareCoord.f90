PROGRAM compareCoord

	IMPLICIT NONE
	DOUBLE PRECISION :: ra_2000,dec_2000

	CALL nasa(0.d0,0.d0,ra_2000,dec_2000)
	write(*,*) ra_2000,dec_2000
	CALL us(0.d0,0.d0,ra_2000,dec_2000)
	write(*,*) ra_2000,dec_2000	

END PROGRAM

SUBROUTINE nasa(gl,gb,ra,dec)
	IMPLICIT NONE
	DOUBLE PRECISION,INTENT(IN) :: gl,gb
	DOUBLE PRECISION,INTENT(OUT) :: ra,dec
	DOUBLE PRECISION,PARAMETER :: pi = 3.141592654d0,radeg=180.0/pi,rapol=12.0d0 + 49.0d0/60.0d0
	DOUBLE PRECISION,PARAMETER :: decpol=27.4d0,dlon=123.0d0
	DOUBLE PRECISION,PARAMETER :: sdp=sin(decpol/radeg),cdp = sqrt(1.0d0-sdp*sdp),radhrs=radeg/15.0d0
	DOUBLE PRECISION :: sgb,cgb,sdec,cdec,sinf,cosf,ras,decs
	
	sgb = sin(gb/radeg)
	cgb = sqrt(1.0d0-sgb*sgb)
	sdec = sgb*sdp + cgb*cdp*cos((dlon-gl)/radeg)
	decs = radeg * asin(sdec)
	
	cdec = sqrt(1.0d0-sdec*sdec)
	sinf = cgb * sin((dlon-gl)/radeg) / cdec
	cosf = (sgb-sdp*sdec) / (cdp*cdec)
	ras = rapol + radhrs*atan2(sinf,cosf)
	ras = ras*15.0d0

    CALL jprecess(ras,decs,ra,dec)

END SUBROUTINE nasa

SUBROUTINE jprecess(ra,dec,ra_2000,dec_2000)
	IMPLICIT NONE
	DOUBLE PRECISION,INTENT(IN) :: ra,dec
	DOUBLE PRECISION,INTENT(OUT) :: ra_2000,dec_2000
	DOUBLE PRECISION,PARAMETER :: epoch = 1950.0d0,radeg=57.29577951,pi = 3.141592654d0
 	DOUBLE PRECISION,PARAMETER :: sec_to_radian = 1.0/radeg/3600.0d0
	DOUBLE PRECISION,DIMENSION(1:6,1:6) :: M
	DOUBLE PRECISION,DIMENSION(1:36) :: source
	DOUBLE PRECISION,PARAMETER,DIMENSION(1:3) :: A=(/-1.62557d-6,-0.31919d-6,-0.13843d-6/)
	DOUBLE PRECISION,PARAMETER,DIMENSION(1:3) :: A_dot=(/1.244d-3,-1.579d-3,-0.660d-3/),r0_dot=(/0.0,0.0,0.0/)
	DOUBLE PRECISION,PARAMETER :: mu_a = 0.0d0,mu_d = 0.0d0

	DOUBLE PRECISION :: ra_rad,dec_rad,cosra,sinra,cosdec,sindec,t,x,y,z,r2,rmag
	DOUBLE PRECISION,DIMENSION(1:3) :: r0,rr,v,rr1,r1,r1_dot
	DOUBLE PRECISION,DIMENSION(1:6) :: R,R_1
	
	M=reshape(source = (/0.9999256782,0.0111820610,0.0048579479,-0.000551,0.238514,-0.435623, &
    	-0.0111820611,0.9999374784,-0.0000271474,-0.238565,-0.002667,0.012254,&
    	-0.0048579477,-0.0000271765,0.9999881997,0.435739,-0.008541,0.002117,&
    	 0.00000242395018,0.00000002710663,0.00000001177656,0.99994704,0.01118251,0.00485767,&
    	-0.00000002710663,0.00000242397878,-0.00000000006582,-0.01118251,0.99995883,-0.00002714,&
    	-0.00000001177656,-0.00000000006587,0.00000242410173,-0.00485767,-0.00002718,1.00000956/),shape=(/6,6/))

	ra_rad = ra/radeg
	dec_rad = dec/radeg
	cosra =  cos( ra_rad )
	sinra = sin( ra_rad )
	cosdec = cos( dec_rad )
	sindec = sin( dec_rad )

 	r0(1)=cosra*cosdec
 	r0(2)=sinra*cosdec
 	r0(3)=sindec

!Remove the effects of the E-terms of aberration to form r1 and r1_dot.

	r1 = r0 - A + (sum(r0 * A))*r0
	r1_dot = r0_dot - A_dot + (sum( r0 * A_dot))*r0

	R_1(1:3) = r1
	R_1(4:6) = r1_dot
 
	R = matmul(M,R_1)
	rr = R(1:3)
	v =  R(4:6)
	t = ((epoch - 1950.0d0) - 50.00021d0)/100.0d0
	rr1 = rr + sec_to_radian*v*t
	x = rr1(1)
	y = rr1(2)
	Z = rr1(3)


 	r2 = x**2 + y**2 + z**2
 	rmag = sqrt( r2 )
 	dec_2000=asin( z / rmag)
 	ra_2000= atan2( y, x)

	IF(ra_2000<0.0) ra_2000=ra_2000+2.d0*pi

 	ra_2000 = ra_2000*radeg
 	dec_2000 = dec_2000*radeg

END SUBROUTINE jprecess


SUBROUTINE us(lon,lat,ra,dec)
	IMPLICIT NONE
	DOUBLE PRECISION,INTENT(IN) :: lon,lat
	DOUBLE PRECISION,INTENT(OUT) :: ra,dec
	DOUBLE PRECISION :: lonR,latR,clat,slat,cgpd,sgpd,a,b,c,d,ca,sa,sq
	
	DOUBLE PRECISION :: dra,ddec,drow,dcol
	INTEGER :: ind
	
	DOUBLE PRECISION,PARAMETER :: pi=3.141592654
	DOUBLE PRECISION,PARAMETER :: deg2rad=pi/180.0
	DOUBLE PRECISION,PARAMETER :: an=32.93192*deg2rad
	DOUBLE PRECISION,PARAMETER :: gpr = 192.8594*deg2rad
	DOUBLE PRECISION,PARAMETER :: gpd = 27.12825*deg2rad

	lonR=lon*deg2rad
	latR=lat*deg2rad
	
	clat=cos(latR)
	slat=sin(latR)
	
	cgpd=cos(gpd)
	sgpd=sin(gpd)
	
	a=lonR-an
	
	ca=cos(a)
	sa=sin(a)
	b=sa
	
	sq=(clat*cgpd*b)+(slat*sgpd)
	dec=asin(sq)
	
	c=clat*ca
	d=(slat*cgpd)-(clat*sgpd*sa)
	
	if(abs(d) < 1.d-20) d=1.d-20
	
	ra=atan(c/d)+gpr
	
	if(d<0) ra=ra+pi
	
	if(ra<0)ra=ra+2*pi
	
	if(ra > 2*pi)ra=ra-2*pi
		
	ra=ra/deg2rad
	dec=dec/deg2rad


END SUBROUTINE us