

# In[]:

import numpy as np
from astropy.io import ascii 
from astropy.table import vstack
from astropy.table import Table
from astropy.table import join
import astropy.constants as const

import matplotlib
import matplotlib.pyplot as plt
import plotUtils as utils
import os
import seaborn as sns




# In[]:


# jktebop output layout
headers=[]
headers.append('id')
headers.append('porb')
headers.append('inclin')
headers.append('critical_inclin')
headers.append('trans_depth1')
headers.append('ellip_depth1')
headers.append('trans_depth2')
headers.append('ellip_depth2')
headers.append('noise')



# In[]:

# data_dir='/padata/beta/users/efarrell/data/plato/eb2_cluster/code/eb2/output'
# data_dir='/padata/beta/users/efarrell/repos/eb2/misc/'
# data_dir='/padata/beta/users/efarrell/repos/BiSEPS_2.0/eb2_old/misc'
data_dir='/padata/beta/users/efarrell/repos/BiSEPS_2.0/eb2_old/output/transits'                                                         

input_file = os.path.join(data_dir, 'transit_depths.dat.onefile')
# input_file = os.path.join(data_dir, 'transit_depths.dat.1')
# input_file = os.path.join(data_dir, 'pop_b_o_depths.csv')
# input_file = os.path.join(data_dir, 'pop_b_y_depths.csv')
# input_file = os.path.join(data_dir, 'binaries.csv')
# input_file = os.path.join(data_dir, 'binaries.dat')

# transits  = ascii.read(input_file, delimiter=',', names=headers)
transits = ascii.read(input_file, names=headers)

output_dir = 'temp_output'


# In[]:

# x  = transits['inclin'].data
# y  = transits['trans_depth1'].data


# In[]:

# probability from cos i
x  = np.log(transits['porb'].data)
y  = np.cos(transits['inclin'].data*np.pi/180)


# In[]:

# probability from R1 + R2 / a
x  = np.log(transits['porb'].data)
y  = np.cos(transits['critical_inclin'].data*np.pi/180)


# In[]:

# transit_depth 
# and probability from R1 + R2 / a
# x  = np.log(transits['trans_depth1'].data)
x  = transits['trans_depth1'].data
y  = np.cos(transits['critical_inclin'].data*np.pi/180)


# In[]:

# inclination on x axis
x  = transits['inclin'].data
y  = np.cos(transits['inclin'].data*np.pi/180)


# In[]:

min_depth=0.001
max_depth=0.01
mask1 = (transits['trans_depth1'].data > min_depth) & (transits['trans_depth1'].data < max_depth)
mask2 = (transits['trans_depth2'].data > min_depth) & (transits['trans_depth2'].data < max_depth)
total_mask = mask1 | mask2
small_transits = transits[total_mask]
x  = np.log(small_transits['porb'].data)
y  = np.cos(small_transits['critical_inclin'].data*np.pi/180)


# In[]:

# Probabilities using critical inclination

min_depth=0.001
max_depth=0.01
mask1 = (transits['trans_depth1'].data > min_depth) & (transits['trans_depth1'].data < max_depth)
mask2 = (transits['trans_depth2'].data > min_depth) & (transits['trans_depth2'].data < max_depth)
total_mask = mask1 | mask2
small_transits = transits[total_mask]
x1  = np.log(small_transits['porb'].data)
y1  = np.cos(small_transits['critical_inclin'].data*np.pi/180)


min_depth=0.01
max_depth=0.2
mask1 = (transits['trans_depth1'].data > min_depth) & (transits['trans_depth1'].data < max_depth)
mask2 = (transits['trans_depth2'].data > min_depth) & (transits['trans_depth2'].data < max_depth)
total_mask = mask1 | mask2
small_transits = transits[total_mask]
x2  = np.log(small_transits['porb'].data)
y2  = np.cos(small_transits['critical_inclin'].data*np.pi/180)


min_depth=0.2
max_depth=1
mask1 = (transits['trans_depth1'].data > min_depth) & (transits['trans_depth1'].data < max_depth)
mask2 = (transits['trans_depth2'].data > min_depth) & (transits['trans_depth2'].data < max_depth)
total_mask = mask1 | mask2
small_transits = transits[total_mask]
x3  = np.log(small_transits['porb'].data)
y3  = np.cos(small_transits['critical_inclin'].data*np.pi/180)


# In[]:


fig = plt.figure(figsize=(8, 8))
ax = plt.subplot(111)
ax.set_title('transit depths - histogram - relative frequency')
# ax.set_title('transit depths - histogram')
# ax.set_title('eclipse probabilities - transit depths')
# ax.set_title('eclipse probabilities - any depth')
# ax.set_title('eclipse probabilities - transit_depth < 1%')
# ax.set_title('eclipse probabilities - specific depths')
ax.grid(True, which='minor')
ax.set_xlabel('transit depth', fontsize=13)
# ax.set_xlabel('log Period', fontsize=13)
# ax.set_xlabel('inclination', fontsize=13)
# ax.set_ylabel('Probability - cos(i)', fontsize=13)
# ax.set_ylabel('Probability - (r1 + r2)/a', fontsize=13)
# ax.set_ylabel('count', fontsize=13)
ax.set_ylabel('frequency', fontsize=13)

# In[]:


# b1 = ax.bar(x, y, width=0.2) 

# ax.set_xlim([0,90])
# ax.set_ylim([0,1.2])

# ax.set_xlim([x.min(), x.max()])
# ax.set_ylim([y.min(), y.max()])


# utils.save_plot(fig, output_dir, 'allbinaries_smalldepth_close_bar')


# In[]:

# binsize=40
binsize=100
# bartype='step'
bartype='bar'
do_normed=False
# do_normed=True

ax.set_xlim([x.min(), x.max()])
# ax.set_xlim([x.min(), 0.1])
# ax.set_xlim([x.min(), 0.4])

ax.hist(x, weights=np.zeros_like(x) + 1. / x.size, bins=binsize, histtype=bartype, normed=do_normed, lw=3, color='blue', alpha=0.5, label='run 1')
# ax.hist(x, bins=binsize, histtype=bartype, normed=do_normed, lw=3, color='blue', alpha=0.5, label='run 1')
# ax.hist(x, bins=binsize, log=True, histtype=bartype, normed=do_normed, lw=3, color='blue', alpha=0.5, label='run 1')

utils.save_plot(fig, output_dir, 'transit_depths_histogram_relative_frequency')
# utils.save_plot(fig, output_dir, 'transit_depths_histogram')
# utils.save_plot(fig, output_dir, 'transit_depths_histogram_log')


# In[]:

fig = plt.figure(figsize=(8, 8))
ax = plt.subplot(111)
ax.set_title('transit depths - one binary')

ax.grid(True, which='minor')

ax.set_xlabel('inclination ', fontsize=13)
ax.set_ylabel('transit depth', fontsize=13)

# scatter plot
x  = transits['inclin'].data
y  = transits['trans_depth1'].data

# ax.set_title(filename)
ax.scatter(x, y, s=5, lw=0, alpha=0.7)

# ax.scatter(x1, y1, s=10, marker='o',lw=0, alpha=0.5, color='red', label="depth < 0.1")
# ax.scatter(x2, y2, s=10, marker='>',lw=0, alpha=0.4, color='green', label="0.1  < depth < 0.2")
# ax.scatter(x3, y3, s=10, marker='^',lw=0, alpha=0.4, color='blue', label="0.2 < depth < 1")
# legend = ax.legend(loc='upper right', shadow=False)


ax.set_xlim([0, 90])
# ax.set_xlim([x.min(), x.max()])
# ax.set_xlim([x.min(), 20.4])

ax.set_ylim([0,1])
# ax.set_ylim([y.min(), y.max()])


# utils.save_plot(fig, output_dir, 'popbo_probs_anydepth')
# utils.save_plot(fig, output_dir, 'popbo_probs_anydepth_inclin_prob')
# utils.save_plot(fig, output_dir, 'popbo_probs_smalldepth')
# utils.save_plot(fig, output_dir, 'allbinaries_anydepth')
# utils.save_plot(fig, output_dir, 'allbinaries_smalldepth')
# utils.save_plot(fig, output_dir, 'allbinaries_smalldepth_close')
# utils.save_plot(fig, output_dir, 'allbinaries_specific_depths')

# utils.save_plot(fig, output_dir, 'allbinaries_anydepth_radius_prob')
# utils.save_plot(fig, output_dir, 'allbinaries_small_depths_close_radius_prob')
# utils.save_plot(fig, output_dir, 'allbinaries_small_depths_radius_prob')
# utils.save_plot(fig, output_dir, 'allbinaries_specific_depths_radius_prob')

utils.save_plot(fig, output_dir, 'onebinary_transitdepth_inclin')

# In[]:




