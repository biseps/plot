# In[]:

import sys, os; 


import numpy as np
from astropy.io import ascii as ascii_file
from astropy.table import vstack
from astropy.table import Table
from astropy.table import join
import astropy.constants as const

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
import matplotlib.cm as cm
from matplotlib.colors import LogNorm
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

# import my handy utilities
sys.path.append('/padata/beta/users/efarrell/repos/biseps')
from common import utils


# import seaborn as sns

# matplotlib.rcParams['savefig.format'] = 'jpg'
print matplotlib.matplotlib_fname()




# In[]:

# read data files
root_dir = '/padata/beta/users/efarrell/data/plato'

data_folders = []
data_folders.append('pop_output/field1_5deg_26/run_1/pop_b_o/78')
data_folders.append('pop_output/field1_5deg_26/run_1/pop_b_y/78')
# data_folders.append('pop_output/field1/run_2/pop_b_o/78')
# data_folders.append('pop_output/field1/run_2/pop_b_y/78')
# data_folders.append('pop_output/field1/run_3/pop_b_o/78')
# data_folders.append('pop_output/field1/run_3/pop_b_y/78')



extract_headers = ['line_num', 'biseps_id', 'mt1', 'reff1', 'teff1', 'lum1', 'mt2', 'reff2', 'teff2', 'lum2', 
        'Porb', 'birth', 'death', 'massTran', 'kw1', 'kw2', 'bkw', 'met', 'evol']



# In[]:


plot_folder = './current_q'
utils.rotate_folder(plot_folder)


# In[]:
# compare mass 1 and mass 2 of binary system


for folder in data_folders:
    input_file    = os.path.join(root_dir, folder, 'extract.1')
    data          = ascii_file.read(input_file, delimiter=' ', names=extract_headers)

    print 'processing file: ' + input_file


    fig, ax = plt.subplots(figsize=(12,9))


    x = data['mt1'].data
    y = data['mt2'].data
    q = y / x

    print "len x: " + str(len(x))
    print "len y: " + str(len(y))
    print "len q: " + str(len(q))

    # remove any backslashes
    # and use as plot title
    # plot_filename = 'log-' + folder.replace('/', '-')
    # plot_filename = 'massq-' + folder.replace('/', '-')
    plot_filename = 'ratio current -' + folder.replace('/', '-')

    ax.set_title(plot_filename, fontsize=16, fontweight='bold')
    # ax.set_xlabel('mass 1', fontsize=16, fontweight='bold')
    # ax.set_ylabel('mass 2',        fontsize=16, fontweight='bold')
    ax.set_xlabel('mass ratio q',        fontsize=16, fontweight='bold')

    # x_log10 = np.log10(x)
    # x_log10 = np.ma.log10(x) # mask invalid values
    # y_log10 = np.ma.log10(y) # mask invalid values

    # _, _, _, hist_image = ax.hist2d(x_log10, y_log10, bins=[50,50])
    # _, _, _, hist_image = ax.hist2d(x, y, bins=[1000,1000])
    # _, _, _, hist_image = ax.hist2d(x, q, bins=[1000,1000])

    # ax.hist([x,y], bins=50, histtype='bar', normed=False, lw=1,  
            # alpha=0.4, label=['mass 1', 'mass 2'], 
            # color=['green', 'yellow'],
            # log=True)

    ax.hist(q, bins=50, histtype='bar', normed=False, lw=1,  
            alpha=0.4, label='current mass ratio "q"', 
            color='green', range=(0,7),
            log=False)

    ax.set_xlim([0, 7])
    ax.set_yscale("log", nonposy='clip')
    # ax.yaxis.set_major_formatter(ScalarFormatter())
    # ax.set_ylim(0, 30000)
    plt.legend()




    utils.save_plot(fig, plot_folder, plot_filename, show=False)

    # plt.show()
    # break




# In[]:

