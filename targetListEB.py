import fortUtil as f
import numpy as np
import biseps as b
import kepprf as kp
import matplotlib as mat
import matplotlib.pyplot as plt
import constants as c
import getopt
import sys as sys


opts, extra = getopt.getopt(sys.argv[1:],'')

#f2py -c -m --opt="-Wno-all -march=native -msse3 -mtune=native -O3" fortUtil fortUtil.f90 > /dev/null && time python test.py


x=b.readFile()
ex=b.extras()

ccd=extra[0]
ffiDir=extra[1]
version=extra[2]


#BIseps output
x.appMag5('mag.'+version)
x.kicProp('kic.'+version)
prfAll=np.load(ffiDir+str(ccd)+'.1.npy')
row,col=f.lonlat2pix(x.appMag.long,x.appMag.lat,np.size(x.appMag.long),ccd,c.season,c.fovDir)
	

#Add zodical emission
prfAll+=c.zod

#Load transit data
x.readTransCalc('transit.dat.'+version)

x.readTarget('target.dat.'+version)
ind=(x.target.bin > 0)&(x.target.bin<12)

#Get prf data
prfn, crpix1p, crpix2p, crval1p, crval2p, cdelt1p, cdelt2p,naxis1,naxis2=kp.getPrfDat(ccd)
with open('eb.dat.'+version,'w') as f:
	for i in np.nditer(np.nonzero(ind)):
		i=int(i)
		r,pix,noise=kp.calcMinR(row[i],col[i],x.kicProp[i].kp,ccd,prfAll,prfn,crpix1p,crpix2p,crval1p,crval2p, cdelt1p,cdelt2p,naxis1,naxis2,i)

		#Now set our EB conditions
		#porb < 125 days
		
		#print i,x.target[i].bin,x.trans[i].porb,x.trans[i].depth1,x.trans[i].depth2,noise
		if x.trans[i].porb < 125.0 and x.trans[i].depth1-noise > 10**(-4) and x.trans[i].depth2-noise > 10**(-4):
			#Prim and secondary depths are both above 10^-4 with noise added
			f.write(str(i)+" 1 \n")
		else:
			f.write(str(i)+" 0 \n")


f.close()

