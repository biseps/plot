import numpy as np
import biseps as b
import matplotlib.pyplot as plt


x=b.readFile()
y=b.readFile()
ex=b.extras()

x.kicProp5('quart.dat.4')

y.kicProp('kic.dat.4')

#y.readTarget('target.dat.6c')

mX=ex.radlogg2mass(x.kicProp.rad,x.kicProp.logg)
mY=ex.radlogg2mass(y.kicProp.rad,y.kicProp.logg)
indY=(((y.target.bin>0)&(y.target.bin<12))|(y.target.bin>13))
indX=(x.kicProp.kp<16.0)&(x.kicProp.kp>0.0)&(x.kicProp.teff>0.0)

np.count_nonzero((y.kicProp.kp<16.0)&(y.kicProp.kp>0.0)&(y.kicProp.teff>0.0))

bins=32
plt.figure()
plt.subplot(121)
indY2=indY&(y.kicProp.logg>0.0)&(y.kicProp.logg<3.5)
indX2=indX&(x.kicProp.logg>0.0)&(x.kicProp.logg<3.5)

minX=np.minimum(np.min(y.kicProp[indY2].kp),np.min(x.kicProp[indX2].kp))
maxX=np.maximum(np.max(y.kicProp[indY2].kp),np.max(x.kicProp[indX2].kp))

plt.hist(y.kicProp[indY2].kp,alpha=0.5,label='Us',bins=bins,range=[8.0,16.0])
plt.hist(x.kicProp[indX2].kp,alpha=0.5,label='KIC',bins=bins,range=[8.0,16.0])
plt.title('log g lt 3.5')

plt.subplot(122)
indY2=indY&(y.kicProp.logg>0.0)&(y.kicProp.logg>3.5)
indX2=indX&(x.kicProp.logg>0.0)&(x.kicProp.logg>3.5)
minX=np.minimum(np.min(y.kicProp[indY2].kp),np.min(x.kicProp[indX2].kp))
maxX=np.maximum(np.max(y.kicProp[indY2].kp),np.max(x.kicProp[indX2].kp))

plt.hist(y.kicProp[indY2].kp,alpha=0.5,label='Us',bins=bins,range=[8.0,16.0])
plt.hist(x.kicProp[indX2].kp,alpha=0.5,label='KIC',bins=bins,range=[8.0,16.0])
plt.title('log g gt 3.5')
plt.legend(loc=0)

plt.show()

############################################################


import numpy as np
import biseps as b
import matplotlib.pyplot as plt

x=b.readFile()
y=b.readFile()
ex=b.extras()

x.kicProp5('quart.dat.4')

y.kicProp5('kic.dat.4')

y.readTarget('target.dat.6b')

mX=ex.radlogg2mass(x.kicProp.rad,x.kicProp.logg)
mY=ex.radlogg2mass(y.kicProp.rad,y.kicProp.logg)

#indY=((((y.target.bin>0)&(y.target.bin<12))|(y.target.bin>13))|((y.target.bin==0)&(y.kicProp.rad<5.0)&(y.kicProp.kp<14.0)))&(y.kicProp.logg<3.5)
indY=((((y.target.bin>0)&(y.target.bin<12))|(y.target.bin>13))|((y.target.bin==0)&(y.kicProp.kp<14.0)&(y.kicProp.rad<10.0)))&(y.kicProp.logg<3.5)
indX=(x.kicProp.kp<16.0)&(x.kicProp.kp>0.0)&(x.kicProp.teff>0.0)&(x.kicProp.logg<3.5)

z=y.kicProp[indY]


bins=40
plt.figure(1)

plotX=np.log10(x.kicProp[indX].teff)
plotY=np.log10(z.teff)

minX=np.minimum(np.min(plotY),np.min(plotX))
maxX=np.maximum(np.max(plotY),np.max(plotX))


plt.hist(plotY,alpha=0.5,label='Us',bins=bins,range=[minX,maxX])
plt.hist(plotX,alpha=0.5,label='KIC',bins=bins,range=[minX,maxX])
plt.legend(loc=0)
plt.title('log g <4.0')
plt.xlabel(' log Teff')

#############
plt.figure(2)

plotX=x.kicProp[indX].logg
plotY=z.logg

minX=np.minimum(np.min(plotY),np.min(plotX))
maxX=np.maximum(np.max(plotY),np.max(plotX))


plt.hist(plotY,alpha=0.5,label='Us',bins=bins,range=[minX,maxX])
plt.hist(plotX,alpha=0.5,label='KIC',bins=bins,range=[minX,maxX])
plt.legend(loc=0)
plt.title('log g <4.0')
plt.xlabel('logg')

#############
plt.figure(3)

plotX=x.kicProp[indX].logz
plotY=z.logz

minX=np.minimum(np.min(plotY),np.min(plotX))
maxX=np.maximum(np.max(plotY),np.max(plotX))


plt.hist(plotY,alpha=0.5,label='Us',bins=bins,range=[minX,maxX])
plt.hist(plotX,alpha=0.5,label='KIC',bins=bins,range=[minX,maxX])
plt.legend(loc=0)
plt.title('log g <4.0')
plt.xlabel('logz')


###############
plt.figure(4)

plotX=np.log10(x.kicProp[indX].rad)
plotY=np.log10(z.rad)

minX=np.minimum(np.min(plotY),np.min(plotX))
maxX=np.maximum(np.max(plotY),np.max(plotX))


plt.hist(plotY,alpha=0.5,label='Us',bins=bins,range=[minX,maxX])
plt.hist(plotX,alpha=0.5,label='KIC',bins=bins,range=[minX,maxX])
plt.legend(loc=0)
plt.title('log g <4.0')
plt.xlabel('log R')


#############

plt.figure(5)

plotX=x.kicProp[indX].kp
plotY=z.kp

minX=np.minimum(np.min(plotY),np.min(plotX))
maxX=np.maximum(np.max(plotY),np.max(plotX))


plt.hist(plotY,alpha=0.5,label='Us',bins=bins,range=[minX,maxX])
plt.hist(plotX,alpha=0.5,label='KIC',bins=bins,range=[minX,maxX])
plt.legend(loc=0)
plt.title('log g <4.0')
plt.xlabel('kp')


#############
plt.figure(6)

ex=b.extras()
plotX=ex.radlogg2mass(x.kicProp[indX].rad,x.kicProp[indX].logg)
plotY=ex.radlogg2mass(z.rad,z.logg)

minX=np.minimum(np.min(plotY),np.min(plotX))
maxX=np.maximum(np.max(plotY),np.max(plotX))


plt.hist(plotY,alpha=0.5,label='Us',bins=bins,range=[minX,maxX])
plt.hist(plotX,alpha=0.5,label='KIC',bins=bins,range=[minX,maxX])
plt.legend(loc=0)
plt.title('log g <4.0')
plt.xlabel('mass')


#############

plt.show()



