import biseps as b
import numpy as np
import scipy as sci
import matplotlib.pyplot as plt
import matplotlib as m
from matplotlib import rc
import getopt
import sys as sys

rc('text', usetex=True)
rc('font', family='serif')

#Assume we have catted all the extracts and target files together
x=b.readFile()
e=b.extras()
#.kep

mass1label=r'Mass $[M_{1,\odot}]$'
mass2label=r'Mass $[M_{2,\odot}]$'
periodlabel=r'$\log_{10}$ Period [days]'
massRlabel=r'$\frac{M_{2}}{M_{1}}$'

x.kepler3('extract.bin.targ.1')
#	.ranLoc
x.ranLoc('ranLoc.bin.targ.1')

#y=b.readFile()
#y.kepler3('extract.bin.targAll.1')
#y.ranLoc('ranLoc.bin.targAll.1')

evol=np.zeros(np.size(x.kep),dtype='S4')
for i in xrange(np.size(x.kep)):
	evol[i]=x.kep.evol[i][-4:-3]

indAll=np.zeros(np.size(evol),dtype=bool)
indAll[:]=True
ind1=('N'==evol)
ind2=('W'==evol)
ind3=('B'==evol)
ind4=('A'==evol)
ind5=('M'==evol)
ind6=('C'==evol)
ind7=('H'==evol)

for i in xrange(np.size(x.kep)):
	evol[i]=x.kep.evol[i][-2:-1]

#indAll=np.zeros(np.size(evol),dtype=bool)
#indAll[:]=True
ind1_2=('N'==evol)
ind2_2=('W'==evol)
ind3_2=('B'==evol)
ind4_2=('A'==evol)
ind5_2=('M'==evol)
ind6_2=('C'==evol)
ind7_2=('H'==evol)

###########################################

plt.figure()

plt.scatter(x.kep[ind1].m1,x.kep[ind1].m2,label='NS',c='green',marker='o')
plt.scatter(x.kep[ind2].m1,x.kep[ind2].m2,label='WD',c='red',marker='o')
plt.scatter(x.kep[ind3].m1,x.kep[ind3].m2,label='HG/GB',c='blue',marker='o')
plt.scatter(x.kep[ind4].m1,x.kep[ind4].m2,label='MS',c='yellow',marker='o')
plt.scatter(x.kep[ind5].m1,x.kep[ind5].m2,label='CHeB',c='orange',marker='o')
plt.scatter(x.kep[ind6].m1,x.kep[ind6].m2,label='AGB',c='cyan',marker='o')
plt.scatter(x.kep[ind7].m1,x.kep[ind7].m2,label='nHe',c='magenta',marker='o')

z=[0.0,np.max(x.kep[ind1|ind2|ind3|ind5|ind6|ind7].m1)]
plt.plot(z,z,'--')

plt.legend(loc=0)
plt.xlabel(mass1label)
plt.ylabel(mass2label)
plt.show()

############

plt.figure()

plt.scatter(x.kep[ind1_2].m1,x.kep[ind1_2].m2,label='NS',c='green',marker='o')
plt.scatter(x.kep[ind2_2].m1,x.kep[ind2_2].m2,label='WD',c='red',marker='o')
plt.scatter(x.kep[ind3_2].m1,x.kep[ind3_2].m2,label='HG/GB',c='blue',marker='o')
plt.scatter(x.kep[ind4_2].m1,x.kep[ind4_2].m2,label='MS',c='yellow',marker='o')
plt.scatter(x.kep[ind5_2].m1,x.kep[ind5_2].m2,label='CHeB',c='orange',marker='o')
plt.scatter(x.kep[ind6_2].m1,x.kep[ind6_2].m2,label='AGB',c='cyan',marker='o')
plt.scatter(x.kep[ind7_2].m1,x.kep[ind7_2].m2,label='nHe',c='magenta',marker='o')

z=[0.0,np.max(x.kep.m1)]
plt.plot(z,z,'--')

plt.legend(loc=0)
plt.xlabel(mass1label)
plt.ylabel(mass2label)
plt.show()

############


plt.figure()
plt.scatter(x.kep[ind1].m1,np.log10(x.kep[ind1].p),label='NS',c='green',marker='o')

plt.scatter(x.kep[ind2].m1,np.log10(x.kep[ind2].p),label='WD',c='red',marker='o')

#ind3=('B'==evol)
#plt.scatter(x.kep[ind3].m1,np.log10(x.kep[ind3].p),label='HG/GB',c='blue',marker='o')

##ind4=('A'==evol)
##plt.scatter(x.kep[ind4].m1,np.log10(x.kep[ind4].p),label='MS',c='yellow',marker='o')

#ind5=('M'==evol)
#plt.scatter(x.kep[ind5].m1,np.log10(x.kep[ind5].p),label='CHeB',c='orange',marker='o')

#ind6=('C'==evol)
#plt.scatter(x.kep[ind6].m1,np.log10(x.kep[ind6].p),label='AGB',c='cyan',marker='o')

#ind7=('H'==evol)
#plt.scatter(x.kep[ind7].m1,np.log10(x.kep[ind7].p),label='nHe',c='magenta',marker='o')

plt.legend(loc=0)
plt.xlabel(mass1label)
plt.ylabel(periodlabel)
plt.show()



#####
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt


fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x.kep[ind1].m1,x.kep[ind1].m2,np.log10(x.kep[ind1].p),label='NS',c='green',marker='o')
ax.scatter(x.kep[ind2].m1,x.kep[ind2].m2,np.log10(x.kep[ind2].p),label='WD',c='blue',marker='D')

ax.set_xlabel(mass1label)
ax.set_ylabel(mass2label)
ax.set_zlabel(periodlabel)

plt.show()

#######################

a=e.porb2a(x.kep.m1,x.kep.m2,x.kep.p)
roche1=e.roche(a,x.kep.m1,x.kep.m2)
roche2=e.roche(a,x.kep.m2,x.kep.m1)

indroche1=(x.kep.r1/roche1 > 0.95)
indroche2=(x.kep.r2/roche2 > 0.95)

indR=(indroche1|indroche2)

plt.figure()
#if np.any(ind1):
	#plt.scatter(x.kep[ind1].m1,np.log10(x.kep[ind1].p),label='NS',c='green',marker='D',alpha=0.15)
#if np.any(ind2):
	#plt.scatter(x.kep[ind2].m1,np.log10(x.kep[ind2].p),label='WD',c='red',marker='D',alpha=0.15)
#if np.any(ind3):
	#plt.scatter(x.kep[ind3].m1,np.log10(x.kep[ind3].p),label='HG/GB',c='blue',marker='D',alpha=0.15)
if np.any(ind4):
	ind=ind4&ind4_2&(np.log10(x.kep.p)<1.0)
	plt.scatter(x.kep[ind].m1,x.kep[ind].m2,s=np.maximum(x.kep[ind].r1/roche1[ind],x.kep[ind].r2/roche2[ind])*30.0,label='MS',c='red',marker='D',alpha=0.15)
#if np.any(ind5):
	#plt.scatter(x.kep[ind5].m1,np.log10(x.kep[ind5].p),label='CHeB',c='orange',marker='D',alpha=0.15)
#if np.any(ind6):
	#plt.scatter(x.kep[ind6].m1,np.log10(x.kep[ind6].p),label='AGB',c='cyan',marker='D',alpha=0.15)

#if np.any(ind1&ind):
	#plt.scatter(x.kep[ind1&ind].m1,np.log10(x.kep[ind1&ind].p),label='NS R',c='green',marker='o')
#if np.any(ind2&ind):
	#plt.scatter(x.kep[ind2&ind].m1,np.log10(x.kep[ind2&ind].p),label='WD R',c='red',marker='o')
#if np.any(ind3&ind):
	#plt.scatter(x.kep[ind3&ind].m1,np.log10(x.kep[ind3&ind].p),label='HG/GB R',c='blue',marker='o')
if np.any(ind4&ind):
	ind=ind4&indR&ind4_2&(np.log10(x.kep.p)<1.0)
	plt.scatter(x.kep[ind].m1,x.kep[ind].m2,s=np.maximum(x.kep[ind].r1/roche1[ind],x.kep[ind].r2/roche2[ind])*30.0,label='MS R',c='yellow',marker='o')
#if np.any(ind5&ind):
	#plt.scatter(x.kep[ind5&ind].m1,np.log10(x.kep[ind5&ind].p),label='CHeB R',c='orange',marker='o')
#if np.any(ind6&ind):
	#plt.scatter(x.kep[ind6&ind].m1,np.log10(x.kep[ind6&ind].p),label='AGB R',c='cyan',marker='o')
plt.legend(loc=0)

plt.show()

plt.figure()
if np.any(ind1):
	plt.scatter(x.kep[ind1].r1/roche1[ind1],x.kep[ind1].r2/roche2[ind1],label='NS ',c='green',marker='o')
if np.any(ind2):
	plt.scatter(x.kep[ind2].r1/roche1[ind2],x.kep[ind2].r2/roche2[ind2],label='WD ',c='red',marker='o')
if np.any(ind3):
	plt.scatter(x.kep[ind3].r1/roche1[ind3],x.kep[ind3].r2/roche2[ind3],label='HG/GB ',c='blue',marker='o')
if np.any(ind4):
	plt.scatter(x.kep[ind4].r1/roche1[ind4],x.kep[ind4].r2/roche2[ind4],label='MS ',c='yellow',marker='o')
if np.any(ind5):
	plt.scatter(x.kep[ind5].r1/roche1[ind5],x.kep[ind5].r2/roche2[ind5],label='CHeB ',c='orange',marker='o')
if np.any(ind6):
	plt.scatter(x.kep[ind6].r1/roche1[ind6],x.kep[ind6].r2/roche2[ind6],label='AGB ',c='cyan',marker='o')
if np.any(ind7):
	plt.scatter(x.kep[ind7].r1/roche1[ind7],x.kep[ind7].r2/roche2[ind7],label='n He ',c='magenta',marker='o')
plt.legend(loc=0)
plt.show()




####################################
a=e.porb2a(x.kep.m1,x.kep.m2,x.kep.p)
roche1=e.roche(a,x.kep.m1,x.kep.m2)
roche2=e.roche(a,x.kep.m2,x.kep.m1)

indroche1=(x.kep.r1/roche1 > 0.95)
indroche2=(x.kep.r2/roche2 > 0.95)

ind=(indroche1|indroche2)

plt.figure()
plt.scatter(np.minimum(x.kep[ind4].m2,x.kep[ind4].m1)/np.maximum(x.kep[ind4].m2,x.kep[ind4].m1),np.log10(x.kep[ind4].p),label='MS',c='yellow',marker='D',alpha=0.15)

plt.scatter(np.minimum(x.kep[ind4&ind].m2,x.kep[ind4&ind].m1)/np.maximum(x.kep[ind4&ind].m2,x.kep[ind4&ind].m1),np.log10(x.kep[ind4&ind].p),label='MS R',c='yellow',marker='o')

plt.show()

###########################################################################################


plt.figure()
#if np.any(ind1):
	#plt.scatter(x.kep[ind1].m1,np.log10(x.kep[ind1].p),label='NS',c='green',marker='D',alpha=0.5)
#if np.any(ind2):
	#plt.scatter(x.kep[ind2].m1,np.log10(x.kep[ind2].p),label='WD',c='red',marker='D',alpha=0.5)
#if np.any(ind3):
	#plt.scatter(x.kep[ind3].m1,np.log10(x.kep[ind3].p),label='HG/GB',c='blue',marker='D',alpha=0.5)
##if np.any(ind4):
	##plt.scatter(x.kep[ind4].m1,np.log10(x.kep[ind4].p),label='MS',c='yellow',marker='D',alpha=0.5)
#if np.any(ind5):
	#plt.scatter(x.kep[ind5].m1,np.log10(x.kep[ind5].p),label='CHeB',c='orange',marker='D',alpha=0.5)
#if np.any(ind6):
	#plt.scatter(x.kep[ind6].m1,np.log10(x.kep[ind6].p),label='AGB',c='cyan',marker='D',alpha=0.5)

if np.any(ind1_2&ind1):
	plt.scatter(x.kep[ind1_2&ind1].m2,np.log10(x.kep[ind1_2&ind1].p),label='NS 2',c='green',marker='o',alpha=0.5)
if np.any(ind2_2&ind1):
	plt.scatter(x.kep[ind2_2&ind1].m2,np.log10(x.kep[ind2_2&ind1].p),label='WD 2',c='red',marker='o',alpha=0.5)
if np.any(ind3_2&ind1):
	plt.scatter(x.kep[ind3_2&ind1].m2,np.log10(x.kep[ind3_2&ind1].p),label='HG/GB 2',c='blue',marker='o',alpha=0.5)
if np.any(ind4_2&ind1):
	plt.scatter(x.kep[ind4_2&ind1].m2,np.log10(x.kep[ind4_2&ind1].p),label='MS 2',c='yellow',marker='o',alpha=0.5)
if np.any(ind5_2&ind1):
	plt.scatter(x.kep[ind5_2&ind1].m2,np.log10(x.kep[ind5_2&ind1].p),label='CHeB 2',c='orange',marker='o',alpha=0.5)
if np.any(ind6_2&ind1):
	plt.scatter(x.kep[ind6_2&ind1].m2,np.log10(x.kep[ind6_2&ind1].p),label='AGB 2',c='cyan',marker='o',alpha=0.5)
plt.legend(loc=0)

plt.show()



######################
ind1=np.zeros(np.size(x.kep),dtype=bool)
ind2=np.zeros(np.size(x.kep),dtype=bool)
evol=np.zeros(np.size(x.kep),dtype='S4')
for i in xrange(np.size(x.kep)):
	evol[i]=x.kep.evol[i][-4:-3]
	ind1[i]=('N' in evol[i])
	ind2[i]=('W' in evol[i])

#Get the diff mass transfer phases
ind_ce=np.zeros(np.size(x.kep),dtype=bool)
ind_mt=np.zeros(np.size(x.kep),dtype=bool)
for i in xrange(np.size(x.kep)):
	ind_ce[i]=('E' in x.kep.evol[i])
	ind_mt[i]=('r' in x.kep.evol[i]) |('d' in x.kep.evol[i])|('t' in x.kep.evol[i])|('n' in x.kep.evol[i])


plt.figure()

#indAll=ind1&(~ind_ce)&(~ind_mt)
#if np.any(indAll):
	#plt.scatter(x.kep[indAll].m2,np.log10(x.kep[indAll].p),label='NS',c='green',marker='o')
indAll=ind2&(~ind_ce)&(~ind_mt)
if np.any(indAll):
	plt.scatter(x.kep[indAll].m1/x.kep[indAll].m2,np.log10(x.kep[indAll].p),label='WD',c='red',marker='o')

#indAll=ind1&ind_ce&(~ind_mt)
#if np.any(indAll):
	#plt.scatter(x.kep[indAll].m2,np.log10(x.kep[indAll].p),label='NS CE',c='green',marker='D')
indAll=ind2&ind_ce&(~ind_mt)
if np.any(indAll):
	plt.scatter(x.kep[indAll].m1/x.kep[indAll].m2,np.log10(x.kep[indAll].p),label='WD CE',c='red',marker='D')

#indAll=ind1&ind_mt&(~ind_ce)
#if np.any(indAll):
	#plt.scatter(x.kep[indAll].m2,np.log10(x.kep[indAll].p),label='NS Mt',c='green',marker='+')
indAll=ind2& ind_mt&(~ind_ce)
if np.any(indAll):
	plt.scatter(x.kep[indAll].m1/x.kep[indAll].m2,np.log10(x.kep[indAll].p),label='WD Mt',c='red',marker='+')

#indAll=ind1&ind_mt&ind_ce
#if np.any(indAll):
	#plt.scatter(x.kep[indAll].m2,np.log10(x.kep[indAll].p),label='NS Mt CE',c='green',marker='s')
indAll=ind2& ind_mt&ind_ce
if np.any(indAll):
	plt.scatter(x.kep[indAll].m1/x.kep[indAll].m2,np.log10(x.kep[indAll].p),label='WD Mt CE',c='red',marker='s')


plt.legend(loc=0)
plt.xlabel(r'$\dfrac{M_{wd}}{M_{2}}$')
plt.ylabel(periodlabel)
plt.xlim(0.0,2.0)
plt.show()

#########################################
mydescr =np.dtype([('id','float'),('age','float'),('m1','float'),('t','int'),
('m2','float'),('t2','int'),('p','float'),('bt','int'),('evol','S4'),])
birth = x.read_array(filename='wd.txt', dtype=mydescr,size=9)

plt.scatter(birth[birth.t==1].m1,np.log10(birth[birth.t==1].p),c='red')
plt.scatter(birth[birth.t==2].m1,np.log10(birth[birth.t==2].p),c='yellow')
plt.show()

#######################
import numpy as np
import matplotlib as m
import matplotlib.pyplot as plt

q0=np.load('34.1.npy')
q1=np.load('34.q1.npy')
qm1=np.load('34.qm1.npy')

q0=q0+10**(19/-2.5)
q1=q1+10**(19/-2.5)
qm1=qm1+10**(19/-2.5)
import matplotlib as m
cm = plt.get_cmap('jet_r')

minq=np.fmin(np.fmin(np.min(np.log10(q0)),np.min(np.log10(q1))),np.min(np.log10(qm1)))
maxq=np.fmax(np.fmax(np.max(np.log10(q0)),np.max(np.log10(q1))),np.max(np.log10(qm1)))

norm=m.colors.Normalize(vmin=minq,vmax=maxq)

ax=plt.figure()
ax1=plt.subplot(1,3,1)
plt.imshow(np.log10(q0),interpolation='nearest',origin='lower',cmap=cm,norm=norm)
ax2=plt.subplot(1,3,2,sharex=ax1, sharey=ax1)
plt.imshow(np.log10(q1),interpolation='nearest',origin='lower',cmap=cm,norm=norm)
ax3=plt.subplot(1,3,3,sharex=ax1, sharey=ax1)
plt.imshow(np.log10(qm1),interpolation='nearest',origin='lower',cmap=cm,norm=norm)
plt.tight_layout()
plt.show()



######################################

######################
ind1=np.zeros(np.size(x.kep),dtype=bool)
ind2=np.zeros(np.size(x.kep),dtype=bool)
evol=np.zeros(np.size(x.kep),dtype='S4')
for i in xrange(np.size(x.kep)):
	evol[i]=x.kep.evol[i][-4:-3]
	ind1[i]=('N' in evol[i])
	ind2[i]=('W' in evol[i])

#Get the diff mass transfer phases
ind_ce=np.zeros(np.size(x.kep),dtype=bool)
ind_ce_gb=np.zeros(np.size(x.kep),dtype=bool)
ind_ce_agb=np.zeros(np.size(x.kep),dtype=bool)
ind_mt=np.zeros(np.size(x.kep),dtype=bool)
for i in xrange(np.size(x.kep)):
	ind_ce_gb[i]=('BE' in x.kep.evol[i])
	ind_ce_agb[i]=('CE' in x.kep.evol[i])

	ind_ce[i]=('E' in x.kep.evol[i])
	ind_mt[i]=('r' in x.kep.evol[i]) |('d' in x.kep.evol[i])|('t' in x.kep.evol[i])|('n' in x.kep.evol[i])


plt.figure()


indAll=ind2&(~ind_ce)&(~ind_mt)
if np.any(indAll):
	plt.scatter(x.kep[indAll].m1/x.kep[indAll].m2,np.log10(x.kep[indAll].p),label='WD',c='red',marker='o')

indAll=ind2&ind_ce&(~ind_mt)&(~ind_ce_gb)&(~ind_ce_agb)
if np.any(indAll):
	plt.scatter(x.kep[indAll].m1/x.kep[indAll].m2,np.log10(x.kep[indAll].p),label='WD CE',c='gree',marker='o')

indAll=ind2&ind_ce&(~ind_mt)&ind_ce_gb
if np.any(indAll):
	plt.scatter(x.kep[indAll].m1/x.kep[indAll].m2,np.log10(x.kep[indAll].p),label='WD CE GB',c='blue',marker='o')

indAll=ind2&ind_ce&(~ind_mt)&ind_ce_agb&(x.kep.p<80.0)
if np.any(indAll):
	plt.scatter(x.kep[indAll].m1/x.kep[indAll].m2,np.log10(x.kep[indAll].p),label='WD CE AGB ',c='black',marker='D')

indAll=ind2&ind_ce&(~ind_mt)&ind_ce_agb&(x.kep.p>80.0)
if np.any(indAll):
	plt.scatter(x.kep[indAll].m1/x.kep[indAll].m2,np.log10(x.kep[indAll].p),label='WD CE AGB ',c='black',marker='+')

indAll=ind2& ind_mt&(~ind_ce)
if np.any(indAll):
	plt.scatter(x.kep[indAll].m1/x.kep[indAll].m2,np.log10(x.kep[indAll].p),label='WD Mt',c='magenta',marker='o')

indAll=ind2& ind_mt&ind_ce
if np.any(indAll):
	plt.scatter(x.kep[indAll].m1/x.kep[indAll].m2,np.log10(x.kep[indAll].p),label='WD Mt CE',c='orange',marker='o')


plt.legend(loc=0)
plt.xlabel(r'$\dfrac{M_{wd}}{M_{2}}$')
plt.ylabel(periodlabel)
plt.xlim(0.0,2.0)
plt.show()


####################
import biseps as b
import numpy as np
import scipy as sci
import matplotlib.pyplot as plt
import matplotlib as m
from matplotlib import rc
import getopt
import sys as sys

rc('text', usetex=True)
rc('font', family='serif')

#Assume we have catted all the extracts and target files together
x=b.readFile()
e=b.extras()
#.kep

x.kepler3('extract.bin.targ.1')
#	.ranLoc
x.ranLoc('ranLoc.bin.targ.1')

x.readBirth('../../channels/WEB_birth.dat.1')
	
with open('birth.targ.1',"w") as f:
	for i in x.kep.num:
		f.write(' '.join([`num` for num in x.birth[x.birth.num==int(i)][0]])+"\n")
		print i

f.close()


