# attempt as doing error bars

import biseps as b
import matplotlib.pyplot as plt
from matplotlib import rc
import numpy as np

rc('text', usetex=True)
rc('font', family='serif')

z=b.readFile()
z.kic3(file='kic.r1.out')
(z_n,z_bins,z_pathcs)=plt.hist(z.kic3.kp,bins=32,range=[8,16])


x=b.readFile()
x.ranLoc('ran.sing.loc')
(x_n,x_bins,x_patchs)=plt.hist(x.ranLoc.appMag,bins=32,range=[8,16])
x_n2=x_n
sumxN=np.sum(x_n)
x_n=x_n/(1.0*sumxN)

culSum=np.zeros(np.size(x_n),dtype='float64')
culSumSq=np.zeros(np.size(x_n),dtype='float64')
stddev=np.zeros(np.size(x_n),dtype='float64')

for i in range(1,13):
	y=b.readFile()
	y.ranLoc("ran.sing.err."+str(i))
	(y_n,y_bins,y_patchs)=plt.hist(y.ranLoc.appMag,bins=32,range=[8,16])
	y_n=y_n/(1.0*np.sum(y_n))
	culSum=culSum+y_n
	culSumSq=culSumSq+y_n**2
	plt.cla()
	plt.clf()
	plt.close()
	print i

stddev=np.sqrt(np.subtract(culSumSq/12.0,(culSum/12.0)**2))


x_bins2=np.zeros(np.size(x_n),dtype='float64')
for i in range(0,np.size(x_bins)-1):
	x_bins2[i]=(x_bins[i]+x_bins[i+1])/2.0

plt.figure(1)
plt.bar(x_bins[0:np.size(x_bins)-1],x_n,width=x_bins[1]-x_bins[0],alpha=0.5,label='Us',color='green')

plt.plot(x_bins2,x_n+stddev,'r--', linewidth=1,label='Std Dev')
plt.plot(x_bins2,x_n-stddev,'r--', linewidth=1)

x_n2=np.sqrt(x_n2)/sumxN

#print x_n2

plt.plot(x_bins2,x_n+x_n2,'r-.', linewidth=1,label=r'$\sqrt n$')
plt.plot(x_bins2,x_n-x_n2,'r-.', linewidth=1)

plt.bar(x_bins[0:np.size(x_bins)-1],z_n/np.sum(1.0*z_n),width=x_bins[1]-x_bins[0],alpha=0.5,label='kic',color='blue')

#plt.xlabel('Galactic latitude, b (degrees)')
plt.xlabel(r'$m_{Kp}$')
plt.ylabel('Relative Frequency')
#plt.title('Latitude distribution with errors, single stars')
plt.legend(loc=0)
#plt.show()
plt.savefig('appMag_err.png')
#plt.cla()
#plt.close()
