

# In[]:

import numpy as np
from astropy.io import ascii
from astropy.table import vstack
from astropy.table import Table
from astropy.table import join
import astropy.constants as const

import matplotlib
import matplotlib.pyplot as plt
import plotUtils as utils


import seaborn as sns


# In[]:

# totals3=np.genfromtxt("totals.csv", delimiter=',', names=True)




# In[]:

# data_dir='/padata/beta/users/efarrell/data/ngts_fields/11_run/field1/blends_5'

data_dir = '/padata/beta/users/efarrell/data/plato/sim_transits/code/blends'

input_file = os.path.join(data_dir, 'planet_like.1.csv')

totals  = ascii.read(input_file, delimiter=' ')


# runs = np.arange(0,100)

# In[]:

fig = plt.figure(figsize=(10, 15))

# binsize=50
binsize=20 # for 2nd eclipse

# bartype='step'
bartype='bar'
# do_normed=True
do_normed=False

# data1=totals['transit_probability'].data * 

# data2=totals['a'].data

# data1=totals['actual_transit_depth_1'].data
# data2=totals['actual_transit_depth_2'].data

data1=totals['measured_transit_depth_1'].data
data2=totals['measured_transit_depth_2'].data

ax = plt.subplot(111)
# h2 = ax.hist(data1, bins=binsize, histtype=bartype, normed=do_normed, lw=3, color='green', alpha=0.3, range=[0, 0.05], label='1st (primary) eclipse')
h3 = ax.hist(data2, bins=binsize, histtype=bartype, normed=do_normed, lw=3, color='blue', alpha=0.3, range=[0, 0.01], label='2nd (secondary) eclipse')

ax.set_title('Blended transit depths: Latitude 50   (square 76)')

handles, labels = ax.get_legend_handles_labels()
ax.legend(handles, labels)
ax.grid(True, which='minor')

ax.set_xlabel('transit depth (blended)', fontsize=13, fontweight='bold', labelpad=20)
ax.set_ylabel('Number of stars', fontsize=13, fontweight='bold', labelpad=20)

# ax.set_xlim([0,0.05])
ax.set_xlim([0,0.02]) # for 2nd eclipse

# ax.set_ylim([0,900])

plt.show()

# In[]:

# scatter plot

ax = plt.subplot(111)
# ax.set_title(filename)
ax.scatter(totals['bin_luminosity1'].data, totals['bin_luminosity2'].data, s=15, lw=0, alpha=0.7)

ax.set_xlim([0,40])
ax.set_ylim([0,5])

ax.set_title('characteristics of binaries producing planet-like transits')
ax.grid(True, which='minor')
ax.set_xlabel('luminosity binary 1 (solar luminosities)', fontsize=13)
ax.set_ylabel('luminosity binary 2', fontsize=13)

plt.show()
# save_plot(fig, '.', 'probs.jpg')


# In[]:




