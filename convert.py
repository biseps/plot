import numpy as np

def convert(lon,lat):
	
	deg2rad=np.pi/180.0
	lon=lon*deg2rad
	lat=lat*deg2rad
	
	clat=np.cos(lat)
	slat=np.sin(lat)
	
	an=32.93192*deg2rad #/* G lng of asc node on equator */
	gpr = 192.8594*deg2rad #  /* RA of North Gal Pole, 2000 */
	gpd = 27.12825*deg2rad #/* Dec of  " */
	
	cgpd=np.cos(gpd)
	sgpd=np.sin(gpd)
	
	a=lon-an
	
	ca=np.cos(a)
	sa=np.sin(a)
	b=sa
	
	sq=(clat*cgpd*b)+(slat*sgpd)
	dec=np.arcsin(sq)
	
	c=clat*ca
	d=(slat*cgpd)-(clat*sgpd*sa)
	
	ind=(np.abs(d) < 1e-20)
	d[ind]=1e-20

	ra=np.arctan(c/d)+gpr
	
	ind=(d<0)
	ra[ind]=ra[ind]+np.pi
	ind=(ra<0)
	ra[ind]=ra[ind]+2*np.pi
	ind=(a > 2*np.pi)
	ra[ind]=ra[ind]-2*np.pi
	
	return ra/deg2rad,dec/deg2rad
