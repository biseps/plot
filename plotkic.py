import numpy as np
import biseps as b
import kepprf as kp
import fortUtil as f
import matplotlib as mat
import matplotlib.pyplot as plt
import sys as sys
import constants as c

x=b.readFile()
ex=b.extras()

x.readFOV2('fov/3/1.a.mag')

#row,col=f.lonlat2pix(x.fov.long,x.fov.lat,np.size(x.fov.long),1,3)

prfAll=kp.prfImage(x.fov.row,x.fov.col,x.fov.mag,1)

#Add zodiacal noise, ~ one 19th mag star per pixel
prfAll=prfAll+10**(-19.0/2.5)


prfAll[(prfAll>0.0)]=np.sqrt(prfAll[(prfAll>0.0)])

#Various pixels used by keplet not for science
prfAll[0:19,12:1111]=0.0
prfAll[1044:1069,12:1111]=0.0
prfAll[0:1069,0:11]=0.0
prfAll[0:1069,1112:1131]=0.0
prfAll[1059:1062,12:1111]=0.0

colmap='jet_r'
norm=mat.colors.Normalize(vmin=np.min(prfAll),vmax=np.max(prfAll))
plt.imshow(prfAll,cmap=colmap,norm=norm,origin='lower',interpolation='nearest')
plt.colorbar()
plt.show()