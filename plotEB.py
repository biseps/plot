import biseps as b
import numpy as np
import matplotlib.pyplot as plt
import sys
#import stellAct as s
import getopt
import sys as sys
from itertools import cycle

colours = ["g","y","c","m"]
colcycler = cycle(colours)

opts, extra = getopt.getopt(sys.argv[1:],'')

version=extra[0]

def stdDev(f):
	files1=np.unique(f)
	n=np.zeros([len(files1)])
	for i in range(1,len(files1)+1):
		n[i-1]=np.count_nonzero(f==i)
	nStd=np.std(n,ddof=1)
	return nStd*1.0


x=b.readFile()

version="1"
#numTrys=10.0


bins=40
pMax=20.0
pMin=1.0

y=np.genfromtxt('/padata/beta/users/rfarmer/data/keplerEB.v2.txt',case_sensitive='lower',dtype=[('kic', '<f8'), ('type', 'a3'), ('bjd0', '<f8'), ('p0', '<f8'), ('kmag', '<f8'), ('t2t1', '<f8'), ('p1p2', '<f8'), ('q', '<f8'), ('esinw', '<f8'), ('ecosw', '<f8'), ('ff', '<f8'), ('sini', '<f8')])

y['t2t1'][y['t2t1']>1.0]=1.0/y['t2t1'][y['t2t1']>1.0]

kepInd=(y['p0']>=pMin)&(y['p0']<=pMax)&(y['t2t1']>0.0)&(y['type']=="D")
#kepInd=((y['type']=="ELV"))&(y['t2t1']>0.0)&(y['p0']>=pMin)&(y['p0']<=pMax)

#kepPlot=np.degrees(np.arcsin(y['sini']))
#kepPlot[kepPlot>90.0]=180.0-kepPlot[kepPlot>90.0]

#y['q'][y['q']>1.0]=1.0/y['q'][y['q']>1.0]
#kepPlot=1.0/y['q']

#kepPlot=np.log10(y['p0'])
#kepPlot=y['t2t1']
plt.figure()

#maxP1P2=np.max(y['p1p2'][kepInd])
#minP1P2=np.min(y['p1p2'][kepInd])
#maxP=np.max(y['p0'][kepInd])
#minP=np.min(y['p0'][kepInd&(y['p0']>0.0)])

print "kepler",np.count_nonzero(kepInd),np.count_nonzero(kepInd)/165434.0,np.count_nonzero(y['type']=="D")/165434.0,np.count_nonzero(y['type']=="SD")/165434.0

rng=[0.0,1.0]
#rng=[np.log10(pMin),np.log10(pMax)]

size=bins
bins2=np.linspace(rng[0],rng[1],size+1)

width=np.diff(bins2)
total=np.zeros(size)

hSum=np.histogram(kepPlot[kepInd&((y['type']=="D")|(y['type']=="SD"))],bins=bins2)[0]
hSum=np.sum(hSum * width*1.0)
#print hSum

h=np.histogram(kepPlot[kepInd&(y['type']=="D")],bins=bins2)[0]
plt.bar(bins2[0:-1],h/hSum,width=width,bottom=total,label="D",color='b')
total+=h/hSum

h=np.histogram(kepPlot[kepInd&(y['type']=="SD")],bins=bins2)[0]
plt.bar(bins2[0:-1],h/hSum,width=width,bottom=total,label="SD",color='r')

total+=h/hSum
h=np.histogram(kepPlot[kepInd&(y['type']=="ELV")],bins=bins2)[0]
plt.bar(bins2[0:-1],h/hSum,width=width,bottom=total,label="ELV",color='k')
#n,binLoc,patchs=plt.hist(kepPlot[kepInd&(y['type']=="D")],bins=bins,range=rng,normed=True,alpha=0.50)

##n,binLoc,patchs=plt.hist(kepPlot[kepInd&(y['type']=="SD")],bins=bins,range=rng,normed=True,alpha=0.50)



##h=np.histogram(xPlot[ind],bins=bins)[0]
##b=plt.bar(bins[0:-1],h,width=width,bottom=total,label=translate(i)+"+"+translate(j),color=next(colcycler))
##total+=h

##rng=[np.log10(pMin),np.log10(pMax)]
##n,binLoc,patchs=plt.hist(np.log10(y['p0'][kepInd]),bins=bins,range=rng,normed=True,alpha=0.50)

def plotEB(folder,v="1",totNum=1):
	f='/padata/beta/users/rfarmer/data/kepEB2/'+folder+"/"
	x=b.readFile()
	x.kepler5(f+'merge/ext.eb.targ.1')

	try:
		x.readEB3(f+'merge/eb.eb.targ.3')
	except:
		x.readEB2(f+'merge/eb.eb.targ.1')

	r1r2=(x.kep.r1+x.kep.r2)/x.kep.a
	#xPlot=(x.kep.r1+x.kep.r2)/x.kep.a
	#xPlot=np.log10(x.kep.p)
	#xPlot=1.0/x.kep.mr
	xPlot=x.kep.mr
	
	#xPlot=np.log10(x.eb.depth1)

	#detectBoth=((x.eb.stype>=0)&(x.eb.stype<=2))&(((x.eb.dd1==1)&(x.eb.dd2==1))|(x.eb.depth1>0.2/x.kep.r1)|(x.eb.depth2>0.2/x.kep.r2))&(x.kep.p<=pMax)&(x.kep.p>pMin)
	#detectBoth=((x.eb.stype==2))&(((x.eb.dd1==1)&(x.eb.dd2==1))|(x.eb.depth1>0.2/x.kep.r1)|(x.eb.depth2>0.2/x.kep.r2))&(x.kep.p<=pMax)&(x.kep.p>pMin)

	detached=(x.eb.stype==0)&(((x.eb.dd1Eclip==1)&(x.eb.dd12Eclip==1))|((x.eb.d1==1)|(x.eb.d2==1)))
	semi=(x.eb.stype==1)&(((x.eb.dd1Eclip==1)&(x.eb.dd12Eclip==1))|((x.eb.d1==1)|(x.eb.d2==1)))
	ellip=(x.eb.stype==2)
	detectBoth=(detached|semi)&(x.kep.p<=pMax)&(x.kep.p>=pMin)
	#print np.count_nonzero(detectBoth)
	
	f1=0
	numTrys=0
	f1=np.unique(x.eb.fileNum[detectBoth])
	numTrys=np.count_nonzero(f1)*1.0
	#num,binP,patchs=plt.hist(x1,alpha=0.5,bins=bins,range=[xmin,xmax],label=l1,normed=True)

	num=np.histogram(xPlot[detectBoth],bins=bins,range=rng,normed=True)[0]

	n,binP=np.histogram(xPlot[detectBoth],bins=bins,range=rng)
	n=n/numTrys
	scale=np.sum(n)*np.diff(binP)
	
	n2=np.zeros([int(numTrys),bins])
	for i in range(len(f1)):
		n2[i]=np.histogram(xPlot[detectBoth&(x.eb.fileNum==f1[i])],bins=bins,range=rng)[0]

	nStd=np.std(n2,axis=0,ddof=1)


	c=next(colcycler)
	plt.plot((binP[:-1]+binP[1:])/2,num,label=folder+" "+v,linewidth=3,c=c)
	plt.errorbar((binP[:-1]+binP[1:])/2,num,yerr=nStd/scale,fmt=None,linewidth=2,ecolor=c)
	print(folder,np.count_nonzero(detectBoth)/numTrys,np.count_nonzero(detectBoth)/numTrys/totNum,
		np.count_nonzero(detached&(x.kep.p<pMax)&(x.kep.p>pMin))/numTrys/totNum,np.count_nonzero(semi&(x.kep.p<pMax)&(x.kep.p>pMin))/numTrys/totNum,
		np.count_nonzero((x.eb.stype==0)&(x.kep.p<pMax)&(x.kep.p>pMin))/numTrys/totNum,np.count_nonzero((x.eb.stype==1)&(x.kep.p<pMax)&(x.kep.p>pMin))/numTrys/totNum)


	#x.kepler5(f+'merge/extract.bin.targAll.1')
	#xPlot=x.kep.mr
	##xPlot=np.log10(x.kep.p)
	##xPlot=(x.kep.r1+x.kep.r2)/x.kep.a
	#detectBoth=(x.kep.p<=pMax)&(x.kep.p>=pMin)
	#num=np.histogram(xPlot[detectBoth],bins=bins,range=rng,normed=True)[0]
	#plt.plot((binP[:-1]+binP[1:])/2,num,'--',linewidth=3,c=c)

	#detectBoth=(x.kep.p>=0.0)
	#num=np.histogram(xPlot[detectBoth],bins=bins,range=rng,normed=True)[0]
	#plt.plot((binP[:-1]+binP[1:])/2,num,'-.',linewidth=3,c=c)
		
	#x.kepler5(f+'merge/extract.bin.targ.1')
	#xPlot=x.kep.mr
	##sxPlot=np.log10(x.kep.p)
	##xPlot=(x.kep.r1+x.kep.r2)/x.kep.a
	#detectBoth=(x.kep.p<=pMax)&(x.kep.p>=pMin)
	#num=np.histogram(xPlot[detectBoth],bins=bins,range=rng,normed=True)[0]
	#plt.plot((binP[:-1]+binP[1:])/2,num,'-.',linewidth=3,c=c)

	#detectBoth=(x.kep.p>=pMin)
	#num=np.histogram(xPlot[detectBoth],bins=bins,range=rng,normed=True)[0]
	#plt.plot((binP[:-1]+binP[1:])/2,num,'--',linewidth=3,c=c)

plotEB('flat_bfrac_0.5',totNum=213110.0)
#plotEB('flat_bfrac_0.5_b2',totNum=213110.0,v="2")
#plotEB('qp1.0_bfrac_0.5',totNum=231815.0)
#plotEB('qm0.5_bfrac_0.5',totNum=187569.0)
#plotEB('m1m2imf_bfrac_0.5',totNum=187569.0)
#plotEB('qm1.0_bfrac_0.5',totNum=208090.0)


plt.ylabel('Normalised Frequency',fontsize=28)
plt.legend(loc=0)
plt.xlabel("t2/t1",fontsize=28)
#plt.xlabel("r1+r2/a",fontsize=28)
#plt.xlabel("log p",fontsize=28)
#plt.xlabel("degrees",fontsize=28)
plt.show()
#exit()

#folder='flat_bfrac_0.5'
#f='/padata/beta/users/rfarmer/data/kepEB2/'+folder+"/"
#x=b.readFile()
#x.kepler5(f+'merge/ext.eb.targ.1')
#x.readEB(f+'merge/eb.eb.targ.1')


#def unique_rows(a):
    #unique_a = np.unique(a.view([('', a.dtype)]*a.shape[1]))
    #return unique_a.view(a.dtype).reshape((unique_a.shape[0], a.shape[1]))


#def translate(kw):
	#if (kw==1): return "MS"
	#if (kw==2): return "HG"
	#if (kw==3): return "GB"
	#if (kw==4): return "CHe"
	#if (kw==5): return "AGB"
	#if (kw==7): return "nHe"
	#if (kw==10): return "WD"
	#if (kw==13): return "NS"

##evol=x.kep.evol
##evol=evol.strip("'")
#r1r2=(x.kep.r1+x.kep.r2)/x.kep.a
#xPlot=x.kep.tr

#detached=(x.eb.stype==0)
#semi=(x.eb.stype==1)
#ellip=(x.eb.stype==2)
#detectBoth=(detached|semi|ellip)&(x.kep.p<=pMax)&(x.kep.p>=pMin)&(((x.eb.dd1==1)&(x.eb.dd2==1))|(x.eb.depth1>0.1/x.kep.r1)|(x.eb.depth2>0.1/x.kep.r2))
#detectBoth=detectBoth&((x.eb.depth2/x.eb.depth1 >1.00001)|(x.eb.depth2/x.eb.depth1 <0.99999))
#print x.eb[(detectBoth)&(x.kep.kw1==13)]

#sType=np.zeros([np.size(x.eb),2])

#sType[:,0]=x.kep.kw1
#sType[:,1]=x.kep.kw2

#sType[(sType>=10.0)&(sType<=12.0)]=10.0
#sType[(sType>=7.0)&(sType<=9.0)]=7.0
#sType[(sType>=0.0)&(sType<=1.0)]=1.0
#sType[(sType>=5.0)&(sType<=6.0)]=5.0

#sType[np.all(sType)==1.0]=-1.0

#uniqS=unique_rows(sType)

#colours = ["b","g","r","y","c","m"]
#colcycler = cycle(colours)

#plt.figure()
#plt.title(folder)
#size=50
#bins=np.linspace(0.0,1.0,size+1)

#width=np.diff(bins)

#total=np.zeros(size)
#for i,j in uniqS:
	#ind=detectBoth&(x.kep.kw1==i)&(x.kep.kw2==j)
	#if np.count_nonzero(ind) > 1:
		#h=np.histogram(xPlot[ind],bins=bins)[0]
		#b=plt.bar(bins[0:-1],h,width=width,bottom=total,label=translate(i)+"+"+translate(j),color=next(colcycler))
		#total+=h

#plt.xlabel("t2/t1",fontsize=28)
#plt.legend(loc=0)
#plt.show()



	















