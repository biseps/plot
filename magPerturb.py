#Takes the output from makeAllMags and pertubs the values based
# on a linear shift plus guassian

#Outputs two files one with all objects suitable for ffi.py and
#one with kp <16 suitable for stellar_parms2.pro

#Version used in farmer,kolb,norton 2013 (true stellar parameters)

import numpy as np
import numpy.random as ra
import getopt
import sys as sys


opts, extra = getopt.getopt(sys.argv[1:],'')
version=extra[0]

idIn,longIn,lat,u,g,r,i,z,d51,jj,jh,jk,kp=np.loadtxt('mag.dat.'+str(version),unpack=True)

#Perturb stars
gL=-0.01819
gC=0.02535
rL=-0.01192
rC=0.05728
iL=-0.02209
iC=0.09656
zL=-0.01313
zC=0.08599
dL=-0.0222
dC=-0.0571


g2=g+gL+gC*(g-r)
r2=r+rL+rC*(r-i)
i2=i+iL+iC*(r-i)
z2=z+zL+zC*(i-z)
d512=d51+dL+dC*(r-d51)


random=ra.randn(np.count_nonzero(g))

g2=random*(0.01921)+g2
i2=random*(0.00995)+i2
r2=random*(0.0)+r2
z2=random*(0.02611)+z2
d512=random*(0.00001)+d512



#Ouput all lines

def output(outName,data):
	f=open(outName,'w')
	f.write(' '.join([`num` for num in data])+"\n")


data=zip(idIn,longIn,lat,u,g2,r2,i2,z2,d512,jj,jh,jk,kp)
np.savetxt('mag.dat.perturb.'+version,data,fmt="%u %f %f %f %f %f %f %f %f %f %f %f %f")
ind=(kp<16.0)
data=zip(idIn[ind],longIn[ind],lat[ind],u[ind],g2[ind],r2[ind],i2[ind],z2[ind],d512[ind],jj[ind],jh[ind],jk[ind],kp[ind])
np.savetxt('mag.dat.perturb.'+version+'.kic',data,fmt="%u %f %f %f %f %f %f %f %f %f %f %f %f")