

# In[]:

import numpy as np
from astropy.io import ascii
from astropy.table import vstack
from astropy.table import Table
from astropy.table import join
import astropy.constants as const

import matplotlib
import matplotlib.pyplot as plt
import plotUtils as utils
import os




# In[]:

# totals3=np.genfromtxt("totals.csv", delimiter=',', names=True)


# In[]:

inclin          = np.arange(0, 90)
cos_i           = np.array(np.cos(inclin*np.pi/180))

output_dir = 'temp_output'


# In[]:
fig = plt.figure(figsize=(8, 8))
ax = plt.subplot(111)

b1 = ax.bar(inclin, cos_i, width=0.5) 

ax.set_title('Probability of inclination angle "i" for orbits with random orientation')
ax.grid(True, which='minor')
ax.set_xlabel('inclination angle i', fontsize=13)
ax.set_ylabel('probability', fontsize=13)
utils.save_plot(fig, output_dir, 'inclin_probs_bar')

# In[]:



# scatter plot
fig = plt.figure(figsize=(8, 8))
ax = plt.subplot(111)

ax = plt.subplot(111)

# ax.set_title(filename)
ax.scatter(inclin, cos_i, s=15, lw=0, alpha=0.7)

ax.set_xlim([0,90])
ax.set_ylim([0,1.2])

ax.set_title('Probability of inclination angle "i" for orbits with random orientation')
ax.grid(True, which='minor')
ax.set_xlabel('inclination angle', fontsize=13)
ax.set_ylabel('probability', fontsize=13)

# plt.show()
utils.save_plot(fig, output_dir, 'inclin_probs_scatter')

# In[]:




