#Extract from extract.dat.1 the stars we want based on target.dat.*c file
import biseps as b
import numpy as np
import getopt
import sys as sys

#extra=['sing','../pop_s_y']
opts, extra = getopt.getopt(sys.argv[1:],'')
version=str(extra[0])

try:
	v2=str(extra[1])
except:
	v2=version
	  
sysType='targ'

x=b.readFile()

x.readTarget('target.dat.'+v2)

ind=[((x.target.bin > 0)&(x.target.bin<12))|(x.target.bin>13)][0]

x.ranLoc('ranLoc.targAll.'+version)
x.write_array('ranLoc.'+sysType+'.'+version,x.ranLoc[x.target[ind].id],'w')
 
try:
	x.kepler5('extract.targAll.'+version)
except:
	x.kepler3('extract.targAll.'+version)
	
x.write_array('extract.'+sysType+'.'+version,x.kep[x.target[ind].id],'w')

x.kicProp('kic.targAll.'+version)
x.write_array('kic.'+sysType+'.'+version,x.kicProp[x.target[ind].id],'w')

x.appMag5('mag.targAll.'+version)
x.write_array('mag.'+sysType+'.'+version,x.appMag[x.target[ind].id],'w')

x.readPop('pop.targAll.'+version)
x.write_array('pop.'+sysType+'.'+version,x.pop[x.target[ind].id],'w')