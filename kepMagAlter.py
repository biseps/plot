import biseps as b
import numpy as np
import matplotlib.pyplot as plt
import numpy.random as ra
import scipy as sci

x=b.readFile()
y=b.readFile()

x.appMag5('mag.all.targAll.1')
y.readKicMag('kic.targAll.mag.2',sep=",")

yInd=(y.appMag.jj>0.0)&(y.appMag.jh>0.0)&(y.appMag.jk>0.0)&(y.appMag.g>0.0)&(y.appMag.r>0.0)&(y.appMag.i>0.0)&(y.appMag.z>0.0)&(y.appMag.d51>0.0)


def sdss2kic(g,r,i,z,d51):
	ik1=((i+0.0583)-0.0696*(r+0.0383))/(0.9304+(0.0696*0.0548))
	rk1=((r+0.0383)+0.0548*ik1)/1.0548
	gk1=((g+0.0985)+(0.0921*rk1))/1.0921	
	zk1=((z+0.0597)-(0.1587*ik1))/0.84132
	d51k1=((d51+0.0597)-(0.1587*ik1))/0.84132	
	gk1=gk1+0.04
	rk1=rk1+0.028
	ik1=ik1+0.045
	zk1=zk1+0.042
	d51k1=d51k1+0.042

	i=ik1
	r=rk1
	g=gk1
	z=zk1
	d51=d51k1


def stellLoc(ri):
	gr=1.39*(1.0-np.exp(-4.9*ri**3-2.45*ri**2-1.68*ri-0.05))
	return gr


def plotIM2(x1,y1,x2,y2,xlabel='',ylabel='',bins=100):
	plt.figure()

	minX=np.minimum(x1.min(),x2.min())
	maxX=np.maximum(x1.max(),x2.max())
	minY=np.minimum(y1.min(),y2.min())
	maxY=np.maximum(y1.max(),y2.max())

	ax1=plt.subplot(121)
	hist,xedges,yedges = np.histogram2d(x1,y1,bins=bins,range=[[minX,maxX],[minY,maxY]],normed=True)
	extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
	plt.imshow(hist.T,extent=extent,interpolation='nearest',origin='lower',cmap='Greys')
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.colorbar()

	gridRI=np.arange(minY,maxY,0.001)
	plt.plot(stellLoc(gridRI),gridRI)
	plt.xlim(minX,maxX)


	plt.subplot(122,sharex=ax1,sharey=ax1)
	hist,xedges,yedges = np.histogram2d(x2,y2,bins=bins,range=[[minX,maxX],[minY,maxY]],normed=True)
	extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
	plt.imshow(hist.T,extent=extent,interpolation='nearest',origin='lower',cmap='Greys')
	plt.colorbar()
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.plot(stellLoc(gridRI),gridRI)
	plt.xlim(minX,maxX)
	plt.show()



#sdss2kic(x.appMag.g,x.appMag.r,x.appMag.i,x.appMag.z,x.appMag.d51)

plotIM2(x.appMag.g-x.appMag.r,x.appMag.r-x.appMag.i,y.appMag[yInd].g-y.appMag[yInd].r,y.appMag[yInd].r-y.appMag[yInd].i,'g-r','r-i')

