import fortUtil as f
import numpy as np
import biseps as b
import kepprf as kep
import matplotlib as mat
import constants as c
import getopt
import sys as sys
import os


opts, extra = getopt.getopt(sys.argv[1:],'')
	
#f2py -c -m --opt="-Wno-all -march=native -msse3 -mtune=native -O3" fortUtil fortUtil.f90 > /dev/null && time python test.py

#extra=['1','2','../pop_s_y','../pop_s_o','../pop_b_y','../pop_b_o']

prfMax=np.zeros([c.numRows,c.numCols],dtype='float')
prfMin=np.zeros([c.numRows,c.numCols],dtype='float')
version=extra[1]
dirTrans=extra[2]

for fin in extra[3:]:
	#print extra[0],fin+'/'+str(extra[0])+'/mag.dat.'+version

	x=b.readFile()
	isKic=2
	if isKic==0:
		#BIseps output
		x.appMag5('mag.dat.'+version+'.kic')
		x.kicProp('kic.dat.'+version)
		row,col=f.lonlat2pix(x.appMag.long,x.appMag.lat,np.size(x.appMag.long),extra[0],c.season,c.fovDir)
	elif isKic==1:
		#Kepelr data
		x.kicProp3('kic.dat.'+str(extra[0]))
		row,col=f.lonlat2pix(x.kicProp.long,x.kicProp.lat,np.size(x.kicProp.long),extra[0],c.season,c.fovDir)
	else:
		#KIC data
		x.kicProp5('kic.dat.1',sep=" ")
		row,col=f.lonlat2pix(x.kicProp.long,x.kicProp.lat,np.size(x.kicProp.long),extra[0],c.season,c.fovDir)


#Now scale each binaries flux by the transit depth doing this twice once for the max and once for min transit
	if os.path.isfile(dirTrans+"/transit.dat."+version):
		x.readTransCalc('transit.1')
		maxTrans=np.maximum(x.trans.depth1,x.trans.depth2)
		minTrans=np.minimum(x.trans.depth1,x.trans.depth2)
		prfMax=prfMax+kep.prfImage(row,col,x.kicProp.kp*(1.0-maxTrans),extra[0])
		prfMin=prfMin+kep.prfImage(row,col,x.kicProp.kp*(1.0-minTrans),extra[0])
	else:
		prfMax=prfMax+kep.prfImage(row,col,x.kicProp.kp,extra[0])
		prfMin=prfMin+kep.prfImage(row,col,x.kicProp.kp,extra[0])
			

		
np.save(extra[0]+'.blendMax.'+version,prfMax)
np.save(extra[0]+'.blendMin.'+version,prfMin)

#x=b.readFile()
#x.readFOV2('fov/3/1.a.mag')
#prfAll=kp.prfImage(x.fov.row,x.fov.col,x.fov.mag,1)
#np.save('kicIn/1.npy',prfAll)
