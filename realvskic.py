
import numpy as np
import matplotlib.pyplot as plt
import biseps as b
from matplotlib import rc
rc('text', usetex=True)
rc('font', family='serif')

G=6.67384*10**(-11)
Msun=1.98892*10**(30)
Rsun=6.955*10**(8)
nbins=50

sK=b.readFile()
sR=b.readFile()

bK=b.readFile()
bR=b.readFile()

sK.kicProp('kic.sing.targAll.1')
sR.readExtract('extract.sing.targAll.1')

bK.kicProp('kic.bin.targAll.1')
bR.readExtract('extract.bin.targAll.1')


sKInd=(sK.kicProp.tefferr < 10**10.0)&(sK.kicProp.loggerr < 9.0)&(sK.kicProp.logzerr < 9.0)
sInd=sKInd&(sR.kep.t1<40000.0)


bKInd=(bK.kicProp.tefferr < 10**10.0)&(bK.kicProp.loggerr < 9.0)&(bK.kicProp.logzerr < 9.0)
bInd=bKInd

def plotIm(x1,y1,xlabel,ylabel,title,fig):
	f=plt.figure(fig)
	f.canvas.set_window_title(title)
	hist,xedges,yedges = np.histogram2d(x1,y1,bins=200)
	extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
	#hist=np.log10(hist)
	hist[hist==0]=np.log10(0.0)
	plt.imshow(hist.T,extent=extent,interpolation='nearest',origin='lower',cmap='jet',aspect='auto')
	plt.title(title)
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.plot([np.min(x1),np.max(x1)],[np.min(x1),np.max(x1)],c='black')
	plt.colorbar()
	#plt.savefig('realkic/'+title)

ex=b.extras()
massSing=ex.radlogg2mass(sK.kicProp.rad,sK.kicProp.logg)
massBin=ex.radlogg2mass(bK.kicProp.rad,bK.kicProp.logg)

slogg1=ex.massrad2logg(sR.kep.m1,sR.kep.r1)

blogg1=ex.massrad2logg(bR.kep.m1,bR.kep.r1)
blogg2=ex.massrad2logg(bR.kep.m2,bR.kep.r2)


#sInd=sInd&(np.abs(sR.kep.t1-sK.kicProp.teff)<1000.0)
plotIm(sK.kicProp.teff[sInd],sR.kep.t1[sInd]-sK.kicProp.teff[sInd],'kic teff','real-kic teff','teff',1)
plt.show()

plotIm(sK.kicProp.logg[sInd],slogg1[sInd],'kic logg','real logg','logg',1)

plt.figure(1)
plt.scatter(blogg1[bInd],blogg2[bInd],c=np.log10(bK.kicProp.teff[bInd]),s=(np.log10(bR.kep.t2[bInd])*5.0)+1.0)
plt.plot(np.array([0.0,7.0]),np.array([0.0,7.0]),c='black')
plt.colorbar()
plt.show()

#sEvol1=sR.kep[*].evol[-6:-5]

#bEvol1=bR.kep.evol[-6:-5]
#bEvol2=bR.kep.evol[-4:-3]
plt.figure(1)
plt.scatter(bK.kicProp.logg[bInd],blogg1[bInd],c=np.log10(bR.kep.t1[bInd]),alpha=0.3)
plt.colorbar()

plt.figure(2)
plt.scatter(bK.kicProp.teff[bInd],bR.kep.t1[bInd],c=bR.kep.m1[bInd])
plt.colorbar()

plt.show()

bInd=bKInd&(bR.kep.t1<40000.0)
plotIm(bK.kicProp.teff[bInd],bR.kep.t1[bInd],'t','t','t',1)
plt.show()

j=0
arr1=np.zeros(107,dtype='a100')
for i in np.unique(bR.kep.evol):
	#arr1[j]=i[-5:-4]+i[-3:-2]
	arr1[j]=i[-5:-1]
	j=j+1

j=0
for i in np.unique(arr1):
	j=j+1
	ind=bInd&(bR.kep.evol.endswith(i+"'"))
	if np.count_nonzero(ind) > 0:
		print i,j
		f=plt.figure(j)
		f.canvas.set_window_title(i)
		plt.scatter(bK.kicProp[ind].logg,blogg1[ind],c=bK.kicProp[ind].teff)
		plt.colorbar()
		#plt.s(bK.kicProp.teff[bInd&(bR.kep.evol.endswith(i+"'"))],bR.kep.t1[bInd&(bR.kep.evol.endswith(i+"'"))],'t','t',i,j)

plt.show()



#########################################


import numpy as np
import matplotlib.pyplot as plt
import biseps as b
from matplotlib import rc
rc('text', usetex=True)
rc('font', family='serif')

G=6.67384*10**(-11)
Msun=1.98892*10**(30)
Rsun=6.955*10**(8)
nbins=50

sK=b.readFile()
sR=b.readFile()

bK=b.readFile()
bR=b.readFile()

sK.kicProp('kic.sing.targAll.1')
sR.readExtract('extract.sing.targAll.1')

bK.kicProp('kic.bin.targAll.1')
bR.readExtract('extract.bin.targAll.1')


sKInd=(sK.kicProp.tefferr < 10**10.0)&(sK.kicProp.loggerr < 9.0)&(sK.kicProp.logzerr < 9.0)
sInd=sKInd&(sR.kep.t1<40000.0)


bKInd=(bK.kicProp.tefferr < 10**10.0)&(bK.kicProp.loggerr < 9.0)&(bK.kicProp.logzerr < 9.0)
bInd=bKInd


ex=b.extras()
massSing=ex.radlogg2mass(sK.kicProp.rad,sK.kicProp.logg)
massBin=ex.radlogg2mass(bK.kicProp.rad,bK.kicProp.logg)

slogg1=ex.massrad2logg(sR.kep.m1,sR.kep.r1)

blogg1=ex.massrad2logg(bR.kep.m1,bR.kep.r1)
blogg2=ex.massrad2logg(bR.kep.m2,bR.kep.r2)



def plotIm(x1,y1,xlabel,ylabel,title,fig):
	f=plt.figure(fig)
	f.canvas.set_window_title(title)
	hist,xedges,yedges = np.histogram2d(x1,y1,bins=200)
	extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
	#hist=np.log10(hist)
	hist[hist==0]=np.log10(0.0)
	plt.imshow(hist.T,extent=extent,interpolation='nearest',origin='lower',cmap='jet',aspect='auto')
	plt.title(title)
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	#plt.plot([np.min(x1),np.max(x1)],[np.min(x1),np.max(x1)],c='black')
	plt.colorbar()

	#step=(xedges[-1]-xedges[0])/200.0
	#xArr=np.arange(xedges[0],xedges[-1],step)
	
	#histMedian=np.zeros(np.size(xArr))
	#histStd=np.zeros(np.size(xArr))
	#for i in range(np.size(xArr)):
		#indd=0
		#try:
			#indd=(x1>=xArr[i])&(y1<xArr[i+1])
		#except IndexError:
			#indd=(x1>=xArr[i])&(x1<np.max(x1))
		#histMedian[i]=np.mean(y1[indd])
		#histStd[i]=np.std(y1[indd])
		##print i,histMedian[i],histStd[i],xArr[i],xArr[i+1]
	##print xArr
	##print histMedian
	#plt.plot(xArr,histMedian[0:200],c='black')


plotIm(sK.kicProp.logg[sInd],slogg1[sInd],'kic ','real','logg',1)
plt.show()


plt.scatter(np.log10(sK.kicProp.rad[sInd]),np.log10(sR.kep.r1[sInd]),c=sK.kicProp.teff[sInd],s=(sK.kicProp.logg[sInd]*5.0)+2.0)
plt.colorbar()
plt.show()



lg1=ex.massrad2logg(np.array([1.0,1.0]),np.array([0.1,1000.0]))
lg2=ex.massrad2logg(np.array([2.0,2.0]),np.array([0.1,1000.0]))

plt.figure()
ax1=plt.subplot(121)
plt.scatter(sK.kicProp.logg[sInd],np.log10(sK.kicProp.rad[sInd]),c=massSing[sInd])
plt.plot(lg1,[0.1,3.0])
plt.plot(lg2,[0.1,3.0])

plt.colorbar()
plt.subplot(122,sharex=ax1,sharey=ax1)
plt.scatter(slogg1[sInd],np.log10(sR.kep.r1[sInd]),c=sR.kep.m1[sInd])
plt.plot(lg1,[0.1,3.0])
plt.plot(lg2,[0.1,3.0])
plt.colorbar()
plt.show()


mR=np.minimum(bR.kep.m1,bR.kep.m2)/np.maximum(bR.kep.m1,bR.kep.m2)
tR=np.minimum(bR.kep.t1,bR.kep.t2)/np.maximum(bR.kep.t1,bR.kep.t2)

plotIm(massBin[bInd],mR[bInd],'kic mass','M2/M1','mass','1')
plotIm(bK.kicProp.teff[bInd],tR[bInd],'kic teff','t2/t2','teff','1')


#######################################

j=0
arr1=np.zeros(107,dtype='a100')
for i in np.unique(bR.kep.evol):
#arr1[j]=i[-5:-4]+i[-3:-2]
	arr1[j]=i[-5:-1]
	j=j+1

j=0
ind=np.zeros([107,np.size(bR.kep)],dtype='int')
for i in np.unique(arr1):
	ind[j,:]=bR.kep.evol.endswith(i+"'")
	j=j+1


