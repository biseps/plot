import fortUtil as f
import numpy as np
import scipy as sci
import kepio as kepio
import glob as glob
import biseps as b
import sys as sys

import matplotlib as mat
import matplotlib.pyplot as plt
import constants as c

def prf(column,row,mag,crval1p,crval2p,prfn,naxis1,naxis2):			
	# interpolate the calibrated PRF shape to the target position

	prf = np.zeros(np.shape(prfn[0]),dtype='float32')
	prfWeight = np.zeros((5),dtype='float32')
	for i in range(5):
		prfWeight[i] = np.sqrt((column - crval1p[i])**2 + (row - crval2p[i])**2)
		if prfWeight[i] == 0.0:
			prfWeight[i] = 1.0e6
		prf = prf + prfn[i] / prfWeight[i]
		prf = prf / np.sum(prf)

	ySmall=naxis2/50
	xSmall=naxis1/50
	
	res=np.zeros([ySmall,xSmall],dtype='float32')
	for j in range(xSmall):
		for i in range(ySmall):
			res[i,j]=np.sum(prf[i*50:(i*50)+50,j*50:(j*50)+50])

	res=((1.0*res)/np.sum(res))*10**(mag/(-2.5))

	return res

	
def prfImage(row,col,mag,fileNum):

	dataOut=np.zeros([c.numRows,c.numCols],dtype='float',order='F')

	prfn, crpix1p, crpix2p, crval1p, crval2p, cdelt1p, cdelt2p,naxis1,naxis2=getPrfDat(fileNum)

	#Combination of the actual edge of the cdd plus the edges of the science pixels
	ind=(col>=0)&(row>=0)&(col<c.numCols)&(row<c.numRows)&(row>19)&(row<=1044)&(col>12)&(col<=1111)
	x=col[ind]
	y=row[ind]
	m=mag[ind]

	dataOut=f.prfall(y,x,m,np.size(x),c.numRows,c.numCols,crval1p,crval2p,prfn,naxis1,naxis2)
		
	return dataOut
	
def getPrfDat(fileNum):
	logfile=''
	verbose=False
	# determine suitable PRF calibration file
	prfglob = c.prfDir + '/' + str(fileNum) +'.fits'
	try:
		prffile = glob.glob(prfglob)[0]
	except:
		print 'ERROR -- KEPPRF: No PRF file found in ' + c.prfdir
			
			#status = kepmsg.err(logfile,message,verbose)

	# read PRF images
	prfn = [0,0,0,0,0]
	crpix1p = np.zeros((5),dtype='float32')
	crpix2p = np.zeros((5),dtype='float32')
	crval1p = np.zeros((5),dtype='float32')
	crval2p = np.zeros((5),dtype='float32')
	cdelt1p = np.zeros((5),dtype='float32')
	cdelt2p = np.zeros((5),dtype='float32')
	for i in range(5):
		prfn[i], crpix1p[i], crpix2p[i], crval1p[i], crval2p[i], cdelt1p[i], cdelt2p[i],naxis1,naxis2,status \
			= kepio.readPRFimage(prffile,i+1,logfile,verbose)
		#Scale prfn up by np.min() so all values are positive, only relative diffs matter as we normalise latter ( i think)
		prfn[i][prfn[i]<0.0]=0.0
		#Normalise prfs
		#prfn[i]=prfn[i]/np.sum(prfn[i])
	#print prfn
				
	return prfn, crpix1p, crpix2p, crval1p, crval2p, cdelt1p, cdelt2p,naxis1,naxis2
	
def getLimBig(naxis1,naxis2,x,y):
	#Calcs how wwell a stars prf will fit on the global ccd image returns edge coords
	xGap=((naxis1/50)-1)/2
	yGap=((naxis2/50)-1)/2
	
	xLow=x-xGap
	xLow[(xLow<0)]=0.0
	xHigh=x+xGap
	xHigh[(xHigh>=c.numCols)]=c.numCols-1
	yLow=y-yGap
	yLow[(yLow<0)]=0.0
	yHigh=y+yGap
	yHigh[(yHigh>=c.numRows)]=c.numRows-1

	return xLow,xHigh,yLow,yHigh

def getLimSmall(naxis1,naxis2,x,y):
	#IF star hangs of edge of global ccd we need to alter the pixels used in prf
	#if star doesnt hang of edge this returns the size of the prf
	xGap=((naxis1/50)-1)/2
	yGap=((naxis2/50)-1)/2

	xresLow=np.zeros(np.size(x))
	xresHigh=np.zeros(np.size(x))
	xresHigh[:]=(naxis1/50)-1
	
	yresLow=np.zeros(np.size(x))
	yresHigh=np.zeros(np.size(x))
	yresHigh[:]=(naxis2/50)-1
	
	xresLow[(x<xGap)]=np.abs(x-xGap)
	yresLow[(y<yGap)]=np.abs(y-yGap)

	if np.shape(x[(x+xGap>=c.numCols)])[0]:
		xresHigh[(x+xGap>=c.numCols)]=xGap+(xGap-((x+xGap)-(c.numCols-1)))
		#xresHigh[(x+xGap>=c.numCols)]=xGap+xresHigh[(x+xGap>=c.numCols)]-c.numCols

	if np.shape(y[(y+yGap>=c.numRows)])[0]:
		yresHigh[(y+yGap>=c.numRows)]=yGap+(yGap-((y+yGap)-(c.numRows-1)))
		#xresHigh[(y+yGap>=c.numRows)]=yGap+yresHigh[(y+yGap>=c.numRows)]-c.numRows
	return xresLow,xresHigh,yresLow,yresHigh
	
def numSample(mass,rad,a):
	#In sol units a is in metres
	ttr=np.zeros(np.shape(a),dtype='float')
	ttr=2.0*rad*c.rsun*np.sqrt(a/(c.g*mass*c.msun))*(np.pi/4.0)
	#ttr=2.0*rad*c.rsun*np.sqrt(a/(c.g*mass*c.msun))
	#sampleLen=30*60
	
	return ttr/(60.0*30.0)
	
def numTransits(mass,a):
	#sol units a in metres
	p=np.zeros(np.shape(a),dtype='float')
	p=2.0*np.pi*np.sqrt((a**3)/(c.g*mass*c.msun))
	p=p/c.year

	return 3.5/p
	
def calcA(radius,teff):
	#radius is sol units, teff in kelvin
	lum=lumin(radius,teff)
	
	#calc three zones at 5 radius, 0.5 HZ, HZ
	a=np.zeros([c.numA,np.size(radius)])
	a[0]=5.0*radius*c.rsun
	a[1]=0.95*0.5*np.sqrt(lum)*c.au
	a[2]=0.95*np.sqrt(lum)*c.au

	#returns in si
	return a
	
def lumin(radius,teff):
	return (radius**2)*(teff/c.teffsun)**4
	
def calcMinR(row,col,mag,fileNum,ccd,prfn, crpix1p, crpix2p, crval1p, crval2p, cdelt1p, cdelt2p,naxis1,naxis2,jjj):
	#Takes the row col and generate prf for 1 star
	#uses the ccd (image of all objects) to calc the background flux
	#returns the fstar/fstar+fbg,number of pixels used,noise
	#print "*",mag,"*" 
	
	fileNum=int(fileNum)
	x=np.array([col])
	y=np.array([row])
	m=np.array([mag])
	
	#if np.any(ccd<0.0):
		#print "a2"
		#exit()

	xLow,xHigh,yLow,yHigh=getLimBig(naxis1,naxis2,x,y)
	xresLow,xresHigh,yresLow,yresHigh=getLimSmall(naxis1,naxis2,x,y)

	#Places stars prf inside the columns 
	image=f.prfcalc(y,x,m,crval1p,crval2p,prfn,naxis1,naxis2)
	
	
	#image2=prfImage([row],[col],[mag],fileNum)

	img=np.zeros([c.numRows,xresHigh[0]-xresLow[0]],order='F')
	#print np.shape(img)
	img[yLow[0]:yHigh[0],xresLow[0]:xresHigh[0]]=image[yresLow[0]:yresHigh[0],xresLow[0]:xresHigh[0]]
	
	#Extract bg around star
	ccdDiff=np.zeros([c.numRows,xHigh[0]-xLow[0]],dtype='float')
	ccdDiff=np.copy(ccd[:,xLow[0]:xHigh[0]])
	#sys.exit()
	#Diff between (background+image) and image leaves just the background 
	ccdDiff[yLow[0]:yHigh[0],xresLow[0]:xresHigh[0]]=(ccdDiff[yLow[0]:yHigh[0],xresLow[0]:xresHigh[0]]-
														img[yLow[0]:yHigh[0],xresLow[0]:xresHigh[0]])

	imgCopy=np.copy(img)
	ccdDiffCopy=np.copy(ccdDiff)

	#print row,col
	#print yresLow[0],yresHigh[0],xresLow[0],xresHigh[0]
	#print yLow[0],yHigh[0],xLow[0],xHigh[0]
	#print np.shape(image),np.shape(img),np.shape(ccd),np.shape(ccdDiff)
	#plt.figure()
	#ax1=plt.subplot(1,4,1)
	#plt.imshow((image[yresLow[0]:yresHigh[0],xresLow[0]:xresHigh[0]]),interpolation='nearest',origin='lower',cmap='jet_r')
	##plt.imshow(np.log10(image),interpolation='nearest',origin='lower',cmap='jet_r')
	#plt.colorbar()
	#ax2=plt.subplot(1,4,2)
	#plt.imshow((img[yLow[0]:yHigh[0],xresLow[0]:xresHigh[0]]),interpolation='nearest',origin='lower',cmap='jet_r')
	##plt.imshow(np.log10(img),interpolation='nearest',origin='lower',cmap='jet_r')
	#plt.colorbar()
	#ax3=plt.subplot(1,4,3)
	#plt.imshow((ccd[yLow[0]:yHigh[0],xLow[0]:xHigh[0]]),interpolation='nearest',origin='lower',cmap='jet_r')
	##plt.imshow(np.log10(ccd),interpolation='nearest',origin='lower',cmap='jet_r')
	#plt.colorbar()
	#ax4=plt.subplot(1,4,4)
	#plt.imshow((ccdDiff[yLow[0]:yHigh[0],xresLow[0]:xresHigh[0]]),interpolation='nearest',origin='lower',cmap='jet_r')
	##plt.imshow(np.log10(ccdDiff),interpolation='nearest',origin='lower',cmap='jet_r')
	#plt.colorbar()
	#plt.show()
	ccdDiff=np.asfortranarray(ccdDiff)

	f.smear(img,np.shape(img)[0],np.shape(img)[1],c.readTime,c.numRows,c.f12,c.tint)
	f.smear(ccdDiff,np.shape(ccdDiff)[0],np.shape(ccdDiff)[1],c.readTime,c.numRows,c.f12,c.tint)

	isSat=0
	if np.any(img>c.wellDepth[fileNum-1]):
		f.saturate(img,c.wellDepth[fileNum-1],np.shape(img)[0],np.shape(img)[1])
		isSat=1
	if np.any(ccdDiff>c.wellDepth[fileNum-1]):
		f.saturate(ccdDiff,c.wellDepth[fileNum-1],np.shape(ccdDiff)[0],np.shape(ccdDiff)[1])

	f.cte(img,c.ctePar,c.cteSer,np.shape(img)[0],np.shape(img)[1],xresLow[0])
	f.cte(ccdDiff,c.ctePar,c.cteSer,np.shape(ccdDiff)[0],np.shape(ccdDiff)[1],xresLow[0])

	numPix,noise,signal,pixX,pixY,bgFlux=f.snr(img,ccdDiff,c.readNoiseSq[fileNum-1],c.quantErrSq[fileNum-1],c.numInt,np.shape(img)[0],np.shape(img)[1])

	#print "DEBUG kepprf pixX,Y",pixX,pixY
	#print "DEBUG shpae img0 1 ",np.shape(img)[0],np.shape(img)[1]
	#print "DEBUG indPix",indPix
	
	indPix=(pixX>=0)&(pixY>=0)
	pixX[indPix]=pixX[indPix]-1
	pixY[indPix]=pixY[indPix]-1
	
	r=(np.sum(imgCopy[pixY[indPix],pixX[indPix]])
				/(np.sum(imgCopy[pixY[indPix],pixX[indPix]])+np.sum(ccdDiffCopy[pixY[indPix],pixX[indPix]])))
				
	#pixX (column axis) works in a relative indexing 0->naxis/50 this translates that back to an absoulte index scheme
	pixX=col+(pixX[:numPix]-naxis1/(2*50))
	pixX=[np.int(x) for x in pixX]

	return	r,numPix,noise,signal,bgFlux,isSat,pixX,pixY[:numPix]

