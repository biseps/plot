import pyfits as fits
import numpy as np
import matplotlib as m
import matplotlib.pyplot as plt
import os as os
import glob as glob

path = 'kp/'


f=fits.open(path+'kplr02.1_2011265_prf.fits')

xSize=1024
ySize=1100

dataOut=np.zeros([ySize,xSize],dtype='float')

for num in range(2):
	#x=np.random.randint(0,xSize)
	#y=np.random.randint(0,ySize)
	
	x=100
	y=1000


	dis=np.zeros(4)
	#Dont need to check the fifth prf as thats the centre and allways used
	for i in range(0,4):
		dis[i]=np.sqrt((x-f[i+1].header['crval1p'])**2+(y-f[i+1].header['crval2p'])**2)

	xaxis=f[1].header['naxis1']
	yaxis=f[1].header['naxis2']

	sort=np.argsort(dis)

	a=dis[sort[0]]
	locA=sort[0]+1

	b=dis[sort[1]]
	locB=sort[1]+1

	c=np.sqrt((x-f[5].header['crval1p'])**2+(y-f[5].header['crval2p'])**2)
	locC=5

	scaleA=(1.0/a)/((1.0/a)+(1.0/b)+(1.0/c))
	scaleB=(1.0/b)/((1.0/a)+(1.0/b)+(1.0/c))
	scaleC=(1.0/c)/((1.0/a)+(1.0/b)+(1.0/c))

	dtype=f[1].data.dtype.name
	data=np.zeros([xaxis,yaxis],dtype=dtype)

	data=scaleA*f[locA].data[:,:]+scaleB*f[locB].data[:,:]+scaleC*f[locC].data[:,:]

	xSmall=xaxis/50
	ySmall=yaxis/50
	res=np.zeros([ySmall,xSmall],dtype=dtype)
	for j in range(xSmall):
		for i in range(ySmall):
			res[i,j]=np.sum(data[i*50:(i*50)+50,j*50:(j*50)+50])
				
	res=(res)*10**(((np.random.rand()*8.0)+8.0)/(-2.5))
		
	xGap=((xaxis/50)-1)/2
	yGap=((yaxis/50)-1)/2

	xLow=np.max([x-xGap,0])
	xHigh=np.min([x+xGap,xSize])
	yLow=np.max([y-yGap,0])
	yHigh=np.min([y+yGap,ySize])
	
	xresLow=0
	xresHigh=(xaxis/50)-1
	yresLow=0
	yresHigh=(yaxis/50)-1	
	
	if x-xGap <0:
		xresLow=np.abs(x-xGap)
	if x+xGap>xSize:
		xresHigh=xGap+np.abs(x-xSize)
	if y-yGap <0:
		yresLow=np.abs(y-yGap)
	if y+yGap>ySize:
		yresHigh=yGap+np.abs(y-ySize)	
	
	#print x,y
	#print yLow,yHigh,yresLow,yresHigh,yGap
	#print xLow,xHigh,xresLow,xresHigh,xGap
	#print "*"
	
	dataOut[yLow:yHigh,xLow:xHigh]=np.add(res[yresLow:yresHigh,xresLow:xresHigh],dataOut[yLow:yHigh,xLow:xHigh])
	
plt.figure()
cmap='gray'
cm = plt.get_cmap(cmap)


norm=m.colors.Normalize(vmin=np.min(dataOut),vmax=np.max(dataOut))
plt.imshow(dataOut,cmap=cm,norm=norm,origin='lower',interpolation='nearest')

##plt.imshow(data,cmap=cm,norm=norm,origin='lower',interpolation='nearest')

#plt.colorbar()
#plt.grid(which='both')
#plt.title(infile)
plt.show()



