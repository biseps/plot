import biseps as b
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc

rc('text', usetex=True)
rc('font', family='serif')
#Plots a set of distributions


x=b.readFile()
x.ranLoc('all.ranLoc.1')

fig=plt.figure(1)
plt.subplot(3,2,1)
plt.hist(x.ranLoc.lat,bins=20,range=[5,25],normed=1)
plt.xlabel('Galactic Latitude, b [degrees]')

plt.subplot(3,2,2)
plt.hist(x.ranLoc.long,bins=20,range=[65,85],normed=1)
plt.xlabel('Galactic Longitude, l [degrees]')

plt.subplot(3,2,3)
plt.hist(np.log10(x.ranLoc.dist),normed=1,range=[0.0,np.log10(50000)],bins=100)
plt.xlabel('log10 Distance [pc]')


plt.subplot(3,2,5)
#app mag
plt.hist(x.ranLoc.mag,bins=48,range=[-5,12],normed=1)
plt.xlabel(r'$M_{kp}$')

plt.subplot(3,2,6)
#app mag
plt.hist(x.ranLoc.appMag,bins=16,range=[8,16],normed=1)
plt.xlabel(r'$m_{kp}$')

plt.show()

