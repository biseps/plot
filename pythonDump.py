import biseps as b
import matplotlib.pyplot as plt
x=b.readFile()
x.mag(file='ran.sing.mag')

#plt.hist(x.mag.mag2,100)
#plt.savefig('mag.svg')

x.kepler2(file='ran.sing.kep')
plt.hist(x.kep.m1,100)
plt.savefig('mag.svg')

#######################################
import biseps as b
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt

x=b.readFile()
x.ranLoc('WEB_ranLoc.1')
fig = plt.figure()
ax = Axes3D(fig)

cm = plt.get_cmap('jet_r')

p=ax.scatter(x.ranLoc.dist,x.ranLoc.long,x.ranLoc.lat)

p=ax.set_xlabel('log10 Distance')
p=ax.set_ylabel('Longitude')
p=ax.set_zlabel('Latitude')

plt.show()


plt.cla()
plt.close()



plt.figure(1)
plt.subplot(211)
plt.hist(np.log10(x.ranLoc.dist[(x.ranLoc.mag >0.0) & (x.ranLoc.mag < 0.2)]),100)
plt.title('Distance plot for single stars (v1) M > 0 & < 0.2')

plt.subplot(212)
plt.hist(np.log10(x.ranLoc.dist[(x.ranLoc.mag >1.0) & (x.ranLoc.mag < 1.2)]),100)
plt.xlabel('log10 Distance')
plt.title('Distance plot for single stars (v1) M > 1 & < 1.2')
plt.savefig('dist_mag.png')
plt.cla()
plt.close()