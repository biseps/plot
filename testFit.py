import numpy
from scipy import optimize

def normal_profile(par):
    '''
        normal profile in y.
            mean:  polynomial coefficients in x
            sigma: polynomial coefficients in x
    '''
    mean,sigma = par
    return lambda x,y: numpy.exp(-(y - numpy.poly1d(mean)(x))**2 \
        / (2. * numpy.poly1d(sigma)(x)**2))

def gaussian_profile(par):
    '''
        normal profile in y multiplied by an amplitude.
            amplitude: polynomial coeffs. in x
    '''
    amplitude,mean,sigma = par
    return lambda x,y: numpy.poly1d(amplitude) \
        (normal_profile([mean,sigma])(x,y))


def fit_gaussian_y_profile(xx, yy, zz, parshape):
    '''
        parshape determines the order of the three polynomials:
            amplitude, mean, sigma
    '''
    def gaussian_profile_pinit(yy, zz, parshape):
        '''
            rough estimate for initial paramters using
            weighted mean and variance of the data (zz)
            projection onto the y axis.
        '''
        amp = [0]*parshape[0]
        mean = [0]*parshape[1]
        sigma = [0]*parshape[2]
        y = yy.transpose()[0]
        zzprojy = zzdata.sum(1)

        # calculate weighted mean and variance
        zzprojy = zzprojy + abs(zzprojy.min())
        wtot = zzprojy.sum()
        wmean = (zzprojy * y).sum() / wtot
        var = (zzprojy * (y - wmean)**2).sum()
        wvar = numpy.sqrt(var/wtot)

        amp[-2] = zzdata.max() - zzdata.min()
        amp[-1] = zzdata.min()
        mean[-1] = wmean
        sigma[-1] = wvar

        return (amp,mean,sigma)

    def flatten_pars(par):
        pflat = []
        for p in par:
            pflat += list(p)
        return pflat

    def reshape_pars(par, parshape):
        a,b,c = parshape
        par = list(par)
        return [par[:a], par[a:a+b], par[a+b:]]

    def errfn(par, x, y, z):
        '''
            error function for leastsq fit
        '''
        return numpy.ravel(
            gaussian_profile(reshape_pars(par,parshape))(x,y) - z )

    # get the initial guess
    pinit = gaussian_profile_pinit(yy, zz, parshape)
    print 'initial parameters:', pinit

    # fit the data
    pf,success = optimize.leastsq(
        errfn,
        flatten_pars(pinit),
        args=(xx,yy,zz) )

    # reshape the parameters since leastsq only takes
    # a flat list for the fitting parameters.
    pfit = reshape_pars(pf,parshape)

    return (pfit,success)


if __name__ == "__main__":
    from numpy import meshgrid, random
    from scipy import linspace
    from matplotlib import pyplot, cm
    from mpl_toolkits.axes_grid1 import make_axes_locatable

    # generate (x,y) grid and data
    # parameters: [amplitude, mean, sigma]
    #    (each parameter is a list of polynomial coefficients)
    psolution = [[200.,0.], [0.04,-0.4,0.4,40.0], [0.3,15.]]
    bins = (70,130)
    xrng = [-10,10]
    yrng = [-100,120]

    # set up the grid and extent
    x = linspace(xrng[0], xrng[1], bins[0])
    y = linspace(yrng[0], yrng[1], bins[1])
    xx,yy = meshgrid(x,y)
    extent = [xrng[0],xrng[1],yrng[0],yrng[1]]

    # exact solution data
    zzsolution = gaussian_profile(psolution)(xx,yy)

    # smear data with a gaussian in z
    zzdata = zzsolution + 5. * random.normal(0., 1., zzsolution.shape)

    # fit the data...
    # (2,4,2) means 1st-order poly for the amplitude,
    #   3rd-order poly for the mean,
    #   and 1st-order poly for the sigma.
    #
    # hint: try changing the (2,4,2) on the following line
    # to see an attempt to fit using different orders in
    # the polynomials.
    pfit,_ = fit_gaussian_y_profile(xx, yy, zzdata, (2,4,2))

    print 'fit parameters:', pfit
    print 'mean of the fit:'
    print numpy.poly1d(pfit[1])

    fig = pyplot.figure(figsize=(6,4))
    ax = fig.add_subplot(1,1,1)

    # plot data
    plt = ax.imshow(
        zzdata,
        extent=extent,
        origin='lower',
        aspect='auto',
        interpolation='nearest',
        cmap=cm.jet )

    # color bar
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size=0.1, pad=0.1)
    cb = fig.colorbar(plt, cax=cax)

    # plot the mean
    yfit = numpy.poly1d(pfit[1])(x)
    ax.plot(x,yfit,color='black',linewidth=2)

    # plot the contours
    zzfit = gaussian_profile(pfit)(xx,yy)
    ax.contour(xx,yy,zzfit,5,
        extent=extent, origin='lower', aspect='auto',
        linestyle='-', alpha=0.6, colors='blue')

    pyplot.show()