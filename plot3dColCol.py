import numpy as np
import matplotlib.pyplot as plt
import biseps as b


x=b.readFile()

x.appMag5('mag.all.1.kic')
#x.ranLoc('ranLoc.young.targAll.1')
x.readExtract('extract.all.targAll.1')


#plt.hexbin(x.appMag.g-x.appMag.r,x.appMag.r-x.appMag.i,mincnt=1,bins='log')
#plt.colorbar()
#plt.show()

#xInd=(x.appMag.g-x.appMag.r > 1.5)&(x.appMag.r-x.appMag.i<0.75)&(x.appMag.r-x.appMag.i>0.5)
##plt.hexbin(x.appMag[np.logical_not(xInd)].g-x.appMag[np.logical_not(xInd)].r,x.appMag[np.logical_not(xInd)].r-x.appMag[np.logical_not(xInd)].i,mincnt=1,bins='log')
##plt.colorbar()
##plt.show()

#plt.hist(np.log10(x.kep[xInd].r1),bins=100)
#plt.show()

#plt.figure(1)
#plt.scatter(x.appMag[xInd].g-x.appMag[xInd].r,np.log10(x.kep[xInd].r1))
#plt.figure(2)
#plt.scatter(x.appMag[xInd].r-x.appMag[xInd].i,np.log10(x.kep[xInd].r1))

#plt.show()


from mpl_toolkits.mplot3d import Axes3D

#evol=np.zeros(np.size(x.kep),dtype='S4')
#for i in xrange(np.size(x.kep)):
	#evol[i]=x.kep.evol[i][-5:-4]

#indAll=np.zeros(np.size(evol),dtype=bool)
#indAll[:]=True
#ind1=('N'==evol)
#ind2=('W'==evol)
#ind3=('B'==evol)
#ind4=('A'==evol)
#ind5=('M'==evol)
#ind6=('C'==evol)
#ind7=('H'==evol)


xInd=(x.appMag.g-x.appMag.r > 1.5)&(x.appMag.r-x.appMag.i<0.75)&(x.appMag.r-x.appMag.i>0.5)
#xInd=ind2
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

#ax.scatter(x.appMag[xInd].g-x.appMag[xInd].r, x.appMag[xInd].r-x.appMag[xInd].i,(x.kep[xInd].death+x.kep[xInd].birth)/2.0)
ax.scatter(x.appMag[xInd].g-x.appMag[xInd].r, x.appMag[xInd].r-x.appMag[xInd].i, np.log10(x.kep[xInd].r1))

ax.set_xlabel('g-r')
ax.set_ylabel('r-i')
ax.set_zlabel('log r')

plt.show()

#####################
import numpy as np
import matplotlib.pyplot as plt
import biseps as b


x=b.readFile()

#x.appMag5('mag.old.1.kic')
x.ranLoc('ranLoc.all.targAll.1')
#x.readExtract('extract.old.targAll.1')

plt.hexbin(x.ranLoc.long,x.ranLoc.lat,mincnt=1)
plt.colorbar()
plt.show()
