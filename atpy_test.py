# In[]:


import atpy

# In[]:

# t = atpy.Table('sqlite', 'well.sqlite')
# t = atpy.Table('sqlite', 'sol.sqlite', table='webwide_kepler')
# t = atpy.Table('sqlite', 'sol.sqlite', table='web_kepler') # memory error

sql_string = 'select * from web_kepler where num > 20000 and num < 21000;'

# must specify table when executing query
t = atpy.Table('sqlite', 'sol.sqlite', table='web_kepler', query=sql_string) 


# In[]:

t.describe()

t.data


# In[]:

