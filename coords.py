import pylab, numpy, pyfits
from pylab import *
from matplotlib import *
from numpy import *
import sys, urllib, time, re


# -----------------------------------
# convert sexadecimal hours to decimal degrees

def sex2dec(ra,dec):

    ra = re.sub('\s+','|',ra.strip())
    ra = re.sub(':','|',ra.strip())
    ra = re.sub(';','|',ra.strip())
    ra = re.sub(',','|',ra.strip())
    ra = re.sub('-','|',ra.strip())
    ra = ra.split('|')
    outra = (float(ra[0]) + float(ra[1]) / 60 + float(ra[2]) / 3600) * 15.0

    dec = re.sub('\s+','|',dec.strip())
    dec = re.sub(':','|',dec.strip())
    dec = re.sub(';','|',dec.strip())
    dec = re.sub(',','|',dec.strip())
    dec = re.sub('-','|',dec.strip())
    dec = dec.split('|')
    if float(dec[0]) > 0.0:
        outdec = float(dec[0]) + float(dec[1]) / 60 + float(dec[2]) / 3600
    else:
        outdec = float(dec[0]) - float(dec[1]) / 60 - float(dec[2]) / 3600

    return outra, outdec


ra=283.0969
dec=43.3389
darcsec=120.0
season=0

cd1_1 = 0.000702794927969
cd1_2 = -0.000853190160515
cd2_1 = -0.000853190160515
cd2_2 = -0.000702794927969
cd = array([[cd1_1,cd1_2],[cd2_1,cd2_2]])
cd = linalg.inv(cd)

# coordinate limits

x1 = 1.0e30
x2 = x1
darcsec /= 3600.0
ra1 = ra - darcsec / 15.0 / cos(dec * pi / 180)
ra2 = ra + darcsec / 15.0 / cos(dec * pi / 180)
dec1 = dec - darcsec
dec2 = dec + darcsec


url  = 'http://archive.stsci.edu/kepler/kepler_fov/search.php?'
url += 'action=Search'
url += '&kic_degree_ra=' + str(ra1) + '..' + str(ra2)
url += '&kic_dec=' + str(dec1) + '..' + str(dec2)
url += '&max_records=100'
url += '&verb=3'
url += '&outputformat=CSV'

# retrieve results from MAST: nearest KIC source to supplied coordinates

z = ''
x = 1.0e30
lines = urllib.urlopen(url)
for line in lines:
	line = line.strip()
	if (len(line) > 0 and 
		'Kepler' not in line and 
		'integer' not in line and
		'no rows found' not in line):
		out = line.split(',')
		r = (float(out[4].split(' ')[0]) + \
			float(out[4].split(' ')[1]) / 60.0 + \
			float(out[4].split(' ')[2]) / 3600.0) * 15.0
		d = float(out[5].split(' ')[0]) + \
			float(out[5].split(' ')[1]) / 60.0 + \
			float(out[5].split(' ')[2]) / 3600.0
		a = sqrt((abs(r - ra) / 15.0 / cos(d * pi / 180))**2 + abs(d - dec)**2)
		if a < x:
			x = a
			z = line.split(',')
		
# convert coordinates to decimal for the two targets, determine distance from input

zra,zdec = sex2dec(z[4],z[5])
dra = zra - ra
ddec = zdec - dec
drow = cd[0,0] * dra + cd[0,1] * ddec
dcol = cd[1,0] * dra + cd[1,1] * ddec

# pixel coordinates of the nearest KIC target

row = z[70 + season * 5]
column = z[71 + season * 5]

# pixel coordinate of target

row = str(int(float(row) + drow + 0.5))
column = str(int(float(column) + dcol + 0.5))

print row,column
    
    #return kepid,ra,dec,kepmag,skygroup,channel,module,output,row,column
