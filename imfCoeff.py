import numpy as np
import scipy.integrate as sciInt

alow=0.1
ahigh=0.5
apower=-1.5
aFunc=lambda x: x**apower

blow=ahigh
bhigh=1.0
bpower=-2.2
bFunc=lambda x: x**bpower

clow=bhigh
chigh=100.0
cpower=-2.7
cFunc=lambda x: x**cpower

intA=sciInt.romberg(aFunc,alow,ahigh,rtol=10**(-5),divmax=12)
intB=sciInt.romberg(bFunc,blow,bhigh,rtol=10**(-5),divmax=12)
intC=sciInt.romberg(cFunc,clow,chigh,rtol=10**(-5),divmax=12)

#print intA,intB,intC

eqs=([intA,intB,intC],[aFunc(ahigh),-1.0*bFunc(blow),0.0],[0.0,bFunc(bhigh),-1.0*cFunc(clow)])
print eqs
res=([1.0,0.0,0.0])

sol = np.linalg.solve(eqs,res)
print sol
print sol[1]*sciInt.romberg(bFunc,0.8,bhigh,rtol=10**(-5),divmax=12)+sol[2]*sciInt.romberg(cFunc,clow,chigh,rtol=10**(-5),divmax=12)




##################################################

import numpy as np
import scipy.integrate as sciInt

alow=0.1
ahigh=0.5
apower=-1.5
aFunc=lambda x: x**apower

dlow=0.5
dhigh=0.8
dpower=-1.5
dFunc=lambda x: x**dpower

blow=0.8
bhigh=1.0
bpower=-2.2
bFunc=lambda x: x**bpower

clow=bhigh
chigh=100.0
cpower=-2.7
cFunc=lambda x: x**cpower

intA=sciInt.romberg(aFunc,alow,ahigh,rtol=10**(-5),divmax=12)
intD=sciInt.romberg(dFunc,dlow,dhigh,rtol=10**(-5),divmax=12)
intB=sciInt.romberg(bFunc,blow,bhigh,rtol=10**(-5),divmax=12)
intC=sciInt.romberg(cFunc,clow,chigh,rtol=10**(-5),divmax=12)

#print intA,intB,intC

eqs=([intA,intD,intB,intC],[aFunc(ahigh),-1.0*dFunc(dlow),0.0,0.0],[0.0,dFunc(dhigh),-1.0*bFunc(blow),0.0],[0.0,0.0,bFunc(bhigh),-1.0*cFunc(clow)])
print eqs
res=([1.0,0.0,0.0,0.0])

sol = np.linalg.solve(eqs,res)
print sol


print sol[1]*sciInt.romberg(bFunc,blow,bhigh,rtol=10**(-5),divmax=12)+sol[2]*sciInt.romberg(cFunc,clow,chigh,rtol=10**(-5),divmax=12)
