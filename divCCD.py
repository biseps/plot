import fortUtil as f
import numpy as np
import biseps as b
import constants as c
import getopt
import sys as sys

opts, extra = getopt.getopt(sys.argv[1:],'') 
#for i in range(1,132):
	#print i
	#x=b.readFile()
	#x.kicProp4('raw/kic.target.'+str(i))
	 
x=b.readFile()
x.quarter1('raw/quarter_1a.txt',sep=" ")
row,col=f.lonlat2pix(x.kic.long,x.kic.lat,np.size(x.kic.long),extra[0],c.season,c.fovDir)

ind=(row>=0)&(col>=0)&(row<c.numRows)&(col<c.numCols)
x.write_array(extra[0]+'/quart.dat.1',x.kic[ind],'w')
	