

# In[]:

import numpy as np
from astropy.io import ascii
from astropy.table import vstack
from astropy.table import Table
from astropy.table import join
import astropy.constants as const

import matplotlib
import matplotlib.pyplot as plt
import plotUtils as utils




# In[]:

# totals3=np.genfromtxt("totals.csv", delimiter=',', names=True)

# In[]:
headers = ['total_stars','total_binaries', 'potential_blends', 'planet_like_blends']


data_dir='/padata/beta/users/efarrell/data/ngts_fields/11_run/field1/blends_5'
# data_dir='/padata/beta/users/efarrell/data/ngts_fields/11_run/field1/blends_10'
# data_dir='/padata/beta/users/efarrell/data/ngts_fields/11_run/field1/blends'

input_file = os.path.join(data_dir, 'totals.csv')

totals  = ascii.read(input_file, delimiter=',', names=headers)

# totals  = ascii.read('totals.csv', delimiter=',')

runs = np.arange(0,100)

# In[]:

fig = plt.figure(figsize=(10, 15))

binsize=40
# bartype='step'
bartype='bar'
# do_normed=True
do_normed=False

data = totals['planet_like_blends'].data
ax = plt.subplot(111)
# bins = [10, 20, 30, 40, 50, 60, 70, 90, 90, 100]
# h1 = ax.hist(totals['total_stars'], bins=binsize, histtype=bartype, normed=do_normed, lw=3, color='blue', alpha=0.5, label='stars')
# h2 = ax.hist(totals['potential_blends'].data, bins=runs, histtype=bartype, normed=do_normed, lw=3, color='green', alpha=0.5, label='blends')
h2 = ax.hist(totals['potential_blends'].data, bins=binsize, histtype=bartype, normed=do_normed, lw=3, color='green', alpha=0.5, label='all transits inside plate scale')
h3 = ax.hist(totals['planet_like_blends'].data, bins=binsize, histtype=bartype, normed=do_normed, lw=3, color='red', alpha=0.5, label='planet_like transits')

ax.set_title('100 random runs - number of transits within 5arc')

handles, labels = ax.get_legend_handles_labels()
ax.legend(handles, labels)
ax.grid(True, which='minor')
ax.set_xlabel('number of transits per run', fontsize=13)
ax.set_ylabel('runs', fontsize=13)
# ax.set_xlim([0,20])
ax.set_ylim([0,20])

plt.show()

# In[]:

# scatter plot

ax = plt.subplot(111)
# ax.set_title(filename)
ax.scatter(runs, totals['planet_like_blends'], s=3, lw=0, alpha=0.7)


plt.show()
utils.save_plot(fig, '.', 'probs.jpg')


# In[]:




