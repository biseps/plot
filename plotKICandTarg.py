
import biseps as b
import numpy as np
import scipy as sci
import matplotlib.pyplot as plt
import matplotlib as m
from matplotlib import rc
import getopt
import sys as sys

m.rcParams.update({'font.size': 16})

opts, extra = getopt.getopt(sys.argv[1:],'') 
rc('text', usetex=True)
rc('font', family='serif')

#Assume we have catted all the extracts and target files together
x=b.readFile()
e=b.extras()
#.kep

masslabel=r'Mass $[M_{\odot}]$'
radlabel=r'$\log_{10} (R [R_{\odot}])$'
tefflabel=r'$T_{eff} [k]$'
teffLoglabel=r'$\log_{10} (T_{eff} [k])$'
logglabel=r'$\log g$'
appMaglabel=r'$m_{kp}$'
absmaglabel=r'$M_{kp}$'
latlabel=r'Galactic Latitude, b [degrees]'
longlabel=r'Galactic Longitude, l [degrees]'
dislabel=r'$\log_{10}$ Distance [pc]'
periodlabel=r'$\log_{10}$ Period [days]'
semilabel=r'$\log_{10}$ (a [$R_{\odot}$])'

massRlabel=r'$\frac{M_{2}}{M_{1}}$'
teffRlabel=r'$\frac{T_{2}}{T_{1}}$'
radRlabel=r'$\frac{R_{2}}{R_{1}}$'
loggRlabel=r'$\frac{\log g_{2}}{\log g_{1}}$'


def imfCalc(x,bins=50):

	y=np.zeros(np.size(x))

#	y[x<=0.1]=0.0
	y[(x>0.1) & (x<=0.5)]=0.29056*x[(x>0.1) & (x<=0.5)]**(-1.3)
	y[(x>0.5) & (x<=1.0)]=0.1557*x[(x>0.5) & (x<=1.0)]**(-2.2)
	y[(x>1.0)]=0.1557*x[(x>1.0)]**(-2.7)

	#y=y/(np.sum(y)*((np.max(x)-np.min(x))/bins))


	#y=y/((x[1]-x[0])*bins)

	return y

def semiCalc():
	x=np.array([np.log10(3.0),6.0])
	y=np.zeros(np.size(x))

	y[x<=np.log10(3.0)]=0.0
	y[(x>np.log10(3.0)) & (x<6.0)]=0.078636
	y[x>=6.0]=0.0

	return y

def twoHist(y1,y2,l1,l2,title,xtitle,savefig=None,xmin=None,xmax=None,numBins=50,prop=None):
	plt.figure()
	if xmin == None:
		xmin = sci.floor(sci.minimum(y1.min(), y2.min()))
	if xmax == None:
		xmax = sci.ceil(sci.maximum(y1.max(), y2.max()))
	a1,b1,c1=plt.hist(y1,bins=numBins,range=[xmin,xmax],alpha=0.5,label=l1,normed=1)
	a2,b2,c2=plt.hist(y2,bins=numBins,range=[xmin,xmax],alpha=0.5,label=l2,normed=1)

	if prop=='mass':
		y=imfCalc(b1,bins=numBins)
		y=y/(np.sum(y)*((20.0)/numBins))
		plt.plot(b1[b1>0.1],y[b1>0.1],'k--',label=r'$IMF_{1}$')

	#if prop=='mass2':
		#y=imfCalc(b1,bins=numBins)
		#y=b1*y
		#y=y/(np.sum(y)*((np.max(b1)-np.min(b1))/numBins))
		#plt.plot(b1,y,'k--',label=r'$IMF_{2}$')

	if prop=='massR':
		mr=np.zeros(np.size(b1))
		mr[:]=1.0
		plt.plot(b1,mr,'k--',label='IMR')

	if prop=='semi':
		#y=np.array([0.0,np.maximum(a1.max(),a2.max())])
		#x=np.array([np.log10(3.0),np.log10(3.0)])
		#plt.plot(x,y,'k--')
		#x=np.array([6.0,6.0])
		#plt.plot(x,y,'k--')

		x=np.array([np.log10(3.0),6.0])
		y=np.zeros(np.size(x))
		y[:]=0.078636
		y=y/(np.sum(y)*((np.max(x)-np.min(x))/2.0))
		plt.plot(x,y,'k--')
		plt.plot([x[0],x[0]],[0.0,y[0]],'k--')
		

	plt.legend(loc=0)
	plt.xlabel(xtitle)
	plt.title(title)
		
	if savefig==None:
		plt.show()
	else:
		plt.savefig(savefig)

x.ranLoc('ranLoc.all.targ.1')
x.kic5('kic.targ')

twoHist(x.ranLoc.appMag,x.kic5.feh,r'BiSEPS',r'Kepler input catalog','Apparent magntidue distribution ',appMaglabel,savefig='appMag.all.svg')