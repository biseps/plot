#!/bin/bash
#Make sure in merge direcetory, uses output from extractTar.py and extractMag.py
#Cats all the kepler target files together
# VERSION=${1:-1}
VERSION=$1
cat ../pop_s_*/*/ranLoc.targ.$VERSION > ranLoc.sing.targ.$VERSION
cat ../pop_s_*/*/extract.targ.$VERSION > extract.sing.targ.$VERSION
cat ../pop_b_*/*/ranLoc.targ.$VERSION > ranLoc.bin.targ.$VERSION
cat ../pop_b_*/*/extract.targ.$VERSION > extract.bin.targ.$VERSION
cat extract.bin.targ.$VERSION extract.sing.targ.$VERSION > extract.all.targ.$VERSION
cat ranLoc.bin.targ.$VERSION ranLoc.sing.targ.$VERSION > ranLoc.all.targ.$VERSION

sed -i 's/\"//g' extract.all.targ.$VERSION
sed -i 's/\"//g' extract.sing.targ.$VERSION
sed -i 's/\"//g' extract.bin.targ.$VERSION

#Cats all the <16 mag data together
cat ../pop_s_*/*/ranLoc.targAll.$VERSION > ranLoc.sing.targAll.$VERSION
cat ../pop_s_*/*/extract.targAll.$VERSION > extract.sing.targAll.$VERSION
cat ../pop_b_*/*/ranLoc.targAll.$VERSION > ranLoc.bin.targAll.$VERSION
cat ../pop_b_*/*/extract.targAll.$VERSION > extract.bin.targAll.$VERSION
cat extract.bin.targAll.$VERSION extract.sing.targAll.$VERSION > extract.all.targAll.$VERSION
cat ranLoc.bin.targAll.$VERSION ranLoc.sing.targAll.$VERSION > ranLoc.all.targAll.$VERSION

#Get the kic data as well
cat ../pop_s_*/*/kic.targAll.$VERSION > kic.sing.targAll.$VERSION
cat ../pop_s_*/*/kic.targ.$VERSION > kic.sing.targ.$VERSION
cat ../pop_b_*/*/kic.targAll.$VERSION > kic.bin.targAll.$VERSION
cat ../pop_b_*/*/kic.targ.$VERSION > kic.bin.targ.$VERSION
cat kic.bin.targAll.$VERSION kic.sing.targAll.$VERSION > kic.all.targAll.$VERSION
cat kic.bin.targ.$VERSION kic.sing.targ.$VERSION > kic.all.targ.$VERSION


cat ../pop_s_*/*/mag.dat.idl.$VERSION > mag.sing.idl.$VERSION
cat ../pop_b_*/*/mag.dat.idl.$VERSION > mag.bin.idl.$VERSION
cat mag.bin.idl.$VERSION mag.sing.idl.$VERSION > mag.all.idl.$VERSION

cat ../pop_b_*/*/eb.eb.targ.$VERSION > eb.eb.targ.$VERSION
cat ../pop_b_*/*/kic.eb.targ.$VERSION > kic.eb.targ.$VERSION
cat ../pop_b_*/*/ext.eb.targ.$VERSION > ext.eb.targ.$VERSION

cat ../pop_s_*/*/mag.targAll.$VERSION > mag.sing.targAll.$VERSION
cat ../pop_s_*/*/mag.targ.$VERSION > mag.sing.targ.$VERSION
cat ../pop_b_*/*/mag.targAll.$VERSION > mag.bin.targAll.$VERSION
cat ../pop_b_*/*/mag.targ.$VERSION > mag.bin.targ.$VERSION
cat mag.bin.targAll.$VERSION mag.sing.targAll.$VERSION > mag.all.targAll.$VERSION
cat mag.bin.targ.$VERSION mag.sing.targ.$VERSION > mag.all.targ.$VERSION

