#Various comparisons between us and kic

import biseps as b
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc

rc('text', usetex=True)
rc('font', family='serif')

x=b.readFile()
x.kic3(file='kic.r1.out')
y=b.readFile()
y.ranLoc('all.ranLoc.1')

plt.figure(1)
plt.subplot(2,2,1)
plt.hist(x.kic3.lat,bins=20,range=[5,25],normed=1,alpha=0.5,label='KIC')
plt.xlabel('Galactic Latitude, b [degrees]')

plt.subplot(2,2,2)
plt.hist(x.kic3.long,bins=20,range=[65,85],normed=1,alpha=0.5)
plt.xlabel('Galactic Longitude, l [degrees]')

plt.subplot(2,2,3)
#app mag
plt.hist(x.kic3.kp,bins=32,range=[8,16],normed=1,alpha=0.5)
plt.xlabel(r'$m_{Kp}$')


plt.subplot(2,2,1)
plt.hist(y.ranLoc.lat,bins=20,range=[5,25],normed=1,alpha=0.5,label='Us')
plt.legend()

plt.subplot(2,2,2)
plt.hist(y.ranLoc.long,bins=20,range=[65,85],normed=1,alpha=0.5)

plt.subplot(2,2,3)
#app mag
plt.hist(y.ranLoc.appMag,bins=32,range=[8,16],normed=1,alpha=0.5)
plt.show()
#plt.savefig('comp.all.kic.png')


####################################
import biseps as b
import numpy as np
import matplotlib.pyplot as plt

x=b.readFile()
x.kic3(file='kic.r1.out')
y=b.readFile()
y.ranLoc('all.ranLoc.1')

(la1n,la1b,la1p)=plt.hist(x.kic3.lat,bins=20,range=[5,25],normed=1,cumulative=1)
(lo1n,lo1b,lo1p)=plt.hist(x.kic3.long,bins=20,range=[65,85],normed=1,cumulative=1)
(m1n,m1b,m1p)=plt.hist(x.kic3.kp,bins=32,range=[8,16],normed=1,cumulative=1)
(la2n,la2b,la2p)=plt.hist(y.ranLoc.lat,bins=20,range=[5,25],normed=1,cumulative=1)
(lo2n,lo2b,lo2p)=plt.hist(y.ranLoc.long,bins=20,range=[65,85],normed=1,cumulative=1)
(m2n,m2b,m2p)=plt.hist(y.ranLoc.appMag,bins=32,range=[8,16],normed=1,cumulative=1)

plt.figure(1)
plt.subplot(2,2,1)

bins2=np.zeros(np.size(la1n),dtype='float64')
for i in range(0,np.size(la1b)-1):
	bins2[i]=(la1b[i]+la1b[i+1])/2.0

plt.plot(bins2,la1n,label='kic')
plt.plot(bins2,la2n,label='us')
plt.legend(loc=2)
plt.xlabel('Galactic Latitude, b [degrees]')


plt.subplot(2,2,2)
bins2=np.zeros(np.size(lo1n),dtype='float64')
for i in range(0,np.size(lo1b)-1):
	bins2[i]=(lo1b[i]+lo1b[i+1])/2.0

plt.plot(bins2,lo1n,label='kic')
plt.plot(bins2,lo2n,label='us')
plt.legend(loc=2)
plt.xlabel('Galactic Longitude, l [degrees]')



plt.subplot(2,2,3)
bins2=np.zeros(np.size(m1n),dtype='float64')
for i in range(0,np.size(m1b)-1):
	bins2[i]=(m1b[i]+m1b[i+1])/2.0

plt.plot(bins2,m1n,label='kic')
plt.plot(bins2,m2n,label='us')
plt.legend(loc=2)
plt.xlabel('m_kp')
plt.savefig('comp.all.kic.png')
#plt.show()

#############################################
import biseps as b
import numpy as np
import matplotlib.pyplot as plt

x=b.readFile()
x.kic4(file='kic.r2.out')
y=b.readFile()
y.ranLoc('all.ranLoc.1')

(la1n,la1b,la1p)=plt.hist(x.kic4.av[(x.kic4.av>0.0)]*0.88,bins=100,normed=1,cumulative=1)
(la2n,la2b,la2p)=plt.hist(y.ranLoc.ext[(y.ranLoc.ext>0.0)],bins=100,normed=1,cumulative=1)

plt.figure(1)
plt.subplot(2,1,1)

plt.hist(x.kic4.av[(x.kic4.av>0.0)]*0.88,bins=100,normed=1,alpha=0.5,label='KIC')
plt.hist(y.ranLoc.ext[(y.ranLoc.ext>0.0)],bins=100,normed=1,alpha=0.5,label='Us')
plt.legend(loc = 0)

plt.subplot(2,1,2)

bins2=np.zeros(np.size(la1n),dtype='float64')
for i in range(0,np.size(la1b)-1):
	bins2[i]=(la1b[i]+la1b[i+1])/2.0

plt.plot(bins2,la1n,label='kic')
plt.plot(bins2,la2n,label='us')
plt.legend(loc = 0)
plt.savefig('comp.all.kic.ext.png')