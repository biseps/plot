import biseps as b
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc
from scipy import stats
from matplotlib.pyplot import figure, show, axes, sci
from matplotlib import cm, colors
from matplotlib.font_manager import FontProperties
import itertools

rc('text', usetex=True)
rc('font', family='serif')

G=6.67384*10**(-11)
Msun=1.98892*10**(30)
Rsun=6.955*10**(8)
nbins=50

def plotIm(x1,y,xlabel,ylabel,evolArr,title,cols):
	Nr = 3
	Nc = 2

	fig = plt.figure()
	cmap = cm.binary
	figtitle = title+' '+xlabel
	t = fig.text(0.5, 0.95, figtitle,
				horizontalalignment='center',
				fontproperties=FontProperties(size=16))
	
	cax = fig.add_axes([0.2, 0.08, 0.6, 0.04])

	w = 0.38
	h = 0.22
	ax = []
	images = []
	vmin = 1e40
	vmax = -1e40
	histEvol=[]

	for i in range(Nr):
		for j in range(Nc):
			ind=j+(i*2)
			#print i,j,ind
			if ind==6:
				break
			#pos = [0.075 + j*1.1*w, 0.18 + i*1.2*h, w, h]
			pos= [0.085 + j*1.25*w, 0.18 + i*1.2*h, w, h]
			a = fig.add_axes(pos,ylabel=ylabel[ind])
			if i > 0:
				a.set_xticklabels([])

			#images.append(a.imshow(data, cmap=cmap))

			minX=np.min(x1)-0.05*np.abs(np.min(x1))
			maxX=np.max(x1)+0.05*np.abs(np.max(x1))

			if np.abs(np.min(y[ind]))!=0.0:
				minY=np.min(y[ind])-0.05*np.abs(np.min(y[ind]))
			else:
				minY=-0.05
			if np.abs(np.max(y[ind]))!=0.0:
				maxY=np.max(y[ind])+0.05*np.abs(np.max(y[ind]))
			else:
				maxY=0.05

			
			bins=100
			hist,xedges,yedges = np.histogram2d(x1,y[ind],bins=bins,range=[[minX,maxX],[minY,maxY]])

			histEvol=[]
			histTemp=0.0
			for ii in evolArr:
				histTemp,xe,ye=np.histogram2d(x1[ii],y[ind][ii],bins=bins,range=[[minX,maxX],[minY,maxY]])
				histEvol.append(histTemp)


			histB=np.zeros(np.shape(hist))

			maxHist=histEvol[0]
			#print histEvol
			listLevels=[0]
			for ii in range(1,len(evolArr)):
				#print ii,np.shape(histEvol[ii]),np.shape(hist)
				if np.shape(histEvol[ii]) != np.shape(hist):
					continue
				#print len(histEvol),np.shape(histEvol[ii]),np.shape(maxHist),np.shape(histB)
				histB[(histEvol[ii]>maxHist)]=ii
				maxHist[histEvol[ii]>maxHist]=histEvol[ii][histEvol[ii]>maxHist]
				listLevels.append(ii)

			#print histB[histB!=0]
			
			extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
		
			#plt.imshow(hist.T,extent=extent,interpolation='nearest',origin='lower',cmap='jet', aspect='auto')
			vmin = 0.0
			vmax = 4.0
			hist=np.log10(hist)
			#hist=np.log10(hist[hist==0])
			#images.append(a.imshow(hist.T,extent=extent, cmap=cmap,interpolation='nearest',origin='lower',aspect='auto'))
			aa=a.imshow(hist.T,extent=extent, cmap=cmap,interpolation='nearest',origin='lower',vmin=0.0,vmax=4.0,aspect='auto')
			
			for ii in range(0,len(evolArr)):
				histB=np.zeros(np.shape(hist))
				histB[histEvol[ii]==0]=-1
				histB[histEvol[ii]>0]=1
				a.contour(histB.T,extent=extent,origin='lower',linewidth=3,levels=[0,1],colors=[cols[ii]])
			#a.ylabel(ylabel[i+(j*2)])
			#ax.append(a)

	#for i in range(len(y)):
		#ax=plt.subplot(3,2,i)
		#hist,xedges,yedges = np.histogram2d(x1,y[i],bins=250,normed=True)
		#hist[hist==0]=np.log10(hist[hist==0])
		#extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
		#plt.imshow(hist.T,extent=extent,interpolation='nearest',origin='lower',cmap='jet', aspect='auto')
		##plt.title(r'kic in KIC paramteres ')
		##plt.xlim(x1.min(),x1.max())
		#plt.xlabel(xlabel)
		#plt.ylabel(ylabel[i])
		#plt.colorbar()

	#class ImageFollower:
		#'update image in response to changes in clim or cmap on another image'
		#def __init__(self, follower):
			#self.follower = follower
		#def __call__(self, leader):
			#self.follower.set_cmap(leader.get_cmap())
			#self.follower.set_clim(leader.get_clim())
	
	#norm = colors.Normalize(vmin=vmin, vmax=vmax)
	#for i, im in enumerate(images):
		#im.set_norm(norm)
		#if i > 0:
			#images[0].callbacksSM.connect('changed', ImageFollower(im))
	
	# The colorbar is also based on this master image.
	cb=fig.colorbar(aa,cax=cax, orientation='horizontal')
	cb.set_label('Log N')

	#axes(ax[0])     # Return the current axes to the first one,
	#sci(images[0])  # because the current image must be in current axes.
	#tight_layout()
	#show()
	#plt.savefig('usKic/'+xlabel.replace(" ", ""))


x=b.readFile()
y=b.readFile()
single=True
if single:
	x.kicProp('kic.sing.targAll.1')
	#y.kicProp('kic.sing.targAll.1')
	y.readExtract('extract.sing.targAll.1')
	logz=np.zeros(np.size(y.kep.m1))

	logz[0:28281]=np.log10(0.0033/0.02)
	
else:
	x.kicProp('kic.bin.targAll.1')
	#y.kicProp('kic.bin.targAll.1')
	y.readExtract('extract.bin.targAll.1')
	logz=np.zeros(np.size(y.kep.m1))

	logz[0:26439]=np.log10(0.0033/0.02)


#xInd=False
xInd2=(x.kicProp.tefferr < 10**10.0)&(x.kicProp.loggerr < 9.0)&(x.kicProp.logzerr < 9.0)&(x.kicProp.kp>0.0)
xInd2=xInd2&(x.kicProp.kp>0.0)&(x.kicProp.logz<1000000.0)&(x.kicProp.teff>0.0)

massX=0.0
try:
	massX=x.kicProp.mass
except AttributeError:
	ex=b.extras()
	massX=ex.radlogg2mass(x.kicProp.rad,x.kicProp.logg)

try:
	logg=y.kep.logg
except AttributeError:
	logg=ex.massrad2logg(y.kep.m1,y.kep.r1)

xInd=xInd2

cols=('r','g','c','m','y','b')

if single:
	dwarf=y.kep.evol.endswith("'A*A*'")
	giant=np.logical_not(y.kep.evol.endswith("'A*A*'"))
	evolArr=[dwarf[xInd],giant[xInd]]
	title='Single pre Target'
else:
	#dwarf-dwarf
	evolArr0=y.kep.evol.endswith("'A*A*'")|y.kep.evol.endswith("'AnA*'")|y.kep.evol.endswith("'AtA*'")
	#giant-dwarf
	evolArr1=y.kep.evol.endswith("B*A*'")|y.kep.evol.endswith("BnA*'")|y.kep.evol.endswith("BtA*'")
	evolArr1=evolArr1|y.kep.evol.endswith("C*A*'")|y.kep.evol.endswith("CnA*'")
	evolArr1=evolArr1|y.kep.evol.endswith("HeA*'")|y.kep.evol.endswith("M*A*'")
	#giant-giant
	evolArr2=y.kep.evol.endswith("B*B*'")|y.kep.evol.endswith("C*B*'")
	evolArr2=evolArr2|y.kep.evol.endswith("C*M*'")|y.kep.evol.endswith("HeM*'")
	evolArr2=evolArr2|y.kep.evol.endswith("M*B*'")|y.kep.evol.endswith("M*M*'")
	#NS-dwarf
	#evolArr3=y.kep.evol.endswith("NSA*'")|y.kep.evol.endswith("NSAn'")|y.kep.evol.endswith("NSAt'")
	#Ns-giant
	#evolArr4=y.kep.evol.endswith("NSB*'")|y.kep.evol.endswith("NSBn'")|y.kep.evol.endswith("NSHe'")|y.kep.evol.endswith("NSM*'")
	#wd-dwarf
	evolArr5=y.kep.evol.endswith("WDA*'")|y.kep.evol.endswith("WDAn'")|y.kep.evol.endswith("WDAt'")
	#wd-gaint
	evolArr6=y.kep.evol.endswith("WDB*'")|y.kep.evol.endswith("WDBn'")|y.kep.evol.endswith("WDHe'")|y.kep.evol.endswith("WDM*'")|y.kep.evol.endswith("WDBt'")

	evolArr=[evolArr0[xInd],evolArr1[xInd],evolArr2[xInd],evolArr5[xInd],evolArr6[xInd]]

	title='Binary pre Target'

xlist=[np.log10(x.kicProp[xInd].teff),x.kicProp[xInd].logg,np.log10(x.kicProp[xInd].rad),massX[xInd],x.kicProp[xInd].logz,x.kicProp[xInd].kp,np.log10(x.kicProp[xInd].teff),x.kicProp[xInd].logg,np.log10(x.kicProp[xInd].rad),massX[xInd],x.kicProp[xInd].logz,x.kicProp[xInd].kp]
label=['Kic Log Teff','Kic Log g','Kic Log10 R','Kic Mass','Kic Log Z','Kic Kp','Kic Log Teff','Kic Log g','Kic Log10 R','Kic Mass','Kic Log Z','Kic Kp']


yList=[np.log10(y.kep[xInd].t1),logg[xInd],np.log10(y.kep.r1[xInd]),y.kep.m1[xInd],logz[xInd],x.kicProp[xInd].kp]
ylabel=['Real Log Teff','Real Log g','Real Log10 R','Real Mass','Real Log Z','Real Kp']

for i in range(len(xlist)/2):
#for i in range(1):
	print label[i]
	xxx=xlist[i+1:i+6]+[yList[i]]
	yyy=label[i+1:i+6]+[ylabel[i]]
	plotIm(xlist[i],xxx,label[i],yyy,evolArr,title,cols)

plt.show()





