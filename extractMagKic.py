#Extract from extract.dat.1 the stars we want based on target.dat.*c file
import biseps as b
import numpy as np
import getopt
import sys as sys

#extra=['sing','../pop_s_y']
opts, extra = getopt.getopt(sys.argv[1:],'')
version=extra[0]
sysType='targ'

x=b.readFile()

x.readTarget('target.dat.'+version)

ind=[(x.target.bin > 0)&(x.target.bin<12)][0]

#print ind
x.ranLoc('ranLoc.targAll.'+version)
with open('ranLoc.'+sysType+'.'+version,"w") as f:
	for i in range(np.count_nonzero(ind)):
		strin=''
		for j in range(0,7):
			strin+=str(x.ranLoc[x.target[ind][i].id][j])+' '
		f.write(strin+"\n")
f.close()

x.kepler3('extract.targAll.'+version)
with open('extract.'+sysType+'.'+version,"w") as f:
	for i in range(np.count_nonzero(ind)):
		strin=''
		for j in range(0,17):
			strin+=str(x.kep[x.target[ind][i].id][j])+' '
		f.write(strin+"\n")
f.close()

#Read KIC data
#x.kicProp('kic.dat.'+version)
#with open('kic.'+sysType+'.'+version,"w") as f:
	#for i in range(np.count_nonzero(ind)):
		#strin=''
		#for j in range(0,12):
			#strin+=str(x.kicProp[x.target[ind][i].id][j])+' '
		#f.write(strin+"\n")
#f.close()

