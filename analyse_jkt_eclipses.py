# In[]:

import os

import atpy
import numpy as np

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
import matplotlib.cm as cm
from matplotlib.colors import LogNorm
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

# import my handy utilities
sys.path.append('/padata/beta/users/efarrell/repos/biseps')
from common import utils

from matplotlib.ticker import MultipleLocator, FormatStrFormatter


# In[]:

# t = atpy.Table('sqlite', 'well.sqlite')
# t = atpy.Table('sqlite', 'sol.sqlite', table='webwide_kepler')
# t = atpy.Table('sqlite', 'sol.sqlite', table='web_kepler') # memory error

sql_string = 'select trans_depth1, trans_depth2 from depths where inclin = 90;'


base_folder = '/padata/beta/users/efarrell/data/plato/jkt_output'

db_file = os.path.join(base_folder, 'run_1/78/depths_78.sqlite')
# db_file = os.path.join(base_folder, 'run_4/78/depths_78.sqlite') # new jkt surface brightness


# In[]:


# must specify table 
# when executing query
t = atpy.Table('sqlite', db_file, table='depths', query=sql_string) 

t.describe()
t.data


# In[]:

depth1 = t['trans_depth1']
depth2 = t['trans_depth2']
   

# In[]:

# plot trans_depth1 and trans_depth2 plain histogram

fig = plt.figure(figsize=(12, 8))
ax = plt.subplot(111)

x = depth1
y = depth2

# ax.set_xlim([0, 100])
# ax.set_ylim([0, 100])

ax.set_title('NEW jkt code: primary & secondary transit depths. Inclination = 90 deg', fontsize=16, fontweight='bold')
# ax.set_title('OLD jkt code: primary & secondary transit depths. Inclination = 90 deg', fontsize=16, fontweight='bold')
ax.set_xlabel('transit depth ', fontsize=16, fontweight='bold')
ax.set_ylabel('number of systems',     fontsize=16, fontweight='bold')

# ax.scatter(x, y, color='red', s=5, lw=1, alpha=0.5, label='xxxx')

# line plot
# ax.plot(x_means, y_means, color='red', lw=1, alpha=0.5, label='xxxx')

# line and marker plot
# ax.plot(x_means, y_means, 
        # color='blue', lw=1, linestyle='dashed', marker='o',
        # markerfacecolor='red', markersize=3, label='mean')

# histogram
h1 = ax.hist(x, bins=100, histtype='bar', normed=False, lw=1, 
        color='green', alpha=0.6,  label='transit depth 1', log=True)

h2 = ax.hist(y, bins=100, histtype='bar', normed=False, lw=1, 
        color='red', alpha=0.4,  label='transit depth 2', log=True)

handles, labels = ax.get_legend_handles_labels()
ax.legend(handles, labels)


# x_formatter      = FormatStrFormatter('%3.2f')
# y_formatter      = FormatStrFormatter('%d')

# ax.xaxis.set_minor_formatter(x_formatter)
# ax.xaxis.set_major_formatter(x_formatter)
# ax.xaxis.set_minor_locator(MultipleLocator(50))
# ax.xaxis.set_major_locator(MultipleLocator(20))

# ax.yaxis.set_minor_formatter(y_formatter)
# ax.yaxis.set_major_formatter(y_formatter)
# ax.yaxis.set_minor_locator(MultipleLocator(50))
# ax.yaxis.set_major_locator(MultipleLocator(20))


# for label in ax.get_xticklabels():
    # label.set_rotation(45)

# ax.minorticks_on() 
# ax.grid(True, which='minor')
ax.grid(True, which='major')
# ax.grid(True, which='both')


plt.show()

# In[]:

# plot ratio of trans_depth1 to trans_depth2

fig = plt.figure(figsize=(12, 8))
ax = plt.subplot(111)

ratio = depth2 / depth1 
masked_ratio = np.ma.masked_invalid(ratio)
valid_values = masked_ratio.filled(fill_value=0)

# ax.set_title('New jkt code: ratio primary & secondary transit depths. Inclination = 90 deg', fontsize=16, fontweight='bold')
ax.set_title('OLD jkt code: ratio primary & secondary transit depths. Inclination = 90 deg', fontsize=16, fontweight='bold')
ax.set_xlabel('ratio ', fontsize=16, fontweight='bold')
ax.set_ylabel('number of systems',     fontsize=16, fontweight='bold')

# ax.scatter(x, y, color='red', s=5, lw=1, alpha=0.5, label='xxxx')

# line plot
# ax.plot(x_means, y_means, color='red', lw=1, alpha=0.5, label='xxxx')

# line and marker plot
# ax.plot(x_means, y_means, 
        # color='blue', lw=1, linestyle='dashed', marker='o',
        # markerfacecolor='red', markersize=3, label='mean')


h1 = ax.hist(valid_values, bins=100, histtype='bar', normed=False, lw=1, 
        color='green', alpha=0.6,  range=[0,10], label='ratio', log=True)


handles, labels = ax.get_legend_handles_labels()
ax.legend(handles, labels)


# x_formatter      = FormatStrFormatter('%3.2f')
# y_formatter      = FormatStrFormatter('%d')

# ax.xaxis.set_minor_formatter(x_formatter)
# ax.xaxis.set_major_formatter(x_formatter)
# ax.xaxis.set_minor_locator(MultipleLocator(50))
# ax.xaxis.set_major_locator(MultipleLocator(20))

# ax.yaxis.set_minor_formatter(y_formatter)
# ax.yaxis.set_major_formatter(y_formatter)
# ax.yaxis.set_minor_locator(MultipleLocator(50))
# ax.yaxis.set_major_locator(MultipleLocator(20))


# for label in ax.get_xticklabels():
    # label.set_rotation(45)

# ax.minorticks_on() 
# ax.grid(True, which='minor')
ax.grid(True, which='major')
# ax.grid(True, which='both')


plt.show()

# In[]:



