
import numpy as np
import matplotlib.pyplot as plt
import biseps as b
from matplotlib import rc
from matplotlib.colors import LinearSegmentedColormap
rc('text', usetex=False)
rc('font', family='serif')

G=6.67384*10**(-11)
Msun=1.98892*10**(30)
Rsun=6.955*10**(8)
nbins=50

sK=b.readFile()
sR=b.readFile()

bK=b.readFile()
bR=b.readFile()

sK.kicProp('kic.sing.targAll.10')
sR.readExtract('extract.sing.targAll.10')

bK.kicProp('kic.bin.targAll.10')
bR.readExtract('extract.bin.targAll.10')


sKt=b.readFile()
sRt=b.readFile()

bKt=b.readFile()
bRt=b.readFile()

sKt.kicProp('kic.sing.targ.10')
sRt.readExtract('extract.sing.targ.10')

bKt.kicProp('kic.bin.targ.10')
bRt.readExtract('extract.bin.targ.10')



sKInd=(sK.kicProp.tefferr < 10**10.0)&(sK.kicProp.loggerr < 9.0)&(sK.kicProp.logzerr < 9.0)
sInd=sKInd&(sR.kep.t1<40000.0)
bKInd=(bK.kicProp.tefferr < 10**10.0)&(bK.kicProp.loggerr < 9.0)&(bK.kicProp.logzerr < 9.0)
bInd=bKInd


sKIndt=(sKt.kicProp.tefferr < 10**10.0)&(sKt.kicProp.loggerr < 9.0)&(sKt.kicProp.logzerr < 9.0)
sIndt=sKIndt&(sRt.kep.t1<40000.0)
bKIndt=(bKt.kicProp.tefferr < 10**10.0)&(bKt.kicProp.loggerr < 9.0)&(bKt.kicProp.logzerr < 9.0)
bIndt=bKIndt


ex=b.extras()
massSing=ex.radlogg2mass(sK.kicProp.rad,sK.kicProp.logg)
massSingt=ex.radlogg2mass(sKt.kicProp.rad,sKt.kicProp.logg)
massBin=ex.radlogg2mass(bK.kicProp.rad,bK.kicProp.logg)
massBint=ex.radlogg2mass(bKt.kicProp.rad,bKt.kicProp.logg)


slogg1=ex.massrad2logg(sR.kep.m1,sR.kep.r1)
slogg1t=ex.massrad2logg(sRt.kep.m1,sRt.kep.r1)

blogg1=ex.massrad2logg(bR.kep.m1,bR.kep.r1)
blogg2=ex.massrad2logg(bR.kep.m2,bR.kep.r2)

blogg1t=ex.massrad2logg(bRt.kep.m1,bRt.kep.r1)
blogg2t=ex.massrad2logg(bRt.kep.m2,bRt.kep.r2)

mR=np.minimum(bR.kep.m1,bR.kep.m2)/np.maximum(bR.kep.m1,bR.kep.m2)
mRt=np.minimum(bRt.kep.m1,bRt.kep.m2)/np.maximum(bRt.kep.m1,bRt.kep.m2)
tR=np.minimum(bR.kep.t1,bR.kep.t2)/np.maximum(bR.kep.t1,bR.kep.t2)
tRt=np.minimum(bRt.kep.t1,bRt.kep.t2)/np.maximum(bRt.kep.t1,bRt.kep.t2)

cdict3 = {'red':  ((0.0, 0.0, 0.0),
                   (0.25,0.0, 0.0),
                   (0.495,1.0, 1.0),
                   (0.505, 1.0, 1.0),
                   (0.75,1.0, 1.0),
                   (1.0, 0.4, 1.0)),

         'green': ((0.0, 0.0, 0.0),
                   (0.25,0.0, 0.0),
                   (0.495, 1.0, 1.0),
                   (0.505, 1.0, 0.9),
                   (0.75,0.0, 0.0),
                   (1.0, 0.0, 0.0)),

         'blue':  ((0.0, 0.0, 0.4),
                   (0.25,1.0, 1.0),
				   (0.495,1.0, 1.0),
                   (0.505, 1.0, 0.8),
                   (0.75,0.0, 0.0),
                   (1.0, 0.0, 0.0))
        }

plt.register_cmap(name='BlueRed3', data=cdict3)


def plotHist(x1,y1,x2,y2,xlabel,ylabel,f1,f2,sType,log=False,ratio=False):

	if log:
		x1=np.log10(x1)
		y1=np.log10(y1)
		x2=np.log10(x2)
		y2=np.log10(y2)

	minX=np.minimum(x1.min(),x2.min())
	maxX=np.maximum(x1.max(),x2.max())
	minY=np.minimum(y1.min(),y2.min())
	maxY=np.maximum(y1.max(),y2.max())

	hist,xedges,yedges = np.histogram2d(x1,y1,bins=100,range=[[minX,maxX],[minY,maxY]],normed=True)
	hist2,xedges,yedges = np.histogram2d(x2,y2,bins=100,range=[[minX,maxX],[minY,maxY]],normed=True)
	#vmin=np.minimum(hist.min(),hist2.min())
	#vmin=0.0
	#vmax=np.maximum(hist.max(),hist2.max())

	histDiff=hist.T-hist2.T

	maxHist=np.maximum(hist.max(),hist2.max())
	minHist=np.minimum(hist[hist>0].min(),hist2[hist2>0].min())

	hist2[hist2==0]=np.nan

	extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
	hist[hist==0]=np.nan
	print np.count_nonzero(np.logical_not(np.isfinite(hist2))),np.count_nonzero(np.logical_not(np.isfinite(hist)))

	minV=np.minimum(minX,minY)
	maxV=np.minimum(maxX,maxY)
	plt.figure()
	plt.clf()
	plt.cla()
	ax1=plt.subplot(121)
	plt.imshow(hist.T,extent=extent,interpolation='nearest',origin='lower',cmap='jet',aspect='auto',vmin=minHist,vmax=maxHist)
	plt.title(sType+' Pre target')
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	if ratio==False:
		plt.plot([minV,maxV],[minV,maxV],c='k')
	#plt.ylim(0.0,1.0)
	plt.colorbar()

	plt.subplot(122,sharex=ax1,sharey=ax1)
	plt.imshow(hist2.T,extent=extent,interpolation='nearest',origin='lower',cmap='jet',aspect='auto',vmin=minHist,vmax=maxHist)
	plt.colorbar()
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	if ratio==False:
		plt.plot([minV,maxV],[minV,maxV],c='k')
	#plt.ylim(0.0,1.0)
	plt.title(sType+' Post target')
	plt.savefig('compare/'+f1)
	plt.clf()
	plt.cla()

	vmax=np.max(np.abs(histDiff))
	plt.figure()
	plt.title(sType)
	plt.imshow(histDiff,extent=extent,interpolation='nearest',origin='lower',cmap=plt.get_cmap('BlueRed3'),aspect='auto',vmin=-1.0*vmax,vmax=vmax)
	cb=plt.colorbar()
	cb.set_label('Pre-Post')
	if ratio==False:
		plt.plot([minV,maxV],[minV,maxV],c='k')
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.savefig('compare/'+f2)

def plotHist2(x1,y1,x2,y2,xlabel,ylabel,log=False,ratio=False,title=''):

	if log:
		x1=np.log10(x1)
		y1=np.log10(y1)
		x2=np.log10(x2)
		y2=np.log10(y2)
	
	minX=np.minimum(x1.min(),x2.min())
	maxX=np.maximum(x1.max(),x2.max())
	minY=np.minimum(y1.min(),y2.min())
	maxY=np.maximum(y1.max(),y2.max())

	hist,xedges,yedges = np.histogram2d(x1,y1,bins=100,range=[[minX,maxX],[minY,maxY]],normed=True)
	hist2,xedges,yedges = np.histogram2d(x2,y2,bins=100,range=[[minX,maxX],[minY,maxY]],normed=True)

	histDiff=hist.T-hist2.T

	maxHist=np.maximum(np.max(hist),np.max(hist2))
	minHist=np.minimum(hist[hist>0].min(),hist2[hist2>0].min())

	hist2[hist2==0.0]=np.nan

	extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
	hist[hist==0]=np.nan
	#print np.count_nonzero(np.logical_not(np.isfinite(hist2))),np.count_nonzero(np.logical_not(np.isfinite(hist)))

	minV=np.minimum(minX,minY)
	maxV=np.minimum(maxX,maxY)
	plt.figure()
	#plt.clf()
	#plt.cla()
	ax1=plt.subplot(121)
	plt.imshow(hist.T,extent=extent,interpolation='nearest',origin='lower',cmap='jet',aspect='auto',vmin=minHist,vmax=maxHist)
	plt.title(title+' Pre target')
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	if ratio==False:
		plt.plot([minV,maxV],[minV,maxV],c='k')
	plt.colorbar()
	plt.xlim(maxX,minX)
	plt.ylim(maxY,minY)

	plt.subplot(122,sharex=ax1,sharey=ax1)
	plt.imshow(hist2.T,extent=extent,interpolation='nearest',origin='lower',cmap='jet',aspect='auto',vmin=minHist,vmax=maxHist)
	plt.colorbar()
	if ratio==False:
		plt.plot([minV,maxV],[minV,maxV],c='k')
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.xlim(maxX,minX)
	plt.ylim(maxY,minY)

	plt.title(title+' Post target')
	#plt.clf()
	#plt.cla()

	vmax=np.max(np.abs(histDiff))
	plt.figure()
	plt.title(title)
	plt.imshow(histDiff,extent=extent,interpolation='nearest',origin='lower',cmap=plt.get_cmap('BlueRed3'),aspect='auto',vmin=-1.0*vmax,vmax=vmax)
	cb=plt.colorbar()
	if ratio==False:
		plt.plot([minV,maxV],[minV,maxV],c='k')
	cb.set_label('Pre-Post')
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.xlim(maxX,minX)
	plt.ylim(maxY,minY)
#plotIm(bR.kep.m1[bInd],bR.kep.m2[bInd],bRt.kep.m1[bIndt],bRt.kep.m2[bIndt],'M1','M2')
#plt.show()

#plotIm(bK.kicProp.teff[bInd],tR[bInd],bKt.kicProp.teff[bIndt],tRt[bIndt],r'Kic $T_{eff}$',r'$T_2 / T_1$')
#plt.show()

#plotIm(massSing[sInd],sR.kep.m1[sInd],massSingt[sIndt],sRt.kep.m1[sIndt],'Kic Mass','Real Mass')
#plt.show()

#plotHist(sK.kicProp.logg[sInd],slogg1[sInd],sKt.kicProp.logg[sIndt],slogg1t[sIndt],'Kic Log g','Real Log g')
#plt.show()

#plotHist(massSing[sInd],sR.kep.m1[sInd],massSingt[sIndt],sRt.kep.m1[sIndt],'Kic Mass','Real Mass')
#plt.show()

#plotHist(sK.kicProp.teff[sInd],sR.kep.t1[sInd],sKt.kicProp.teff[sIndt],sRt.kep.t1[sIndt],'Kic Teff','Real Teff')
#plt.show()


#plotHist2(np.log10(sK.kicProp.teff[sInd]),sK.kicProp.logg[sInd],np.log10(sKt.kicProp.teff[sIndt]),sKt.kicProp.logg[sIndt],'KIC log teff','KIC log g',title='Single',ratio=True)
#plotHist2(np.log10(bK.kicProp.teff[bInd]),bK.kicProp.logg[bInd],np.log10(bKt.kicProp.teff[bIndt]),bKt.kicProp.logg[bIndt],'KIC log teff','KIC log g',title='Binary',ratio=True)


plotHist2(np.log10(sR.kep.t1[sInd]),slogg[sInd],np.log10(sRt.kep.t1[sIndt]),sloggt[sIndt],'KIC log t1','KIC log g',title='Single',ratio=True)
plotHist2(np.log10(bR.kep.t1[bInd]),blogg[bInd],np.log10(bRt.kep.t1[bIndt]),bloggt[bIndt],'KIC log t1','KIC log g',title='Binary',ratio=True)


plt.show()


#plotHist(massSing[sInd],sR.kep.m1[sInd],massSingt[sIndt],sRt.kep.m1[sIndt],'KIC Mass','Mass','m1.s.pre.post.png','m1.s.diff.png','Single')
#plotHist(sK.kicProp.teff[sInd],sR.kep.t1[sInd],sKt.kicProp.teff[sIndt],sRt.kep.t1[sIndt],'KIC Log Teff','Log Teff','t1.s.pre.post.png','t1.s.diff.png','Single',log=True)
#plotHist(sK.kicProp.logg[sInd],slogg1[sInd],sKt.kicProp.logg[sIndt],slogg1t[sIndt],'KIC Log g','Log g','logg1.s.pre.post.png','logg1.s.diff.png','Single')


#plotHist(massBin[bInd],bR.kep.m1[bInd],massBint[bIndt],bRt.kep.m1[bIndt],'KIC Log Mass','Log M1','m1.b.pre.post.png','m1.b.diff.png','Binaries',log=True)
#plotHist(massBin[bInd],bR.kep.m2[bInd],massBint[bIndt],bRt.kep.m2[bIndt],'KIC Log Mass','Log M2','m2.b.pre.post.png','m2.b.diff.png','Binaries',log=True)
#plotHist(massBin[bInd],mR[bInd],massBint[bIndt],mRt[bIndt],'Kic Mass','M1/M2','mR.pre.post.png','mR.b.diff.png','Binaries',ratio=True)

#plotHist(bK.kicProp.teff[bInd],bR.kep.t1[bInd],bKt.kicProp.teff[bIndt],bRt.kep.t1[bIndt],'KIC Log Teff','Log T1','t1.b.pre.post.png','t1.b.diff.png','Binaries',log=True)
#plotHist(bK.kicProp.teff[bInd],bR.kep.t2[bInd],bKt.kicProp.teff[bIndt],bRt.kep.t2[bIndt],'KIC Log Teff','Log T2','t2.b.pre.post.png','t2.b.diff.png','Binaries',log=True)
#plotHist(bK.kicProp.teff[bInd],tR[bInd],bKt.kicProp.teff[bIndt],tRt[bIndt],'Kic Teff','T1/T2','tR.pre.post.png','tR.b.diff.png','Binaries',ratio=True)

#plotHist(bK.kicProp.logg[bInd],blogg1[bInd],bKt.kicProp.logg[bIndt],blogg1t[bIndt],'KIC Log g','Log g1','logg1.b.pre.post.png','logg1.b.diff.png','Binaries')
#plotHist(bK.kicProp.logg[bInd],blogg2[bInd],bKt.kicProp.logg[bIndt],blogg2t[bIndt],'KIC Log g','Log g2','logg2.b.pre.post.png','logg2.b.diff.png','Binaries')


#############################################
#ind=(bR.kep.evol.endswith("B*B*'"))|(bR.kep.evol.endswith("BnB*'"))|(bR.kep.evol.endswith("BtB*'"))
#indt=(bRt.kep.evol.endswith('B*B*\'"'))|(bRt.kep.evol.endswith('BnB*\'"'))|(bRt.kep.evol.endswith('BtB*\'"'))


#def plotAllB(bInd,bIndt):
	#plotHist2(massBin[bInd],bR.kep.m1[bInd],massBint[bIndt],bRt.kep.m1[bIndt],'KIC Log Mass','Log M1',log=True)
	#plotHist2(massBin[bInd],bR.kep.m2[bInd],massBint[bIndt],bRt.kep.m2[bIndt],'KIC Log Mass','Log M2',log=True)
	#plotHist2(massBin[bInd],mR[bInd],massBint[bIndt],mRt[bIndt],'Kic Mass','M1/M2',ratio=True)
	#plotHist2(bK.kicProp.teff[bInd],bR.kep.t1[bInd],bKt.kicProp.teff[bIndt],bRt.kep.t1[bIndt],'KIC Log Teff','Log T1',log=True)
	#plotHist2(bK.kicProp.teff[bInd],bR.kep.t2[bInd],bKt.kicProp.teff[bIndt],bRt.kep.t2[bIndt],'KIC Log Teff','Log T2',log=True)
	#plotHist2(bK.kicProp.teff[bInd],tR[bInd],bKt.kicProp.teff[bIndt],tRt[bIndt],'Kic Teff','T1/T2',ratio=True)
	#plotHist2(bK.kicProp.logg[bInd],blogg1[bInd],bKt.kicProp.logg[bIndt],blogg1t[bIndt],'KIC Logg','Log g1')
	#plotHist2(bK.kicProp.logg[bInd],blogg2[bInd],bKt.kicProp.logg[bIndt],blogg2t[bIndt],'KIC Logg','Log g2')
	#plt.show()

	
#plotAllB(bInd&ind,bIndt&indt)

#ind=(sR.kep.evol.endswith("WDA*'"))
#indt=(sRt.kep.evol.endswith('WDA*\'"'))


#def plotAllS(bInd,bIndt):
	#plotHist2(massSing[sInd],sR.kep.m1[sInd],massSingt[sIndt],sRt.kep.m1[sIndt],'KIC Log Mass','Log M1',log=True)
	#plotHist2(sK.kicProp.teff[sInd],sR.kep.t1[sInd],sKt.kicProp.teff[sIndt],sRt.kep.t1[sIndt],'KIC Log Teff','Log T1',log=True)
	#plotHist2(sK.kicProp.logg[sInd],slogg1[sInd],sKt.kicProp.logg[sIndt],slogg1t[sIndt],'KIC Logg','Log g1')
	#plt.show()

	
#plotAllS(sInd&ind,sIndt&indt)



