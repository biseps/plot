import biseps as b
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc

rc('text', usetex=True)
rc('font', family='sans',size=14)

x=b.readFile()
#x.kepler3('extract.bin.targ.1')
x.kepler3('extract.bin')
x.ranLoc('ranLoc.bin')

evol=np.zeros(np.size(x.kep),dtype='S4')
for i in xrange(np.size(x.kep)):
	evol[i]=x.kep.evol[i][-3:-2]

indMag=(x.ranLoc.appMag<15.0)&(x.ranLoc.appMag>8.0)&(x.kep.t1<7500.0)&(x.kep.t2<7500.0)
indAll=np.zeros(np.size(evol),dtype=bool)
indAll[:]=True
ind1=('N'==evol)&indMag
ind2=('W'==evol)&indMag
ind3=('B'==evol)&indMag
ind4=('A'==evol)&indMag
ind5=('M'==evol)&indMag
ind6=('C'==evol)&indMag
ind7=('H'==evol)&indMag

plt.figure()
ind=np.zeros(np.size(evol),dtype=bool)
ind=False
#if np.any(ind1):
	#ind=ind|ind1
	#plt.scatter(np.log10(x.kep[ind1].t1),np.log10(x.kep[ind1].l1),label='NS',c='green',marker='o',alpha=0.5)
#if np.any(ind2):
	#ind=ind|ind2
	#plt.scatter(np.log10(x.kep[ind2].t1),np.log10(x.kep[ind2].l1),label='WD',c='red',marker='o',alpha=0.5)
if np.any(ind3):
	ind=ind|ind3
	plt.scatter(np.log10(x.kep[ind3].t1),np.log10(x.kep[ind3].l1),label='HG/GB',c='blue',marker='o',alpha=0.5)
#if np.any(ind4):
	#ind=ind|ind4
	#plt.scatter(np.log10(x.kep[ind4].t1),np.log10(x.kep[ind4].l1),label='MS',c='yellow',marker='o',alpha=0.5)
#if np.any(ind5):
	#ind=ind|ind5
	#plt.scatter(np.log10(x.kep[ind5].t1),np.log10(x.kep[ind5].l1),label='CHeB',c='orange',marker='o',alpha=0.5)
#if np.any(ind6):
	#ind=ind|ind6
	#plt.scatter(np.log10(x.kep[ind6].t1),np.log10(x.kep[ind6].l1),label='AGB',c='cyan',marker='o',alpha=0.5)
#if np.any(ind7):
	#ind=ind|ind7
	#plt.scatter(np.log10(x.kep[ind7].t2),np.log10(x.kep[ind7].l2),label='nHe',c='magenta',marker='o',alpha=0.5)

plt.xlabel(r'$\log T_{eff}$')
plt.ylabel(r'$\log L [L_{\odot}]$')
plt.legend(loc=0)
#plt.xlim(4.5,np.min(np.log10(x.kep[ind].t1)))
plt.xlim(np.max(np.log10(x.kep[ind].t1)),np.min(np.log10(x.kep[ind].t1)))
#plt.savefig('hr.sing.a.png')
plt.show()


ast=b.astroSeis()

a1=x.kep[ind3&ind3_3]


astRes1=ast.soldet(x.kep.t1,x.kep.r1,x.kep.m1,x.ranLoc.appMag)
astRes2=ast.soldet(x.kep.t2,x.kep.r2,x.kep.m2,x.ranLoc.appMag)


plt.figure()
plt.hexbin(np.log10(x.kep[in1].t1),np.log10(x.kep[in1].l1),mincnt=1)
plt.xlim(np.max(np.log10(x.kep[in1].t1)),np.min(np.log10(x.kep[in1].t1)))
plt.colorbar()
plt.show()


plt.figure()
in1=(astRes1>0.9)
plt.scatter(np.log10(x.kep[in1].t1),np.log10(x.kep[in1].l1),label='HG/GB',c='blue',marker='o',alpha=0.5)

plt.xlabel(r'$\log T_{eff}$')
plt.ylabel(r'$\log L [L_{\odot}]$')
plt.legend(loc=0)
#plt.xlim(4.5,np.min(np.log10(x.kep[ind].t1)))
plt.xlim(np.max(np.log10(x.kep[in1].t1)),np.min(np.log10(x.kep[in1].t1)))
#plt.savefig('hr.sing.a.png')
plt.show()




ind=ind3&ind3_3
plt.scatter(np.log10(x.kep[ind].t2),np.log10(x.kep[ind].l2),label='HG/GB',c='blue',marker='o',alpha=0.5)
plt.xlabel(r'$\log T_{eff}$')
plt.ylabel(r'$\log L [L_{\odot}]$')
plt.legend(loc=0)
#plt.xlim(4.5,np.min(np.log10(x.kep[ind].t1)))
plt.xlim(np.max(np.log10(x.kep[ind].t2)),np.min(np.log10(x.kep[ind].t2)))
#plt.savefig('hr.sing.a.png')
plt.show()



x=b.readFile()
x.kepler3('extract.bin.targAll.1')
#x.kepler('WEBwide_kepler.dat.1')

evol=np.zeros(np.size(x.kep),dtype='S4')
for i in xrange(np.size(x.kep)):
	evol[i]=x.kep.evol[i][-4:-3]

indAll=np.zeros(np.size(evol),dtype=bool)
indAll[:]=True
ind1=('N'==evol)
ind2=('W'==evol)
ind3=('B'==evol)
ind4=('A'==evol)
ind5=('M'==evol)
ind6=('C'==evol)
ind7=('H'==evol)

ind=np.zeros(np.size(evol),dtype=bool)
ind=False
if np.any(ind1):
	ind=ind|ind1
	plt.scatter(np.log10(x.kep[ind1].t1),np.log10(x.kep[ind1].l1),label='NS',c='green',marker='o',alpha=0.5)
if np.any(ind2):
	ind=ind|ind2
	plt.scatter(np.log10(x.kep[ind2].t1),np.log10(x.kep[ind2].l1),label='WD',c='red',marker='o',alpha=0.5)
if np.any(ind3):
	ind=ind|ind3
	plt.scatter(np.log10(x.kep[ind3].t1),np.log10(x.kep[ind3].l1),label='HG/GB',c='blue',marker='o',alpha=0.5)
if np.any(ind4):
	ind=ind|ind4
	plt.scatter(np.log10(x.kep[ind4].t1),np.log10(x.kep[ind4].l1),label='MS',c='yellow',marker='o',alpha=0.5)
if np.any(ind5):
	ind=ind|ind5
	plt.scatter(np.log10(x.kep[ind5].t1),np.log10(x.kep[ind5].l1),label='CHeB',c='orange',marker='o',alpha=0.5)
if np.any(ind6):
	ind=ind|ind6
	plt.scatter(np.log10(x.kep[ind6].t1),np.log10(x.kep[ind6].l1),label='AGB',c='cyan',marker='o',alpha=0.5)
if np.any(ind7):
	ind=ind|ind7
	plt.scatter(np.log10(x.kep[ind7].t1),np.log10(x.kep[ind7].l1),label='nHe',c='magenta',marker='o',alpha=0.5)

plt.xlabel(r'$\log T_{eff}$')
plt.ylabel(r'$L [L_{\odot}]$')
plt.title('BiSEPS binaries pre KIC')
plt.xlim(np.max(np.log10(x.kep[ind].t1)),np.min(np.log10(x.kep[ind].t1)))
plt.savefig('hr.bin.targAll.1.png')


###################################################################################

import biseps as b
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc

rc('text', usetex=True)
rc('font', family='serif')

x=b.readFile()
#x.kepler3('extract.bin.targ.1')
x.kepler('WEB_kepler.dat.1')

evol=np.zeros(np.size(x.kep),dtype='S4')
for i in xrange(np.size(x.kep)):
	evol[i]=x.kep.evol[i][-4:-3]

indAll=np.zeros(np.size(evol),dtype=bool)
indAll[:]=True
ind3=('B'==evol)

ind=np.zeros(np.size(evol),dtype=bool)
ind=False

if np.any(ind3):
	ind=ind|ind3
	plt.scatter(np.log10(x.kep[ind3].t1),np.log10(x.kep[ind3].l1),label='HG/GB',c='blue',marker='o',alpha=0.5)

x=b.readFile()
#x.kepler3('extract.bin.targ.1')
x.kepler('WEB_kepler.dat.2')

evol=np.zeros(np.size(x.kep),dtype='S4')
for i in xrange(np.size(x.kep)):
	evol[i]=x.kep.evol[i][-4:-3]

indAll=np.zeros(np.size(evol),dtype=bool)
indAll[:]=True
ind3=('B'==evol)

ind=np.zeros(np.size(evol),dtype=bool)
ind=False

if np.any(ind3):
	ind=ind|ind3
	plt.scatter(np.log10(x.kep[ind3].t1),np.log10(x.kep[ind3].l1),label='HG/GB',c='red',marker='D',alpha=1.0)



plt.xlabel(r'$\log T_{eff}$')
plt.ylabel(r'$L [L_{\odot}]$')
plt.title('BiSEPS binaries post KIC')
plt.xlim(np.max(np.log10(x.kep[ind].t1)),np.min(np.log10(x.kep[ind].t1)))
plt.show()

####################################################################

for i in range(1,np.int(x.kep[-1].id),10):
	y=(x.kep.id==i)
	plt.plot(np.log10(x.kep[y].t1),np.log10(x.kep[y].l1),c='blue')

plt.xlim(np.max(np.log10(x.kep.t1)),np.min(np.log10(x.kep.t1)))
plt.show()
############


plt.hexbin(np.log10(x.kep[ind3].t1),np.log10(x.kep[ind3].l1),mincnt=1)
plt.xlim(np.max(np.log10(x.kep[ind3].t1)),np.min(np.log10(x.kep[ind3].t1)))
plt.colorbar()
plt.show()


#############
ast=b.astroSeis()
astRes1=np.zeros(np.count_nonzero(ind3))

astRes1=ast.soldet(x.kep[ind3].t1,x.kep[ind3].r1,x.kep[ind3].m1,x.ranLoc[ind3].appMag)


teff,rad,mass,vkic
x.kep[(np.log10(x.kep.t1)>3.6)&(np.log10(x.kep.t1)<3.65)&(np.log10(x.kep.l1)>0.5)&(np.log10(x.kep.l1)<0.56)]

#########################################################################################


import biseps as b
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc
ast=b.astroSeis()
rc('text', usetex=True)
rc('font', family='sans',size=14)

x=b.readFile()
#x.kepler3('extract.bin.targ.1')
x.kepler3('extract.bin')
x.ranLoc('ranLoc.bin')

indMag=(x.ranLoc.appMag<15.0)&(x.ranLoc.appMag>8.0)&(x.kep.t1<7500.0)&(x.kep.t2<7500.0)

evol=np.zeros(np.size(x.kep),dtype='S4')
for i in xrange(np.size(x.kep)):
	evol[i]=x.kep.evol[i][-5:-4]
ind3=('B'==evol)&indMag

for i in xrange(np.size(x.kep)):
	evol[i]=x.kep.evol[i][-3:-2]
ind3_2=('B'==evol)&indMag

ind=ind3&ind3_2

plt.scatter(np.log10(x.kep[ind].t1),np.log10(x.kep[ind].l1),label='HG/GB 1',c='blue',marker='o',alpha=0.5)
plt.scatter(np.log10(x.kep[ind].t2),np.log10(x.kep[ind].l2),label='HG/GB 2',c='green',marker='o',alpha=0.5)


plt.xlabel(r'$\log T_{eff}$')
plt.ylabel(r'$\log L [L_{\odot}]$')
plt.legend(loc=0)
#plt.xlim(4.5,np.min(np.log10(x.kep[ind].t1)))
plt.xlim(np.max(np.log10(x.kep[ind].t1)),np.min(np.log10(x.kep[ind].t1)))
#plt.savefig('hr.sing.a.png')
plt.show()

a=x.kep[ind]
mag=x.ranLoc[ind].appMag

astRes1=ast.soldet(a.t1,a.r1,a.m1,mag)
astRes2=ast.soldet(a.t2,a.r2,a.m2,mag)

ind2=(astRes1>0.9)&(astRes2>0.9)

plt.scatter(np.log10(a[ind2].t1),np.log10(a[ind2].l1),label='HG/GB 1',c='blue',marker='o',alpha=0.5)
plt.scatter(np.log10(a[ind2].t2),np.log10(a[ind2].l2),label='HG/GB 2',c='green',marker='o',alpha=0.5)


plt.xlabel(r'$\log T_{eff}$')
plt.ylabel(r'$\log L [L_{\odot}]$')
plt.legend(loc=0)
#plt.xlim(4.5,np.min(np.log10(x.kep[ind].t1)))
plt.xlim(np.max(np.log10(a[ind2].t1)),np.min(np.log10(a[ind2].t1)))
#plt.savefig('hr.sing.a.png')
plt.show()

plt.hist(np.minimum(a[ind2].m1,a[ind2].m2)/np.maximum(a[ind2].m1,a[ind2].m2))
plt.show()


############################################


import biseps as b
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc
ast=b.astroSeis()
rc('text', usetex=True)
rc('font', family='sans',size=14)

x=b.readFile()
#x.kepler3('extract.bin.targ.1')
x.kepler3('extract.sing')
x.ranLoc('ranLoc.sing')

indMag=(x.ranLoc.appMag<15.0)&(x.ranLoc.appMag>8.0)&(x.kep.t1<7500.0)&(x.kep.t2<7500.0)

evol=np.zeros(np.size(x.kep),dtype='S4')
for i in xrange(np.size(x.kep)):
	evol[i]=x.kep.evol[i][-5:-4]
ind3=('B'==evol)&indMag

ind=ind3

plt.scatter(np.log10(x.kep[ind].t1),np.log10(x.kep[ind].l1),label='HG/GB',c='blue',marker='o',alpha=0.5)

plt.xlabel(r'$\log T_{eff}$')
plt.ylabel(r'$\log L [L_{\odot}]$')
plt.legend(loc=0)
#plt.xlim(4.5,np.min(np.log10(x.kep[ind].t1)))
plt.xlim(np.max(np.log10(x.kep[ind].t1)),np.min(np.log10(x.kep[ind].t1)))
#plt.savefig('hr.sing.a.png')
plt.show()

a=x.kep[ind]
mag=x.ranLoc[ind].appMag

astRes1=ast.soldet(a.t1,a.r1,a.m1,mag)

ind2=(astRes1>0.9)

plt.scatter(np.log10(a[ind2].t1),np.log10(a[ind2].l1),label='HG/GB 1',c='blue',marker='o',alpha=0.5)

plt.xlabel(r'$\log T_{eff}$')
plt.ylabel(r'$\log L [L_{\odot}]$')
plt.legend(loc=0)
#plt.xlim(4.5,np.min(np.log10(x.kep[ind].t1)))
plt.xlim(np.max(np.log10(a[ind2].t1)),np.min(np.log10(a[ind2].t1)))
#plt.savefig('hr.sing.a.png')
plt.show()
