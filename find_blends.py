# -*- coding: utf-8 -*-


# In[]:

import os, sys, time, shutil
import math

import constants as c


from astropy.coordinates import ICRS, Galactic
from astropy.coordinates import match_coordinates_sky
from astropy import units as u
from astropy.io import ascii
from astropy.table import vstack
from astropy.table import hstack
from astropy.table import Table
from astropy.table import join
import astropy.constants as const


import numpy as np
from random import randint

import matplotlib
import matplotlib.pyplot as plt

import plotUtils as utils
import getopt



# In[]:

# version  = '1'
# base_dir = 'input_data/ngts_06'
# task_id  = '1'
# output_dir = 'out_test'


# Get command line arguments
opts, extra = getopt.getopt(sys.argv[1:],'')

version     = str(extra[0])
base_dir    = str(extra[1])
task_id     = str(extra[2])
output_dir  = str(extra[3])

print 'version: ' + version
print 'base_dir: ' + base_dir
print 'task_id: ' + task_id
print 'output_dir: ' + output_dir


# In[]:


def porb2a(m1, m2, porb):
    """ Calculate semi-major axis

        Porb      is in years
        m1, m2    are in solar masses
        a         is in solar radii

    """

    n = 2.0 * np.pi / (porb/365.0)

    return (c.gn * (m1+m2) / n**2)**(1.0/3.0)




def get_file_list(base_dir, task_id, filename, version):
    """return a list of files 
       which was the output from a cluster job

    """
    file_list=[]
    star_types = ['pop_s_o', 'pop_s_y', 'pop_b_o', 'pop_b_y']

    for star_type in star_types:
        file_list.append(base_dir + '/' + star_type + '/' + task_id + '/' + filename + '.' + version)

    return file_list




def write_blend_info(filename, stars, binaries, eclipse):
    """Convert flux to magnitude.

    """


    fields=[]
    fields.append(stars['id_1'])
    fields.append(stars['mt1'])
    fields.append(stars['lum1'])
    fields.append(binaries['id_1'])
    fields.append(binaries['evol'])
    fields.append(binaries['mt1'])
    fields.append(binaries['mt2'])
    fields.append(stars['i'])
    fields.append(binaries['i'])
    fields.append(eclipse['delta_mag'])
    fields.append(binaries['Porb'])
    fields.append(eclipse['a'])
    fields.append(eclipse['transit_prob'])
    fields.append(binaries['reff1'])
    fields.append(binaries['reff2'])
    fields.append(binaries['lum1'])
    fields.append(binaries['lum2'])
    fields.append(eclipse['total_lum'])
    fields.append(eclipse['lum_in_transit1'])
    fields.append(eclipse['lum_in_transit2'])
    fields.append(eclipse['lum_ratio1'])
    fields.append(eclipse['lum_ratio2'])
    fields.append(eclipse['transit_depth_actual1'])
    fields.append(eclipse['transit_depth_actual2'])
    fields.append(eclipse['transit_depth_measured1'])
    fields.append(eclipse['transit_depth_measured2'])


    header=[]
    header.append('star_id')
    header.append('star mass')
    header.append('star luminosity')
    header.append('binary id')
    header.append('binary evol')
    header.append('binary mass1')
    header.append('binary mass2')
    header.append('star mag')
    header.append('binary mag')
    header.append('delta_mag')
    header.append('Period')
    header.append('a')
    header.append('transit probability')
    header.append('binary radius1')
    header.append('binary radius2')
    header.append('binary luminosity1')
    header.append('binary luminosity2')
    header.append('binary total lum')
    header.append('luminosity in transit1')
    header.append('luminosity in transit2')
    header.append('luminosity ratio transit1')
    header.append('luminosity ratio transit2')
    header.append('actual fractional transit depth 1')
    header.append('actual fractional transit depth 2')
    header.append('measured fractional transit depth 1')
    header.append('measured fractional transit depth 2')


    # create astropy table object
    data = Table(fields, names=header)

    # write out table object
    ascii.write(data, filename, delimiter=' ')





def fluxToMag_astropyhics_module(flux):
    a = -2.5/np.log(10.0)
    return a * np.log(flux)


def fluxToMag(flux, system="Relative"):
    """Convert flux to magnitude.

    """
    if system == "AB":
        mag = (-2.5) * np.log10(flux) + 8.926

    elif system == "Relative":
        mag = (-2.5) * np.log10(flux)

    else:
        mag=None

    return mag



def magToFlux(mag, system="Relative"):
    """Convert magnitude to flux.

    """
    if system == "AB":
        # For the AB magnitude system, a fixed 
        # zero-point-flux is used as a reference
        # instead of the star Vega.
        # Output flux is in Janskys.
        zero_point_flux = 3631 
        flux = (0.3981)**mag * zero_point_flux

    elif system == "Relative":
        flux = (10)**((-0.4) * mag) 

    else:
        flux=None

    return flux



def addMags(mag1, mag2):
    # Taken from: 
    # John Southward PDF
    expr1       = 10**((-0.4) * mag1)
    expr2       = 10**((-0.4) * mag2)
    total_mag   = (-2.5) * np.log10(expr1 + expr2) 
    return total_mag



# NGTS telescope on-sky pixel size
NGTS_plate_scale = 5.36 * u.arcsec;

# region where blend likely
# blend_region = NGTS_plate_scale * 2
blend_region = NGTS_plate_scale 



# In[]:

# Get magnitudes from "mag.dat" files

input_files = get_file_list(base_dir, task_id, 'mag.dat', version)

headers = ['id', 'long', 'lat', 'u', 'g', 'r', 'i', 'z', 'd51', 'jj', 'jk', 'jh', 'kp']

                                                           
mag_pop_s_o  = ascii.read(input_files[0], delimiter=' ', names=headers)
mag_pop_s_y  = ascii.read(input_files[1], delimiter=' ', names=headers)
mag_pop_b_o  = ascii.read(input_files[2], delimiter=' ', names=headers)
mag_pop_b_y  = ascii.read(input_files[3], delimiter=' ', names=headers)


# get other stellar/binary properties from "extract" files

input_files = get_file_list(base_dir, task_id, 'extract', version)

headers = ['id', 'num', 'mt1', 'reff1', 'teff1', 'lum1', 'mt2', 'reff2', 'teff2', 'lum2', 
        'Porb', 'birth', 'death', 'massTran', 'kw1', 'kw2', 'bkw', 'met', 'evol']

extract_pop_s_o  = ascii.read(input_files[0], delimiter=' ', names=headers)
extract_pop_s_y  = ascii.read(input_files[1], delimiter=' ', names=headers)
extract_pop_b_o  = ascii.read(input_files[2], delimiter=' ', names=headers)
extract_pop_b_y  = ascii.read(input_files[3], delimiter=' ', names=headers)


# combine young and old info
star_mags     = vstack([mag_pop_s_o, mag_pop_s_y])
binary_mags   = vstack([mag_pop_b_o, mag_pop_b_y])
star_props    = vstack([extract_pop_s_o, extract_pop_s_y])
binary_props  = vstack([extract_pop_b_o, extract_pop_b_y])


# Finally combine magnitudes and properties 
# into single tables for easier access
stars    = hstack([star_mags, star_props])
binaries = hstack([binary_mags, binary_props])



# In[]:


# convert coordinates into astropy units

star_coords     = Galactic(stars['long'], 
                           stars['lat'], 
                           unit=(u.degree, u.degree))

binary_coords   = Galactic(binaries['long'], 
                          binaries['lat'], 
                          unit=(u.degree, u.degree))



# In[]:

# get 4 closest binaries

# closest_index=[]
# dists_2d=[]
# dists_3d=[]

# for i in range(1,5):
    # idx, 2d_distance, d3d = match_coordinates_sky(star_coords, binary_coords, nthneighbor=i, storekdtree=True)

    # closest_index.append(idx)
    # dists_2d.append(2d_distance)
    # dists_3d.append(d3d)


# get nearest neighbour for each star
nearest_idx, dist_2d, dist_3d = match_coordinates_sky(star_coords, 
                                                      binary_coords, 
                                                      nthneighbor=1, 
                                                      storekdtree=True)


# In[]:

# matches = binary_coords[nearest_idx]

# dist_l = (matches.l - star_coords.l).arcmin
# dist_b = (matches.b - star_coords.b).arcmin


# create array of closest binaries
# close_binary[0] = 1st closest binary array
# close_binary[1] = 2st closest binary array
# etc etc..

# close_binary=[]

# for i in range(0,4):
    # close_binary.append(binary_coords[nearest_idx[i]])




# re-index the "binaries" array 
# into "nearest neighbour" order
closest_binary        = binaries[nearest_idx]



# Match all binaries that fall INSIDE 
# the blend region which means they are 
# close to a foreground star
mask = (dist_2d.arcsecond < blend_region.value)

filter_stars     = stars[mask]
filter_binaries  = closest_binary[mask]



# In[]:

# add magnitudes
# mag_combined  = addMags(filter_binaries['i'].data, filter_stars['i'].data)

# delta_mag     = filter_binaries['i'].data - filter_stars['i'].data

# star_flux     = magToFlux(filter_stars['i'].data)
# binary_flux   = magToFlux(filter_binaries['i'].data)
# delta_mag2    = fluxToMag(binary_flux / star_flux)



# Discard binaries that are greater than
# 10 magnitudes fainter...they wont create a blend
delta_mag = filter_binaries['i'].data - filter_stars['i'].data

mask              = (delta_mag < 10)

filter_stars      = filter_stars[mask]
filter_binaries   = filter_binaries[mask]
delta_mag         = delta_mag[mask]



# In[]:

# Convert Period into semi-major axis
a  = porb2a(filter_binaries['mt1'].data, 
            filter_binaries['mt2'].data, 
            filter_binaries['Porb'].data) 




# In[]:


# geometric probability of transit
radius1 = filter_binaries['reff1'].data
radius2 = filter_binaries['reff2'].data

transit_prob = (radius1 + radius2) / a




# In[]:

# key step:
# work out fractional transit depths 
# ( both actual and measured)

lum1 = filter_binaries['lum1'].data
lum2 = filter_binaries['lum2'].data

total_lum = lum1 + lum2

# transit 1:
# binary 2 is eclipsing binary 1

# transit 2:
# binary 1 is eclipsing binary 2

# luminosity in primary transit   = L1(1 - ∆f/f) + L2
# luminosity in secondary transit = L2(1 - ∆f/f) + L1

# ∆f/f is proportional to radius2/radius1.
# If the (ratio of radii) < 0 then the star in question is occulted
# in which case set ∆f/f=1. This will result in the luminosity
# of the occulted star being zero. Crude!

lum_in_transit1 = lum1*(1 - np.minimum(1, (radius2/radius1)**2)) + lum2
lum_in_transit2 = lum2*(1 - np.minimum(1, (radius1/radius2)**2)) + lum1


# Calculations as per R. Farmer Report 2011

lum_ratio1 = lum_in_transit1 / total_lum
lum_ratio2 = lum_in_transit2 / total_lum


# ACTUAL fractional transit depth
transit_depth_actual1 = 1 - lum_ratio1
transit_depth_actual2 = 1 - lum_ratio2


# MEASURED fractional transit depth
transit_depth_measured1 = transit_depth_actual1 / (1 + 10**(delta_mag/2.5))
transit_depth_measured2 = transit_depth_actual2 / (1 + 10**(delta_mag/2.5))



# In[]:

fields=[]
fields.append(delta_mag)
fields.append(a)
fields.append(transit_prob)
fields.append(total_lum)
fields.append(lum_in_transit1)
fields.append(lum_in_transit2)
fields.append(lum_ratio1)
fields.append(lum_ratio2)
fields.append(transit_depth_actual1)
fields.append(transit_depth_actual2)
fields.append(transit_depth_measured1)
fields.append(transit_depth_measured2)

header=[]
header.append('delta_mag')
header.append('a')
header.append('transit_prob')
header.append('total_lum')
header.append('lum_in_transit1')
header.append('lum_in_transit2')
header.append('lum_ratio1')
header.append('lum_ratio2')
header.append('transit_depth_actual1')
header.append('transit_depth_actual2')
header.append('transit_depth_measured1')
header.append('transit_depth_measured2')


eclipse_info = Table(fields, names=header)

# In[]:

# only select those blends with a transit 
# depth that would mimic a planetary transit 
mask1 = (transit_depth_measured1 > 0.001) & (transit_depth_measured1 < 0.01)
mask2 = (transit_depth_measured2 > 0.001) & (transit_depth_measured2 < 0.01)
total_mask = mask1 | mask2

blend_stars         = filter_stars[total_mask]
blend_binaries      = filter_binaries[total_mask]
blend_eclipses      = eclipse_info[total_mask]


# In[]:

# create output folder to hold blend files
utils.make_folder(output_dir)




# In[]:


# create output folder to hold blend files
utils.make_folder(output_dir)

# write out blend files
fileout = os.path.join(output_dir, 'all_blends.csv')
write_blend_info(fileout, filter_stars,  filter_binaries, eclipse_info)

fileout = os.path.join(output_dir, 'planet_like.csv')
write_blend_info(fileout, blend_stars,  blend_binaries,  blend_eclipses)


# write out totals file
headers = ['total_stars','total_binaries', 'potential_blends', 'planet_like_blends']
totals  = [len(stars), len(binaries), len(filter_stars), len(blend_stars)]
data    = np.array(totals).reshape(-1, 4)

fileout = os.path.join(output_dir, 'totals.csv')
np.savetxt(fileout, data, fmt='%d', delimiter="," )


# txt_headers= ', '.join(map(str, headers))
# np.savetxt(fileout, data, fmt='%d', header=txt_headers, delimiter="," )


# In[]:

