import fortUtil as f
import numpy as np
import biseps as b
import kepprf as kep
import constants as c

x=b.readFile()
ccd=1
x.kicProp4('kic.dat.1')
#prfAll=np.load(ffiDir+str(ccd)+'.2a.npy')
row,col=f.lonlat2pix(x.kicProp.long,x.kicProp.lat,np.size(x.kicProp.long),1,c.season,c.fovDir)

prfAll=np.zeros([c.numRows,c.numCols],dtype='float')
prfAll=prfAll+kep.prfImage(row,col,x.kicProp.kp,1)
np.save("1.npy",prfAll)
