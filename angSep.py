import biseps as b
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as m
import constants as c

#Assume we have catted all the extracts and target files together
x=b.readFile()
e=b.extras()

x.kepler3('extract.bin')

#x.kicProp('kic.bin')
x.ranLoc('ranLoc.bin')

semi=e.porb2a(x.kep.m1,x.kep.m2,x.kep.p)*c.rsun/c.au

ind=(x.ranLoc.appMag>0.0) & (x.ranLoc.appMag<16.0) & (x.kep.p > 0.0*365.0)

i=np.arange(0.0,np.pi/2.0,0.0001)



plt.scatter(semi[ind],x.ranLoc[ind].appMag)
plt.show()