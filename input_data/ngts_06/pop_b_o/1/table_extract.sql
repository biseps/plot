--  E Farrell 2014


.print
.print 'input File: extract'
.print 'creating table ...'

CREATE TABLE IF NOT EXISTS extract
(
    id          integer primary key, 
    num         real, 
    mt1         real, 
    reff1       real, 
    teff1       real, 
    lum1        real, 
    mt2         real, 
    reff2       real, 
    teff2       real, 
    lum2        real, 
    Porb        real, 
    birth       real, 
    death       real, 
    massTran    int, 
    kw1         int, 
    kw2         int, 
    bkw         int, 
    met         real, 
    evol        text 
);

--  kepler 6
--  num
--  m1
--  r1
--  t1
--  l1
--  m2
--  r2
--  t2
--  l2
--  p
--  ecc
--  birth
--  death
--  massTran
--  kw1
--  kw2
--  bw
--  met
--  evol


.print 'importing data...'
begin transaction;

.separator " "
.import extract.2 extract

commit;
.print 'data imported'


.print 'Number of records :'
select count(*) from extract;


