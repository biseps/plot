import numpy as np
import sys as sys

def getPix(lon,lat,fileNum,fold):
	fovdir='fov/'
	mydescr = np.dtype([('inRa', 'float'), ('inDec', 'float'),
	('inLon', 'float'), ('inLat', 'float'), ('inRow', 'int'), ('inCol', 'int')])
	kep= read_array(filename=fovdir+str(fold)+str(fileNum)+'.ra', dtype=mydescr,size=6)
	
	if np.size(lon) != np.size(lat):
		print "Bad array sizes"
		sys.exit()
	
	ra,dec=convert(lon,lat)
	
	row,col=lonLat2pix(ra,dec,kep.inRa,kep.inDec,kep.inRow,kep.inCol)
		
	return row,col

def lonLat2pix(ra,dec,inRa,inDec,inRow,inCol):
	
	ind=np.zeros(np.size(ra),dtype='int')
	for i in xrange(np.size(ra)):
		ind[i]=np.sqrt((np.abs(inRa-ra[i])/15.0/np.cos(inDec*np.pi/180))**2+np.abs(inDec-dec[i])**2).argmin()
		
	cd1_1 = 0.000702794927969
	cd1_2 = -0.000853190160515
	cd2_1 = -0.000853190160515
	cd2_2 = -0.000702794927969
	cd = np.array([[cd1_1,cd1_2],[cd2_1,cd2_2]])
	cd = np.linalg.inv(cd)
	#cd=np.array([[575.18724454,-698.27495613],[-698.27495613 -575.18724454]])

	dra = inRa[ind] - ra
	ddec =inDec[ind] - dec
	
	drow = cd[0,0] * dra + cd[0,1] * ddec
	dcol = cd[1,0] * dra + cd[1,1] * ddec
	
	# pixel coordinates of the nearest KIC target

	row = inRow[ind]
	column =inCol[ind]
	
	# pixel coordinate of target
	row = np.rint(row + drow + 0.5)
	column = np.rint(column + dcol + 0.5)
		
	return row,column
		
		
def read_array(filename,dtype,size,sep=None):
	""" Read a file with an arbitrary number of columns.
	The type of data in each column is arbitrary
	It will be cast to the given dtype at runtime
	"""
	cast = np.cast
	data = [[] for dummy in xrange(size)]
	with open(filename) as f:
		for line in f:
			fields = line.strip().split(sep)
			for i, number in enumerate(fields):
				#if len(number) == 0 or number=='\n':
					#number=0
				data[i].append(number)
	for i in xrange(size):
		data[i] = cast[dtype[i]](data[i])
	return np.rec.array(data, dtype=dtype)

def convert(lon,lat):
	
	deg2rad=np.pi/180.0
	lon=lon*deg2rad
	lat=lat*deg2rad
	
	clat=np.cos(lat)
	slat=np.sin(lat)
	
	an=32.93192*deg2rad #/* G lng of asc node on equator */
	gpr = 192.8594*deg2rad #  /* RA of North Gal Pole, 2000 */
	gpd = 27.12825*deg2rad #/* Dec of  " */
	
	cgpd=np.cos(gpd)
	sgpd=np.sin(gpd)
	
	a=lon-an
	
	ca=np.cos(a)
	sa=np.sin(a)
	b=sa
	
	sq=(clat*cgpd*b)+(slat*sgpd)
	dec=np.arcsin(sq)
	
	c=clat*ca
	d=(slat*cgpd)-(clat*sgpd*sa)
	
	ind=(np.abs(d) < 1e-20)
	d[ind]=1e-20

	ra=np.arctan(c/d)+gpr
	
	ind=(d<0)
	ra[ind]=ra[ind]+np.pi
	ind=(ra<0)
	ra[ind]=ra[ind]+2*np.pi
	ind=(a > 2*np.pi)
	ra[ind]=ra[ind]-2*np.pi
	
	return ra/deg2rad,dec/deg2rad