
#http://www.stsci.edu/instruments/wfpc2/Wfpc2_hand_current/ch6_exposuretime5.html
from astropysics.coords import EclipticCoordinatesEquinox as e
from astropysics.coords import GalacticCoordinates as g
import numpy as np

l=np.arange(180.0,-1.0,-15.0)
b=np.arange(0.0,90.1,15.0)

colCorrect=0.88

zod=np.array([
	[22.1,22.4,22.7,23.0,23.2,23.4,23.3],
	[22.3,22.5,22.8,23.0,23.2,23.4,23.3],
	[22.4,22.6,22.9,23.1,23.3,23.4,23.3],
	[22.4,22.6,22.9,23.2,23.3,23.4,23.3],
	[22.4,22.6,22.9,23.2,23.3,23.3,23.3],
	[22.2,22.5,22.9,23.1,23.3,23.3,23.3],
	[22.0,22.3,22.7,23.0,23.2,23.3,23.3],
	[21.7,22.2,22.6,22.9,23.1,23.2,23.3],
	[21.3,21.9,22.4,22.7,23.0,23.2,23.3],
	[100.0,100.0,22.1,22.5,22.9,23.1,23.3],
	[100.0,100.0,100.0,22.3,22.7,23.1,23.3],
	[100.0,100.0,100.0,100.0,22.6,23.0,23.3],
	[100.0,100.0,100.0,100.0,22.6,23.0,23.3]
])

zod=10**(zod/(-2.5))

lo=np.zeros(np.shape(zod))
la=np.zeros(np.shape(zod))

for i in range(np.size(l)):
	for j in range(np.size(b)):
		a=e(l[i],b[j]).convert(g)
		lo[i,j]=a.l.degrees
		la[i,j]=a.b.degrees
 

def zodVal(x):
	return -2.5*np.log10(((10**(x/-2.5))*4.0))
