# -*- coding: utf-8 -*-
"""
Created on Sat Nov 19 14:55:24 2011

@author: Robert Farmer
@email:r.farmer@open.ac.uk
"""

import numpy as np
#import matplotlib as m
#import matplotlib.pyplot as plt
import os, errno
#from matplotlib import rc
import constants as c
import scipy as sci
import scipy.stats as stats

#rc('text', usetex=False)
#rc('font', family='serif')

#Directorys
#Path to python binary
pythonBin="/csoft/epd-7.2-64/bin/python"
sourceDir="/padata/beta/users/rfarmer/code/source"
pyDir="/padata/beta/users/rfarmer/code/plot"
scriptsDir="/padata/beta/users/rfarmer/code/scripts"

class bfuncs:
	def __init__(self):
		self.g=6.67384*10**(-11)
		self.rsun=6.995*10**8
		self.msun=1.9891*10**30
		self.au=149.6*10**9
		self.teffsun=5777.0
		self.year=365*24*60*60.0
		self.gn=self.g*5.86676*10**(18)
		self.rearth=6378.1*10**3
	
	def rlobe(self,a,m1,m2):
		"""
		! Function to evaluate the radius of the Roche lobe
		
		! m1 and m2 are expressed in solar masses
		! a and rlobe are expressed in solar radii
		"""
		q13=(m1/m2)**(1.0/3.0)
		fq=(0.49*q13**2)/(0.6*q13**2+np.log(1.0+q13))
		return a*fq
		
	def porb2a(self,m1,m2,porb):

		n=2.0*np.pi/(porb/365.0)

		return (c.gn*(m1+m2)/n**2)**(1.0/3.0)
		
	def radlogg2mass(self,rad,logg):
		return (((rad*c.rsun)**2)*10**logg)/(100.0*c.g*c.msun)

	def massrad2logg(self,mass,rad):
		return np.log10((100.0*c.g*c.msun*mass)/((rad*c.rsun)**2))

	def classify(self,evol,d1,d2,e1,e2):
		"""
		Classifies systems into

		-1 Unclassifed
		0 Detatched
		1 Semi detached
		2 Ellipsodial
		3 Contact
		"""
		binType=np.zeros(np.size(evol),dtype="int")
		evol=evol.replace("'","")

		binType[:]=-1
		binType[(d1>0.0)|(d2>0.0)]=0
		s1=np.array([x[-3] for x in evol],dtype="a1")
		s2=np.array([x[-1] for x in evol],dtype="a1")

		binType[(s1=="n")|(s1=="t")|(s2=="n")|(s2=="t")]=1
		binType[(s1=="o")|(s2=="o")]=3

		binType[((d1==0.0)&(d2==0.0))&(e1>0.0)&(e2>0.0)]=2
		
		return binType

	def filloutFact(self,m1,m2):
		pass

	def getType(self,sType):
		#Given a evolutionary string "A*A*B*A*" etc calc the stars number (label array in output.f90)

		sTypeOut=np.zeros([np.size(sType),2],dtype="int")
		sType=sType.replace("'","")
	
		s1=np.array([x[-4] for x in sType],dtype="a1")
		s2=np.array([x[-2] for x in sType],dtype="a1")
	
		sTypeOut[(s1=="A"),0]=1
		sTypeOut[(s1=="B"),0]=3
		sTypeOut[(s1=="M"),0]=4
		sTypeOut[(s1=="C"),0]=5
		sTypeOut[(s1=="H"),0]=7
		sTypeOut[(s1=="W"),0]=10
		sTypeOut[(s1=="N"),0]=13
		
		sTypeOut[(s2=="A"),1]=1
		sTypeOut[(s2=="B"),1]=3
		sTypeOut[(s2=="M"),1]=4
		sTypeOut[(s2=="C"),1]=5
		sTypeOut[(s2=="H"),1]=7
		sTypeOut[(s2=="W"),1]=10
		sTypeOut[(s2=="N"),1]=13
	
		return sTypeOut.astype("int")

class readFile(object):
	def __init__(self):
		self.bfun=bfuncs()
	
	def read_array(self,filename,dtype,size=0,sep=None):
		""" Read a file with an arbitrary number of columns.
		The type of data in each column is arbitrary
		It will be cast to the given dtype at runtime
		"""
		cast = np.cast
		size=len(dtype)
		data = [[] for dummy in xrange(size)]
		with open(filename,'r') as f:
			for line in f:
				fields = line.strip().split(sep)
				for i, number in enumerate(fields):
					if len(number) == 0 or number=='\n' or number=="#":
						number=0
					data[i].append(number)
		for i in xrange(size):
			data[i] = cast[dtype[i]](data[i])
		return np.rec.array(data, dtype=dtype)
		
	def write_array(self,filename,data,fileMode="w"):
		with open(filename,fileMode) as f:
			for i in xrange(np.size(data)):
				f.write(' '.join([str(item) for item in data[i]])+"\n")
		f.close()

	def printSingleLine(self,file,data,fileMode="w"):
		with open(file,fileMode) as f:
			f.write(' '.join([str(item) for item in data])+"\n")
		
	def mag(self,file):
		"""
		reads _mag.dat files returns array id,mag1,mag2,magBin
		"""
		mydescr = np.dtype([('id', 'float'), ('mag1', 'float'), ('mag2', 'float'), ('magBin', 'float')])
		
		self.mag = self.read_array(filename=file, dtype=mydescr,size=4)
		
	def kepler(self,file):
		"""
		reads _kepler.dat files returns array id,m1,r1,t2,l1,m2,r2,t2,l2,p
		birth,death,massTrans,spec1,spec2,evol
		"""
		mydescr = np.dtype([('id','float'),('m1','float'),('r1','float'),('t1','float'),('l1','float'),
		('m2','float'),('r2','float'),('t2','float'),('l2','float'),('p','float'),
            ('birth','float'),('death','float'),('massTran','int'),('spec1','a10'),('spec2','a10'),('evol','a100')])
		self.kep = self.read_array(filename=file, dtype=mydescr,size=16)
		
	def kepler2(self,file):
		mydescr = np.dtype([('id','float'),('m1','float'),('r1','float'),('t1','float'),('l1','float'),
		('m2','float'),('r2','float'),('t2','float'),('l2','float'),('p','float'),
            ('birth','float'),('death','float'),('massTran','int')])
		self.kep = self.read_array(filename=file, dtype=mydescr,size=13)

	def kepler3(self,file):
		mydescr = np.dtype([('id','int'),('num','float'),('m1','float'),('r1','float'),('t1','float'),('l1','float'),
		('m2','float'),('r2','float'),('t2','float'),('l2','float'),('p','float'),
            ('birth','float'),('death','float'),('massTran','int'),('spec1','a10'),('spec2','a10'),('evol','a100')])
		self.kep = self.read_array(filename=file, dtype=mydescr,size=17)

		sType=self.bfun.getType(self.kep.evol)
		self.kep.kw1=sType[:,0]
		self.kep.kw2=sType[:,1]

		self.kep.tr=np.zeros(np.size(self.kep.m1))	
		self.kep.tr[self.kep.m1>=self.kep.m2]=self.kep.t2[self.kep.m1>=self.kep.m2]/self.kep.t1[self.kep.m1>=self.kep.m2]
		self.kep.tr[self.kep.m1<self.kep.m2]=self.kep.t1[self.kep.m1<self.kep.m2]/self.kep.t2[self.kep.m1<self.kep.m2]

		self.kep.mr=np.zeros(np.size(self.kep.m1))
		self.kep.mr[self.kep.m1>=self.kep.m2]=self.kep.t2[self.kep.m1>=self.kep.m2]/self.kep.t1[self.kep.m1>=self.kep.m2]
		self.kep.mr[self.kep.m1<self.kep.m2]=self.kep.t1[self.kep.m1<self.kep.m2]/self.kep.t2[self.kep.m1<self.kep.m2]

		self.kep.a=self.bfun.porb2a(self.kep.m1,self.kep.m2,self.kep.p)
		self.kep.r1r2=(self.kep.r1+self.kep.r2)/self.kep.a

	def kepler4(self,file):
		mydescr = np.dtype([('num','float'),('m1','float'),('r1','float'),('t1','float'),('l1','float'),
		('m2','float'),('r2','float'),('t2','float'),('l2','float'),('p','float'),
            ('birth','float'),('death','float'),('massTran','int'),('kw1','int'),('kw2','int'),('bw','int'),('met','float'),('evol','a100')])
		self.kep = self.read_array(filename=file, dtype=mydescr,size=19)

		self.kep.tr=np.zeros(np.size(self.kep.m1))	
		self.kep.tr[self.kep.m1>=self.kep.m2]=self.kep.t2[self.kep.m1>=self.kep.m2]/self.kep.t1[self.kep.m1>=self.kep.m2]
		self.kep.tr[self.kep.m1<self.kep.m2]=self.kep.t1[self.kep.m1<self.kep.m2]/self.kep.t2[self.kep.m1<self.kep.m2]

		self.kep.mr=np.zeros(np.size(self.kep.m1))
		self.kep.mr[self.kep.m1>=self.kep.m2]=self.kep.t2[self.kep.m1>=self.kep.m2]/self.kep.t1[self.kep.m1>=self.kep.m2]
		self.kep.mr[self.kep.m1<self.kep.m2]=self.kep.t1[self.kep.m1<self.kep.m2]/self.kep.t2[self.kep.m1<self.kep.m2]

		self.kep.a=self.bfun.porb2a(self.kep.m1,self.kep.m2,self.kep.p)

		self.kep.logg1=self.bfun.massrad2logg(self.kep.m1,self.kep.r1)
		self.kep.logg2=self.bfun.massrad2logg(self.kep.m2,self.kep.r2)
		self.kep.r1r2=(self.kep.r1+self.kep.r2)/self.kep.a

	def kepler5(self,file):
		mydescr = np.dtype([('id','int64'),('num','float'),('m1','float'),('r1','float'),('t1','float'),('l1','float'),
		('m2','float'),('r2','float'),('t2','float'),('l2','float'),('p','float'),
            ('birth','float'),('death','float'),('massTran','int'),('kw1','int'),('kw2','int'),('bw','int'),('met','float'),('evol','a100')])
		self.kep = self.read_array(filename=file, dtype=mydescr,size=19)

		#self.kep.tr=np.zeros(np.size(self.kep.m1))
		self.kep.tr=np.minimum(self.kep.t1,self.kep.t2)/np.maximum(self.kep.t1,self.kep.t2)
		self.kep.mr=np.minimum(self.kep.m1,self.kep.m2)/np.maximum(self.kep.m1,self.kep.m2)
		
		self.kep.a=self.bfun.porb2a(self.kep.m1,self.kep.m2,self.kep.p)
		self.kep.logg1=np.zeros(np.size(self.kep.m1))
		self.kep.logg2=np.zeros(np.size(self.kep.m1))
		self.kep.logg1=self.bfun.massrad2logg(self.kep.m1,self.kep.r1)
		self.kep.logg2=self.bfun.massrad2logg(self.kep.m2,self.kep.r2)

		self.kep.r1r2=(self.kep.r1+self.kep.r2)/self.kep.a

	def kepler6(self,file):
		mydescr = np.dtype([('num','float'),('m1','float'),('r1','float'),('t1','float'),('l1','float'),
		('m2','float'),('r2','float'),('t2','float'),('l2','float'),('p','float'),('ecc','float'),
            ('birth','float'),('death','float'),('massTran','int'),('kw1','int'),('kw2','int'),('bw','int'),('met','float'),('evol','a100')])
		self.kep = self.read_array(filename=file, dtype=mydescr)

		self.kep.tr=np.zeros(np.size(self.kep.m1))	
		self.kep.tr[self.kep.m1>=self.kep.m2]=self.kep.t2[self.kep.m1>=self.kep.m2]/self.kep.t1[self.kep.m1>=self.kep.m2]
		self.kep.tr[self.kep.m1<self.kep.m2]=self.kep.t1[self.kep.m1<self.kep.m2]/self.kep.t2[self.kep.m1<self.kep.m2]

		self.kep.mr=np.zeros(np.size(self.kep.m1))
		self.kep.mr[self.kep.m1>=self.kep.m2]=self.kep.t2[self.kep.m1>=self.kep.m2]/self.kep.t1[self.kep.m1>=self.kep.m2]
		self.kep.mr[self.kep.m1<self.kep.m2]=self.kep.t1[self.kep.m1<self.kep.m2]/self.kep.t2[self.kep.m1<self.kep.m2]

		self.kep.a=self.bfun.porb2a(self.kep.m1,self.kep.m2,self.kep.p)

		self.kep.logg1=self.bfun.massrad2logg(self.kep.m1,self.kep.r1)
		self.kep.logg2=self.bfun.massrad2logg(self.kep.m2,self.kep.r2)
		self.kep.r1r2=(self.kep.r1+self.kep.r2)/self.kep.a

	def kepler7(self,file):
		mydescr = np.dtype([('id','int64'),('num','float'),('m1','float'),('r1','float'),('t1','float'),('l1','float'),
		('m2','float'),('r2','float'),('t2','float'),('l2','float'),('p','float'),('ecc','float'),
            ('birth','float'),('death','float'),('massTran','int'),('kw1','int'),('kw2','int'),('bw','int'),('met','float'),('evol','a100')])
		self.kep = self.read_array(filename=file, dtype=mydescr)

		#self.kep.tr=np.zeros(np.size(self.kep.m1))
		self.kep.tr=np.minimum(self.kep.t1,self.kep.t2)/np.maximum(self.kep.t1,self.kep.t2)
		self.kep.mr=np.minimum(self.kep.m1,self.kep.m2)/np.maximum(self.kep.m1,self.kep.m2)
		
		self.kep.a=self.bfun.porb2a(self.kep.m1,self.kep.m2,self.kep.p)
		self.kep.logg1=np.zeros(np.size(self.kep.m1))
		self.kep.logg2=np.zeros(np.size(self.kep.m1))
		self.kep.logg1=self.bfun.massrad2logg(self.kep.m1,self.kep.r1)
		self.kep.logg2=self.bfun.massrad2logg(self.kep.m2,self.kep.r2)

		self.kep.r1r2=(self.kep.r1+self.kep.r2)/self.kep.a
		
	def weights(self,file):
		mydescr = np.dtype([('id','int64'),('num','float'),('weight','float')])
		self.weights = self.read_array(filename=file, dtype=mydescr,size=3)
		
	def kepGridTot(self,filename):
		names=('num')
		formats=('float')
		self.kepGridTot=np.loadtxt(filename,
                    dtype={'names':names,
                           'formats':formats
                           }
                   )
				   
	def kepGrid(self,file):
		mydescr = np.dtype([('x','float'),('y','float')])
		data = self.read_array(filename=file, dtype=mydescr,size=2)
		
		self.kepGrid=np.zeros((84,5,2))
		#Remove the 3rd element in every block of 5
		a=0
		z=[0,1,3,4]
		for x in range(0,419,5):
			tmp=np.zeros((4,2))
			for i,j in enumerate(z):
				tmp[i,0]=data.x[(a*5)+j]
				tmp[i,1]=data.y[(a*5)+j]

			self.kepGrid[a,0,0]=tmp[np.argmin(tmp[:,0]),0]
			self.kepGrid[a,0,1]=tmp[np.argmin(tmp[:,0]),1]

			self.kepGrid[a,1,0]=tmp[np.argmin(tmp[:,1]),0]
			self.kepGrid[a,1,1]=tmp[np.argmin(tmp[:,1]),1]

			self.kepGrid[a,2,0]=tmp[np.argmax(tmp[:,0]),0]
			self.kepGrid[a,2,1]=tmp[np.argmax(tmp[:,0]),1]

			self.kepGrid[a,3,0]=tmp[np.argmax(tmp[:,1]),0]
			self.kepGrid[a,3,1]=tmp[np.argmax(tmp[:,1]),1]

			self.kepGrid[a,4,0]=tmp[np.argmin(tmp[:,0]),0]
			self.kepGrid[a,4,1]=tmp[np.argmin(tmp[:,0]),1]
			a=a+1
			
	def ranLoc(self,file):
		mydescr = np.dtype([('id','int64'),('mag','float'),('long','float'),('lat','float'),('dist','float'),
		('ext','float'),('appMag','float')])
		self.ranLoc = self.read_array(filename=file, dtype=mydescr,size=7)
		
	def kic(self,file):
		mydescr = np.dtype([('long','float'),('lat','float'),('g','float'),('r','float'),
		('i','float'),('z','float'),('j','float'),('h','float'),('k','float'),
		('kp','float'),('teff','float'),('logg','float'),('feh','float'),('eb_v','float'),('av','float'),('rad','float')])
		self.kic = self.read_array(filename=file, dtype=mydescr,size=16)
		
	def kic2(self,file):
		mydescr = np.dtype([('long','float'),('lat','float'),('kp','float'),('eb_v','float'),('av','float')])
		self.kic2 = self.read_array(filename=file, dtype=mydescr,size=5,sep=",")
		
	def kic3(self,file):
		mydescr = np.dtype([('long','float64'),('lat','float64'),('kp','float64')])
		self.kic3 = self.read_array(filename=file, dtype=mydescr,size=3)
		
	def kic4(self,file):
		mydescr = np.dtype([('long','float64'),('lat','float64'),('kp','float64'),('av','float')])
		self.kic4 = self.read_array(filename=file, dtype=mydescr,size=4)
		
	def kic5(self,file,sep=None):
		mydescr = np.dtype([('long','float'),('lat','float'),('kp','float'),('av','float'),('teff','float'),
		('logg','float'),('logz','float'),('rad','float'),('mass','float')])
		self.kicProp = self.read_array(filename=file, dtype=mydescr,size=9,sep=sep)
		
	def kicProp(self,file):
		mydescr = np.dtype([('id','float'),('teff','float'),('tefferr','float'),('logz','float'),('logzerr','float'),
		('logg','float'),('loggerr','float'),('ext','float'),('exterr','float'),('rad','float'),
		('kp','float'),('chi','float')])
		self.kicProp = self.read_array(filename=file, dtype=mydescr,size=12)
		self.kicProp.mass=self.bfun.radlogg2mass(self.kicProp.rad,self.kicProp.logg)
	
	def kicProp2(self,file):
		mydescr = np.dtype([('teff','float'),('logz','float'),
		('logg','float'),('rad','float'),('kp','float')])
		self.kicProp = self.read_array(filename=file, dtype=mydescr,size=5)
		self.kicProp.mass=self.bfun.radlogg2mass(self.kicProp.rad,self.kicProp.logg)

		
	def kicProp3(self,file):
		mydescr = np.dtype([('ra','float'),('dec','float'),('teff','float'),('logz','float'),
		('logg','float'),('rad','float'),('kp','float'),('long','float'),('lat','float')])
		self.kicProp = self.read_array(filename=file, dtype=mydescr,size=9,sep=',')
		self.kicProp.mass=self.bfun.radlogg2mass(self.kicProp.rad,self.kicProp.logg)
		
	def kicProp4(self,file,sep=None):
		mydescr = np.dtype([('ra','float'),('dec','float'),('teff','float'),('logz','float'),
		('logg','float'),('rad','float'),('kp','float'),('long','float'),('lat','float'),('inFov','int')])
		self.kicProp = self.read_array(filename=file, dtype=mydescr,size=10,sep=sep)
		self.kicProp.mass=self.bfun.radlogg2mass(self.kicProp.rad,self.kicProp.logg)
		
	def kicProp5(self,file,sep=None):
		mydescr = np.dtype([('kepId','int64'),('ra','float'),('dec','float'),('teff','float'),('logg','float'),
		('logz','float'),('rad','float'),('kp','float'),('long','float'),('lat','float')])
		self.kicProp = self.read_array(filename=file, dtype=mydescr,size=10,sep=sep)

		self.kicProp.mass=self.bfun.radlogg2mass(self.kicProp.rad,self.kicProp.logg)
		
	def appMag(self,file):
		mydescr = np.dtype([('id','float'),('u','float'),('g','float'),('r','float'),('i','float'),
		('z','float'),('d51','float'),('jj','float'),('jk','float'),('jh','float')])
		self.appMag = self.read_array(filename=file, dtype=mydescr,size=10)
		
	def appMag2(self,file):
		mydescr = np.dtype([('id','float'),('u','float'),('g','float'),('r','float'),('i','float'),
		('z','float'),('gred','float'),('d51','float'),('jj','float'),('jk','float'),('jh','float')])
		self.appMag = self.read_array(filename=file, dtype=mydescr,size=11)

	def appMag3(self,file,sep=None):
		mydescr = np.dtype([('id','float'),('u','float'),('g','float'),('r','float'),('i','float'),
		('z','float'),('gred','float'),('d51','float'),('jj','float'),('jk','float'),('jh','float'),('kp','float')])
		self.appMag = self.read_array(filename=file, dtype=mydescr,size=12,sep=sep)
		
	def appMag4(self,file):
		mydescr = np.dtype([('id','float'),('u','float'),('g','float'),('r','float'),('i','float'),
		('z','float'),('d51','float'),('jj','float'),('jk','float'),('jh','float'),('kp','float')])
		self.appMag = self.read_array(filename=file, dtype=mydescr,size=11)
		
	def appMag5(self,file,sep=None):
		mydescr = np.dtype([('id','int64'),('long','float'),('lat','float'),('u','float'),('g','float'),('r','float'),('i','float'),
		('z','float'),('d51','float'),('jj','float'),('jk','float'),('jh','float'),('kp','float')])
		self.appMag = self.read_array(filename=file, dtype=mydescr,size=13,sep=sep)

	def appMag6(self,file,sep=None):
		mydescr = np.dtype([('id','int64'),('long','float'),('lat','float'),('u','float'),('g','float'),('r','float'),('i','float'),
		('z','float'),('gred','float'),('d51','float'),('jj','float'),('jk','float'),('jh','float'),('kp','float')])
		self.appMag = self.read_array(filename=file, dtype=mydescr,size=14,sep=sep)
		
	def appMag7(self,file,sep=None):
		mydescr = np.dtype([('id','int64'),('long','float'),('lat','float'),('met','float'),('u','float'),('g','float'),('r','float'),('i','float'),
		('z','float'),('d51','float'),('jj','float'),('jk','float'),('jh','float'),('kp','float')])
		self.appMag = self.read_array(filename=file, dtype=mydescr,size=14,sep=sep)
		
	def readExitTest(self,file):
		mydescr = np.dtype([('long','float'),('lat','float'),('dist','float'),('ext','float')])
		self.extTest = self.read_array(filename=file, dtype=mydescr,size=4)
		
	def readExtAll(self,file):
		mydescr = np.dtype([('long','float'),('lat','float'),('dist','float'),('kep','float'),
		('hak','float'),('drim','float')])
		self.readExtAll = self.read_array(filename=file, dtype=mydescr,size=6)
		
	def readSingCol(self,file):
		mydescr = np.dtype([('id','float'),('data','float')])
		self.readSingCol = self.read_array(filename=file, dtype=mydescr,size=2)
		
	def readExtract(self,file):
		mydescr = np.dtype([('id','int64'),('num','float'),('m1','float'),('r1','float'),('t1','float'),('l1','float'),
		('m2','float'),('r2','float'),('t2','float'),('l2','float'),('p','float'),
            ('birth','float'),('death','float'),('massTran','int'),('spec1','a10'),('spec2','a10'),('evol','a100')])
		self.kep = self.read_array(filename=file, dtype=mydescr,size=17)

		self.kep.tr=np.minimum(self.kep.t1,self.kep.t2)/np.maximum(self.kep.t1,self.kep.t2)
		self.kep.mr=np.minimum(self.kep.m1,self.kep.m2)/np.maximum(self.kep.m1,self.kep.m2)
		
		self.kep.a=self.bfun.porb2a(self.kep.m1,self.kep.m2,self.kep.p)
		self.kep.logg1=np.zeros(np.size(self.kep.m1))
		self.kep.logg2=np.zeros(np.size(self.kep.m1))
		self.kep.logg1=self.bfun.massrad2logg(self.kep.m1,self.kep.r1)
		self.kep.logg2=self.bfun.massrad2logg(self.kep.m2,self.kep.r2)

		self.kep.r1r2=(self.kep.r1+self.kep.r2)/self.kep.a



	def readExtract2(self,file):
		mydescr = np.dtype([('num','float'),('m1','float'),('r1','float'),('t1','float'),('l1','float'),
		('m2','float'),('r2','float'),('t2','float'),('l2','float'),('p','float'),
            ('birth','float'),('death','float'),('massTran','int'),('spec1','a10'),('spec2','a10'),('evol','a100')])
		self.kep = self.read_array(filename=file, dtype=mydescr,size=16)
	
	def readFOV(self,file):
		mydescr = np.dtype([('ra','float'),('dec','float'),('long','float'),('lat','float'),
			('row','int'),('col','int')])
		self.fov = self.read_array(filename=file, dtype=mydescr,size=6)	
		
	def readFOV2(self,file):
		mydescr = np.dtype([('ra','float'),('dec','float'),('long','float'),('lat','float'),
			('row','int'),('col','int'),('mag','float')])
		self.fov = self.read_array(filename=file, dtype=mydescr,size=7)
		
	def readTarget(self,file):
		mydescr = np.dtype([('id','int64'),('bin','int')])
		self.target = self.read_array(filename=file, dtype=mydescr,size=2)
		
	def readBirth(self,file):
		mydescr = np.dtype([('num','float'),('tphys','float'),('m1','float'),('type1','int'),('m2','float'),
		('type2','int'),('p','float'),('btype','int'),('evol','a100')])
		self.birth = self.read_array(filename=file, dtype=mydescr,size=9)
		
	def readTransCalc(self,file):
		mydescr = np.dtype([('id','int64'),('porb','float'),('inclin','float'),('crit','float'),
		('depth1','float'),('ellip1','float'),('depth2','float'),('ellip2','float')])
		self.trans = self.read_array(filename=file, dtype=mydescr,size=8)

	def quarter1(self,file,sep=None):
		mydescr = np.dtype([('id','int64'),('mag','float'),('m','int'),('o','int'),('i','int'),('j','int'),('comtam','float'),('teff','float'),('logg','float'),('rad','float'),('gr','float'),('jk','float'),('prog','a10')])
		self.kic = self.read_array(filename=file, dtype=mydescr,size=13,sep=sep)

	def pcebRanLoc(self,file):
		mydescr = np.dtype([('file','a80'),('line','int'),('long','float'),('lat','float'),('dis','float'),('u','float'),('g','float'),('r','float'),
							('i','float'),('z','float')])
		self.pceb = self.read_array(filename=file, dtype=mydescr,size=10)

	def readKicMag(self,file,sep=None):
		mydescr = np.dtype([('u','float'),('g','float'),('r','float'),('i','float'),
		('z','float'),('gred','float'),('d51','float'),('jj','float'),('jk','float'),('jh','float'),('kp','float')])
		self.appMag = self.read_array(filename=file, dtype=mydescr,size=11,sep=sep)

	def readEB(self,file,sep=None):
		mydescr = np.dtype([('id','int64'),('fileNum','int'),('bin','int'),('inclin','float'),('depth1','float'),('depth2','float'),('s1','float'),('s2','float')
		,('noise','float'),('stellBG','float'),('sn1','float'),('sn2','float'),('d1','int'),('d2','int'),('dd1','int'),('dd2','int'),('stype','i4')])
		self.eb = self.read_array(filename=file, dtype=mydescr,size=17)

	def readEB2(self,file,sep=None):
		mydescr = np.dtype([('id','int64'),('fileNum','int'),('bin','int'),('inclin','float'),('depth1','float'),('depth2','float'),('s1','float'),('s2','float')
		,('noise','float'),('bgflux','float'),('stellBG','float'),('sn1','float'),('sn2','float'),('d1','int'),('d2','int'),('dd1','int'),('dd2','int'),('stype','i4')])
		self.eb = self.read_array(filename=file, dtype=mydescr,size=17)

	def readKepEB2(self,file,sep=None):
		mydescr = np.dtype([('id','float'),('type','a3'),('bjd','float'),('p','float'),('kp','float'),('tr','float'),('p1p2','float'),('q','float'),
		('esinw','float'),('ecosw','float'),('ff','float'),('sini','float')])
		self.eb = self.read_array(filename=file, dtype=mydescr,sep=sep,size=12)

	def readEB2(self,file,sep=None):
		mydescr = np.dtype([('id','int64'),('fileNum','int'),('bin','int'),('inclin','float'),('depth1','float'),('depth2','float'),('ellip1','float'),('ellip2','float')
		,('s1','float'),('s2','float'),('noise','float'),('bgflux','float'),('stellBG','float'),('sn1Eclip','float'),('sn2Eclip','float'),('sn1Ellip','float'),('sn2Ellip','float'),('d1','int'),('d2','int'),('dd1Eclip','int'),('dd2Eclip','int'),('dd1Ellip','int'),('dd2Ellip','int'),('stype','i4')])
		self.eb = self.read_array(filename=file, dtype=mydescr,size=17)

	def readPop(self,file,sep=None):
		mydescr = np.dtype([('id','int64'),('num','float'),('mt1','float'),('mt2','float'),('p','float'),('birth','float'),('death','float'),('evol','a100')])
		self.pop = self.read_array(filename=file, dtype=mydescr,sep=sep,size=12)

class extras(object):
	def mkdir_p(self,path):
		try:
			os.makedirs(path)
		except OSError as exc: # Python >2.5
			if exc.errno == errno.EEXIST:
				pass
			else: raise

	def writeJobSubmit(self,base):
		with open(base+"/cluster/jobSubmit.sh", "w") as f:
			f.write('#!/bin/bash\n')
			f.write('#The .NUMBER at end of all files\n')
			f.write('VERSION=${1:-1}\n')

			f.write('bisepsS=$(qsub -terse -v VERSION=$VERSION  biseps_s.clus.sh)\n')
			f.write('bisepsB=$(qsub -terse -v VERSION=$VERSION  biseps_b.clus.sh)\n')
			f.write('bisepsUtil=$(qsub -terse -hold_jid ${bisepsS%%.*},${bisepsB%%.*} -v VERSION=$VERSION  bisepsUtil.clus.sh)\n')
			
			f.write('sy=$(qsub -terse -hold_jid ${bisepsUtil%%.*} -v VERSION=$VERSION  pop_s_y.clus.sh)\n')
			f.write('so=$(qsub -terse -hold_jid_ad ${sy%%.*} -v VERSION=$VERSION pop_s_o.clus.sh)\n')
			f.write('bo=$(qsub -terse -hold_jid_ad ${so%%.*} -v VERSION=$VERSION pop_b_o.clus.sh)\n')
			f.write('by=$(qsub -terse -hold_jid_ad ${bo%%.*} -v VERSION=$VERSION pop_b_y.clus.sh)\n')
			
			f.write('id1=$(qsub -terse -hold_jid ${sy%%.*} -v VERSION=$VERSION pop_s_y.idl.clus.sh)\n')
			f.write('id2=$(qsub -terse -hold_jid ${sy%%.*} -v VERSION=$VERSION pop_s_y.idl.clus2.sh)\n')
			f.write('id3=$(qsub -terse -hold_jid ${so%%.*} -v VERSION=$VERSION pop_s_o.idl.clus.sh)\n')
			f.write('id4=$(qsub -terse -hold_jid ${so%%.*} -v VERSION=$VERSION pop_s_o.idl.clus2.sh)\n')
			f.write('id5=$(qsub -terse -hold_jid ${by%%.*} -v VERSION=$VERSION pop_b_y.idl.clus.sh)\n')
			f.write('id6=$(qsub -terse -hold_jid ${by%%.*} -v VERSION=$VERSION pop_b_y.idl.clus2.sh)\n')
			f.write('id7=$(qsub -terse -hold_jid ${bo%%.*} -v VERSION=$VERSION pop_b_o.idl.clus.sh)\n')
			f.write('id8=$(qsub -terse -hold_jid ${bo%%.*} -v VERSION=$VERSION pop_b_o.idl.clus2.sh)\n')
			
			f.write('ffi=$(qsub -terse -hold_jid ${id1%%.*},${id2%%.*},${id3%%.*},${id4%%.*},${id5%%.*},${id6%%.*},${id7%%.*},${id8%%.*} -v VERSION=$VERSION ffi.clus.sh)\n')
			
			f.write('syt=$(qsub -terse -hold_jid_ad ${ffi%%.*} -v VERSION=$VERSION pop_s_y.target.clus.sh)\n')
			f.write('sot=$(qsub -terse -hold_jid_ad ${syt%%.*} -v VERSION=$VERSION pop_s_o.target.clus.sh)\n')
			f.write('bot=$(qsub -terse -hold_jid_ad ${sot%%.*} -v VERSION=$VERSION pop_b_o.target.clus.sh)\n')
			f.write('byt=$(qsub -terse -hold_jid_ad ${bot%%.*} -v VERSION=$VERSION pop_b_y.target.clus.sh)\n')


	def writeKeplerOut(self,baseFile,num,binaryClass,disk,sfr,scaleR,scaleZ,isSingle,kepCCD,ext,imfType,imr,colourCorrect,bFrac):

		self.mkdir_p(baseFile)
		print baseFile
		
		for i in range(0,84):
			folder=baseFile+'/'+str(i+1)
			self.mkdir_p(folder)
			with open(folder+"/pop.in", "w") as f:
				f.write('# Binary class\n')
				f.write(str(binaryClass)+'\n')
				f.write('# BiSEPS input model'+'\n')		
				f.write('1'+'\n')
				f.write('# Starburst (burst) or continuous (ctu) star formation rate'+'\n')
				f.write('ctu '+str(disk)+' '+str(sfr)+'\n')
				f.write('# Ranges and number of bins in time'+'\n')
				f.write('0.0d0 1.5d4 300'+'\n')
				f.write('# Age of the galaxy in Myr'+'\n')
				f.write('13000.0'+'\n')
				f.write('# (R,z) coordinates of the Sun in pc'+'\n')
				f.write('8500.0 30.0'+'\n')
				f.write('# Scale length and height of the Galactic disc in pc'+'\n')
				f.write(str(scaleR)+' '+str(scaleZ)+'\n')
				f.write('# Ranges (degrees) and number of fields in galactic longitude and latitude'+'\n')
				f.write(str(np.min(kepCCD[i,:,0]))+' '+str(np.max(kepCCD[i,:,0]))+' 1\n')
				f.write(str(np.min(kepCCD[i,:,1]))+' '+str(np.max(kepCCD[i,:,1]))+' 1\n')
				f.write('# Want transits yes=1, are we on single stars yes=1,numTransits'+'\n')
				f.write('0 '+str(isSingle)+' 30'+'\n')
				f.write('# Min and max mag to run over'+'\n')
				f.write('0.0 21.0'+'\n')
				f.write(str(kepCCD[i,1,0])+' '+str(kepCCD[i,2,0])+' '+str(kepCCD[i,3,0])+' '+str(kepCCD[i,0,0])+'\n')
				f.write(str(kepCCD[i,1,1])+' '+str(kepCCD[i,2,1])+' '+str(kepCCD[i,3,1])+' '+str(kepCCD[i,0,1])+'\n')
				f.write(str(ext)+' '+str(imfType)+' '+str(imr)+' '+str(colourCorrect)+' '+str(bFrac)+'\n')
				f.close()


	def writeKep(self,base,imfType='k',ext='d'):
		x=readFile()
		x.kepGrid('/padata/beta/users/rfarmer/code/plot/kepCCD2.txt')
		self.writeKeplerOut(base+'/pop_s_y',84,3,'young',3000.0,2800.0,300.0,1,x.kepGrid,ext,imfType,0.0,0.88,0.5)
		self.writeKeplerOut(base+'/pop_s_o',84,3,'old',3000.0,3700.0,1000.0,1,x.kepGrid,ext,imfType,0.0,0.88,0.5)
		self.writeKeplerOut(base+'/pop_b_y',84,2,'young',3000.0,2800.0,300.0,0,x.kepGrid,ext,imfType,0.0,0.88,0.5)
		self.writeKeplerOut(base+'/pop_b_o',84,2,'old',3000.0,3700.0,1000.0,0,x.kepGrid,ext,imfType,0.0,0.88,0.5)

		self.writeKepClus(base,'pop_s_y','WEBwide',1)
		self.writeKepClus(base,'pop_s_o','WEBwide',1)
		self.writeKepClus(base,'pop_b_y','WEB',0)
		self.writeKepClus(base,'pop_b_o','WEB',0)
		print base+"\n"
		
	def writeKepClus(self,base,popType,sysType,isSingle):
		baseFile=base+'/cluster'
		self.mkdir_p(baseFile)
		
		with open(baseFile+"/"+popType+".clus.sh", "w") as f:
			f.write("#!/bin/bash\n")
			f.write("#$ -N "+popType+"\n")
			f.write("#$ -P physics\n")
			f.write("#$ -l s_rt=100:00:00,virtual_free=3G,h_vmem=6G\n")
			f.write("#$ -t 1-84:1\n")
			f.write("#$ -S /bin/bash\n")
			f.write("#$ -j y\n")
			f.write("#$ -o "+baseFile+"\n")
			f.write("cd "+base+"/"+popType+"/$SGE_TASK_ID\n")
			f.write("time "+base+"/"+"code/population pop.in "+base+"/channels/ > ranPick.dat.$VERSION 2>> pop.err\n")
			f.write("sort -n ranPick.dat.$VERSION > tmp && mv tmp ranPick.dat.$VERSION\n")
			f.write("/padata/beta/users/rfarmer/code/scripts/ranLoc ranPick.dat.$VERSION "+base+"/channels/"+sysType+"_mag.dat.$VERSION pop.in > ranLoc.dat.$VERSION\n")
			f.write("/padata/beta/users/rfarmer/code/scripts/extract ranPick.dat.$VERSION "+base+"/channels/"+sysType+"_kepler.dat.$VERSION > extract.$VERSION\n")
			f.write("/padata/beta/users/rfarmer/code/scripts/makeAllMags ranLoc.dat.$VERSION extract.$VERSION "+str(isSingle)+" > mag.dat.$VERSION\n")
			f.write("awk '{if($13<=16.0) print $0}' mag.dat.$VERSION > mag.dat.$VERSION.kic\n")
		f.close()

	def writeBiseps(self,base):
		self.writeBisepsOut(base+'/biseps_s',0.1,20.0,10000,0.1,0.1,1,10000000.0,10000000.0,1,1,'Kp',0,1)
		self.writeBisepsOut(base+'/biseps_b',0.1,20.0,50,0.1,20.0,50,3.0,1000000.0,300,2,'Kp',0,0)
		self.writeBisepsClus(base,'biseps_s',1)
		self.writeBisepsClus(base,'biseps_b',150)


	def writeBisepsOut(self,baseFile,minM1,maxM1,numM1,minM2,maxM2,numM2,minP,maxP,numP,numPStep,filterBand,wantTransit,isSingle):
		self.mkdir_p(baseFile)

		for i in range(0,numP/numPStep):
			folder=baseFile+'/'+str(i+1)
			self.mkdir_p(folder)
			pStep=((np.log10(maxP)-np.log10(minP))/(1.0*numP))*numPStep
			with open(folder+"/BiSEPS.in.1", "w") as f:
				f.write(str(minM1)+' '+str(maxM1)+' '+str(minM2)+' '+str(maxM2)+' 0.0033 15000.0\n')
				f.write('1 0.5 0.0 1.0 1.0\n')
				f.write(str(10**(np.log10(minP)+(i*pStep)))+' '+str(10**(np.log10(minP)+((i+1)*pStep)))+'\n') #period
				f.write('1 0.0 0.0 -1.0 0.0\n')
				f.write('0 0 0\n')
				f.write('1.0d-7 2.7d-7 0.2\n')
				f.write('0 1.0 0.5\n')
				f.write('1.0\n')
				f.write('0.1 -1 0.01\n')
				f.write('0.01 8.0 16.0\n')
				if isSingle == 1:
					f.write('0 0\n')
				else:
					f.write('200 190\n')
				f.write('0.005 0.02 0.05 0.001\n')
				f.write(str(numPStep)+' '+str(numM1)+' '+str(numM2)+'\n')
				f.write(filterBand+'\n')
				#f.write('1.d-2 1.d0 30\n')
				f.write(str(wantTransit)+' '+str(isSingle)+'\n')
				f.close()

	def writeBisepsClus(self,base,popType,num):
		with open(base+"/cluster/"+popType+".clus.sh", "w") as f:
			f.write('#!/bin/bash\n')
			f.write('#$ -N '+popType+'\n')
			f.write("#$ -P physics\n")
			f.write("#$ -l s_rt=100:00:00,virtual_free=3G,h_vmem=6G\n")
			f.write("#$ -t 1-"+str(num)+":1\n")
			f.write("#$ -S /bin/bash\n")
			f.write("#$ -j y\n")
			f.write("#$ -o "+base+"/cluster\n")
			f.write("cd "+base+"/"+popType+"/$SGE_TASK_ID\n")
			f.write(base+"/code/BiSEPS BiSEPS.in.1")
		

	def writeIDLclus(self,base,popType):

		with open(base+"/cluster/"+popType+".idl.clus.sh", "w") as f:
			f.write('#!/bin/bash\n')
			f.write('#$ -N '+popType+'_idl\n')
			f.write('#$ -P physics\n')
			f.write('#$ -l s_rt=100:00:00,virtual_free=3G,h_vmem=6G,IDL_license=1\n')
			f.write('#$ -S /bin/bash\n')
			f.write('#$ -j y\n')
			f.write('#$ -o '+base+'/cluster\n')

			f.write('export IDL_PATH="<IDL_DEFAULT>"\n')
			f.write('export IDL_PATH=${IDL_PATH}:+/padata/beta/users/rfarmer/code/IDL:+/padata/beta/users/rfarmer/code/kic\n')
			f.write("for i in $(seq 1 41);do\n")
			f.write('cd '+base+'/'+popType+'/$i\n')
			f.write('idl -queue -e ".r /padata/beta/users/rfarmer/code/kic/stellar_parms2.pro" -args mag.dat.$VERSION.kic kic.dat.$VERSION mag.dat.idl.$VERSION\n')
			f.write("echo $i\n")
			f.write("done\n")

		with open(base+"/cluster/"+popType+".idl.clus2.sh", "w") as f:
			f.write('#!/bin/bash\n')
			f.write('#$ -N '+popType+'_idl\n')
			f.write('#$ -P physics\n')
			f.write('#$ -l s_rt=100:00:00,virtual_free=3G,h_vmem=6G,IDL_license=1\n')
			f.write('#$ -S /bin/bash\n')
			f.write('#$ -j y\n')
			f.write('#$ -o '+base+'/cluster\n')

			f.write('export IDL_PATH="<IDL_DEFAULT>"\n')
			f.write('export IDL_PATH=${IDL_PATH}:+/padata/beta/users/rfarmer/code/IDL:+/padata/beta/users/rfarmer/code/kic\n')
			f.write("for i in $(seq 42 84);do\n")
			f.write('cd '+base+'/'+popType+'/$i\n')
			f.write('idl -queue -e ".r /padata/beta/users/rfarmer/code/kic/stellar_parms2.pro" -args mag.dat.$VERSION.kic kic.dat.$VERSION mag.dat.idl.$VERSION\n')
			f.write("echo $i\n")
			f.write("done\n")

	def writeIDL(self,base):
		self.writeIDLclus(base,'pop_s_y')
		self.writeIDLclus(base,'pop_b_y')
		self.writeIDLclus(base,'pop_b_o')
		self.writeIDLclus(base,'pop_s_o')

	def writeBisepsUtil(self,base):

		with open(base+"/cluster/bisepsUtil.clus.sh", "w") as f:
			f.write('#!/bin/bash\n')
			f.write('#$ -N bisepsUtil\n')
			f.write('#$ -P physics\n')
			f.write('#$ -l s_rt=100:00:00,virtual_free=3G,h_vmem=6G\n')
			f.write('#$ -S /bin/bash\n')
			f.write('#$ -j y\n')
			f.write('#$ -o '+base+'/cluster\n')

			f.write('cd '+base+'\n')
			f.write("export PYTHONPATH="+pyDir+":/csoft/epd-7.2-64/lib/python2.7/site-packages\n")
			f.write(pythonBin+' '+pyDir+'/bisepsUtil.py '+base+'\n')

			
	def getCode(self,base):
		import subprocess as s

		git='git clone /padata/beta/users/rfarmer/code/source '+base+'/code'
		s.call(git,shell=True)
		git='git ls-remote /padata/beta/users/rfarmer/code/source HEAD > '+base+'/code/git.hash'
		s.call(git,shell=True)

	def linkChannels(self,channIn,channOut):
	#Links the channels folder given by channIn to a folder in channOut
		try:
			os.symlink(channIn,channOut)
		except OSError as exc: # Python >2.5
			if exc.errno == errno.EEXIST:
				pass
			else: raise

	def mergeBiseps(self,base):
		import subprocess as s
		#import shutil as sh
		bin='/padata/beta/users/rfarmer/code/bin/'
		#base='/padata/beta/users/rfarmer/data/keplerIMF'
		#self.mkdir_p(base+'/channels')

		os.symlink(base+'/biseps_b/1/WEB_grid.dat.1',base+'/channels/WEB_grid.dat.1')

		types=["WEBwide_grid.dat.1","WEBwide_kepler.dat.1","WEBwide_mag.dat.1","WEBwide_pop.dat.1"]
		for j in types:
			try:
				os.symlink(base+'/biseps_s/1/'+j,base+'/channels/'+j)
			except OSError as exc: # Python >2.5
				if exc.errno == errno.EEXIST:
					pass
				else: raise
			print j

		types=["WEB_kepler.dat.1","WEB_mag.dat.1","WEB_pop.dat.1"]
#		types=["WEB_pop.dat.1"]
		for j in types:
			s.call("echo 150 "+j+" "+base+"/biseps_b/ |" +bin+"mergeBiSEPS >"+base+"/channels/"+j,shell=True)
			print j
		
	def point_inside_polygon(self,x,y,polyx,polyy):
		inside =False 

		for j in xrange(0,83):
			p1x = polyx[j*4]
			p1y = polyy[j*4]
			
			for i in range(5):
				p2x = polyx[j*4+(i % 5)]
				p2y = polyy[j*4+(i % 5)]
				if y > min(p1y,p2y):
					if y <= max(p1y,p2y):
						if x <= max(p1x,p2x):
							if p1y != p2y:
								xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
							if p1x == p2x or x <= xinters:
								inside = not inside
				p1x,p1y = p2x,p2y
	
			if inside:
				 return inside
		return inside

	def compileCode(self,base):
		build_dir = base+"/code"

		cwd = os.getcwd() # get current directory
		try:
			os.chdir(build_dir)
			os.system("make -sf Makefile_population 2>/dev/null")
		finally:
			os.chdir(cwd)

		cwd = os.getcwd() # get current directory
		try:
			os.chdir(build_dir)
			os.system("make -sf Makefile_BiSEPS 2>/dev/null")
		finally:
			os.chdir(cwd)

	def makeKepFolder(self,base):
		#Make root dir
		self.mkdir_p(base)
		print "Root Done "+base

		self.mkdir_p(base+"/channels")
		self.mkdir_p(base+"/cluster")
		#Link in channels folder
		#self.linkChannels('/padata/beta/users/rfarmer/data/keplerIMF/channels',base+'/channels')
		#print "Channels done"

		#Get code
		self.getCode(base)
		print "Code extracted"

		#Compile code
		self.compileCode(base)
		print "Code compiled"

		#Make biseps code
		self.writeBiseps(base)
		print "BiSEPS made"
		
		#Make pop folders and pop.in plus cluster and pop.sh
		self.writeKep(base,imfType='k',ext='d')
		print "Pop made"

		self.writeBisepsUtil(base)
		print "Biseps util done"

		self.writeIDL(base)
		print "IDL cluster done"

		self.writeKepTarg(base)
		print "Kepler Target done"

		self.writeJobSubmit(base)
		print "jobSubmit done"

		print "Ready to run"


	def writeTargetClus(self,base,popType):
		with open(base+"/cluster/"+popType+".target.clus.sh", "w") as f:
			f.write('#!/bin/bash\n')
			f.write('#$ -N '+popType+'_target\n')
			f.write('#$ -P physics\n')
			f.write('#$ -l s_rt=100:00:00,virtual_free=3G,h_vmem=6G\n')
			f.write("#$ -t 1-84:1\n")
			f.write('#$ -S /bin/bash\n')
			f.write('#$ -j y\n')
			f.write('#$ -o '+base+'/cluster\n')

			f.write("cd "+base+"/"+popType+"/$SGE_TASK_ID\n")
			f.write("export PYTHONPATH="+pyDir+":/csoft/epd-7.2-64/lib/python2.7/site-packages\n")
			f.write(pythonBin+' '+pyDir+'/targetList.py $SGE_TASK_ID '+base+'/ffi/ $VERSION \n')
			f.write(pythonBin+' '+pyDir+'/extractMag.py $VERSION\n')
			f.write(pythonBin+' '+pyDir+'/extractTar.py $VERSION\n')

	def writeFFIClus(self,base):
		self.mkdir_p(base+'/ffi')
		
		with open(base+"/cluster/ffi.clus.sh", "w") as f:
			f.write('#!/bin/bash\n')
			f.write('#$ -N ffi\n')
			f.write('#$ -P physics\n')
			f.write('#$ -l s_rt=100:00:00,virtual_free=3G,h_vmem=6G\n')
			f.write("#$ -t 1-84:1\n")
			f.write('#$ -S /bin/bash\n')
			f.write('#$ -j y\n')
			f.write('#$ -o '+base+'/cluster\n')

			f.write('cd '+base+'/ffi \n')
			f.write("export PYTHONPATH="+pyDir+":/csoft/epd-7.2-64/lib/python2.7/site-packages\n")
			
			f.write(pythonBin+' '+pyDir+'/ffi.py $SGE_TASK_ID $VERSION ../pop_s_y ../pop_s_o ../pop_b_y ../pop_b_o\n')
				
	def writeKepTarg(self,base):
		self.writeFFIClus(base)

		self.writeTargetClus(base,'pop_s_y')
		self.writeTargetClus(base,'pop_s_o')
		self.writeTargetClus(base,'pop_b_y')
		self.writeTargetClus(base,'pop_b_o')

		
#class plot(object):

	#def histOutline(self,dataIn, *args, **kwargs):
		#(histIn, binsIn) = np.histogram(dataIn, *args, **kwargs)
		#stepSize = binsIn[1] - binsIn[0]
		#bins = np.zeros(len(binsIn)*2 + 2, dtype=np.float)
		#data = np.zeros(len(binsIn)*2 + 2, dtype=np.float)
		#for bb in range(len(binsIn)):
			#bins[2*bb + 1] = binsIn[bb]
			#bins[2*bb + 2] = binsIn[bb] + stepSize
			#if bb < len(histIn):
				#data[2*bb + 1] = histIn[bb]
				#data[2*bb + 2] = histIn[bb]
	
		#bins[0] = bins[1]
		#bins[-1] = bins[-2]
		#data[0] = 0
		#data[-1] = 0
		#return (bins, data)

	#def kepTot(self,data,title='',total=True,cmap='jet_r',fileOut=False):
		#"""
		#import biseps as b
		#import numpy as np
		#desc=np.dtype([('sing','float'),('bin','float')])
		#x=b.readFile()
		#x.stars=x.read_array('total_le16.0.count.1',dtype=desc,size=2)
		#y=b.plot()
		#y.kepTot(x.stars.sing,total=False,fileOut='sing.all.png')
		#y.kepTot(x.stars.bin,total=False,fileOut='bin.all.png')
		#"""

		#x=readFile()
	
	##        grid=x.kepGrid('/home/Rob/projects/scripts/kepCCDconvert.txt')
		#grid=x.kepGrid('/padata/beta/users/rfarmer/code/plot/kepCCD2.txt')
	##       data=x.kepGridTot(fileIn)
	##        data=np.random.rand(84)*10000000
	
		#fig=plt.figure()
	
		#xmin=np.min(x.kepGrid[:,:,0])
		#xmax=np.max(x.kepGrid[:,:,0])
		#ymin=np.min(x.kepGrid[:,:,1])
		#ymax=np.max(x.kepGrid[:,:,1])
	
		#ax1=fig.add_axes([0.075,0.09,0.75,0.75],
							#title=title,
							#xlabel='Galactic Longitude, (l [degrees])',
							#ylabel='Galactic Latitude, (b [degrees])',
							#xticks=range(np.int(xmin),np.int(xmax),
											#np.int((xmax-xmin)/5)),
							#yticks=range(np.int(ymin),np.int(ymax),
											#np.int((ymax-ymin)/5))
						#)
	
		#if cmap=='bw':
			#cmap='binary'
		#cm = plt.get_cmap(cmap)
	
		#for i in range(84):
			#plt.fill(x.kepGrid[i,0:5,0],x.kepGrid[i,0:5,1],
						#color=cm(data[i]/np.max(data)))
	
		##a=np.max(x.kepGrid[:,:,0])*0.93
		##b=np.max(x.kepGrid[:,:,1])*0.9625
		#if total == True:
			#totalLab='Total = '+ str(np.int(np.sum(data)))
			#props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
			#ax1.text(0.69, 0.96, totalLab, transform=ax1.transAxes,
						#fontsize=14,verticalalignment='top', bbox=props)
	
		#norm=m.colors.Normalize(vmin=0.0,vmax=np.max(data))
		#ax2=fig.add_axes([0.8625, 0.09, 0.018, 0.75])
	
	
		#cb=m.colorbar.ColorbarBase(ax2,cmap=cm,norm=norm,
									#orientation='vertical',format='%5.2E')
		#cb.set_ticks((0,np.int(np.max(data)*0.25),np.int(np.max(data)*0.5),
						#np.int(np.max(data)*0.75),np.int(np.max(data))))
	
		#if fileOut == False:
			#plt.show()
		#else:
			#plt.savefig(fileOut)

	#def plotAllStats(self,fileIn='all.ranLoc.1',fileOut=False):
		#"""Plots a set of distributions"""
	
		#x=readFile()
		#x.ranLoc(fileIn)
		
		#fig=plt.figure(1)
		#plt.subplot(3,2,1)
		#plt.hist(x.ranLoc.lat,bins=20,range=[5,25],normed=1)
		#plt.xlabel('Galactic Latitude, b [degrees]')
		
		#plt.subplot(3,2,2)
		#plt.hist(x.ranLoc.long,bins=20,range=[65,85],normed=1)
		#plt.xlabel('Galactic Longitude, l [degrees]')
		
		#plt.subplot(3,2,3)
		#plt.hist(np.log10(x.ranLoc.dist),normed=1,range=[0.0,np.log10(50000)],bins=100)
		#plt.xlabel('log10 Distance [pc]')
		
		
		#plt.subplot(3,2,5)
		##app mag
		#plt.hist(x.ranLoc.mag,bins=48,range=[-5,12],normed=1)
		#plt.xlabel(r'$M_{kp}$')
		
		#plt.subplot(3,2,6)
		##app mag
		#plt.hist(x.ranLoc.appMag,bins=16,range=[8,16],normed=1)
		#plt.xlabel(r'$m_{kp}$')

		#if fileOut == False:
			#plt.show()
		#else:
			#plt.savefig(fileOut)

	#def plotCompDist(self,fileUs='all.ranLoc.1',fileKic='kic.r1.out',fileOut=False):
		#""" Plost distributions for us and KIC datasets"""
		#x=readFile()
		#x.kic3(file=fileKic)
		#y=readFile()
		#y.ranLoc(fileUs)
		
		#plt.figure(1)
		#plt.subplot(2,2,1)
		#plt.hist(x.kic3.lat,bins=20,range=[5,25],normed=1,alpha=0.5,label='KIC')
		#plt.xlabel('Galactic Latitude, b [degrees]')
		
		#plt.subplot(2,2,2)
		#plt.hist(x.kic3.long,bins=20,range=[65,85],normed=1,alpha=0.5)
		#plt.xlabel('Galactic Longitude, l [degrees]')
		
		#plt.subplot(2,2,3)
		##app mag
		#plt.hist(x.kic3.kp,bins=32,range=[8,16],normed=1,alpha=0.5)
		#plt.xlabel(r'$m_{Kp}$')
		
		
		#plt.subplot(2,2,1)
		#plt.hist(y.ranLoc.lat,bins=20,range=[5,25],normed=1,alpha=0.5,label='Us')
		#plt.legend()
		
		#plt.subplot(2,2,2)
		#plt.hist(y.ranLoc.long,bins=20,range=[65,85],normed=1,alpha=0.5)
		
		#plt.subplot(2,2,3)
		##app mag
		#plt.hist(y.ranLoc.appMag,bins=32,range=[8,16],normed=1,alpha=0.5)

		#if fileOut == False:
			#plt.show()
		#else:
			#plt.savefig(fileOut)

	#def plotCompCul(self,fileUs='all.ranLoc.1',fileKic='kic.r1.out',fileOut=False):
		#"""Plots culmative plots"""
		#x=readFile()
		#x.kic3(file=fileKic)
		#y=readFile()
		#y.ranLoc(fileUs)
		
		#(la1n,la1b,la1p)=plt.hist(x.kic3.lat,bins=20,range=[5,25],normed=1,cumulative=1)
		#(lo1n,lo1b,lo1p)=plt.hist(x.kic3.long,bins=20,range=[65,85],normed=1,cumulative=1)
		#(m1n,m1b,m1p)=plt.hist(x.kic3.kp,bins=32,range=[8,16],normed=1,cumulative=1)
		#(la2n,la2b,la2p)=plt.hist(y.ranLoc.lat,bins=20,range=[5,25],normed=1,cumulative=1)
		#(lo2n,lo2b,lo2p)=plt.hist(y.ranLoc.long,bins=20,range=[65,85],normed=1,cumulative=1)
		#(m2n,m2b,m2p)=plt.hist(y.ranLoc.appMag,bins=32,range=[8,16],normed=1,cumulative=1)
		
		#plt.figure(1)
		#plt.subplot(2,2,1)
		
		#bins2=np.zeros(np.size(la1n),dtype='float64')
		#for i in range(0,np.size(la1b)-1):
			#bins2[i]=(la1b[i]+la1b[i+1])/2.0
		
		#plt.plot(bins2,la1n,label='kic')
		#plt.plot(bins2,la2n,label='us')
		#plt.legend(loc=2)
		#plt.xlabel('Galactic Latitude, b [degrees]')
		
		
		#plt.subplot(2,2,2)
		#bins2=np.zeros(np.size(lo1n),dtype='float64')
		#for i in range(0,np.size(lo1b)-1):
			#bins2[i]=(lo1b[i]+lo1b[i+1])/2.0
		
		#plt.plot(bins2,lo1n,label='kic')
		#plt.plot(bins2,lo2n,label='us')
		#plt.legend(loc=2)
		#plt.xlabel('Galactic Longitude, l [degrees]')
		
		
		
		#plt.subplot(2,2,3)
		#bins2=np.zeros(np.size(m1n),dtype='float64')
		#for i in range(0,np.size(m1b)-1):
			#bins2[i]=(m1b[i]+m1b[i+1])/2.0
		
		#plt.plot(bins2,m1n,label='kic')
		#plt.plot(bins2,m2n,label='us')
		#plt.legend(loc=2)
		#plt.xlabel(r'$m_{Kp}$')
		
		#if fileOut == False:
			#plt.show()
		#else:
			#plt.savefig(fileOut)

	#def plotCompExt(self,fileUs='all.ranLoc.1',fileKic='kic.r2.out',fileOut=False):

		#x=readFile()
		#x.kic4(file=fileKic)
		#y=readFile()
		#y.ranLoc(fileUs)
		
		#(la1n,la1b,la1p)=plt.hist(x.kic4.av[(x.kic4.av>0.0)]*0.88,bins=100,normed=1,cumulative=1)
		#(la2n,la2b,la2p)=plt.hist(y.ranLoc.ext[(y.ranLoc.ext>0.0)],bins=100,normed=1,cumulative=1)
		
		#plt.figure(1)
		#plt.subplot(2,1,1)
		
		#plt.hist(x.kic4.av[(x.kic4.av>0.0)]*0.88,bins=100,normed=1,alpha=0.5,label='KIC')
		#plt.hist(y.ranLoc.ext[(y.ranLoc.ext>0.0)],bins=100,normed=1,alpha=0.5,label='Us')
		#plt.legend(loc = 0)
		
		#plt.subplot(2,1,2)
		
		#bins2=np.zeros(np.size(la1n),dtype='float64')
		#for i in range(0,np.size(la1b)-1):
			#bins2[i]=(la1b[i]+la1b[i+1])/2.0
		
		#plt.plot(bins2,la1n,label='kic')
		#plt.plot(bins2,la2n,label='us')
		#plt.legend(loc = 0)
		#if fileOut == False:
			#plt.show()
		#else:
			#plt.savefig(fileOut)

	#def plotDeriveKpMags(self,fileUs='all.makeAllMags.1',fileKic='kic.mag2.out',fileOut=False):

		#x=readFile()
		#x.appMag4(file=fileUs)
		#a=readFile()
		#a.appMag3(file=fileKic)
		
		#alpha=0.33
		
		#fig=plt.figure(1)
		##fig.text(.5, 0.95,r'Different Kp calculations (hakkila ectinction)',
		##horizontalalignment='center',verticalalignment='top')
		
		#plt.subplot(2,2,1)
		#kep=np.zeros(np.size(x.appMag.u),dtype='float')
		#kep=0.1*x.appMag.g[(x.appMag.g-x.appMag.r <=0.8)]+0.9*x.appMag.r[(x.appMag.g-x.appMag.r <=0.8)]
		#kep=0.2*x.appMag.g[(x.appMag.g-x.appMag.r >0.8)]+0.8*x.appMag.r[(x.appMag.g-x.appMag.r >0.8)]
		
		#(c,d,e)=plt.hist(a.appMag.kp[a.appMag.kp<99.9],bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='KIC')
		#(c,d,e)=plt.hist(x.appMag.kp,bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='Us')
		#(c,d,e)=plt.hist(kep,bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='Us (g,r)')
		#plt.xlabel('Kp')
		#plt.legend(loc=0)
		
		#plt.subplot(2,2,2)
		#kep=np.zeros(np.size(x.appMag.u),dtype='float')
		#kep=0.55*x.appMag.g[(x.appMag.g-x.appMag.i <=-0.05)]+0.45*x.appMag.i[(x.appMag.g-x.appMag.i <=-0.05)]
		#kep=0.3*x.appMag.g[(x.appMag.g-x.appMag.i >-0.05)]+0.7*x.appMag.i[(x.appMag.g-x.appMag.i >-0.05)]
		
		#(c,d,e)=plt.hist(a.appMag.kp[a.appMag.kp<99.9],bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='KIC')
		#(c,d,e)=plt.hist(x.appMag.kp,bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='Us')
		#(c,d,e)=plt.hist(kep,bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='Us (g,i)')
		#plt.xlabel('Kp')
		#plt.legend(loc=0)
		
		#plt.subplot(2,2,3)
		#kep=np.zeros(np.size(x.appMag.u),dtype='float')
		#kep=0.65*x.appMag.r[(x.appMag.r-x.appMag.i <=0.673)]+0.35*x.appMag.i[(x.appMag.r-x.appMag.i <=0.673)]
		#kep=1.2*x.appMag.r[(x.appMag.r-x.appMag.i >0.673)]-0.2*x.appMag.i[(x.appMag.r-x.appMag.i >0.673)]
		
		#(c,d,e)=plt.hist(a.appMag.kp[a.appMag.kp<99.9],bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='KIC')
		#(c,d,e)=plt.hist(x.appMag.kp,bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='Us')
		#(c,d,e)=plt.hist(kep,bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='Us (r,i)')
		#plt.xlabel('Kp')
		#plt.legend(loc=0)
		
		#plt.subplot(2,2,4)
		#kep=np.zeros(np.size(x.appMag.u),dtype='float')
		#kep=0.25*x.appMag.g[(x.appMag.g-x.appMag.r <=0.3)]+0.75*x.appMag.r[(x.appMag.g-x.appMag.r <=0.3)]
		#kep=0.3*x.appMag.g[(x.appMag.g-x.appMag.r >0.3)]+0.7*x.appMag.i[(x.appMag.g-x.appMag.r >0.3)]
		
		#(c,d,e)=plt.hist(a.appMag.kp[a.appMag.kp<99.9],bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='KIC')
		#(c,d,e)=plt.hist(x.appMag.kp,bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='Us')
		#(c,d,e)=plt.hist(kep,bins=100,normed=1,range=[0.0,20.0],alpha=alpha,label='Us (g,r,i)')
		#plt.xlabel('Kp')
		#plt.legend(loc=0)
		
		#if fileOut == False:
			#plt.show()
		#else:
			#plt.savefig(fileOut)

	#def plotAppMags(self,fileSing='sing.makeAllMags.1',fileBin='bin.makeAllMags.1',fileKic='kic.mag.out',fileOut=False):

		#x=readFile()
		#x.appMag4(file=fileSing)
		#y=readFile()
		#y.appMag4(file=fileBin)
		#a=readFile()
		##kic.mag.out
		#a.appMag2(file=fileKic)
		
		#alpha=0.33
		
		#fig=plt.figure(1)
		##fig.text(.5, 0.95,r'KIC extinction comparing KIC to us for $8 \leq m_{kp} \leq 16$',
		##horizontalalignment='center',verticalalignment='top')
		#plt.subplot(3,3,1)
		#(c,d,e)=plt.hist(a.appMag.u[(a.appMag.u<99.9)],bins=100,normed=1,label='KIC',range=[0.0,25.0],alpha=alpha)
		#(c,d,e)=plt.hist(x.appMag.u,bins=100,normed=1,label='Single',range=[0.0,25.0],alpha=alpha)
		#(c,d,e)=plt.hist(y.appMag.u,bins=100,normed=1,label='Binary',range=[0.0,25.0],alpha=alpha)
		
		#plt.xlabel('U')
		#plt.legend(loc=0)
		
		#plt.subplot(3,3,2)
		#(c,d,e)=plt.hist(a.appMag.g[(a.appMag.g<99.9)],bins=100,normed=1,label='KIC',range=[0.0,20.0],alpha=alpha)
		#(c,d,e)=plt.hist(x.appMag.g,bins=100,normed=1,range=[0.0,25.0],alpha=alpha)
		#(c,d,e)=plt.hist(y.appMag.g,bins=100,normed=1,range=[0.0,25.0],alpha=alpha)
		
		#plt.xlabel('g')
		
		#plt.subplot(3,3,3)
		#(c,d,e)=plt.hist(a.appMag.r[(a.appMag.r<99.9)],bins=100,normed=1,label='KIC',range=[0.0,25.0],alpha=alpha)
		#(c,d,e)=plt.hist(x.appMag.r,bins=100,normed=1,range=[0.0,25.0],alpha=alpha)
		#(c,d,e)=plt.hist(y.appMag.r,bins=100,normed=1,range=[0.0,25.0],alpha=alpha)
		
		#plt.xlabel('r')
		
		#plt.subplot(3,3,4)
		#(c,d,e)=plt.hist(a.appMag.i[(a.appMag.i<99.9)],bins=100,normed=1,label='KIC',range=[0.0,25.0],alpha=alpha)
		#(c,d,e)=plt.hist(x.appMag.i,bins=100,normed=1,range=[0.0,25.0],alpha=alpha)
		#(c,d,e)=plt.hist(y.appMag.i,bins=100,normed=1,range=[0.0,25.0],alpha=alpha)
		
		#plt.xlabel('i')
		
		#plt.subplot(3,3,5)
		#(c,d,e)=plt.hist(a.appMag.z[(a.appMag.z<99.9)],bins=100,normed=1,label='KIC',range=[0.0,25.0],alpha=alpha)
		#(c,d,e)=plt.hist(x.appMag.z,bins=100,normed=1,range=[0.0,25.0],alpha=alpha)
		#(c,d,e)=plt.hist(y.appMag.z,bins=100,normed=1,range=[0.0,25.0],alpha=alpha)
		
		#plt.xlabel('z')
		
		#plt.subplot(3,3,6)
		#(c,d,e)=plt.hist(a.appMag.d51[(a.appMag.d51<99.9)],bins=100,normed=1,label='KIC',range=[0.0,25.0],alpha=alpha)
		#(c,d,e)=plt.hist(x.appMag.d51,bins=100,normed=1,range=[0.0,25.0],alpha=alpha)
		#(c,d,e)=plt.hist(y.appMag.d51,bins=100,normed=1,range=[0.0,25.0],alpha=alpha)
		
		#plt.xlabel('d51')
		
		#plt.subplot(3,3,7)
		#(c,d,e)=plt.hist(a.appMag.jj[(a.appMag.jj<99.9)],bins=100,normed=1,label='KIC',range=[0.0,20.0],alpha=alpha)
		#(c,d,e)=plt.hist(x.appMag.jj,bins=100,normed=1,range=[0.0,20.0],alpha=alpha)
		#(c,d,e)=plt.hist(y.appMag.jj,bins=100,normed=1,range=[0.0,20.0],alpha=alpha)
		
		#plt.xlabel('jj')
		
		#plt.subplot(3,3,8)
		#(c,d,e)=plt.hist(a.appMag.jk[(a.appMag.jk<99.9)],bins=100,normed=1,label='KIC',range=[0.0,20.0],alpha=alpha)
		#(c,d,e)=plt.hist(x.appMag.jk,bins=100,normed=1,range=[0.0,20.0],alpha=alpha)
		#(c,d,e)=plt.hist(y.appMag.jk,bins=100,normed=1,range=[0.0,20.0],alpha=alpha)
		
		#plt.xlabel('jk')
		
		#plt.subplot(3,3,9)
		#(c,d,e)=plt.hist(a.appMag.jh[(a.appMag.jh<99.9)],bins=100,normed=1,label='KIC',range=[0.0,20.0],alpha=alpha)
		#(c,d,e)=plt.hist(x.appMag.jh,bins=100,normed=1,range=[0.0,20.0],alpha=alpha)
		#(c,d,e)=plt.hist(y.appMag.jh,bins=100,normed=1,range=[0.0,20.0],alpha=alpha)
		#plt.xlabel('jh')
		
		
		#if fileOut == False:
			#plt.show()
		#else:
			#plt.savefig(fileOut)

	#def plotKICProp(self,fileUs='sing.kic.1',fileBin='bin.kic.1',fileKic='kic.r3.out',fileOut=False):

		#G=6.67384*10**(-11)
		#Msun=1.98892*10**(30)
		#Rsun=6.955*10**(8)
		#nbins=50
		
		#x=readFile()
		#x.kicProp(fileSing)
		#y=readFile()
		#y.kicProp(fileBin)
		#z=readFile()
		#z.kic5(fileKic)
		
		#plt.figure(1)
		#plt.title('Plots system properties using the KIC property code')
		
		#plt.subplot(3,2,1)
		#plt.hist(x.kicProp.teff[(x.kicProp.tefferr < 10**9.99)],alpha=0.25,normed=1,bins=nbins,label='Single',range=[0.0,20000.0])
		#plt.hist(y.kicProp.teff[(y.kicProp.tefferr < 10**9.99)],alpha=0.25,normed=1,bins=nbins,label='Binary',range=[0.0,20000.0])
		#plt.hist(z.kic5.teff[(z.kic5.teff < 999999.9)],alpha=0.25,normed=1,bins=nbins,label='Kic',range=[0.0,20000.0])
		#plt.xlabel(r'$T_{eff}$')
		#plt.legend(loc=0)
		
		#plt.subplot(3,2,2)
		#plt.hist(x.kicProp.logg[(x.kicProp.loggerr < 9.99)],alpha=0.25,normed=1,bins=nbins,label='Single',range=[0.0,5.5])
		#plt.hist(y.kicProp.logg[(y.kicProp.loggerr < 9.99)],alpha=0.25,normed=1,bins=nbins,label='Binary',range=[0.0,5.5])
		#plt.hist(z.kic5.logg[(z.kic5.logg < 999999.9)],alpha=0.25,normed=1,bins=nbins,label='Kic',range=[0.0,5.5])
		#plt.xlabel(r'$\log g$')
		
		#plt.subplot(3,2,3)
		#plt.hist(np.log10(x.kicProp.rad[(x.kicProp.loggerr < 9.99)]),alpha=0.25,normed=1,bins=nbins,label='Single',range=[-1.0,2.0])
		#plt.hist(np.log10(y.kicProp.rad[(y.kicProp.loggerr < 9.99)]),alpha=0.25,normed=1,bins=nbins,label='Binary',range=[-1.0,2.0])
		#plt.hist(np.log10(z.kic5.rad[(z.kic5.rad < 999999.9)]),alpha=0.25,normed=1,bins=nbins,label='Kic',range=[-1.0,2.0])
		#plt.xlabel(r'$\log_{10} R_{\odot}$')
		
		#plt.subplot(3,2,4)
		#mass=np.zeros(np.size(x.kicProp.rad[(x.kicProp.loggerr < 9.99)]))
		#mass=(10**(x.kicProp.logg[(x.kicProp.loggerr < 9.99)])*(x.kicProp.rad[(x.kicProp.loggerr < 9.99)]*Rsun)**2)/(100.0*G*Msun)
		#plt.hist(mass,alpha=0.25,normed=1,bins=nbins,label='Single',range=[0.0,5.0])
		
		#mass=np.zeros(np.size(y.kicProp.rad[(y.kicProp.loggerr < 9.99)]))
		#mass=(10**(y.kicProp.logg[(y.kicProp.loggerr < 9.99)])*(y.kicProp.rad[(y.kicProp.loggerr < 9.99)]*Rsun)**2)/(100.0*G*Msun)
		#plt.hist(mass,alpha=0.25,normed=1,bins=nbins,label='Binary',range=[0.0,5.0])
		#plt.hist(z.kic5.mass[(z.kic5.mass < 999999.9)],alpha=0.25,normed=1,bins=nbins,label='Kic',range=[0.0,5.0])
		#plt.xlabel(r'$M_{\odot}$')
		
		#plt.subplot(3,2,5)
		#plt.hist(x.kicProp.logz[(x.kicProp.logzerr < 9.99)],alpha=0.25,normed=1,bins=nbins,label='Single',range=[-3.0,1.0])
		#plt.hist(y.kicProp.logz[(y.kicProp.logzerr < 9.99)],alpha=0.25,normed=1,bins=nbins,label='Binary',range=[-3.0,1.0])
		#plt.hist(z.kic5.feh[(z.kic5.feh < 999999.9)],alpha=0.25,normed=1,bins=nbins,label='Kic',range=[-3.0,1.0])
		#plt.xlabel(r'$\log z$')
		
		#if fileOut == False:
			#plt.show()
		#else:
			#plt.savefig(fileOut)

	#def plotHistoErr(self,fileUs='sing.ranLoc.1',fileKic='kic.r1.out',numFiles=10,fileBase='sing.ranLoc.',fileOut=False):

		#z=readFile()
		#z.kic3(file=fileKic)
		#(z_n,z_bins,z_pathcs)=plt.hist(z.kic3.kp,bins=32,range=[8,16])
		
		
		#x=b.readFile()
		#x.ranLoc(fileUs)
		#(x_n,x_bins,x_patchs)=plt.hist(x.ranLoc.appMag,bins=32,range=[8,16])
		#x_n2=x_n
		#sumxN=np.sum(x_n)
		#x_n=x_n/(1.0*sumxN)
		
		#culSum=np.zeros(np.size(x_n),dtype='float64')
		#culSumSq=np.zeros(np.size(x_n),dtype='float64')
		#stddev=np.zeros(np.size(x_n),dtype='float64')
		
		#for i in range(1,numFiles+1):
			#y=b.readFile()
			#y.ranLoc(fileErrBase+str(i))
			#(y_n,y_bins,y_patchs)=plt.hist(y.ranLoc.appMag,bins=32,range=[8,16])
			#y_n=y_n/(1.0*np.sum(y_n))
			#culSum=culSum+y_n
			#culSumSq=culSumSq+y_n**2
			#plt.cla()
			#plt.clf()
			#plt.close()
			#print i
		
		#stddev=np.sqrt(np.subtract(culSumSq/numFiles*1.0,(culSum/numFiles*1.0)**2))
		
		
		#x_bins2=np.zeros(np.size(x_n),dtype='float64')
		#for i in range(0,np.size(x_bins)-1):
			#x_bins2[i]=(x_bins[i]+x_bins[i+1])/2.0
		
		#plt.figure(1)
		#plt.bar(x_bins[0:np.size(x_bins)-1],x_n,width=x_bins[1]-x_bins[0],alpha=0.5,label='Us',color='green')
		
		#plt.plot(x_bins2,x_n+stddev,'r--', linewidth=1,label='Std Dev')
		#plt.plot(x_bins2,x_n-stddev,'r--', linewidth=1)
		
		#x_n2=np.sqrt(x_n2)/sumxN
		
		##print x_n2
		
		#plt.plot(x_bins2,x_n+x_n2,'r-.', linewidth=1,label=r'$\sqrt n$')
		#plt.plot(x_bins2,x_n-x_n2,'r-.', linewidth=1)
		
		#plt.bar(x_bins[0:np.size(x_bins)-1],z_n/np.sum(1.0*z_n),width=x_bins[1]-x_bins[0],alpha=0.5,label='kic',color='blue')
		
		##plt.xlabel('Galactic latitude, b (degrees)')
		#plt.xlabel(r'$m_{Kp}$')
		#plt.ylabel('Relative Frequency')
		##plt.title('Latitude distribution with errors, single stars')
		#plt.legend(loc=0)
		##plt.show()
		#if fileOut == False:
			#plt.show()
		#else:
			#plt.savefig(fileOut)
			
	#def plotAllKpMags(self,fileKic,files,fileOut=False):

		#plt.figure(1)
		#plt.xlabel(r'$m_{Kp}$')

		#alpha=1.0/(len(files)+1.0)
		
		#x=readFile()
		#x.kic3(file=fileKic)
		#label='KIC ='+str(np.size(x.kic3.kp))
		#plt.hist(x.kic3.kp,bins=32,range=[8,16],normed=1,alpha=alpha,label=label)

		#for l,f in files.iteritems():
			#y=readFile()
			#y.ranLoc(f)
			#label=l+'='+str(np.size(y.ranLoc.appMag))
			#plt.hist(y.ranLoc.appMag,bins=32,range=[8,16],normed=1,alpha=alpha,label=label)

		#plt.legend(loc=0)

		#if fileOut == False:
			#plt.show()
		#else:
			#plt.savefig(fileOut)

	#def plotAllKpMags2(self,fileKic,files,fileOut=False):

	    	#plt.figure(1)
		#plt.xlabel(r'$m_{Kp}$')

		#alpha=1.0/(len(files)+1.0)
		
		#x=readFile()
		#x.kic3(file=fileKic)

		#z=1
		#for l,f in files.iteritems():
			#plt.subplot(np.ceil(np.sqrt(len(files))),np.ceil(np.sqrt(len(files))),z)
			#z=z+1
			#label='KIC ='+str(np.size(x.kic3.kp))
			#plt.hist(x.kic3.kp,bins=32,range=[8,16],normed=1,alpha=alpha,label=label)
			
			#y=readFile()
			#y.ranLoc(f)
			#label=l+'='+str(np.size(y.ranLoc.appMag))
			#plt.hist(y.ranLoc.appMag,bins=32,range=[8,16],normed=1,alpha=alpha,label=label)

			#plt.legend(loc=0)

		#if fileOut == False:
			#plt.show()
		#else:
			#plt.savefig(fileOut)

			
			
	#def plotAllExt(self,file,fileOut=False):

		#plt.figure(1)
		
		#x=readFile()
		#x.readExtAll(file)

		#small=0.1

		#plt.subplot(3,2,1)
		#lon=65.0
		#lat=5.0
		#dist=8000.0
		##plt.xlabel('Distance')
		#plt.ylabel('Extinction')
		#plt.title('Extinction at longitude='+str(lon)+' lattitude='+str(lat))
		#ind=(x.readExtAll.long < lon+small) & (x.readExtAll.long > lon-small) &(x.readExtAll.lat < lat+small) & (x.readExtAll.lat > lat-small) & (x.readExtAll.dist < dist)
		#plt.plot(x.readExtAll.dist[ind],x.readExtAll.kep[ind],label='Kepler')
		#plt.plot(x.readExtAll.dist[ind],x.readExtAll.hak[ind],label='Hakkila')
		#plt.plot(x.readExtAll.dist[ind],x.readExtAll.drim[ind],label='Drimmel')

		#plt.subplot(3,2,2)
		#lon=70.0
		#lat=5.0
		#dist=8000.0
		##plt.xlabel('Distance')
		##plt.ylabel('Extinction')
		#plt.title('Extinction at longitude='+str(lon)+' lattitude='+str(lat))
		#ind=(x.readExtAll.long < lon+small) & (x.readExtAll.long > lon-small) &(x.readExtAll.lat < lat+small) & (x.readExtAll.lat > lat-small) & (x.readExtAll.dist < dist)
		#plt.plot(x.readExtAll.dist[ind],x.readExtAll.kep[ind],label='Kepler')
		#plt.plot(x.readExtAll.dist[ind],x.readExtAll.hak[ind],label='Hakkila')
		#plt.plot(x.readExtAll.dist[ind],x.readExtAll.drim[ind],label='Drimmel')

		#plt.subplot(3,2,3)
		#lon=75.0
		#lat=15.0
		#dist=8000.0
		##plt.xlabel('Distance')
		#plt.ylabel('Extinction')
		#plt.title('Extinction at longitude='+str(lon)+' lattitude='+str(lat))
		#ind=(x.readExtAll.long < lon+small) & (x.readExtAll.long > lon-small) &(x.readExtAll.lat < lat+small) & (x.readExtAll.lat > lat-small) & (x.readExtAll.dist < dist)
		#plt.plot(x.readExtAll.dist[ind],x.readExtAll.kep[ind],label='Kepler')
		#plt.plot(x.readExtAll.dist[ind],x.readExtAll.hak[ind],label='Hakkila')
		#plt.plot(x.readExtAll.dist[ind],x.readExtAll.drim[ind],label='Drimmel')
				
		#plt.subplot(3,2,4)
		#lon=70.0
		#lat=10.0
		#dist=8000.0
		##plt.xlabel('Distance')
		##plt.ylabel('Extinction')
		#plt.title('Extinction at longitude='+str(lon)+' lattitude='+str(lat))
		#ind=(x.readExtAll.long < lon+small) & (x.readExtAll.long > lon-small) &(x.readExtAll.lat < lat+small) & (x.readExtAll.lat > lat-small) & (x.readExtAll.dist < dist)
		#plt.plot(x.readExtAll.dist[ind],x.readExtAll.kep[ind],label='Kepler')
		#plt.plot(x.readExtAll.dist[ind],x.readExtAll.hak[ind],label='Hakkila')
		#plt.plot(x.readExtAll.dist[ind],x.readExtAll.drim[ind],label='Drimmel')

		#plt.subplot(3,2,5)
		#lon=85.0
		#lat=15.0
		#dist=8000.0
		#plt.xlabel('Distance')
		#plt.ylabel('Extinction')
		#plt.title('Extinction at longitude='+str(lon)+' lattitude='+str(lat))
		#ind=(x.readExtAll.long < lon+small) & (x.readExtAll.long > lon-small) &(x.readExtAll.lat < lat+small) & (x.readExtAll.lat > lat-small) & (x.readExtAll.dist < dist)
		#plt.plot(x.readExtAll.dist[ind],x.readExtAll.kep[ind],label='Kepler')
		#plt.plot(x.readExtAll.dist[ind],x.readExtAll.hak[ind],label='Hakkila')
		#plt.plot(x.readExtAll.dist[ind],x.readExtAll.drim[ind],label='Drimmel')

		#plt.subplot(3,2,6)
		#lon=70.0
		#lat=15.0
		#dist=8000.0
		#plt.xlabel('Distance')
		##plt.ylabel('Extinction')
		#plt.title('Extinction at longitude='+str(lon)+' lattitude='+str(lat))
		#ind=(x.readExtAll.long < lon+small) & (x.readExtAll.long > lon-small) &(x.readExtAll.lat < lat+small) & (x.readExtAll.lat > lat-small) & (x.readExtAll.dist < dist)
		#plt.plot(x.readExtAll.dist[ind],x.readExtAll.kep[ind],label='Kepler')
		#plt.plot(x.readExtAll.dist[ind],x.readExtAll.hak[ind],label='Hakkila')
		#plt.plot(x.readExtAll.dist[ind],x.readExtAll.drim[ind],label='Drimmel')
		#plt.legend(loc=0)

		#if fileOut == False:
			#plt.show()
		#else:
			#plt.savefig(fileOut)

	#def plotGRContours(self):
		#gr0=[1.2513,1.2498,1.1961,1.0716,0.916,0.7827,0.6775,0.5908,0.5136,0.4419,0.3763,0.3153,0.2578,0.2033,0.1511,0.1007,0.0268,-0.0201,-0.0668,-0.1132,-0.1521,-0.1827,
		#-0.2078,-0.2289,-0.2465,-0.2612,-0.2735,-0.2923,-0.3058,-0.3161,-0.3248,-0.3325,-0.34,-0.3539,-0.3672,-0.3799,-0.3917,-0.4029,-0.4133,-0.4231,-0.4319,-0.4398,
		#-0.4467,-0.4528,-0.4587,-0.4649,-0.4715,-0.4784,-0.4847,-0.4898,-0.4933,-0.4954,-0.4962,-0.4962,-0.4960]
		#ri0=[0.7444,0.5781,0.4550,0.3744,0.3117,0.2573,0.2112,0.1734,0.1407,0.1113,0.0832,0.0555,0.0286,0.0015,-0.0257,-0.0532,-0.0946,-0.1219,-0.1474,-0.1692,-0.1884,-0.2051,
		#-0.2187,-0.23,-0.2395,-0.2476,-0.2546,-0.2657,-0.2744,-0.2812,-0.2868,-0.2918,-0.2964,-0.3051,-0.3133,-0.321,-0.3285,-0.3356,-0.3424,-0.3487,-0.3549,-0.3607,-0.3657,
		#-0.3701,-0.3742,-0.3783,-0.3828,-0.3874,-0.3919,-0.3956,-0.3983,-0.4,-0.4011,-0.4012,-0.4007]
		
		#grp1=[0.9667,0.9691,1.0762,1.1136,1.0577,0.9498,0.8390,0.7416,0.6555,0.5739,0.4953,0.4203,0.3501,0.2811,0.2179,0.1424,0.084,0.0277,-0.0303,-0.0822,-0.1231,
		#-0.1594,-0.1913,-0.2184,-0.2411,-0.2596,-0.2749,-0.2978,-0.3139,-0.3258,-0.3357,-0.3446,-0.3530,-0.3686,-0.3823,-0.3947,-0.4060,-0.4167,-0.4264,-0.4348,-0.4416,
		#-0.4470,-0.4517,-0.4566,-0.4623,-0.4687,-0.4750,-0.4806,-0.4850,-0.4880,-0.4902]
		#rip1=[1.0994,0.7684,0.5329,0.4001,0.323,0.2636,0.2112,0.1673,0.1320,0.1016,0.0732,0.0446,0.0159,-0.0123,-0.0405,-0.0807,-0.1088,-0.1356,-0.1602,-0.1823,-0.2006,
		#-0.217,-0.2306,-0.2421,-0.2519,-0.2603,-0.2674,-0.2791,-0.2877,-0.2941,-0.2993,-0.3038,-0.3083,-0.3169,-0.3250,-0.3326,-0.3399,-0.3471,-0.3540,-0.3606,-0.3664,-0.3708
		#,-0.3744,-0.3777,-0.3816,-0.3863,-0.313,-0.3960,-0.3997,-0.4023,-0.4024]
		
		#grm1=[1.4712,1.3093,1.1409,0.9579,0.8030,0.6882,0.6007,0.5261,0.4580,0.3928,0.3329,0.2776,0.2264,0.181,0.1310,0.085,0.0384,-0.0328,-0.0757,-0.1180,-0.1560,-0.1851,
		#-.2086,-0.2274,-0.2434,-0.2565,-0.2675,-0.2844,-0.2975,-0.3082,-0.3174,-0.3259,-0.3337,-0.3483,-0.3621,-0.3751,-0.3870,-0.3981,-0.4082,-0.4178,-0.4265,-0.4349,-0.4427
		#,-0.4499,-0.4568,-0.4635,-0.4703,-0.4771,-0.4835,-0.4892,-0.4935,-0.4966,-0.4983,-0.4986,-0.4981]
		#rim1=[0.6636,0.5621,0.4729,0.3829,0.3171,0.2666,0.2253,0.1896,0.1579,0.1275,0.0985,0.0701,0.0424,0.0147,-0.0128,-0.0404,-0.0683,-0.1084,-0.1351,-0.1583,-0.1779,
		#-0.1956,-0.2104,-0.2223,-0.2321,-0.24,-0.2467,-0.2574,-0.2659,-0.2731,-0.2795,-0.2851,-0.2904,-0.3,-0.3089,-0.317,-0.3248,-0.3320,-0.3385,-0.3445,-0.3502,-0.3557,-0.3609,
		#-0.3657,-0.3702,-0.3747,-0.3791,-0.3836,-0.388,-0.3914,-0.3939,-0.3955,-0.3962,-0.3960,-0.3949]
		
		#grm2=[1.1435,1.2592,1.0626,0.8976,0.7636,0.6648,0.5804,0.5046,0.4373,0.3746,0.3181,0.2666,0.2183,0.1719,0.1264,0.0814,0.0350,-0.0342,-0.0761,-0.1175,-0.1550,
		#-0.1839,-0.2069,-0.2256,-0.2410,-0.2537,-0.2642,-0.2810,-0.2941,-0.3051,-0.3147,-0.3234,-0.3316,-0.3470,-0.3611,-0.3742,-0.3861,-0.3971,-0.4072,-0.4164,-0.4251,
		#-0.4334,-0.4415,-0.4492,-0.4566,-0.4638,-0.4708,-0.4775,-0.4837,-0.4891,-0.4931,-0.4960]
		#rim2=[0.439,0.5604,0.4744,0.3960,0.3284,0.2824,0.241,0.2042,0.1701,0.1374,0.1064,0.0775,0.0492,0.0215,-0.0059,-0.0333,-0.0624,-0.1015,-0.1289,-0.1534,-0.1734,-0.1913,
		#-0.2066,-0.2189,-0.2288,-0.2368,-0.2436,-0.2544,-0.2630,-0.2704,-0.2769,-0.2829,-0.2885,-0.2985,-0.3077,-0.3162,-0.3241,-0.3313,-0.3378,-0.3437,-0.3492,-0.3544,
		#-0.3596,-0.3646,-0.3695,-0.3742,-0.3787,-0.3832,-0.3873,-0.3907,-0.3931,-0.3946]
		
		#plt.plot(grp1,rip1, linewidth=2,label='+1.0')
		#plt.plot(gr0,ri0, linewidth=2,label='0.0')
		#plt.plot(grm1,rim1, linewidth=2,label='-1.0')
		#plt.plot(grm2,rim2, linewidth=2,label='-2.0')
		#plt.legend(loc=0)


	#def plotLabel(self,a1,a2,a3):
		#'''Plots lables given by a1 at position a2,a3'''
		#for label, xx, yy in zip(a1,a2,a3):
			##if isinstance(label, str):
				##l=label
			##else:
				##l='{:.2}'.format(label)
		
			#ann=plt.annotate(
				#'{:.2}'.format(label),
				#xy = (xx, yy), xytext = (-20, 20),
				#textcoords = 'offset points', ha = 'right', va = 'bottom',
				#bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.75),
				#arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))
			#ann.draggable()
			
class astroSeis(object):
	# Name:    IDL procedure "soldet1.pro"
	# Author:  W J Chaplin, 30 April 2012
	#
	# Purpose: Estimate probability of detection of solar-like
	#          oscillations in any given target
	#
	# Inputs:  Effective temperature, teff
	#          Radius, rad
	#          Mass, mass
	#          Kepler apparent magnitude, vkic
	#
	#Converted to python By R Farmer 4 May 2012
	

	# Name:    IDL procedure "calcfalse.pro"
	# Author:  W J Chaplin, 5 July 2010
	#
	# Purpose: Estimate probability of passing false-alarm test
	#
	# Inputs:  snr:    Underlying global SNR
	#          numax:  Frequency of maximum oscillations power
	#          T:      Length of dataset, in days
	#          pfalse: False-alarm probability (fractional)
	#
	# Output:  pfinal: Approximate probability of making a "detection"
	def calcfalse(self,snr,numax,T,pfalse):
		# Convert T to seconds, and estimate width of frequency bins, bw:
		
		tlen=T*86400.0
		bw=(10.0**6)/tlen
		
		
		# How many independent frequency power-spectrum bins in numax?
		
		nbins=numax/bw
		
		
		# False-alarm SNR threshold, "snrthresh":
		#
		# The IDL function CHISQR_CVF computes the threshold value, "snrthresh" in a chi-square distribution with "2*nbins" dof
		# such that the probability that some random variable is greater than the threshold is equal to the
		# supplied probability "pfalse"
		
		snrthresh=self.chisqr_cvf(pfalse,2.0*nbins)/(2.0*nbins)-1.0
		
		#
		# Test values: nbins=100,  pfalse=0.01 --> snrthresh=0.24723
		#              nbins=1000, pfalse=0.01 --> snrthresh=0.07503
		#
		
		
		# Approximate probability of passing false-alarm threshold, "pfinal", given underlying SNR, "snr":
		#
		# The IDL function CHISQR_PDF computes the probability "pfinal" that, in a chi-square distribution with "2*nbins" dof,
		# a random variable is less than or equal to the supplied threshold, "(snrthresh+1.0)/(snr+1.0)":
		
		pfinal=stats.chi2.sf((snrthresh+1.0)/(snr+1.0)*2.0*nbins,2*nbins)
		
		#
		# Test values: nbins=100,  snrthresh=0.24723, snr=0.27 --> pfinal=0.558
		#              nbins=1000, snrthresh=0.07503, snr=0.10 --> pfinal=0.762
		#
		return pfinal
	
	
	def chisqr_cvf(self,p,df):
	
	#return, bisect_pdf([1-p, df], 'chisqr_pdf', up, below)
	
		if p <0 or p > 1:
			exit()
	
		if p == 0:
			return 10**12
		if p == 1:
			return 0
		if df < 0:
			exit()
	
		if df == 1:
			up=300.0
		elif df == 2:
			up=100.0
		elif df > 2 and df <= 5:
			up=30.0
		elif df >5 and df <= 14:
			up=20.0
		else:
			up=12.0
		below=0.0
	
		while (1.0-stats.chi2.sf(up,df) < 1.0-p):
			below=up
			up=2.0*up
	
		return self.bisect_pdf(1.0-p,up,below,df)
	
	
	def bisect_pdf(self,p,u,l,df,delta=10**(-6)):
		up=u
		low=l
		mid=l+(u-l)*p
		count=1
		z=-99.9
		z=1.0-stats.chi2.sf(mid,df)
		while(np.abs(up-low) > delta*mid and count < 100):
			if z > p:
				up=mid
			else:
				low=mid
			mid = (up + low)/2.0
	
			z=1.0-stats.chi2.sf(mid,df)
			count=count+1
		return mid
	
	
	def soldet(self,teff,rad,mass,vkic,pfalse=0.01,days=30.0):
		teff=np.atleast_1d(teff)
		rad=np.atleast_1d(rad)
		mass=np.atleast_1d(mass)
		vkic=np.atleast_1d(vkic)
		
		# Estimate frequency of maximum oscillations power, "numax" (micro-Hz):
		numax=3090.0*(np.divide(mass,(rad**2))*np.sqrt(5777.0/teff))
		
		# Estimate large frequency separation:
		
		dnu=135.1*np.sqrt(mass/(rad**3))
		
		# Amplitude prediction correction, "beta", for hot solar-like stars:
		#
		#--Estimate "teffred", temperature on red-edge of instability strip at luminosity, "lum", of star:
		
		lum=rad**2*(teff/5777.0)**4
		teffred=10.0**((np.log10(lum)-42.46)/(-10.75))
		
		#--Get "beta":
		beta=np.zeros(np.size(teff))
		beta=1.0-np.exp(-(teffred-teff)/1250.0)
		beta[teff>=teffred]=0.0
		#if teff >= teffred:
			#beta=0.0
		
		
		# Total underlying RMS power, "ptot", in solar-like oscillations spectrum:
		
		amp=2.5*beta*(lum/mass)*(teff/5777.0)**(-2.0)
		ptot=(1.55*amp**2)*numax/dnu*(np.sin(np.pi/2.0*numax/8496.0)/(np.pi/2.0*numax/8496.0))**2
		
		
		# Sigma, "sig", due to instrumental noise:
		
		#--detections per cadence, "cc":
		
		cc=1.28*10**(0.4*(12.0-vkic)+7.0)
		
		#--Get "sig":
		
		sig=10.0**6/cc*np.sqrt(cc+9.5*10.0**5*(14.0/vkic)**5)
		
		# Total underlying background power, "bgtot", across frequency range occupied by modes
		# including granulation contribution:
		
		bgtot=(2.3*sig**2*58.85*10**(-6)+0.1*(numax/3090.0)**(-2.0))*numax
		
		
		# Global SNR, "snr":
		
		snr=ptot/bgtot

		
		# False-alarm probability, "pfalse":
		
		#pfalse=0.01
		
		
		# Probability of detection, "pfinal", for different dataset lengths, T (days)
		#print 'Approximate probability of making a "detection"'
		#print snr,numax
		##--T=30 days:
		##pfinal=calcfalse(snr, numax, 30.0, pfalse)
		#print 'T=30 days ',calcfalse(snr, numax, 30.0, pfalse)
		#print 'T=60 days ',calcfalse(snr, numax, 60.0, pfalse)
		#print 'T=180 days ',calcfalse(snr, numax, 1800.0, pfalse)
		#print 'T=360 days ',calcfalse(snr, numax, 360.0, pfalse)
		#print 'T=720 days ',calcfalse(snr, numax, 720.0, pfalse)
		#print 'T=1440 days ',calcfalse(snr, numax, 1440.0, pfalse)
		#print 'T=3600 days ',calcfalse(snr, numax, 3600.0, pfalse)

		days=np.zeros(np.shape(snr))
		#Assume LC for all objects
		days[:]=4.0*365.0
		#We need SC data for these objects
		days[numax>280]=30.0
		
		vecCalcFalse=np.vectorize(self.calcfalse)
		result=vecCalcFalse(snr,numax,days,pfalse)
		return result
	
	def soldet2(self,teff,rad,mass,vkic,pfalse=0.01,days=30.0,OC="LC"):
		teff=np.atleast_2d(teff).T
		rad=np.atleast_2d(rad).T
		mass=np.atleast_2d(mass).T
		vkic=np.atleast_1d(vkic)

		if OC=="LC":
			days=4*365.0
		if OC=="SC":
			days=30.0

		nuSun=3090.0
		#I think this is the solar value
		dnuSun=135.1
		delT=1550.0

		# Estimate frequency of maximum oscillations power, "numax" (micro-Hz):
		numax=nuSun*(np.divide(mass,(rad**2))*np.sqrt(5777.0/teff))
		#print numax
		# Estimate large frequency separation:

		dnu=dnuSun*np.sqrt(mass/(rad**3))
		#print dnu
		# Amplitude prediction correction, "beta", for hot solar-like stars:
		#
		#--Estimate "teffred", temperature on red-edge of instability strip at luminosity, "lum", of star:

		lum=rad**2*(teff/5777.0)**4
		teffred=10.0**((np.log10(lum)-42.46)/(-10.75))
		#print lum
		#print teffred
		#--Get "beta":
		#beta=np.zeros(np.shape(teff))
		beta=1.0-np.exp(-(teffred-teff)/delT)
		#print beta
		beta[beta<=0.0]=0.0
		#print beta
		#if teff >= teffred:
		#beta=0.0

			#Luminosity in Kepler band pass see ballot et al 2011 A&A 531 124
		bk=lum/(teff/5934.0)**0.8
		#print bk
		bkS=np.sum(bk,axis=1)
		bk=(bk.T/bkS.T).T
		#print bk
		# Total underlying RMS power, "ptot", in solar-like oscillations spectrum:

		amp=2.5*beta*(lum/mass)*((5777.0/teff)**(2.0))
		#print amp
		ptot=(1.55*amp**2)*numax/dnu*(np.sin(np.pi/2.0*numax/8496.0)/(np.pi/2.0*numax/8496.0))**(2)*bk**2
		#print ptot

		# Sigma, "sig", due to instrumental noise:

		#--detections per cadence, "cc":

		cc=1.28*10**(0.4*(12.0-vkic)+7.0)
		#print cc
		#--Get "sig":

		sig=10.0**6/cc*np.sqrt(cc+9.5*10.0**5*(14.0/vkic)**5)
		#print sig
		# Total underlying background power, "bgtot", across frequency range occupied by modes
		# including granulation contribution:

		bgsig=2.3*sig**2*58.85*10**(-6)
		#print bgsig
		#bgran=1.8*((3150.0/numax)**2)*bk**2
		bgran=0.1*(numax/nuSun)**(-2.0)*bk**2
		#print bgran
		tau=210.0*(nuSun/numax)


			#bgtot=(2.3*sig**2*58.85*10**(-6)+0.1*(numax/3150.0)**(-2.0))*numax
			#bgtot=(bgsig+np.add(bgran[0]/(1.0+(2.0*np.pi*tau[0]*numax[0]*10**(-6))**2),bgran[1]/(1.0+(2.0*np.pi*tau[1]*numax[1]*10**(-6))**2)))*numax
			#bgtot=(bgsig+np.sum(bgran/(1.0+(2.0*np.pi*tau*numax*10**(-6))**2)))*numax
		#bgtot=np.zeros(np.shape(tau))
		#bgtot[:,0]=(bgsig+(bgran[:,0]/(1.0+(2.0*np.pi*tau[:,0]*numax[:,0]*10**(-6))**2))+(bgran[:,1]/(1.0+(2.0*np.pi*tau[:,1]*numax[:,0]*10**(-6))**2)))*numax[:,0]

		#bgtot[:,1]=(bgsig+(bgran[:,0]/(1.0+(2.0*np.pi*tau[:,0]*numax[:,1]*10**(-6))**2))+(bgran[:,1]/(1.0+(2.0*np.pi*tau[:,1]*numax[:,1]*10**(-6))**2)))*numax[:,1]
		# Global SNR, "snr":

		snr=np.zeros(np.shape(numax))
		#snr=ptot/((np.sum(bgran,axis=1)+bgsig)*numax)
		snr[:,0]=ptot[:,0]/((bgran[:,0]+bgran[:,1]+bgsig)*numax[:,0])
		snr[:,1]=ptot[:,1]/((bgran[:,0]+bgran[:,1]+bgsig)*numax[:,1])
		# False-alarm probability, "pfalse":
		#print snr
		#pfalse=0.01

		# Probability of detection, "pfinal", for different dataset lengths, T (days)
		#print 'Approximate probability of making a "detection"'
		#print snr,numax
		##--T=30 days:
		##pfinal=calcfalse(snr, numax, 30.0, pfalse)
		#print 'T=30 days ',calcfalse(snr, numax, 30.0, pfalse)
		#print 'T=60 days ',calcfalse(snr, numax, 60.0, pfalse)
		#print 'T=180 days ',calcfalse(snr, numax, 1800.0, pfalse)
		#print 'T=360 days ',calcfalse(snr, numax, 360.0, pfalse)
		#print 'T=720 days ',calcfalse(snr, numax, 720.0, pfalse)
		#print 'T=1440 days ',calcfalse(snr, numax, 1440.0, pfalse)
		#print 'T=3600 days ',calcfalse(snr, numax, 3600.0, pfalse)
		result=np.zeros(np.shape(snr))

		#days=np.zeros(np.shape(snr))
	##Assume LC for all objects

		#if OC=="LC":
			#days[:,:]=4.0*365.0
		#if OC=="SC":
			#days[:,:]=30.0

		#print np.shape(teff)
		#print np.shape(result)
		for i in range(np.shape(teff)[0]):
			for j in range(np.shape(teff)[1]):
				result[i,j]=self.calcfalse(snr[i,j],numax[i,j],days,pfalse)
		#if OC=="LC":
			#result[numax>280.0]=0.0
		#if OC=="SC":
			#result[numax<280.0]=0.0
		
		return result,numax
	