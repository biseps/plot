import numpy as np
import biseps as b
import kepprf as kp
import fortUtil as f
import matplotlib as mat
import matplotlib.pyplot as plt
import sys as sys
import constants as c

#f2py -c -m --opt="-Wno-all -march=native -msse3 -mtune=native -O3" fortUtil fortUtil.f90 > /dev/null && time python test.py


x=b.readFile()
ex=b.extras()

ccd='1'
ptype='pop_s_y'

x.ranLoc('/padata/beta/users/rfarmer/data/keplerFaint/'+ptype+'/'+ccd+'/ranLoc.dat.1')


x.kicProp('/padata/beta/users/rfarmer/data/keplerFaint/'+ptype+'/'+ccd+'/kic.dat.1')

row,col=f.lonlat2pix(x.ranLoc.long,x.ranLoc.lat,np.size(x.ranLoc.long),ccd,c.season,c.fovDir)

prfAll=np.load('/padata/beta/users/rfarmer/data/keplerFaint/ffi/'+ccd+'.npy')
#prfAll=np.load('1.npy')


a=np.zeros([c.numA,np.size(row)])
a=kp.calcA(x.kicProp.rad,x.kicProp.teff)

mass=ex.radlogg2mass(x.kicProp.rad,x.kicProp.logg)
samples=kp.numSample(mass,x.kicProp.rad,a)
trans=kp.numTransits(mass,a)

minEarth=np.zeros(c.numA,dtype='float')
noise=np.zeros(c.numA,dtype='float')

with open('target','w') as f:

	for i in xrange(np.size(row)):
		#r[i]=kp.calcMinR(row[i],col[i],x.ranLoc[i].appMag,1,prfAll)
		if x.kicProp[i].kp < 16.0:
			#Filter out bad kepler Prop points
			if x.kicProp[i].tefferr > 50000.0 or x.kicProp[i].logzerr > 9.5 or x.kicProp[i].loggerr > 9.5:
				continue
			
			r,pix,noise=kp.calcMinR(row[i],col[i],x.kicProp[i].kp,ccd,prfAll)

			#print r,pix,noise,kp.calcNoise(pix,x.kicProp[i].kp)
			#sys.exit()
			#noise=kp.calcNoise(pix,x.kicProp[i].kp)

			tar=0
			if r > 0.0:
				minEarth[:]=0.0
				minEarth=(x.kicProp[i].rad*c.rsun*np.sqrt((7.1*(noise/(np.sqrt(samples[:,i]*trans[:,i]))))/r))/c.rearth
				print i,mass[i],x.kicProp[i].rad,x.kicProp[i].teff
				print "  ",a[:,i]
				print "  ",trans[:,i]
				print "  ",samples[:,i]
				print "  ",noise
				print "  ",minEarth[:]
				print "  ",r,pix,x.kicProp[i].kp
				
				if trans[0][i] >= 3 and minEarth[0] <= 2.0 and x.kicProp[i].kp < 16.0:
					tar=13
				if trans[0][i] >= 3 and minEarth[0] <= 2.0 and x.kicProp[i].kp< 15.0:
					tar=12
				if trans[2][i] >= 3 and minEarth[2] <= 2.0 and x.kicProp[i].kp < 16.0:
					tar=11
				if trans[2][i] >= 3 and minEarth[2] <= 2.0 and x.kicProp[i].kp < 15.0:
					tar=10
				if trans[0][i] >= 3 and minEarth[0] <= 2.0 and x.kicProp[i].kp < 14.0:
					tar=9
				if trans[1][i] >= 3 and minEarth[1] <= 2.0 and x.kicProp[i].kp < 14.0:
					tar=8
				if trans[1][i] >= 3 and minEarth[1] <= 1.0 and x.kicProp[i].kp < 14.0:
					tar=7
				if trans[2][i] >= 3 and minEarth[2] <= 1.0 and x.kicProp[i].kp < 16.0:
					tar=6
				if trans[2][i] >= 3 and minEarth[2] <= 1.0 and x.kicProp[i].kp < 15.0:
					tar=5
				if trans[2][i] >= 3 and minEarth[2] <= 2.0 and x.kicProp[i].kp < 14.0:
					tar=4
				if trans[2][i] >= 3 and minEarth[2] <= 1.0 and x.kicProp[i].kp < 14.0:
					tar=3
				if trans[2][i] >= 3 and minEarth[2] <= 2.0 and x.kicProp[i].kp < 13.0:
					tar=2
				if trans[2][i] >= 3 and minEarth[2] <= 1.0 and x.kicProp[i].kp < 13.0:
					tar=1

				print tar
				print "***"
			f.write(str(x.ranLoc.id[i])+' '+str(tar)+'\n')
			
f.close()
